$(document).ready(function() {
    $("#tbodyUser").on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    UserId: id,
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#user_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $("#tbodyUser").on("click", "a.link_status", function(){
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if(statusId != $('input#statusId_' + id).val()) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    UserId: id,
                    StatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        $('td#statusName_' + id).html(json.data.StatusName);
                        $('input#statusId_' + id).val(statusId);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});
$(document).ready(function () {
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#btnGetOrder').click(function () {
        $('.loading-getorder').fadeOut();
        $('#modalGetDataOrder').modal('show');
    });

    $('input.cbTeam').on('ifToggled', function(e) {
        var cbShop = $('#divTeam_' + e.currentTarget.value).find('input.cbShop');
        if (e.currentTarget.checked) cbShop.iCheck('check');
        else cbShop.iCheck('uncheck');
    });


    $('#modalGetDataOrder .submit').click(function () {
        var this_ = $(this);
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        if (fromDate == '' || toDate == '') {
            showNotification('Please choose From date and End date', 0);
            return false;
        }
        var shopIds = [];
        $('input.cbShop').each(function(){
            if($(this).parent('div').hasClass('checked')) shopIds.push($(this).val());
        });
        if(shopIds.length == 0){
            showNotification('Please choose shops', 0);
            return false;
        }
        this_.prop('disabled', true);
        $('.loading-getorder').fadeIn();
        $.ajax({
            type: "POST",
            url: $('input#getOrderUrl').val(),
            data: {fromDate: fromDate, toDate: toDate, shopIds: shopIds},
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                this_.prop('disabled', false);
                $('.loading-getorder').fadeOut();
                if(json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
                this_.prop('disabled', false);
                $('.loading-getorder').fadeOut();
            }
        });
    });

    var orderShopifyIds = [];
    var isCheckAll = false;
    $('input#checkAll').change(function() {
        orderShopifyIds = [];
        if(this.checked){
            isCheckAll = true;
            var cb = $('input.iCheckItem');
            cb.prop('checked', true);
            cb.each(function(){
                orderShopifyIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').prop('checked', false);
            isCheckAll = false;
        }
    });
    $('input.iCheckItem').change(function(){
        if(this.checked){
            if(!isCheckAll) orderShopifyIds.push(this.value);
        }
        else{
            var index = orderShopifyIds.indexOf(this.value);
            if(index >= 0) orderShopifyIds.splice(index, 1);
        }
    });

    $('#selectAction').on('change', function () {
        if(orderShopifyIds.length > 0) {
            var actionCode = $(this).val();
            if (actionCode === 'send_factory') {
                //if($('input#orderStatusIdHidden').val() == '1') {
                $('span.countOrder').text(orderShopifyIds.length);
                $('#modalSendFactory').modal();
                //} else showNotification('Please choose Order status is Not send', 0);
            }
            else if(actionCode == 'send_invoice'){
                var factoryId = $('input#factoryIdHidden').val();
                if(factoryId != ''){
                    if(factoryId.split(',').length == 1){
                        $('span.countOrder').text(orderShopifyIds.length);
                        $('input#factoryIdInvoice').val(factoryId);
                        $('#spanFactoryName').text($('select#factoryIds option[value="' + factoryId + '"]').text());
                        $('#modalSendInvoice').modal();
                    }
                    else showNotification('Please search order by 1 factory', 0);
                }
                else showNotification('Please search order by 1 factory', 0);
            }
            else if(actionCode == 'export_excel'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action', $('#baseUrl').attr('href') + 'ordershopify/exportExcel');
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if(actionCode == 'export_excel2'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action', $('#baseUrl').attr('href') + 'ordershopify/exportExcel2');
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if(actionCode == 'export_excel3'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action', $('#baseUrl').attr('href') + 'ordershopify/exportExcel3');
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if (actionCode.indexOf('change_status-') >= 0) {
                actionCode = actionCode.split('-');
                if (actionCode.length == 2) {
                    var statusId = parseInt(actionCode[1]);
                    if (confirm('Do you really want to change ?')) {
                        $.ajax({
                            type: "POST",
                            url: $('input#changeItemStatusUrl').val(),
                            data: {
                                OrderShopifyIds: orderShopifyIds,
                                StatusId: statusId
                            },
                            success: function (response) {
                                var json = $.parseJSON(response);
                                showNotification(json.message, json.code);
                                if (json.code == 1) {
                                    for (var i = 0; i < orderShopifyIds.length; i++) $('#trItem_' + orderShopifyIds[i]).remove();
                                }
                            },
                            error: function (response) {
                                showNotification('An error occurred during the execution', 0);
                            }
                        });
                    }

                }
            }
        }
        else showNotification('Please choose Order', 0);
        $(this).val('');
    });

    $('#btnSendFactory').click(function(){
        var factoryId = parseInt($('select#factoryId1').val());
        if(factoryId > 0 && orderShopifyIds.length > 0) {
            $.ajax({
                type: 'POST',
                url: $('input#sendFactoryUrl').val(),
                data: {
                    FactoryId: factoryId,
                    FactoryRequestId: $('select#factoryRequestId').val(),
                    Comment: $('#commentFactory').val(),
                    OrderShopifyIds: orderShopifyIds
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                },
                error: function (err) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        else showNotification('Please choose Factory and Order', 0);
    });

    $('#btnSendInvoice').click(function(){
        var factoryId = parseInt($('input#factoryIdInvoice').val());
        if(factoryId > 0 && orderShopifyIds.length > 0){
            var teamOrders = [];
            var teamId = 0;
            for(var i = 0; i < orderShopifyIds.length; i++){
                teamId = $('input#teamId_' + orderShopifyIds[i]).val();
                if(typeof(teamOrders[teamId]) == 'undefined') teamOrders[teamId] = [orderShopifyIds[i]];
                else teamOrders[teamId].push(orderShopifyIds[i]);
            }
            $.ajax({
                type: 'POST',
                url: $('input#sendInvoiceUrl').val(),
                data: {
                    FactoryId: factoryId,
                    Comment: $('#commentInvoice').val(),
                    TeamOrders: teamOrders
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                },
                error: function (err) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        else showNotification('Please choose Team, Factory and Order', 0);
    });

    $('#btnUploadExcel').click(function(){
        chooseFile('Files', function(fileUrl){
            $('input#fileExcelUrl').val(fileUrl);
        });
    });

    $('#btnImportTracking').click(function(){
        $('input#fileExcelUrl').val('');
        $('input#importTypeId').val('1');
        $('#modalImportExcel h4').text('Import TrackingCode from Excel');
        $('#modalImportExcel').modal('show');
    });

    $('#btnImportWaitingOrder').click(function(){
        $('input#fileExcelUrl').val('');
        $('input#importTypeId').val('2');
        $('#modalImportExcel h4').text('Import Waiting Order from Excel');
        $('#modalImportExcel').modal('show');
    });

    $("#btnImportExcelFactory").click(function(){
        $('input#fileExcelUrl').val('');
        $('input#importTypeId').val('3');
        $('#modalImportExcel h4').text('Import Excel Factory Scan');
        $('#modalImportExcel').modal('show');
    });

    $("#btnImportWarehouseFactory").click(function(){
        $('input#fileExcelUrl').val('');
        $('input#importTypeId').val('4');
        $('#modalImportExcel h4').text('Import Excel Warehouse Scan');
        $('#modalImportExcel').modal('show');
    });

    $("#btnImportCsvOrder").click(function(){
        $('input#fileExcelUrl').val('');
        $('input#importTypeId').val('5');
        $('#modalImportExcel h4').text('Import Excel Order');
        $('#modalImportExcel').modal('show');
    });

    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            $('#btnImportExcel').prop('disabled', true);
            $('.imgLoading').show();
            var importTypeId = parseInt($('input#importTypeId').val());
            if(importTypeId == 3 || importTypeId == 4){
                $.ajax({
                    type: "POST",
                    url: $('input#importOrderScanUrl').val(),
                    data: {
                        FileUrl: fileExcelUrl,
                        ImportTypeId: importTypeId
                    },
                    success: function (response) {
                        $('.imgLoading').hide();
                        $('#modalImportExcel').modal('hide');
                        $('#btnImportExcel').prop('disabled', false);
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) redirect(true, '');
                    },
                    error: function (response) {
                        $('.imgLoading').hide();
                        showNotification('An error occurred during the execution', 0);
                        $('#btnImportExcel').prop('disabled', false);
                    }
                });
            }
            else{
                $.ajax({
                    type: "POST",
                    url: $('input#importOrderUrl').val(),
                    data: {
                        FileUrl: fileExcelUrl,
                        ImportTypeId: importTypeId
                    },
                    success: function (response) {
                        $('.imgLoading').hide();
                        $('#modalImportExcel').modal('hide');
                        $('#btnImportExcel').prop('disabled', false);
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) redirect(true, '');
                    },
                    error: function (response) {
                        $('.imgLoading').hide();
                        showNotification('An error occurred during the execution', 0);
                        $('#btnImportExcel').prop('disabled', false);
                    }
                });
            }
            
        }
        else showNotification('Please choose Excel file', 0);
    });

    var factoryId = $('select#factoryId1').val();
    $('select#factoryRequestId option[data-id="' + factoryId + '"]').show();
    $('select#factoryId1').change(function(){
        factoryId = $(this).val();
        $('select#factoryRequestId option').hide();
        $('select#factoryRequestId option[data-id="' + factoryId + '"]').show();
        $('select#factoryRequestId option[value="0"]').show();
        $('select#factoryRequestId').val('0');
    });
});
$(document).ready(function(){
    $("#tbodyTeam").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#teamId').val(id);
        $('input#teamName').val($('td#teamName_' + id).text());
        $('input#teamCode').val($('td#teamCode_' + id).text());
        $('input#email').val($('td#email_' + id).text());
        scrollTo('input#teamName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteTeamUrl').val(),
                data: {
                    TeamId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#team_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#teamForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#teamForm')) {
            var form = $('#teamForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                      
                        if(data.IsAdd == 1){
                            var html = '<tr id="team_' + data.TeamId + '">';
                            html += '<td id="teamCode_' + data.TeamId + '">' + data.TeamCode + '</td>';
                            html += '<td id="teamName_' + data.TeamId + '">' + data.TeamName + '</td>';
                            html += '<td id="email_' + data.TeamId + '">' + data.Emails + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TeamId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TeamId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyTeam').prepend(html);
                        }
                        else{
                            $('td#teamCode_' + data.TeamId).text(data.TeamCode);
                            $('td#teamName_' + data.TeamId).text(data.TeamName);
                            $('td#email_' + data.TeamId).text(data.Emails);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
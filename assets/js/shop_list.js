$(document).ready(function () {
    $("#tbodyShop").on("click", "a.link_delete", function () {
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    ShopId: id,
                    StatusId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#shop_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }

        return false;
    });
    $("#tbodyShop").on("click", "a.link_status", function () {
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if (statusId != $('input#statusId_' + id).val()) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    ShopId: id,
                    StatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        $('td#statusName_' + id).html(json.data.StatusName);
                        $('input#statusId_' + id).val(statusId);
                    }
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});
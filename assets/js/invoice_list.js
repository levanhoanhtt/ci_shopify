$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var invoiceIds = [];
    var isCheckAll = false;
    $('input#checkAll').change(function() {
        invoiceIds = [];
        if(this.checked){
            isCheckAll = true;
            var cb = $('input.iCheckItem');
            cb.prop('checked', true);
            cb.each(function(){
                invoiceIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').prop('checked', false);
            isCheckAll = false;
        }
    });
    $('input.iCheckItem').change(function(){
        if(this.checked){
            if(!isCheckAll){
                invoiceIds.push(this.value);
            } 
        }
        else{
            var index = invoiceIds.indexOf(this.value);
            if(index >= 0) invoiceIds.splice(index, 1);
        }
    });

    $('#selectAction').on('change', function () {
        if(invoiceIds.length > 0) {
            var actionCode = $(this).val();
            if(actionCode == 'delete'){
                if (confirm('Do you really want to delete ?')) {
                    $.ajax({
                        type: "POST",
                        url: $('input#deleteUrl').val(),
                        data: {
                            InvoiceIds: invoiceIds
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if (json.code == 1) {
                                for (var i = 0; i < invoiceIds.length; i++) $('#trItem_' + invoiceIds[i]).remove();
                            }
                        },
                        error: function (response) {
                            showNotification('An error occurred during the execution', 0);
                        }
                    });
                }
            }
            else if(actionCode == 'mail'){
                if($('input#invoiceStatusIdSearch').val() == '3') {
                    if (confirm('Do you really want to send mail ?')) {
                        $.ajax({
                            type: "POST",
                            url: $('input#sendMailInvoiceUrl').val().trim(),
                            data: {
                                InvoiceIds: invoiceIds
                            },
                            success: function (response) {
                                var json = $.parseJSON(response);
                                showNotification(json.message, json.code);
                            },
                            error: function (response) {
                                showNotification('An error occurred during the execution', 0);
                            }
                        });
                    }
                }
                else showNotification('Please search waiting invoice', 0);
            }
        }
        else showNotification('Please choose Invoice', 0);
        $(this).val('');
    });
});

$(document).ready(function () {
    $('input.iCheckTable, input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    var factoryScanIds = [];
    var isCheckAll = false;
    $('body').on('ifToggled', 'input#checkAll', function (e) {
        factoryScanIds = [];
        if (e.currentTarget.checked) {
            isCheckAll = true;
            $('input.iCheckItem').iCheck('check');
            $('#tbodyScan input.iCheckItem').each(function(){
                factoryScanIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').iCheck('uncheck');
            isCheckAll = false;
        }
    }).on('ifToggled', 'input.iCheckItem', function (e) {
        if(e.currentTarget.checked){
            if(!isCheckAll) factoryScanIds.push(e.currentTarget.value);
        }
        else{
            var index = factoryScanIds.indexOf(e.currentTarget.value);
            if(index >= 0) factoryScanIds.splice(index, 1);
        }
    });
    $('#btnTrackingCode').click(function(){
        if(factoryScanIds.length > 0){
            $('span#countProduct').text(factoryScanIds.length);
            $('#modalTrackingCode').modal('show');
        }
        else showNotification('Please choose product scan', 0);
    });
    $('#btnInsertTrackingCode').click(function(){
        var trackingCode = $('input#trackingCodeInsert').val().trim();
        if(trackingCode == ''){
            showNotification('Chưa điền mã nội địa', 0);
            $('input#trackingCodeInsert').focus();
            return false;
        }
        $.ajax({
            type: "POST",
            url: $('input#insertTrackingCodeUrl').val(),
            data: {
                FactoryId: $('input#factoryIdSelect').val(),
                TrackingCode: trackingCode,
                FactoryScanIds: factoryScanIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    });

    $('#tbodyScan').on('click', '.link_image', function(){
        $('div#listImages').html('');
        $.ajax({
            type: "POST",
            url: $('input#getImageUrl').val(),
            data: {
                FactoryScanId: $(this).attr('data-id')
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1){
                    var data = json.data;
                    var html = '';
                    var scanImagePath = $('input#scanImagePath').val();
                    for(var i = 0; i < data.length; i++){
                        html += '<div class="col-sm-6"><img src="' + scanImagePath + data[i] + '"></div>';
                    }
                    $('div#listImages').html(html);
                    $('#modalScanImage').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
        return false;
    });
});
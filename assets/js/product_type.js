$(document).ready(function(){
    $("#tbodyProductType").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#productTypeId').val(id);
        $('input#productTypeName').val($('td#productTypeName_' + id).text());
        $('select#factoryId').val($('input#factoryId_' + id).val());
        $('input#price').val($('td#price_' + id).text());
        scrollTo('input#productTypeName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteProductTypeUrl').val(),
                data: {
                    ProductTypeId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#productType_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    }).on('keydown', 'input#price', function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8|| e.keyCode == 190 || e.keyCode == 110)) {
            e.preventDefault();
        }
    }).on('keyup', 'input#price', function() {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('a#link_cancel').click(function(){
        $('#productTypeForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#productTypeForm')) {
            var form = $('#productTypeForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="productType_' + data.ProductTypeId + '">';
                            html += '<td id="productTypeName_' + data.ProductTypeId + '">' + data.ProductTypeName + '</td>';
                            html += '<td id="factoryName_' + data.ProductTypeId + '">' + data.FactoryName + '</td>';
                            html += '<td id="price_' + data.ProductTypeId + '">' + formatDecimal(data.Price) + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ProductTypeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ProductTypeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="factoryId_' + data.ProductTypeId + '" value="' + data.FactoryId + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyProductType').prepend(html);
                        }
                        else{
                            $('td#productTypeName_' + data.ProductTypeId).text(data.ProductTypeName);
                            $('td#factoryName_' + data.ProductTypeId).text(data.FactoryName);
                            $('td#price_' + data.ProductTypeId).text(formatDecimal(data.Price));
                            $('input#factoryId_' + data.ProductTypeId).val(data.FactoryId);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
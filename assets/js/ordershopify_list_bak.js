$(document).ready(function () {
    $('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $('#btnGetOrder').click(function () {
        $('.loading-getorder').fadeOut();
        $('#modalGetDataOrder').modal('show');
    });

    $('input.cbTeam').on('ifToggled', function(e) {
        var cbShop = $('#divTeam_' + e.currentTarget.value).find('input.cbShop');
        if (e.currentTarget.checked) cbShop.iCheck('check');
        else cbShop.iCheck('uncheck');
    });


    $('#modalGetDataOrder .submit').click(function () {
        var this_ = $(this);
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        if (fromDate == '' || toDate == '') {
            showNotification('Please choose From date and End date', 0);
            return false;
        }
        var shopIds = [];
        $('input.cbShop').each(function(){
            if($(this).parent('div').hasClass('checked')) shopIds.push($(this).val());
        });
        if(shopIds.length == 0){
            showNotification('Please choose shops', 0);
            return false;
        }
        this_.prop('disabled', true);
        $('.loading-getorder').fadeIn();
        $.ajax({
            type: "POST",
            url: $('input#getOrderUrl').val(),
            data: {fromDate: fromDate, toDate: toDate, shopIds: shopIds}, 
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                this_.prop('disabled', false);
                $('.loading-getorder').fadeOut();
                if(json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
                this_.prop('disabled', false);
                $('.loading-getorder').fadeOut();
            }
        });
    });

    var orderShopifyIds = [];
    var isCheckAll = false;
    $('body').on('ifToggled', 'input#checkAll', function (e) {
        orderShopifyIds = [];
        if (e.currentTarget.checked) {
            isCheckAll = true;
            $('input.iCheckItem').iCheck('check');
            $('#tbodyOrder input.iCheckItem').each(function(){
                orderShopifyIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').iCheck('uncheck');
            isCheckAll = false;
        }
    }).on('ifToggled', 'input.iCheckItem', function (e) {
        if(e.currentTarget.checked){
            if(!isCheckAll) orderShopifyIds.push(e.currentTarget.value);
        }
        else{
            var index = orderShopifyIds.indexOf(e.currentTarget.value);
            if(index >= 0) orderShopifyIds.splice(index, 1);
        }
    });

    $('#selectAction').on('change', function () {
        if(orderShopifyIds.length > 0) {
            var actionCode = $(this).val();
            if (actionCode === 'send_factory') {
                if($('input#orderStatusIdHidden').val() == '1') {
                    $('#countOrder').text(orderShopifyIds.length);
                    $('#modalSendFactory').modal();
                }
                else showNotification('Please choose Order status is Not send', 0);
            }
            else if(actionCode == 'export_excel'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action',$('#baseUrl').attr('href') + 'ordershopify/exportExcel');
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if(actionCode == 'export_excel2'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action',$('#baseUrl').attr('href') + 'ordershopify/exportExcel2');
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if(actionCode == 'export_excel3'){
                var orderIds = '';
                for (var i = 0; i < orderShopifyIds.length; i++) orderIds += orderShopifyIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').attr('action',$('#baseUrl').attr('href') + 'ordershopify/exportExcel3'); 
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
            else if (actionCode.indexOf('change_status-') >= 0) {
                actionCode = actionCode.split('-');
                if (actionCode.length == 2) {
                    var statusId = parseInt(actionCode[1]);
                    if (confirm('Do you really want to change ?')) {
                        $.ajax({
                            type: "POST",
                            url: $('input#changeItemStatusUrl').val(),
                            data: {
                                OrderShopifyIds: orderShopifyIds,
                                StatusId: statusId
                            },
                            success: function (response) {
                                var json = $.parseJSON(response);
                                showNotification(json.message, json.code);
                                if (json.code == 1) {
                                    for (var i = 0; i < orderShopifyIds.length; i++) $('#trItem_' + orderShopifyIds[i]).remove();
                                }
                            },
                            error: function (response) {
                                showNotification('An error occurred during the execution', 0);
                            }
                        });
                    }

                }
            }
        }
        else showNotification('Please choose Order', 0);
        $(this).val('');
    });

    $('#btnSendFactory').click(function(){
        var factoryId = parseInt($('select#factoryId1').val());
        if(factoryId > 0 && orderShopifyIds.length > 0) {
            $.ajax({
                type: 'POST',
                url: $('input#sendFactoryUrl').val(),
                data: {
                    FactoryId: factoryId,
                    FactoryRequestId: $('select#factoryRequestId').val(),
                    Comment: $('#commentFactory').val(),
                    OrderShopifyIds: orderShopifyIds
                },
                success: function (data) {
                    var json = JSON.parse(data);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    //$('#modalSendFactory').modal('hide');
                },
                error: function (err) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        else showNotification('Please choose Factory and Order', 0);
    });

    $('#btnImportOrder').click(function(){
        $('#modalImportExcel').modal('show');
        $('input#fileExcelUrl').val('');
        $(this).parents('form').submit(function(e) {
           e.preventDefault();
        });
    });

    $('#btnUploadExcel').click(function(){
        chooseFile('Files', function(fileUrl){
            $('input#fileExcelUrl').val(fileUrl);
        });
    });

    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            $('#btnImportExcel').prop('disabled', true);
            $('.imgLoading').show();
            $.ajax({
                type: "POST",
                url: $('input#importOrderUrl').val(),
                data: {
                    FileUrl: fileExcelUrl
                },
                success: function (response) {
                    $('.imgLoading').hide();
                    $('#modalImportExcel').modal('hide');
                    $('#btnImportExcel').prop('disabled', false);
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                },
                error: function (response) {
                    $('.imgLoading').hide();
                    showNotification('An error occurred during the execution', 0);
                    $('#btnImportExcel').prop('disabled', false);
                }
            });
        }
        else showNotification('Please choose Excel file', 0);
    });

    var factoryId = $('select#factoryId1').val();
    $('select#factoryRequestId option[data-id="' + factoryId + '"]').show();
    $('select#factoryId1').change(function(){
        factoryId = $(this).val();
        $('select#factoryRequestId option').hide();
        $('select#factoryRequestId option[data-id="' + factoryId + '"]').show();
        $('select#factoryRequestId option[value="0"]').show();
        $('select#factoryRequestId').val('0');
    });
});
$(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $(".price").on('input',function(){
        var sumPrice = 0,
        nameQuantity = '.quantity'+$(this).attr('dataId'),
        nameSumPrice = '.sum_price'+$(this).attr('dataId');
        sumPrice = $(nameQuantity).text() * $(this).val();
        $(nameSumPrice).html(formatDecimal(sumPrice.toString()));
    });
    $("#btnExport").click(function(){
        var listExport = [];
        $('#tbodyOrder tr').each(function(){
            var CrDateTime = this.cells[0].innerHTML,
                 TeamName = this.cells[1].innerHTML,
                 ShopName = this.cells[2].innerHTML,
                 ProductType = this.cells[3].innerHTML,
                 Quantity = this.cells[4].innerHTML,
                 Price = this.cells[5].children[0].value,
                 Sum = replaceCost(this.cells[6].innerHTML, true);
            listExport.push(
                {
                    'CrDateTime': CrDateTime,
                    'TeamName' : TeamName,
                    'ShopName': ShopName,
                    'ProductType': ProductType,
                    'Quantity': Quantity,
                    'Price' : Price,
                    'Sum' : Sum
                }
            );
        });
        $('input#listExport').val(JSON.stringify(listExport));
        $('#exportForm').submit();
        $('input#listExport').val('');
    });
});
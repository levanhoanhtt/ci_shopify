/**
 * Created by vu.vuong on 11/7/2017.
 */
$(document).ready(function () {
    $("#tbodyPenalty").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        var factoryId = $(this).attr('data-factory-id');
        var productType = $('#productype_'+id).text();
        $('select#factories-list').val(factoryId);
        $('select#products-list').val(productType);
        $('input#penalty-day').val($('#penaltyallow_'+id).text());
        $('input#penalty-percent').val($('#penaltypercent_'+id).text());
        $('#penaltyId').val(id);
        scrollTo('select#factories-list');
        return false;
    });

    $("#tbodyPenalty").on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deletePenaltyUrl').val(),
                data: {
                    PenaltyRateId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#penalty_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });

    $('a#link_cancel').click(function(){
        $('#penaltyForm').trigger("reset");
        return false;
    });

    $('a#link_update').click(function(){
        if (validateEmpty('#penaltyForm')) {
            var form = $('#penaltyForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="penalty_' + data.PenaltyRateId + '">';
                            html += '<td id="penalty_' + data.PenaltyRateId + '">' + data.FactoryName + '</td>';
                            html += '<td id="productype_' + data.PenaltyRateId + '">' + data.ProductType + '</td>';
                            html += '<td id="penaltyallow_' + data.PenaltyRateId + '">' + data.AllowDay + '</td>';
                            html += '<td id="penaltypercent_' + data.PenaltyRateId + '">' + data.PenaltyPercentage + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.PenaltyRateId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.PenaltyRateId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyPenalty').prepend(html);
                        }
                        else{
                            $('penalty_'+data.PenaltyRateId).val(data.FactoryName);
                            $('#productype_'+data.PenaltyRateId).val(data.ProductType);
                            $('#penaltyallow_'+data.PenaltyRateId).text(data.AllowDay);
                            $('#penaltypercent_'+data.PenaltyRateId).text(data.PenaltyPercentage)
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });


});
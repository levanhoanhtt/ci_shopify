$(document).ready(function () {
    $('.image-product').lazyload();
    $('.image-product').error(function() {
        $(this).remove(); 
    });
    $('.submit').click(function () {
        if (confirm('Are you sure ?')) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    FactoryRequestId: $('input#factoryRequestId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        $('.submit').remove();
                        $('#aExportPdf').show();
                        $('.removeOrderRequest').remove();
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
    });

    $('#aExportExcel').click(function(){
        var totalSku = $('#tbodyOrder tr').length - 1;
        var step = parseInt($('input#limitPerExcel').val());
        if(totalSku > step){
            var html = '';
            var link = $(this).attr('href') + '/';
            var n = Math.ceil(totalSku/step);
            for(var i = 1; i <= n; i++){
                html += '<li><a href="' + link + i + '" target="_blank" class="btn btn-info btn-sm">File ' + i +'</a></li>';
            }
            $('#ulLinkExport').html(html);
            $('#exportExcelModal').modal('show');
            return false;
        }
    });

    $('#aListOrder').click(function(){
        $('#modalOrder').modal('show');
        return false;
    });

    $('#modalOrder').on('hidden.bs.modal', function () {
        if($('input#isReloadPage').val() == '1') redirect(true, '');
    });

    $('.removeOrderRequest').click(function () {
        var orderId = $(this).attr('data-id');
        if (orderId > 0) {
            if (confirm('Are you sure ?')) {
                $.ajax({
                    type: "POST",
                    url: $('input#deleteOrderUrl').val(),
                    data: {
                        OrderShopifyId: orderId,
                        FactoryRequestId: $('input#factoryRequestId').val()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) {
                            $('#order_' + orderId).fadeOut();
                            $('input#isReloadPage').val('1');
                        }
                    },
                    error: function (response) {
                        showNotification('An error occurred during the execution', 0);
                    }
                });
            }
        }
    });
});
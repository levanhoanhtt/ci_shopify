$(document).ready(function() {
    /*$('input.iCheckTable').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });*/
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#shopIds').change(function(){
        var shopIds = $(this).val();
        if(shopIds != null && shopIds.length == 1) $('#aExportAll').attr('href', $('input#exportUrl').val() + shopIds[0]);
    });
    var orderAllIds = [];
    var isCheckAll = false;
    $('input#checkAll').change(function() {
        orderAllIds = [];
        if(this.checked){
            isCheckAll = true;
            var cb = $('input.iCheckItem');
            cb.prop('checked', true);
            cb.each(function(){
                orderAllIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').prop('checked', false);
            isCheckAll = false;
        }

    });
    $('input.iCheckItem').change(function(){
        if(this.checked){
            if(!isCheckAll) orderAllIds.push(this.value);
        }
        else{
            var index = orderAllIds.indexOf(this.value);
            if(index >= 0) orderAllIds.splice(index, 1);
        }
    });
    $('#selectAction').on('change', function () {
        if(orderAllIds.length > 0) {
            var actionCode = $(this).val();
            if(actionCode == 'export_excel'){
                var orderIds = '';
                for (var i = 0; i < orderAllIds.length; i++) orderIds += orderAllIds[i] + ',';
                orderIds = orderIds.substring(0, orderIds.length - 1);
                $('input#orderIds').val(orderIds);
                $('#exportForm').submit();
                $('input#orderIds').val('');
            }
        }
        else showNotification('Please choose Order', 0);
        $(this).val('');
    });
});
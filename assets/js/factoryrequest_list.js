$(document).ready(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#tbodyOrder').on('click', '.link_delete', function(){
        var id = $(this).attr('data-id');
        if (confirm('Are you sure ?')) {
            $.ajax({
                type: "POST",
                url: $('input#deleteUrl').val(),
                data: {
                    FactoryRequestId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1)  $('#trItem_' + id).remove();
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    }).on('click', '.link_export', function(){
        var aThis = $(this);
        var id = aThis.attr('data-id');
        if($('input#countSku_' + id).length > 0) showExportLink(id, parseInt($('input#countSku_' + id).val()), aThis.attr('href') + '/');
        else{
            $.ajax({
                type: "POST",
                url: $('input#countSkuUrl').val(),
                data: {
                    FactoryRequestId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        $('#divCountSku').append('<input type="text" hidden="hidden" id="countSku_'+id+'" value="'+json.data+'">');
                        showExportLink(id, parseInt(json.data), aThis.attr('href') + '/');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('#tbodyOrder .tdQuantity').each(function(){
        var td = $(this);
        var id = td.attr('data-id');
        $.ajax({
            type: "POST",
            url: $('input#sumQuantityUrl').val(),
            data: {
                FactoryRequestId: id
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1){
                    if(parseInt(json.data) > 0) td.text(json.data);
                    else $('#trItem_' + id).remove();
                }
                else $('#trItem_' + id).remove();
            },
            error: function (response) {
                $('#trItem_' + id).remove();
            }
        });
    });
});

function showExportLink(id, totalSku, link){
    var step = parseInt($('input#limitPerExcel').val());
    if(totalSku > step){
        var html = '';
        var n = Math.ceil(totalSku/step);
        for(var i = 1; i <= n; i++){
            html += '<li><a href="' + link + i + '" target="_blank" class="btn btn-info btn-sm">File ' + i +'</a></li>';
        }
        $('#ulLinkExport').html(html);
        $('#exportExcelModal').modal('show');
    }
    else window.location.href  = link;
}
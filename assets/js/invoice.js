$(document).ready(function () {
    var POPercent = replaceCost($('#tdPOPercent').text(), false);
    var exchangeRate = replaceCost($('#tdExchangeRate').text(), false);
    $('body').on('keydown', 'input.cost', function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8|| e.keyCode == 190 || e.keyCode == 110 || e.keyCode == 109 || e.keyCode == 189)) {
            e.preventDefault();
        }
    }).on('keyup', 'input#offsetPrice', function() {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        value = replaceCost(value, false);
        var orderPrice = replaceCost($('#tdOrderPrice').text(), false);
        var result = (orderPrice + value) * POPercent / 100;
        $('#tdTotalPO').text(formatDecimal(result.toString()));
        result = (orderPrice + value) * exchangeRate;
        $('#tdTotalVND').text(formatDecimal(result.toString()));
    }).on('keyup', 'input.price', function() {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        value = replaceCost(value, false);
        var tr = $(this).parent().parent();
        var quantity = replaceCost(tr.find("td.quantity").text(), true);
        var totalPrice = value * quantity;
        tr.find("td.totalPrice").text(formatDecimal(totalPrice.toString()));
        var orderPrice = 0;
        $('#tbodyInvoices tr').each(function(){
            orderPrice += replaceCost($(this).find('td.totalPrice').text(), false);
        });
        $('#tdOrderPrice').text(formatDecimal(orderPrice.toString()));
        var offsetPrice = $('input#offsetPrice').length > 0 ? replaceCost($('input#offsetPrice').val(), false) : replaceCost($('input#offsetPriceOriginal').val(), false);
        var result = (orderPrice + offsetPrice) * POPercent / 100;
        $('#tdPOPercent').text(formatDecimal(result.toString()));
        result = (orderPrice + offsetPrice) * exchangeRate;
        $('#tdTotalVND').text(formatDecimal(result.toString()));
    });

    var invoiceId = parseInt($('input#invoiceId').val());

    $('.btnReceived').click(function () {
        if (confirm('Are you sure ?')) {
            var btn = $('.btnReceived');
            $.ajax({
                type: "POST",
                url: $('input#receivedUrl').val(),
                data: {
                    InvoiceId: invoiceId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });

    $('#btnUpdatePrice').click(function(){
        var priceList = {};
        var orderPrice = 0;
        $('#tbodyInvoices tr').each(function(){
            priceList[$(this).find('td.type').text()] = replaceCost($(this).find('input.price').val(), false);
            orderPrice += replaceCost($(this).find('td.totalPrice').text().trim(), false);
        });
        if(orderPrice > 0){
            var btn = $(this);
            btn.prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateInvoiceUrl').val().trim(),
                data: {
                    InvoiceId: invoiceId,
                    InvoiceStatusId: 3,
                    PriceList: JSON.stringify(priceList),
                    OrderPrice: orderPrice,
                    OffsetPrice: replaceCost($('input#offsetPrice').val(), false),
                    Comment: $('#comment').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                    else btn.prop('disabled', false);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                    btn.prop('disabled', false);
                }
            });
        }
    });

    $('#btnSendMail').click(function(){
        var btn = $(this);
        btn.prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#sendMailInvoiceUrl').val().trim(),
            data: {
                InvoiceIds: [invoiceId]
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1){
                    btn.parent('li').remove();
                    $('#spanInvoiceStatus').html(json.data.StatusName);
                }
                else btn.prop('disabled', false);
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
                btn.prop('disabled', false);
            }
        });
    });
});
$(document).ready(function() {
	$('.submit').click(function(){
		var this_ = $(this);
		var form = $("#shopForm");
		form.submit(function(e){
			e.preventDefault();
	    });
		if(validateEmpty('#shopForm')){
			this_.prop('disabled', true);
			$.ajax({
			    type: "POST",
			    url: form.attr('action'),
			    data: form.serialize(),
			    success: function (response) {
			        var json = $.parseJSON(response);
			        showNotification(json.message, json.code);
					this_.prop('disabled', false);
					if(json.code == 1 && $('input#shopId').val() == '0') redirect(false, $('input#shopEditUrl').val() + '/' + json.data);
			    },
			    error: function (response) {
			        showNotification('An error occurred during the execution', 0);
					this_.prop('disabled', false);
			    }
			});
		}
	});
});	
$(document).ready(function(){
    Sortable.create(ulImages, {});
    $('#btnImage').click(function(){
        chooseFile('Images', function(fileUrl) {
            $('#ulImages').append('<li><a href="' + fileUrl + '" target="_blank"><img src="' + fileUrl + '"></a><i class="fa fa-remove"></i></li>');
        });
    });
    /*$('#btnImage').selectFileUpload({
        resourceName: 'Images',
        callback: function (files) {
            var html = '';
            for(var i = 0; i < files.length; i++){
                html += '<li><a href="' + files[i] + '" target="_blank"><img src="' + files[i] + '"></a><i class="fa fa-remove"></i></li>';
            }
            $('#ulImages').html(html);
        }
    });*/
    $('#ulImages').on('click', 'i', function(){
        $(this).parent().remove();
    });
    $('.submit').click(function(){
        var sku = $('input#sku').val().trim();
        if(sku == ''){
            showNotification('Sku can not be left blank', 0);
            $('input#sku').focus();
            return false;
        }
        var images = [];
        $('#ulImages a').each(function(){
            images.push($(this).attr('href'));
        });
        if(images.length == 0){
            showNotification('Please choose image', 0);
            return false;
        }
        $.ajax({
            type: "POST",
            url: $('#productForm').attr('action'),
            data: {
                ProductId: $('input#productId').val(),
                Sku: sku,
                ProductName: $('input#productName').val().trim(),
                Images: images
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) redirect(false, $('#aProductList').attr('href'));
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
        return false;
    });
});

$(document).ready(function() {
    $("#tbodyConfig").on("click", "a.link_update", function () {
        var id = $(this).attr('data-id');
        var value = $('input#configValue_' + id).val().trim();
        if(value != ''){
            $.ajax({
                type: "POST",
                url: $('input#updateConfigUrl').val(),
                data: {
                    ConfigId: id,
                    ConfigValue: value
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        else{
            showNotification('Config value can not be left blank', 0);
            $('input#configValue_' + id).focus();
        }
        return false;
    });
});

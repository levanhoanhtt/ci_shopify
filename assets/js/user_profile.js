$(document).ready(function(){
    /*$('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    province();*/
    $('.chooseImage').click(function(){
        chooseFile('Users', function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        });
    });
    $(document).on('submit','#profileForm',function (){
        if(validateEmpty('#profileForm')) {
            if ($('input#newPass').val().length > 0 || $('input#rePass').val().length > 0) {
                if ($('input#newPass').val() != $('input#rePass').val()) {
                    showNotification('Password not match', 0);
                    return false;
                }
            }
            if($('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Username must not have space', 0);
                $('input#userName').focus();
                return false;
            }
            var form = $('#profileForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input#userPass').val('');
                        $('input#newPass').val('');
                        $('input#rePass').val('');
                    }
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
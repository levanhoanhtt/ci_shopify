$(document).ready(function(){
    /*province();
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });*/
    var roleId = parseInt($('select#roleId').val());
    if(roleId == 3) $('#divFactory').show();
    else{
        $('#divFactory').hide();
        $('select#factoryId').val('0');
    }
    $('select#roleId').change(function(){
        roleId = parseInt($(this).val());
        if(roleId == 3) $('#divFactory').show();
        else{
            $('#divFactory').hide();
            $('select#factoryId').val('0');
        }
    });
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.chooseImage').click(function(){
        chooseFile('Users', function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        });
    });
    $('a#generatorPass').click(function(){
        var pass = randomPassword(10);
        $('input#newPass').val(pass);
        $('input#rePass').val(pass);
        return false;
    });
    var userId = parseInt($('input#userId').val());
    $('.submit').click(function (){
        if(validateEmpty('#userForm')) {
            if ($('input#newPass').val() != $('input#rePass').val()) {
                showNotification('Password not match', 0);
                return false;
            }
            if($('input#userName').length > 0 && $('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Username must not have space', 0);
                $('input#userName').focus();
                return false;
            }
            var form = $('#userForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 && userId == 0) redirect(false, $('input#userEditUrl').val() + '/' + json.data);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    //var chars = "ABCDEFGHIJKLMNOPQRSTXYZ1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
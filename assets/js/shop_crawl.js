jQuery(document).ready(function($) {
	$('#submit').click(function(event) {
		var id = $('#shopId').val();
		var this_ = $(this);
		if(id == 0)
		{
			 showNotification('Chọn shop trước khi lấy đơn hàng', 0);
			 return false;
		}
		$('.progress').fadeIn();
		$('.progress-bar').css('transition', 'all 8s');
		$('.progress-bar').animate({width: "98%"}, 100);
		this_.prop('disabled', true);
		$.ajax({
		    type: "POST",
		    url: $('input#crawlUrl').val(),
		    data: {
		        ShopId: id
		    },
		    success: function (response) {
		        var json = $.parseJSON(response);
		        $('.progress').fadeOut();
		        showNotification(json.message, json.code);
		        $('.progress-bar').animate({width: "0%"}, 100);
		        

		        this_.prop('disabled', false);
		    },
		    error: function (response) {
		        showNotification('An error occurred during the execution', 0);
		        this_.prop('disabled', false);
		    }
		});

		return false;

	});

 // $('.progress .progress-bar').css("width",function() {
 //      return $(this).attr("aria-valuenow") + "%";
 //    })
});
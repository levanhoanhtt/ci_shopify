$(document).ready(function(){
    $("#tbodyWarehouse").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#warehouseId').val(id);
        $('input#warehouseName').val($('td#warehouseName_' + id).text());
        scrollTo('input#warehouseName');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#warehouseForm').trigger("reset");
        return false;
    });
    $("#tbodyWarehouse").on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteWarehouseUrl').val(),
                data: {
                    WarehouseId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#warehouse_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#warehouseForm')) {
            var form = $('#warehouseForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="warehouse_' + data.WarehouseId + '">';
                            html += '<td id="warehouseName_' + data.WarehouseId + '">' + data.WarehouseName + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.WarehouseId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.WarehouseId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyWarehouse').prepend(html);
                        }
                        else $('td#warehouseName_' + data.WarehouseId).text(data.WarehouseName);
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
$(document).ready(function(){
    $("#tbodyBank").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#bankId').val(id);
        $('input#bankName').val($('td#bankName_' + id).text());
        $('input#bankNumber').val($('td#bankNumber_' + id).text());
        $('input#bankDesc').val($('td#bankDesc_' + id).text());
        scrollTo('input#bankName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteBankUrl').val(),
                data: {
                    BankId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#bank_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#bankForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#bankForm')) {
            var form = $('#bankForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="bank_' + data.BankId + '">';
                            html += '<td id="bankName_' + data.BankId + '">' + data.BankName + '</td>';
                            html += '<td id="bankNumber_' + data.BankId + '">' + data.BankNumber + '</td>';
                            html += '<td id="bankDesc_' + data.BankId + '">' + data.BankDesc + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.BankId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.BankId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyBank').prepend(html);
                        }
                        else {
                        	$('td#bankName_' + data.BankId).text(data.BankName);
                        	$('td#bankNumber_' + data.BankId).text(data.BankNumber);
                        	$('td#bankDesc_' + data.BankId).text(data.BankDesc);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
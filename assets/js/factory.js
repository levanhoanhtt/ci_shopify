$(document).ready(function(){
    $("#tbodyFactory").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#factoryId').val(id);
        $('#tdFactoryId').text(id);
        $('input#factoryName').val($('td#factoryName_' + id).text());
        $('input#factoryDay').val($('td#factoryDay_' + id).text());
        $('input#warehouseDay').val($('td#warehouseDay_' + id).text());
        scrollTo('input#factoryName');
        return false;
    }).on("click", "a.link_delete", function(){
        if (confirm('Do you really want to delete ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteFactoryUrl').val(),
                data: {
                    FactoryId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#factory_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#tdFactoryId').text('');
        $('#factoryForm').trigger("reset");
        return false;
    });
    $('a#link_update').click(function(){
        if (validateEmpty('#factoryForm')) {
            var form = $('#factoryForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        $('#tdFactoryId').text('');
                        form.trigger("reset");
                        var data = json.data;
                        if(data.IsAdd == 1){
                            var html = '<tr id="factory_' + data.FactoryId + '">';
                            html += '<td>' + data.FactoryId + '</td>';
                            html += '<td id="factoryName_' + data.FactoryId + '">' + data.FactoryName + '</td>';
                            html += '<td id="factoryDay_' + data.FactoryId + '">' + data.FactoryDay + '</td>';
                            html += '<td id="warehouseDay_' + data.FactoryId + '">' + data.WarehouseDay + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit" data-id="' + data.FactoryId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete" data-id="' + data.FactoryId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyFactory').prepend(html);
                        }
                        else{
                            $('td#factoryName_' + data.FactoryId).text(data.FactoryName);
                            $('td#factoryDay_' + data.FactoryId).text(data.FactoryDay);
                            $('td#warehouseDay_' + data.FactoryId).text(data.WarehouseDay);
                        }
                    }
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
});
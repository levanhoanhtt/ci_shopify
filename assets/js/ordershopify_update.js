$(document).ready(function() {
    $('#tbodyOrderProducts').on('keydown', 'input.cost', function (e) {
        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8|| e.keyCode == 190 || e.keyCode == 110)) {
            e.preventDefault();
        }
    }).on('keyup', 'input.cost', function() {
        var value = $(this).val();
        $(this).val(formatDecimal(value));
        var tr = $(this).parent().parent();
        var quantity = tr.find('input.quantity').val();
        var price = tr.find('input.price').val();
        var sumPrice = replaceCost(quantity, true) * replaceCost(price, false);
        tr.find('input.sumPrice').val(formatDecimal((sumPrice.toString())));
        calcPrice();
    }).on('click', '.link_delete', function() {
        var tr = $(this).parent().parent();
        var id = tr.attr('data-id');
        if(id == '0'){
            tr.remove();
            calcPrice();
            return false;
        }
        if (confirm('Do you really want to delete ?')) {
            $.ajax({
                type: "POST",
                url: $('input#deleteProductUrl').val(),
                data: {
                    OrderProductId: id,
                    Sku: tr.find('input.sku').val().trim(),
                    Quantity: tr.find('input.quantity').val().trim(),
                    OrderShopifyId: $('#orderShopifyId').val(),
                    OrderName: $('#spanOrderName').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        return false;
    });
    $('#btnAddSku').click(function(){
        var html = '<tr data-id="0" class="trProduct">';
        html += '<td><input type="text" class="form-control productName" value=""></td>';
        html += '<td><input type="text" class="form-control sku" value=""></td>';
        html += '<td><input class="form-control quantity cost" value=""></td>';
        html += '<td><input class="form-control price cost" value=""></td>';
        html += '<td><input class="form-control sumPrice text-right" disabled value=""></td>';
        html += '<td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Delete"></i></a></td></tr>';
        $('#tbodyOrderProducts').prepend(html);
    });
    $('.submit').click(function() {
        var this_ = $(this);
        var form = $("#orderShopifyForm");
        form.submit(function(e) {
            e.preventDefault();
        });
        if (validateEmpty('#orderShopifyForm')) {
            var shipping = {
                name: $('#customerName').val().trim(),
                phone: $('#customerPhone').val().trim(),
                address1: $('#customerAddress1').val().trim(),
                address2: $('#customerAddress2').val().trim(),
                city: $('#customerCity').val().trim(),
                province: $('#customerProvince').val().trim(),
                country: $('#customerCountry').val().trim(),
                zip: $('#customerZip').val().trim(),
                province_code: $('#customerProvinceCode').val().trim(),
                country_code: $('#customerCountryCode').val().trim()
            };
            var products = [];
            var productName = '';
            var sku = '';
            var quantity = 0;
            var price = 0;
            var flag = false;
            $('#tbodyOrderProducts .trProduct').each(function() {
                productName = $(this).find('input.productName').val().trim();
                sku = $(this).find('input.sku').val().trim();
                quantity = replaceCost($(this).find('input.quantity').val().trim(), true);
                price = replaceCost($(this).find('input.price').val().trim(), false);
                if (productName == '' || sku == '' || quantity == 0 || price == 0) {
                    flag = true;
                    showNotification('Product Name or Sku or Quantity or Price can not be left blank', 0);
                    return false;
                }
                var parts = sku.split('-');
                if (parts.length < 4) {
                    flag = true;
                    showNotification('Sku is incorrect', 0);
                    return false;
                }
                products.push({
                    OrderProductId: $(this).attr('data-id'),
                    ProductName: productName,
                    Sku: sku,
                    Quantity: quantity,
                    Price: price
                });
            });
            if (!flag) {
                this_.prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: {
                        OrderShopifyId: $('#orderShopifyId').val(),
                        CustomerName: $('#customerName1').val().trim(),
                        CustomerEmail: $('#customerEmail').val().trim(),
                        Shipping: shipping,
                        Products: products,
                        Comment: $('#comment').val().trim()
                    },
                    success: function(response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        this_.prop('disabled', false);

                    },
                    error: function(response) {
                        showNotification('An error occurred during the execution', 0);
                        this_.prop('disabled', false);
                    }
                });
            }
        }
    });
    return false;
});

function calcPrice() {
    var sumPrice = 0;
    $('#tbodyOrderProducts .trProduct').each(function() {
        sumPrice += replaceCost($(this).find('input.sumPrice').val(), false);
    });
    sumPrice = sumPrice.toFixed(2);
    $('#spanTotalMoney').text(formatDecimal(sumPrice.toString()));
}
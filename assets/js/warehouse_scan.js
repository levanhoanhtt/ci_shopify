$(document).ready(function () {
    $('input.iCheckTable, input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var warehouseScanIds = [];
    var isCheckAll = false;
    $('body').on('ifToggled', 'input#checkAll', function (e) {
        warehouseScanIds = [];
        if (e.currentTarget.checked) {
            isCheckAll = true;
            $('input.iCheckItem').iCheck('check');
            $('#tbodyScan input.iCheckItem').each(function(){
                warehouseScanIds.push($(this).val());
            });
        }
        else{
            $('input.iCheckItem').iCheck('uncheck');
            isCheckAll = false;
        }
    }).on('ifToggled', 'input.iCheckItem', function (e) {
        if(e.currentTarget.checked){
            if(!isCheckAll) warehouseScanIds.push(e.currentTarget.value);
        }
        else{
            var index = warehouseScanIds.indexOf(e.currentTarget.value);
            if(index >= 0) warehouseScanIds.splice(index, 1);
        }
    });
    $('#btnTrackingCode').click(function(){
        if(warehouseScanIds.length > 0){
            $('span#countProduct').text(warehouseScanIds.length);
            $('#modalTrackingCode').modal('show');
        }
        else showNotification('Please choose product scan', 0);
    });
    $('#btnInsertTrackingCode').click(function(){
        var trackingCode = $('input#trackingCodeInsert').val().trim();
        if(trackingCode == ''){
            showNotification('Internal Tracking Code can not be left blank', 0);
            $('input#trackingCodeInsert').focus();
            return false;
        }
        $.ajax({
            type: "POST",
            url: $('input#insertTrackingCodeUrl').val(),
            data: {
                TrackingCode: trackingCode,
                FactoryId: $('select#factoryId').val(),
                WarehouseScanIds: warehouseScanIds
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    });
});
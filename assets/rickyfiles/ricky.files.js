$.fn.selectFileUpload = function (opt) {
    var root = this;
    var conf = $.extend({
        showPopup: true,
        multiSelect: true,
        resourceTypes: ['*'],
        fileTypes: ['*'],
        container: '#selectFile',
        pathSource: 'images',

        resourceName: 'Images',
        url: $('input#rootPath').val() + 'upload',

        callback: function (files) {}
    }, opt);
    var container = {
        folderRoot: null,
        folderChose: conf.pathSource,
        foldeDelete: conf.pathSource,
        folderUpload: conf.pathSource,
        folderLoadFile: conf.pathSource,
        imagesSelect: []
    };
    var methods = {
        init: function () {},
        initData: function () {},
        events: function () {},
        loadImage: function (path) {},
        createFolder: function (folderName) {},
        deleteFolder: function (path) {},
        renderImg: function (data) {},
        uploadFile: function (path) {},
        selectImageSource: function () {},
        choseImage: function (callback) {},
        choseImageSingle: function (file, callback) {},
        deleteFile: function () {},
        loadFolder: function (path) {},
        renderFolder: function (data) {}
    };
    methods.init = function () {
        if($('#modalPopupImage').length == 0) {
            var html =
                '<div id="modalPopupImage" class="modal fade" role="dialog">' +
                '<div class="modal-dialog modal-lg" style="width: 90%">' +
                '<div class="modal-content" style="width: 100%">' +
                '<div class="modal-header">' +
                '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                '<h4 class="modal-title">Upload photos or select image available</h4>' +
                '</div>' +
                '<div class="modal-body">' +
                '<div class="container-fluid">' +
                '<div class="col-md-2">' +
                '<div class="tool-bar">' +
                '<ul class="list-toolbar">' +
                '<li><a href="javascript:void(0)" id="addFolder"><i class="fa fa-folder" aria-hidden="true"></i></a></li>' +
                '<li><a href="javascript:void(0)" id="removeFolder"><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>' +
                '</ul>' +
                '</div>' +
                '<div class="list-folder">' +
                '</div>' +
                '</div>' +
                '<div class="col-md-10">' +
                '<div class="options">' +
                '<input type="file" class="fileUpload" id="fileUpload" multiple style="display:inline-block">' +
                '<button type="button" id="uploadFile">Upload</button>' +
                '<button type="button" id="choseFile" style="margin-left:5px">Choose</button>' +
                '<button type="button" id="choseAllFile" style="margin-left:5px">Choose all</button>' +
                '<button type="button" id="deleteFile" style="margin-left:5px">Delete</button>' +
                '</div>' +
                '<div class="row list-files"></div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
            $(html).insertAfter(root);
            methods.events();
            methods.initData();
        }
    };
    methods.renderFolder = function (folders) {
        var firtTree = true;
        var html = '<ul class="folder-root">' +
            '<li><a href="javascript:;" class="folder-title folder-chose active-folder" data-path="' + conf.pathSource + '">' + container.folderRoot + '</a>' +
            '<ul>{{ulRoot}}</ul>' +
            '</li>' +
            '</ul>';
        var htmlTmp = '';
        for (var key in folders) {
            var htmlFolers = '<li><ul class="folders">' +
                '<li class="folder">' +
                '<a href="javascript:;" class="folder-title folder-chose" data-path="' + conf.pathSource + '/' + key.toString() + '">' + key.toString() + '</a>' +
                '{{folderSub}}' +
                '</li>' +
                '</ul></li>';
            var htmlFolderSub = '<ul class="items">' +
                '{{itemsSub}}' +
                '</ul>';
            var htmlItemsSub = '';
            for (var i = 0; i < folders[key].length; i++) {
                htmlItemsSub += '<li class="item"><a data-path="' + conf.pathSource + '/' + key.toString() + '/' + folders[key][i] + '" href="javascript:;" class="folder-chose">' + folders[key][i] + ' </a></li>';
                firtTree = false;
            }
            htmlFolderSub = htmlFolderSub.replace('{{itemsSub}}', htmlItemsSub);
            htmlFolers = htmlFolers.replace('{{folderSub}}', htmlFolderSub);
            htmlTmp += htmlFolers;
        }
        html = html.replace('{{ulRoot}}', htmlTmp);
        return html;
    };
    methods.loadFolder = function (path) {
        $.ajax({
            type: 'POST',
            url: conf.url + '/loadFolder',
            data: {
                path: path
            },
            success: function (data) {
                data = JSON.parse(data);
                var html = methods.renderFolder(data);
                $('#modalPopupImage').find('.list-folder').html(html);
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    };
    methods.deleteFile = function (paths) {
        $.ajax({
            type: 'POST',
            url: conf.url + '/deleteFile',
            data: {
                paths: paths
            },
            success: function (data) {
                methods.loadImage(conf.folderLoadFile);
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    };
    methods.renderImg = function (data) {
        var htmlFiles = '';
        for (var iFile = 0; iFile < data.length; iFile++) {
            htmlFiles += '<div class="col-md-2">' +
                '<img src="' + data[iFile] + '" class="img-file">' +
                '</div>';
        }
        return htmlFiles;
    };
    methods.loadImage = function (path) {
        $.ajax({
            type: 'POST',
            url: conf.url + '/loadImage',
            data: {
                path: path
            },
            success: function (data) {
                data = JSON.parse(data);
                var htmlFiles = methods.renderImg(data);
                $('#modalPopupImage').find('.list-files').html(htmlFiles);
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    };
    methods.createFolder = function (folderName) {
        var level = container.folderChose.replace(conf.pathSource, '').substr(1).split('/');
        if (level.length == 2) {
            level.splice(level.length - 1, 1);
            container.folderChose = conf.pathSource + '/' + level.join('/');
        }
        if (folderName != null && folderName.length > 0 && /[a-zA-Z0-9_]+/igm.test(folderName)) {
            $.ajax({
                type: 'POST',
                url: conf.url + '/createFolder',
                data: {
                    folderName: folderName.trim(),
                    path: container.folderChose
                },
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.status == 1) {
                        showNotification('Create folder success', 1);
                        methods.loadFolder(conf.pathSource);
                        methods.loadImage(conf.pathSource);
                    }
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
        else showNotification('Folder Name is wrong', 0);
    };
    methods.deleteFolder = function (path) {
        $.ajax({
            type: 'POST',
            url: conf.url + '/deleteFolder',
            data: {
                path: path
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 1) {
                    showNotification('Delete folder success', 1);
                    methods.loadFolder(conf.pathSource);
                    methods.loadImage(conf.pathSource);
                }
            },
            error: function (response) {
                showNotification('An error occurred during the execution', 0);
            }
        });
    };
    methods.uploadFile = function (path) {
        var files = document.getElementById('fileUpload').files;
        if (files.length > 0) {
            var fileForm = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileForm.append(path + '+file-' + i, files[i]);
            }
            $.ajax({
                url: conf.url + '/uploadFile',
                type: 'POST',
                data: fileForm,
                cache: false,
                processData: false,
                contentType: false,
                success: function (data) {
                    data = JSON.parse(data);
                    var htmlTmp = methods.renderImg(data);
                    $('#modalPopupImage').find('.list-files').append(htmlTmp);
                },
                error: function (response) {
                    showNotification('An error occurred during the execution', 0);
                }
            });
        }
    };
    methods.selectImageSource = function (image) {
        var selected = false;
        for (var i = 0; i < container.imagesSelect.length; i++) {
            if ($(image).attr('src') == $(container.imagesSelect[i]).attr('src')) {
                $(image).removeClass('image-selected');
                container.imagesSelect.splice(i, 1);
                selected = true;
                break;
            }
        }
        if (!selected) {
            $(image).addClass('image-selected');
            container.imagesSelect.push(image);
        }
    };
    methods.choseImageSingle = function (file, callback) {
        callback([$(file).attr('src')]);
    };
    methods.choseImage = function (callback) {
        var files = container.imagesSelect.map(function (item) {
            return $(item).attr('src');
        });
        callback(files);
    };
    methods.events = function () {
        var modal = $('#modalPopupImage');
        $(root).click(function () {
            modal.modal('show');
        });
        modal.on('hidden.bs.modal', function () {
            //clean up
            document.getElementById('fileUpload').value = '';
            modal.find('.list-files .img-file').removeClass('image-selected');
            container.imagesSelect = [];

        });
        modal.on('click', '#addFolder', function (e) {
            e.preventDefault();
            showPrompt('Create folder', 'Please input folder name', function(folderName){
                if(folderName != '') methods.createFolder(folderName);
                else showNotification('Please input folder name', 0);
            });
        });
        modal.on('click', '#removeFolder', function (e) {
            e.preventDefault();
            showConfirm('Are you sure?', function(){
                methods.deleteFolder(container.foldeDelete);
            }, function(){});
        });

        modal.on('dblclick', '.folder-title', function () {
            $(this).next().toggle();
            methods.loadImage($(this).attr('data-path'));
        });
        modal.on('click', '.folder-chose', function () {
            modal.find('.folder-chose').removeClass('active-folder');
            $(this).addClass('active-folder');
            container.folderChose = $(this).attr('data-path');
            container.foldeDelete = container.folderChose;
            container.folderUpload = container.folderChose;
            container.folderLoadFile = container.folderChose;
            container.imagesSelect = [];
        });
        modal.on('dblclick', '.folders .folder .items a', function () {
            methods.loadImage($(this).attr('data-path'));
        });
        modal.on('click', '#uploadFile', function () {
            methods.uploadFile(container.folderUpload);
        });
        modal.on('click', '.list-files .img-file', function (e) {
            if (e.ctrlKey) methods.selectImageSource(this);
        });
        modal.on('dblclick', '.list-files .img-file', function () {
            var selected = false;
            for (var i = 0; i < container.imagesSelect.length; i++) {
                if ($(this).attr('src') == $(container.imagesSelect[i]).attr('src')) {
                    selected = true;
                    break;
                }
            }
            if (!selected) container.imagesSelect.push(this);
            $(this).addClass('image-selected');
            methods.choseImageSingle(this, conf.callback);
            modal.modal('hide');
        });
        modal.on('click', '#choseFile', function () {
            if (container.imagesSelect.length > 0) {
                methods.choseImage(conf.callback);
                modal.modal('hide');
            }
            else showNotification('Please choose at least 1 file', 0);
        });
        modal.on('click', '#choseAllFile', function () {
            modal.find('.list-files .img-file').each(function(){
                container.imagesSelect.push(this);
            });
            methods.choseImage(conf.callback);
            modal.modal('hide');
        });
        modal.on('click', '#deleteFile', function () {
            var paths = container.imagesSelect.map(function (item) {
                return $(item).attr('src');
            });
            if (paths.length > 0) methods.deleteFile(paths);
            else showNotification('Please choose at least 1 file', 0);
        });
    };
    methods.initData = function () {
        $.ajax({
            type: 'POST',
            url: conf.url + '/initData',
            data: {
                //pathSource: conf.pathSource
                resourceName: conf.resourceName
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var data = json.data;
                    conf.pathSource = data.pathSource;
                    container.folderRoot = data.folderRoot;
                    container.folderChose = conf.pathSource;
                    container.foldeDelete = conf.pathSource;
                    container.folderUpload = conf.pathSource;
                    container.folderLoadFile = conf.pathSource;
                    //load folders
                    var folders = JSON.parse(data.folders);
                    var html = methods.renderFolder(folders);
                    //render files
                    var files = JSON.parse(data.filesFirst);
                    var htmlFiles = methods.renderImg(files);
                    $('#modalPopupImage').find('.list-folder').html(html);
                    $('#modalPopupImage').find('.list-files').html(htmlFiles);
                }
            },
            error: function (response) {
                //showNotification('An error occurred during the execution', 0);
            }
        });
    };
    methods.init();
    return root;
};
<?php
ini_set("display_errors" , "On");
require_once __DIR__ . '/vendor/autoload.php';
define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', __DIR__ . '/.credentials/drive-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
define('SCOPES', implode(' ', array(
        //Google_Service_Drive::DRIVE_METADATA_READONLY
    Google_Service_Drive::DRIVE
    )
));
$driveapicode = "driveapicode.txt";
function getClient() {
    global $driveapicode;
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    }
     else {
        $authUrl = $client->createAuthUrl();
        $urlAuth = "<a href='{$authUrl}'>CLICK</a>";
        printf("Mở link này và làm theo hd: \n%s\n",$urlAuth );
        if( !file_exists( $driveapicode )  ){
            echo ( "chèn key nhận được  vào trong file : " . $driveapicode  . " ở host folder, làm xong thì ấn tải lại:   " . "<a href='/'>TẢI LẠI</a>" );
            file_put_contents( $driveapicode , "" );
            die;
        }

        $authCode = trim(file_get_contents($driveapicode));
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        if(!file_exists($credentialsPath)) {
            @mkdir(dirname($credentialsPath), 0777, true);
            file_put_contents($credentialsPath, json_encode($accessToken));
        }
        else{
            file_put_contents($credentialsPath, json_encode($accessToken));
        }
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

$client = getClient();
$service = new Google_Service_Drive($client);

//HUG PNA NDT AGN DKA
/*$skus = array('ADA', 'ADC', 'ADK', 'ADX', 'ADY', 'ADZ', 'ALO', 'AMZ', 'ANH', 'AOP', 'APS', 'ARE',
    'BAU', 'BDK', 'BHS', 'BKA', 'BKD', 'BVT', 'BST',
    'CAD', 'CND', 'CNS', 'CUO',
    'DAD', 'DHV', 'DOP', 'DLH', 'DPT', 'DQT', 'DUY', 'DSM', 'DVV',
    'EMT', 'EUT', 'EVT',
    'FNT', 'FTT',
    'GFI', 'GHD', 'GLP', 'GPT', 'GOV',
    'HDT', 'HHH', 'HHS', 'HHX', 'HLT', 'HMX', 'HNT', 'HPS', 'HOM', 'HPN', 'HTA', 'HTS', 'HUN',
    'KTN',
    'LCM', 'LDT', 'LNA', 'LPS',
    'MAG', 'MGT', 'MIU',
    'NDH', 'NMH', 'NNC', 'NND', 'NPK', 'NTN', 'NTT', 'NVH', 'NVS', 'NQC', 'NVQ',
    'OMZ',
    'PAD', 'PCH', 'PHA', 'PHT', 'PNI', 'PVT',
    'QDB', 'QGD', 'QHM',
    'RIM', 'REI',
    'SAO', 'SML', 'SSS', 'SVF',
    'TAA', 'TAZ', 'TBD', 'TBT', 'TBH', 'TBS', 'TCB', 'TDB', 'TDL', 'TDR', 'TEY', 'THC', 'THL', 'TLV', 'TKM', 'TIE', 'TND', 'TNH', 'TNV', 'TQH', 'TRL', 'TRO', 'TSK', 'TSS', 'TUX', 'TUY', 'TUZ', 'TTK', 'TTU',
    'VEP', 'VHV', 'VNP', 'VOK', 'VIL', 'VIT', 'VTD', 'VTH', 'VTU',
    'WBB', 'WIN',
    'XUK',
    'YOY'
);*/
$skus = array('CND', 'PNI', 'NTN', 'TUY', 'TUZ', 'LNA', 'FNT', 'WIN', 'VIL', 'VIT', 'BKD', 'DAD', 'VEP', 'WBB', 'LCM', 'ADX', 'HHH', 'TKM', 'BKA', 'MGT', 'BST', 'TBD', 'TBS', 'HMX', 'SVF', 'VOK', 'ARE', 'REI', 'QGD', 'SAO');

//$skus = array('KTN');
/*$q = "mimeType = 'application/vnd.google-apps.folder' and(";
foreach($skus as $sku) $q .= " name contains '{$sku}' or";
$q = substr($q, 0, strlen($q) - 2).')';*/

$conn = new mysqli('localhost', 'shopify', 'shopify@123456', 'shopify');
foreach($skus as $sku) {
    $optParams = array(
        'pageSize' => 1000, //kieu phan trang
        "q" => "mimeType = 'application/vnd.google-apps.folder' and name contains '{$sku}-'"
    );
    $query = $service->files->listFiles($optParams);
    $allFolders = array();
    foreach ($query->getFiles() as $folder) {
        array_push($allFolders, array(
            "name" => $folder->getName(),
            "id" => $folder->getId()
        ));
    }
    $allFiles = array();
    foreach ($allFolders as $folder) {
        if (strpos($folder['name'], $sku.'-') >= 0) {
            $folderId = $folder["id"];
            $query = $service->files->listFiles(array(
                "q" => "'{$folderId}' in parents"
            ));
            foreach ($query->getFiles() as $file) {
                if (strpos($file->getName(), $sku.'-') >= 0) {
                    $flag = false;
                    if (checkFileExist($conn, $file->getId())) {
                        insertFile($conn, $file->getId(), $file->getName());
                        $flag = true;
                    }
                    if ($flag) echo 'Insert ' . $file->getId() . ' ' . $file->getName() . PHP_EOL;
                    //else echo 'Exist ' . $file->getId() . ' ' . $file->getName() . PHP_EOL;
                }
            }
            sleep(2);
        }
    }
    sleep(2);
}
$conn->close();
function checkFileExist($conn, $fileId){
    $sql = "SELECT GoogleImageId FROM googleimages WHERE FileId = '{$fileId}'";
    $result = $conn->query($sql);
    if($result->num_rows > 0) return false;
    return true;
}

function insertFile($conn, $fileId, $fileName){
    $conn->query("INSERT INTO googleimages(FileId,FileName,IsDownload,CrDateTime) VALUES('{$fileId}', '{$fileName}', 0, NOW())");
}
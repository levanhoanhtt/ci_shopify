<?php
ini_set("display_errors" , "On");
require_once __DIR__ . '/vendor/autoload.php';
define('APPLICATION_NAME', 'Drive API PHP Quickstart');
define('CREDENTIALS_PATH', __DIR__ . '/.credentials/drive-php-quickstart.json');
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');
define('SCOPES', implode(' ', array(
        //Google_Service_Drive::DRIVE_METADATA_READONLY
    Google_Service_Drive::DRIVE
    )
));
$driveapicode = "driveapicode.txt";
function getClient() {
    global $driveapicode;
    $client = new Google_Client();
    $client->setApplicationName(APPLICATION_NAME);
    $client->setScopes(SCOPES);
    $client->setAuthConfig(CLIENT_SECRET_PATH);
    $client->setAccessType('offline');
    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    }
     else {
        $authUrl = $client->createAuthUrl();
        $urlAuth = "<a href='{$authUrl}'>CLICK</a>";
        printf("Mở link này và làm theo hd: \n%s\n",$urlAuth );
        if( !file_exists( $driveapicode )  ){
            echo ( "chèn key nhận được  vào trong file : " . $driveapicode  . " ở host folder, làm xong thì ấn tải lại:   " . "<a href='/'>TẢI LẠI</a>" );
            file_put_contents( $driveapicode , "" );
            die;
        }

        $authCode = trim(file_get_contents($driveapicode));
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
        if(!file_exists($credentialsPath)) {
            @mkdir(dirname($credentialsPath), 0777, true);
            file_put_contents($credentialsPath, json_encode($accessToken));
        }
        else{
            file_put_contents($credentialsPath, json_encode($accessToken));
        }
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}
function expandHomeDirectory($path) {
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

$client = getClient();
$service = new Google_Service_Drive($client);

/*$fileId = '1q7Spi56LwLp3M1iXW236M8J5BxsjzkUE';
$response = $service->files->get($fileId, array(
    'alt' => 'media'));
$content = $response->getBody()->getContents();
file_put_contents('/home/shopify.rickyteam.com/public_html/assets/uploads/images/hoan_test_001.png', $content);
die();*/
$imagePath = "/home/factory.teeallover.com/public_html/assets/uploads/images/";
$conn = new mysqli('localhost', 'shopify', 'shopify@123456', 'shopify');
$listImages = getImageDownload($conn);
foreach($listImages as $im){
    $fileNames = explode('-', $im['FileName']);
    if(count($fileNames) > 2){
        $fileName = $imagePath.trim($fileNames[0]).'/'.trim($fileNames[0]).'-'.trim($fileNames[1]).'/';
        @mkdir($fileName, 0777, true);
        try {
            $response = $service->files->get($im['FileId'], array('alt' => 'media'));
            $content = $response->getBody()->getContents();
            $flag = file_put_contents($fileName . $im['FileName'], $content);
            if ($flag !== false) {
                updateDownloaded($conn, $im['GoogleImageId']);
                echo 'Download ' . $fileName . $im['FileName'] . PHP_EOL;
            }
        }
        catch(Exception $e){
            continue;
        }
    }
}
$conn->close();

function getImageDownload($conn){
    $retVal = array();
    $sql = "SELECT * FROM googleimages WHERE IsDownload = 0";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()) {
            $retVal[] = array(
                'GoogleImageId' => $row['GoogleImageId'],
                'FileId' => $row['FileId'],
                'FileName' => $row['FileName']
            );
        }
    }
    return $retVal;
}

function updateDownloaded($conn, $googleImageId){
    $conn->query("UPDATE googleimages SET IsDownload = 1 WHERE GoogleImageId = {$googleImageId}");
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfactoryorders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "factoryorders";
        $this->_primary_key = "FactoryOrderId";
    }

    public function getListOrderIds($factoryRequestId){
        $retVal = array();
        $ids = $this->getBy(array('FactoryRequestId' => $factoryRequestId), false, '', 'OrderShopifyId');
        foreach($ids as $id) $retVal[] = $id['OrderShopifyId'];
        return $retVal;
    }
}
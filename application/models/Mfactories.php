<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfactories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "factories";
        $this->_primary_key = "FactoryId";
    }
}
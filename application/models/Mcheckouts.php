<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcheckouts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "checkouts";
        $this->_primary_key = "CheckoutId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM checkouts WHERE 1=1" . $this->buildQuery($postData).' ORDER BY CreatedDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ShopId']) && $postData['ShopId'] > 0) $query.=" AND ShopId=".$postData['ShopId'];
        if(isset($postData['TeamId']) && $postData['TeamId'] > 0) $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE TeamId={$postData['TeamId']})";
        if(isset($postData['CustomerName']) && !empty($postData['CustomerName'])) $query .= " AND CustomerName LIKE '%{$postData['CustomerName']}%'";
        if(isset($postData['CustomerEmail']) && !empty($postData['CustomerEmail'])) $query .= " AND CustomerEmail LIKE '%{$postData['CustomerEmail']}%'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CreatedDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CreatedDateTime <= '{$postData['EndDate']}'";
        if(isset($postData['ShopIds']) && !empty($postData['ShopIds'])){
            $in = implode(',', $postData['ShopIds']);
            $query.=" AND ShopId IN({$in})";
        }
        if(isset($postData['TeamIds']) && !empty($postData['TeamIds'])){
            $in = implode(',', $postData['TeamIds']);
            $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE TeamId IN({$in}))";
        }
        return $query;
    }

    public function getByIds($ids){
        return $this->getByQuery('SELECT * FROM checkouts WHERE CheckoutId IN ? ORDER BY CreatedDateTime DESC', array($ids));
    }
}
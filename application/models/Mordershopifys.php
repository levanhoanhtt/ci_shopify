<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mordershopifys extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ordershopifys";
        $this->_primary_key = "OrderShopifyId";
    }

    public function getCount($postData){
        $query = "OrderStatusId > 0 AND HasChild = 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*'){
        $query = "SELECT {$select} FROM ordershopifys WHERE OrderStatusId > 0 AND HasChild = 0" . $this->buildQuery($postData).' ORDER BY OrderDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $flagStatus = isset($postData['OrderStatusId']) && $postData['OrderStatusId'] > 0;
        $query = '';
        if(isset($postData['TransportStatusId']) && $postData['TransportStatusId'] > 0) $query.=" AND TransportStatusId=".$postData['TransportStatusId'];
        if($flagStatus) $query.=" AND OrderStatusId=".$postData['OrderStatusId'];
        if(isset($postData['OrderExtraStatusId']) && $postData['OrderExtraStatusId'] > 0) $query.=" AND OrderExtraStatusId=".$postData['OrderExtraStatusId'];
        if(isset($postData['ShopId']) && $postData['ShopId'] > 0) $query.=" AND ShopId=".$postData['ShopId'];
        if(isset($postData['ShopCode']) && !empty($postData['ShopCode'])) $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE ShopCode='{$postData['ShopCode']}')";
        if(isset($postData['TeamId']) && $postData['TeamId'] > 0) $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE TeamId={$postData['TeamId']})";
        if(isset($postData['OrderName']) && !empty($postData['OrderName'])) $query .= " AND OrderName LIKE '%{$postData['OrderName']}%'";
        if(isset($postData['CustomerName']) && !empty($postData['CustomerName'])) $query .= " AND CustomerName LIKE '%{$postData['CustomerName']}%'";
        if(isset($postData['CustomerEmail']) && !empty($postData['CustomerEmail'])) $query .= " AND CustomerEmail LIKE '%{$postData['CustomerEmail']}%'";
        if(isset($postData['TrackingCode']) && !empty($postData['TrackingCode'])) $query .= " AND TrackingCode='{$postData['TrackingCode']}'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND OrderDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND OrderDateTime <= '{$postData['EndDate']}'";
        if(isset($postData['ShopIds']) && !empty($postData['ShopIds'])){
            $in = implode(',', $postData['ShopIds']);
            $query.=" AND ShopId IN({$in})";
        }
        if(isset($postData['TeamIds']) && !empty($postData['TeamIds'])){
            $in = implode(',', $postData['TeamIds']);
            $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE TeamId IN({$in}))";
        }
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0){
            if($flagStatus && $postData['OrderStatusId'] == 2) $query.=" AND OrderShopifyId IN(SELECT OrderShopifyId FROM factoryorders WHERE FactoryId={$postData['FactoryId']})";
            else $query.=" AND FactoryId=".$postData['FactoryId'];
        }
        elseif(isset($postData['FactoryIds']) && !empty($postData['FactoryIds'])){
            $in = implode(',', $postData['FactoryIds']);
            if($flagStatus && $postData['OrderStatusId'] == 2) $query.=" AND OrderShopifyId IN(SELECT OrderShopifyId FROM factoryorders WHERE FactoryId IN({$in}))";
            else $query.=" AND FactoryId IN({$in})";
        }
        return $query;
    }

    public function getByIds($ids){
        return $this->getByQuery('SELECT * FROM ordershopifys WHERE OrderShopifyId IN ? ORDER BY OrderName DESC', array($ids));
    }

    public function getListSendTracking(){
        return $this->getByQuery("SELECT OrderShopifyId, ItemIds, TrackingCode, ShopId, OrderCode, OrderName FROM ordershopifys WHERE ParentOrderId = 0 AND IsSendTracking = 1 AND ItemIds != '' AND TrackingCode != '' AND OrderDateTime >= '2017-12-18'");
    }

    public function getListByRequest($factoryRequestId){
        $query = 'SELECT os.OrderShopifyId, os.OrderDateTime, os.OrderName, os.CountryName, shops.ShopName, shops.TeamId FROM ordershopifys os
                    INNER JOIN shops ON os.ShopId = shops.ShopId
                    INNER JOIN factoryorders fo ON fo.OrderShopifyId = os.OrderShopifyId WHERE fo.FactoryRequestId = ?';
        return $this->getByQuery($query, array($factoryRequestId));
    }

    public function getListOrderByRequest($factoryRequestId){
        $query = 'SELECT * FROM ordershopifys WHERE OrderShopifyId IN (SELECT OrderShopifyId FROM factoryorders WHERE FactoryRequestId = ?) ORDER BY OrderName DESC';
        return $this->getByQuery($query, array($factoryRequestId));
    }

    public function update($postData = array(), $orderShopifyId = 0, $products = array(), $actionLogs = array()){
        $this->load->model('Mactionlogs');
        $this->db->trans_begin();
        $orderShopifyId = $this->save($postData, $orderShopifyId);
        if($orderShopifyId > 0) {
            if(!empty($products)){
                $updateProducts = array();
                $insertProducts = array();
                foreach($products as $p){
                    if($p['OrderProductId'] > 0) $updateProducts[] = $p;
                    else{
                        unset($p['OrderProductId']);
                        $p['OrderShopifyId'] = $orderShopifyId;
                        $p['FactoryQuantity'] = 0;
                        $p['WarehouseQuantity'] = 0;
                        $insertProducts[] = $p;
                    }
                }
                print_r($insertProducts);
                if(!empty($updateProducts)) $this->db->update_batch('orderproducts', $updateProducts, 'OrderProductId');
                if(!empty($insertProducts)) $this->db->insert_batch('orderproducts', $insertProducts);
            }
            if(!empty($actionLogs)) $this->Mactionlogs->save($actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $orderShopifyId;
        }
    }

    public function changeStatusBatch($orderShopifyIds, $statusId, $user){
        $statusName = $this->Mconstants->orderStatus[$statusId];
        $crDateTime = getCurentDateTime();
        $actionLogs = array();
        $this->db->trans_begin();
        $this->db->query('UPDATE ordershopifys SET OrderStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE OrderShopifyId IN ?', array($statusId, $user['UserId'], $crDateTime, $orderShopifyIds));
        foreach($orderShopifyIds as $orderShopifyId){
            $actionLogs[] = array(
                'ItemId' => $orderShopifyId,
                'ItemTypeId' => 1,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateOrderBatch($orderUpdates, $actionLogs){
        $this->db->trans_begin();
        if(!empty($orderUpdates)) $this->db->update_batch('ordershopifys', $orderUpdates, 'OrderShopifyId');
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function changeTransportStatusBatch($orderShopifyIds, $statusId){
        $this->db->query('UPDATE ordershopifys SET TransportStatusId = ? WHERE OrderShopifyId IN ?', array($statusId, $orderShopifyIds));
        return true;
    }

    public function updateTracking($trackingCode, $orderName){
        $this->db->query('UPDATE ordershopifys SET TrackingCode = ? WHERE OrderName = ?', array($trackingCode, $orderName));
        return true;
    }

    public function updateStatus($orderStatusId, $orderName, $trackingCode = ''){
        $postData = array('OrderStatusId' => $orderStatusId);
        if(!empty($trackingCode)) $postData['TrackingCode'] = $trackingCode;
        $this->db->update('ordershopifys', $postData, array('OrderName' => $orderName));
        return true;
    }

    public function searchReport($postData, $perPage = 0, $page = 1){
        $query = "SELECT os.*,op.ProductName,op.ProductType,SUM(op.Quantity) AS Quantity
                    FROM ordershopifys os INNER JOIN orderproducts op ON os.OrderShopifyId = op.OrderShopifyId
                    WHERE os.OrderStatusId > 0 AND os.OrderStatusId !=3 AND os.HasChild = 0" . $this->buildQueryReport($postData).
                    ' GROUP BY op.ProductType ORDER BY os.OrderDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQueryReport($postData){
        $query = '';
        if(isset($postData['FromDate']) && $postData['FromDate'] > 0) $query.=" AND DATE_FORMAT(os.OrderDateTime,'%d/%c/%Y') >= '".$postData['FromDate']."'";
        if(isset($postData['ToDate']) && $postData['ToDate'] > 0) $query.=" AND DATE_FORMAT(os.OrderDateTime,'%d/%c/%Y') <='".$postData['ToDate']."'";
        if(isset($postData['ShopId']) && $postData['ShopId'] > 0) $query.=" AND ShopId=".$postData['ShopId'];
        if(isset($postData['TeamId']) && $postData['TeamId'] > 0) $query.=" AND ShopId IN(SELECT ShopId FROM shops WHERE TeamId={$postData['TeamId']})";
        return $query;
    }

    public function getListPrepareFactoryLate(){
        $sql = 'SELECT o.OrderShopifyId, OrderStatusId, FactoryDate, fo.FactoryId, DATE(fr.RequestDate) AS RequestDate, DATEDIFF(FactoryDate, RequestDate) AS DistanceDay, f.FactoryDay
                FROM ordershopifys o INNER JOIN factoryorders fo ON o.OrderShopifyId = fo.OrderShopifyId
                INNER JOIN factoryrequests fr ON fr.FactoryRequestId = fo.FactoryRequestId
                INNER JOIN factories f ON fr.FactoryId = f.FactoryId
                WHERE FactoryDate IS NOT NULL AND (OrderStatusId = 2 OR OrderStatusId = 7 OR (OrderStatusId = 9 AND OrderExtraStatusId != 5))
                HAVING DistanceDay > FactoryDay';
        return $this->getByQuery($sql);
    }

    public function getListPrepareWarehouseLate(){
        $sql = 'SELECT o.OrderShopifyId, OrderStatusId, DATEDIFF(WarehouseDate, FactoryDate) AS DistanceDay, f.WarehouseDay
                FROM ordershopifys o INNER JOIN factoryorders fo ON o.OrderShopifyId = fo.OrderShopifyId
                INNER JOIN factoryrequests fr ON fr.FactoryRequestId = fo.FactoryRequestId
                INNER JOIN factories f ON fr.FactoryId = f.FactoryId
                WHERE WarehouseDate IS NOT NULL AND (OrderStatusId = 8 OR OrderStatusId = 9 OR (OrderStatusId = 10 AND OrderExtraStatusId != 6))
                HAVING DistanceDay > WarehouseDay';
        return $this->getByQuery($sql);
    }

    public function getListEnoughWarehouse(){
        $retVal = array();
        $sql = 'SELECT OrderShopifyId FROM ordershopifys WHERE WarehouseDate IS NOT NULL AND OrderStatusId NOT IN(0, 10, 12) ';
        $os = $this->getByQuery($sql);
        foreach($os as $o) $retVal[] = $o['OrderShopifyId'];
        return $retVal;
    }

    /*public function statisticCounts($postData, $orderStatusStatistics = array(), $isStatisticAll = false){
        $retVal = array();
        $retVal[0] = 0;//total
        $orderStatus = $this->Mconstants->orderStatus;
        foreach($orderStatus as $i => $v) $retVal[$i] = 0;
        if(!empty($orderStatusStatistics)) $orderStatusStatistics = array_keys($orderStatusStatistics);
        $flag = false;
        $query = "SELECT ";
        foreach($orderStatus as $i => $v){
            if(in_array($i, $orderStatusStatistics)){
                $flag = true;
                $query .= "COUNT(CASE WHEN OrderStatusId = {$i} THEN 1 END) AS Count{$i},";
            }
        }
        if($isStatisticAll){
            $flag = true;
            $query .= "COUNT(CASE WHEN OrderStatusId > 0 THEN 1 END) AS Total FROM ordershopifys WHERE 1=1";
        }
        else{
            $query = substr($query, 0, strlen($query) - 1);
            $query .= " FROM ordershopifys WHERE 1=1";
        }
        $query.=$this->buildQuery($postData);
        if($flag){
            $counts = $this->getByQuery($query);
            if (!empty($counts)) {
                $counts = $counts[0];
                foreach ($counts as $label => $value) {
                    if ($label == 'Total') $retVal[0] = $value;
                    foreach ($orderStatus as $i => $v) {
                        if ($label == 'Count' . $i) $retVal[$i] = $value;
                    }
                }
            }
        }
        return $retVal;
    }*/
}
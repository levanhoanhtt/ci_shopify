<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //USER
    public $roles = array(
        5 => 'Leader',
        1 => 'Admin',
        4 => 'Staff',
        2 => 'Warehouse',
        3 => 'Factory',

        6 => 'Order Staff',
        7 => 'CS Staff',
        8 => 'Sales staff'
    );

    public $status = array(
        2 => 'Approved',// 'Đã duyệt',
        1 => 'Pending',// 'Chưa duyệt',
        3 => 'Not approved', //'Không được duyệt'
    );

    public $orderStatus = array(
        1 => 'Not Send',
        4 => 'Waiting',
        5 => 'Purchase Order',
        6 => 'Paid Order',
        2 => 'Sent Factory',
        7 => 'Manufacturing',
        8 => 'Factory Delivery Late', // 'Late Order',
        9 => 'Factory Delivery',
        10 => 'Warehouse Received',
        11 => 'Warehouse Received Late',
        12 => 'Shipped',
        3 => 'Canceled'
    );

    public $orderExtraStatus = array(
        1 => 'Factory Delivery Enough', //9
        2 => 'Factory Delivery Lack', //9
        3 => 'Warehouse Received Enough', //10
        4 => 'Warehouse Received Lack', //10,

        5 => 'Factory Delivery Late', //9
        6 => 'Warehouse Received Late' // 10
    );

    public $transportStatus = array(
        1 => 'None',
        2 => 'Waiting for packing',// 'Chờ đóng',
        3 => 'Packed',// 'Đã đóng',
        4 => 'Being delivered',// 'Đang giao', //có trackingcode
    );

    public $requestStatus = array(
        1 => 'Wait manufacturing', //''Chờ sản xuất',
        2 => 'Manufacturing',//'Sản xuất'
    );

    public $invoiceStatus = array(
        3 => 'Waiting',
        1 => 'Not Received',
        2 => 'Received'
    );

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        6 => 'label label-success',
        7 => 'label label-warning',
        8 => 'label label-danger',
        9 => 'label label-default',
        10 => 'label label-success',
        11 => 'label label-warning',
        12 => 'label label-danger'
    );

    public $scanErrors = array(
        1 => 'Error',
        2 => 'OK'
    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả', $attrSelect = ''){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue) return $obj[$objKeyReturn];
        }
        return '';
    }
}
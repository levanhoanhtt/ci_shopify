<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwarehouseproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "warehouseproducts";
        $this->_primary_key = "WarehouseProductId";
    }
}
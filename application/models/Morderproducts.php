<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderproducts";
        $this->_primary_key = "OrderProductId";
    }

    public function deleteId($orderProductId, $actionLogs){
        $this->db->trans_begin();
        $flag = $this->delete($orderProductId);
        if($flag) $this->Mactionlogs->save($actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getScanByBarCodes($barCodes){
        return $this->getByQuery('SELECT * FROM orderproducts WHERE BarCode IN ? AND (FactoryQuantity < Quantity OR WarehouseQuantity < Quantity)', array($barCodes));
        //return $this->getByQuery('SELECT * FROM orderproducts WHERE BarCode IN ?', array($barCodes));
    }

    public function getByOrderStatusId($orderStatusIds){
        if(is_array($orderStatusIds)) $query = 'SELECT * FROM orderproducts WHERE OrderShopifyId IN(SELECT OrderShopifyId FROM ordershopifys WHERE OrderStatusId IN ?)';
        else $query = 'SELECT * FROM orderproducts WHERE OrderShopifyId IN(SELECT OrderShopifyId FROM ordershopifys WHERE OrderStatusId = ?)';
        return $this->getByQuery($query, array($orderStatusIds));
    }

    public function getByFactoryRequestId($factoryRequestId){
        return $this->getByQuery('SELECT * FROM orderproducts WHERE OrderShopifyId IN(SELECT OrderShopifyId FROM factoryorders WHERE FactoryRequestId = ?)', array($factoryRequestId));
    }
}
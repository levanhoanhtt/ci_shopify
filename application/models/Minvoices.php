<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minvoices extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "invoices";
        $this->_primary_key = "InvoiceId";
    }

    public function getCount($postData){
        $query = "InvoiceStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM invoices WHERE InvoiceStatusId > 0" . $this->buildQuery($postData).' ORDER BY InvoiceDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['InvoiceStatusId']) && $postData['InvoiceStatusId'] > 0) $query.=" AND InvoiceStatusId=".$postData['InvoiceStatusId'];
        if(isset($postData['TeamId']) && $postData['TeamId'] > 0) $query.=" AND TeamId=".$postData['TeamId'];
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND FactoryId=".$postData['FactoryId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND InvoiceDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND InvoiceDate <= '{$postData['EndDate']}'";
        return $query;
    }

    public function getSumProductByType($invoiceId){
        $query = "SELECT op.ProductType, SUM(op.Quantity) AS sumQuantity FROM invoiceorders io INNER JOIN orderproducts op ON op.OrderShopifyId = io.OrderShopifyId WHERE io.InvoiceId = ? GROUP BY op.ProductType ORDER BY op.ProductType";
        return $this->getByQuery($query, array($invoiceId));
    }

    public function getTotalPriceByType($orderIds, $productTypePrices){
        $retVal = 0;
        $query = "SELECT op.ProductType, SUM(op.Quantity) AS sumQuantity FROM orderproducts op WHERE op.OrderShopifyId IN ? GROUP BY op.ProductType ORDER BY op.ProductType";
        $pqs = $this->getByQuery($query, array($orderIds));
        foreach($pqs as $p){
            $type = strtoupper($p['ProductType']);
            if(isset($productTypePrices[$type])) $price = $productTypePrices[$type];
            else $price = 0;
            $retVal += $price * $p['sumQuantity'];
        }
        return $retVal;
    }

    public function insert($postData, $listOrderIds, $user){
        $statusName = $this->Mconstants->orderStatus[5];
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $invoiceId = $this->save($postData);
        if($invoiceId > 0){
            $invoiceOrders = array();
            $orderUpdateData = array();
            $actionLogs = array();
            foreach($listOrderIds as $orderId) {
                $invoiceOrders[] = array(
                    'InvoiceId' => $invoiceId,
                    'OrderShopifyId' => $orderId
                );
                $orderUpdateData[] = array(
                    'OrderShopifyId' => $orderId,
                    'OrderStatusId' => 5,
                    'UpdateUserId' => $user['UserId'],
                    'UpdateDateTime' => $crDateTime
                );
                $actionLogs[] = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 1,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            if(!empty($invoiceOrders)) $this->db->insert_batch('invoiceorders', $invoiceOrders);
            if(!empty($orderUpdateData)) $this->db->update_batch('ordershopifys', $orderUpdateData, 'OrderShopifyId');
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $invoiceId;
        }
    }

    public function update($postData, $invoiceId, $actionLogs = array()){
        $this->load->model('Mactionlogs');
        $this->db->trans_begin();
        $invoiceId = $this->save($postData, $invoiceId);
        if($invoiceId > 0){
            if(!empty($actionLogs)) $this->Mactionlogs->save($actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateNotReceived($invoiceMails, $invoiceUpdates){
        $this->db->trans_begin();
        if(!empty($invoiceMails)) $this->db->insert_batch('invoicemails', $invoiceMails);
        if(!empty($invoiceUpdates)) $this->db->update_batch('invoices', $invoiceUpdates, 'InvoiceId');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function received($invoiceId, $orderIds, $user){
        $statusName = $this->Mconstants->orderStatus[6];
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $flag = $this->changeStatus(2, $invoiceId, 'InvoiceStatusId', $user['UserId']);
        if($flag){
            $this->db->query('UPDATE ordershopifys SET OrderStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE OrderShopifyId IN ?', array(6, $user['UserId'], $crDateTime, $orderIds));
            $actionLogs = array();
            foreach($orderIds as $orderId) {
                $actionLogs[] = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 1,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function deleteBatch($invoiceIds, $orderIds, $user){
        $statusName = $this->Mconstants->orderStatus[1];
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $this->db->query('UPDATE invoices SET InvoiceStatusId = 0, UpdateUserId = ?, UpdateDateTime = ? WHERE InvoiceId IN ?', array($user['UserId'], $crDateTime, $invoiceIds));
        $this->db->query('UPDATE ordershopifys SET OrderStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE OrderShopifyId IN ?', array(1, $user['UserId'], $crDateTime, $orderIds));
        $actionLogs = array();
        foreach($invoiceIds as $invoiceId){
            $actionLogs[] = array(
                'ItemId' => $invoiceId,
                'ItemTypeId' => 2,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' delete invoice ',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        foreach($orderIds as $orderId) {
            $actionLogs[] = array(
                'ItemId' => $orderId,
                'ItemTypeId' => 1,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mteams extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "teams";
        $this->_primary_key = "TeamId";
    }

    public function getListByIds($teamIds){
        return $this->getByQuery('SELECT * FROM teams WHERE TeamId IN ?', array($teamIds));
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muserteams extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "userteams";
        $this->_primary_key = "UserTeamId";
    }

    public function getTeamIds($userId){
        $retVal = array();
        $uts = $this->getByQuery('SELECT TeamId FROM userteams WHERE UserId = ?', array($userId));
        foreach($uts as $ut) $retVal[] = $ut['TeamId'];
        return $retVal;
    }
}
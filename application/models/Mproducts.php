<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "products";
        $this->_primary_key = "ProductId";
    }

    public function getCount($postData){
        $query = "StatusId=2" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM products WHERE StatusId=2" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['Sku']) && !empty($postData['Sku'])) $query.=" AND Sku LIKE '%{$postData['Sku']}%'";
        return $query;
    }

    public function checkExist($productId, $sku){
        $p = $this->getByQuery('SELECT ProductId FROM products WHERE ProductId != ? AND Sku = ? AND StatusId=2', array($productId, $sku));
        if(!empty($p)) return true;
        return false;
    }
}

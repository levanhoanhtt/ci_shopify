<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwarehousescans extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "warehousescans";
        $this->_primary_key = "WarehouseScanId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM warehousescans WHERE 1=1" . $this->buildQuery($postData).' ORDER BY ScanDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND FactoryId=".$postData['FactoryId'];
        if(isset($postData['ScanDate']) && !empty($postData['ScanDate'])) $query.=" AND DATE(ScanDate)='{$postData['ScanDate']}'";
        if(isset($postData['NoTrackingCode']) && $postData['NoTrackingCode'] == 1) $query.=" AND InternalTrackingId=0";
        if(isset($postData['TrackingCode']) && !empty($postData['TrackingCode'])) $query.=" AND InternalTrackingId=(SELECT InternalTrackingId FROM internaltrackings WHERE TrackingCode='{$postData['TrackingCode']}')";
        return $query;
    }

    /*public function getListImages($warehouseScanId, $isError = 0){
        $query = "SELECT * FROM scanimages WHERE WarehouseScanId = ".$warehouseScanId;
        if($isError > 0) $query.=" AND IsError=".$isError;
        return $this->getByQuery($query);
    }*/

    public function getListCompare($factoryId, $beginDate, $endDate){
        $query = 'SELECT op.LastDate, op.Sku, op.OrderName, op.FactoryId, op.CurrentQuantity, op.OwnQuantity, op.IsCheck, wc.Quantity AS WarehouseQuantity, fc.Quantity  AS FactoryQuantity
                    FROM ownproducts op LEFT JOIN warehousescans wc ON wc.Sku = op.Sku
                    LEFT JOIN factoryscans fc ON fc.Sku = op.Sku
                    WHERE op.FactoryId = ? AND (op.LastDate BETWEEN ? AND ?)
                    GROUP BY op.LastDate, op.Sku, op.OrderName, op.FactoryId, op.CurrentQuantity, op.OwnQuantity, op.IsCheck
                    ORDER BY op.LastDate DESC';
        return $this->getByQuery($query, array($factoryId, $beginDate, $endDate));
    }

    /*public function getListOwn($factoryId){
        return $this->getByQuery('SELECT * FROM ownproducts WHERE IsCheck = 0 AND OwnQuantity > 0'.($factoryId > 0 ? " AND FactoryId={$factoryId}" : '').' ORDER BY LastDate DESC');
    }*/

    public function insert($postData, $warehouseScanId, $scanImage, $orderProductId){
        $this->db->trans_begin();
        if($warehouseScanId > 0){
            if($scanImage['IsError'] == 2) $warehouseScanId = $this->save($postData, $warehouseScanId);
        }
        else $warehouseScanId = $this->save($postData, $warehouseScanId);
        if($warehouseScanId > 0) {
            $scanImage['WarehouseScanId'] = $warehouseScanId;
            $this->Mscanimages->save($scanImage);
            $this->db->query('UPDATE orderproducts SET WarehouseQuantity = WarehouseQuantity + 1 WHERE OrderProductId = ?', array($orderProductId));
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $warehouseScanId;
        }
    }

    public function updateTrackingCode($internalTrackingId, $warehouseScanIds, $userId){
        $this->db->trans_begin();
        $this->Minternaltrackings->save(array('UseTime' => 2, 'UpdateUserId' => $userId, 'UpdateDateTime' => getCurentDateTime()), $internalTrackingId);
        $updateData = array();
        foreach($warehouseScanIds as $warehouseScanId){
            $updateData[] = array(
                'WarehouseScanId' => $warehouseScanId,
                'InternalTrackingId' => $internalTrackingId
            );
        }
        if(!empty($updateData)) $this->db->update_batch('warehousescans', $updateData, 'WarehouseScanId');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mactionlogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "actionlogs";
        $this->_primary_key = "ActionLogId";
    }

    public function getList($itemId, $itemTypeId){
        return $this->getBy(array('ItemId' => $itemId, 'ItemTypeId' => $itemTypeId), false, 'CrDateTime');
    }

    public function getCount($itemTypeId){
        $query = $this->db->query('SELECT 1 FROM actionlogs WHERE ActionLogId IN (SELECT MAX(ActionLogId) FROM actionlogs GROUP BY ItemId) AND ItemTypeId = ?', array($itemTypeId));
        return $query->num_rows();
    }

    public function search($itemTypeId, $perPage = 0, $page = 1){
        $query = 'SELECT * FROM actionlogs WHERE ActionLogId IN (SELECT MAX(ActionLogId) FROM actionlogs GROUP BY ItemId) AND ItemTypeId = ? ORDER BY CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query, array($itemTypeId));
    }
}

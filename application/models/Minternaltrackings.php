<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minternaltrackings extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "internaltrackings";
        $this->_primary_key = "InternalTrackingId";
    }

    public function getFirstTrackingId(){
        $its = $this->getByQuery('SELECT InternalTrackingId FROM internaltrackings WHERE UseTime = 2 ORDER BY UpdateDateTime ASC LIMIT 1');
        if(!empty($its)) return $its[0]['InternalTrackingId'];
        return 0;
    }
}
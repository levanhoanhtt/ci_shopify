<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minvoiceorders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "invoiceorders";
        $this->_primary_key = "InvoiceOrderId";
    }

    public function getListOrderIds($invoiceIds){
        $retVal = array();
        $ids = $this->getByQuery('SELECT OrderShopifyId FROM invoiceorders WHERE InvoiceId IN ?', array($invoiceIds));
        foreach($ids as $id) $retVal[] = $id['OrderShopifyId'];
        return $retVal;
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpenaltyrate extends MY_Model{

    function __construct() {
        parent::__construct();
        $this->_table_name = "penaltyrates";
        $this->_primary_key = "PenaltyRateId";
    }

    public function getAll(){
        $query = "SELECT penaltyrates.*, factories.FactoryName 
                  AS FactoryName FROM penaltyrates INNER JOIN factories 
                  ON penaltyrates.FactoryID = factories.FactoryId 
                  WHERE penaltyrates.StatusId = ?";
        return $this->getByQuery($query, array(STATUS_ACTIVED));
    }

    public function checkExist($factoryId, $productType){
        $query = "SELECT PenaltyRateId, StatusId FROM penaltyrates WHERE FactoryId = ? AND ProductType = ? LIMIT 1";
        return $this->getByQuery($query, array($factoryId, $productType));
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "producttypes";
        $this->_primary_key = "ProductTypeId";
    }

    public function getListMap(){
        $retVal = array(
            'Factory' => array(),
            'ProductType' => array()
        );
        $pts = $this->getBy(array('StatusId' => STATUS_ACTIVED), false, '', 'ProductTypeName, FactoryId');
        foreach($pts as $pt){
            $retVal['Factory'][$pt['FactoryId']][] = $pt['ProductTypeName'];
            if(!in_array(strtoupper($pt['ProductTypeName']), $retVal['ProductType'])) $retVal['ProductType'][] = strtoupper($pt['ProductTypeName']);
        }
        return $retVal;
    }

    public function getListPriceMap(){
        $retVal = array();
        $pts = $this->getBy(array('StatusId' => STATUS_ACTIVED), false, '', 'ProductTypeName, Price');
        foreach($pts as $pt) $retVal[strtoupper($pt['ProductTypeName'])] = $pt['Price'];
        return $retVal;
    }
}

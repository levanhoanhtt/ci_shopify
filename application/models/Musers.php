<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Musers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "users";
        $this->_primary_key = "UserId";
    }

    public function login($userName, $userPass){
        if(!empty($userName) && !empty($userPass)){
            $query = "SELECT * FROM users WHERE UserPass=? AND StatusId=? AND UserName=? LIMIT 1";
            $users = $this->getByQuery($query, array(md5($userPass), STATUS_ACTIVED, $userName));
            if (!empty($users)) return $users[0];
        }
        return false;
    }

    public function checkExist($userId, $userName, $email){
        $query = "SELECT UserId FROM users WHERE UserId!=? AND StatusId=? AND (UserName=? OR Email=?) LIMIT 1";
        $users = $this->getByQuery($query, array($userId, STATUS_ACTIVED, $userName, $email));
        if (!empty($users)) return true;
        return false;
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM users WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['UserName']) && !empty($postData['UserName'])) $query.=" AND UserName LIKE '%{$postData['UserName']}%'";
        if(isset($postData['FullName']) && !empty($postData['FullName'])) $query.=" AND FullName LIKE '%{$postData['FullName']}%'";
        if(isset($postData['Email']) && !empty($postData['Email'])) $query.=" AND Email LIKE '%{$postData['Email']}%'";
        //if(isset($postData['PhoneNumber']) && !empty($postData['PhoneNumber'])) $query.=" AND PhoneNumber LIKE '%{$postData['PhoneNumber']}%'";
        if(isset($postData['GenderId']) && $postData['GenderId'] > 0) $query.=" AND GenderId=".$postData['GenderId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['RoleId']) && $postData['RoleId'] > 0) $query.=" AND RoleId=".$postData['RoleId'];
        return $query;
    }

    public function getList() {
        return $this->getby('StatusId > 0', false, '', '', 0, 0, 'asc');
    }

    public function update($postData, $userId = 0, $teamIds = array()){
        $isUpdate = $userId > 0;
        $this->db->trans_begin();
        $userId = $this->save($postData, $userId);
        if($userId > 0) {
            if($isUpdate) $this->db->delete('userteams', array('UserId' => $userId));
            $userTeams = array();
            foreach($teamIds as $teamId){
                $userTeams[] = array(
                    'UserId' => $userId,
                    'TeamId' => $teamId
                );
            }
            if(!empty($userTeams)) $this->db->insert_batch('userteams', $userTeams);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $userId;
        }
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mownproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ownproducts";
        $this->_primary_key = "OwnProductId";
    }

    public function getListCheck(){
        return $this->getByQuery('SELECT * FROM ownproducts WHERE OwnQuantity > 0 AND IsCheck = 0');
    }

    public function insert($updateOldOwnProducts, $newOwnProducts, $internalTrackingId){
        $this->db->trans_begin();
        if(!empty($updateOldOwnProducts)) $this->db->update_batch('ownproducts', $updateOldOwnProducts, 'OwnProductId');
        if(!empty($newOwnProducts)) $this->db->insert_batch('ownproducts', $newOwnProducts);
        $this->Minternaltrackings->save(array('UseTime' => 3), $internalTrackingId);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getCount($postData){
        $query = "IsCheck = 0 AND OwnQuantity > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM ownproducts WHERE IsCheck = 0 AND OwnQuantity > 0" . $this->buildQuery($postData).' ORDER BY LastDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND FactoryId=".$postData['FactoryId'];
        return $query;
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfactoryrequests extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "factoryrequests";
        $this->_primary_key = "FactoryRequestId";
    }

    public function getCount($postData){
        $query = "RequestStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $build  = $this->buildQuery($postData);
        /*$query = "SELECT factoryrequests.*,SUM(orderproducts.Quantity) as sumQuantity FROM factoryrequests
                   LEFT JOIN factoryorders ON factoryrequests.FactoryRequestId = factoryorders.FactoryRequestId
                    LEFT join orderproducts on orderproducts.OrderShopifyId = factoryorders.OrderShopifyId WHERE factoryrequests.RequestStatusId > 0 $build
                     group by factoryrequests.FactoryRequestId ORDER BY factoryrequests.RequestDate DESC";*/
        $query = 'SELECT * FROM factoryrequests WHERE RequestStatusId > 0'.$build.' ORDER BY factoryrequests.RequestDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND factoryrequests.FactoryId=".$postData['FactoryId'];
        if(isset($postData['RequestStatusId']) && $postData['RequestStatusId'] > 0) $query.=" AND factoryrequests.RequestStatusId=".$postData['RequestStatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND RequestDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND RequestDate <= '{$postData['EndDate']}'";
        return $query;
    }

    public function getStatusByOrder($orderShopifyId){
        $frs = $this->getByQuery('SELECT factoryrequests.RequestStatusId FROM factoryrequests INNER JOIN factoryorders ON factoryrequests.FactoryRequestId = factoryorders.FactoryRequestId WHERE factoryorders.OrderShopifyId = ?', array($orderShopifyId));
        if(!empty($frs)) return $frs[0]['RequestStatusId'];
        return 0;
    }

    public function insert($postData, $listOrderIds, $factoryRequestId = 0, $user = array()){
        $statusName = $this->Mconstants->orderStatus[2];
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        if($factoryRequestId == 0) $factoryRequestId = $this->save($postData);
        if($factoryRequestId > 0){
            $factoryOrders = array();
            $orderUpdateData = array();
            $actionLogs = array();
            foreach($listOrderIds as $orderId) {
                $factoryOrders[] = array(
                    'FactoryRequestId' =>$factoryRequestId,
                    'OrderShopifyId' => $orderId,
                    'FactoryId' => $postData['FactoryId']
                );
                $orderUpdateData[] = array(
                    'OrderShopifyId' => $orderId,
                    'OrderStatusId' => 2,
                    'UpdateUserId' => $user['UserId'],
                    'UpdateDateTime' => $crDateTime
                );
                $actionLogs[] = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 1,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            if(!empty($factoryOrders)) $this->db->insert_batch('factoryorders', $factoryOrders);
            if(!empty($orderUpdateData)) $this->db->update_batch('ordershopifys', $orderUpdateData, 'OrderShopifyId');
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return 0;
        }
        else {
            $this->db->trans_commit();
            return $factoryRequestId;
        }
    }

    public function manufacturing($factoryRequestId, $orderIds, $user){
        $statusName = $this->Mconstants->orderStatus[7];
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $flag = $this->changeStatus(2, $factoryRequestId, 'RequestStatusId', $user['UserId']);
        if($flag){
            $this->db->query('UPDATE ordershopifys SET OrderStatusId = ?, UpdateUserId = ?, UpdateDateTime = ? WHERE OrderShopifyId IN ?', array(7, $user['UserId'], $crDateTime, $orderIds));
            $actionLogs = array();
            foreach($orderIds as $orderId) {
                $actionLogs[] = array(
                    'ItemId' => $orderId,
                    'ItemTypeId' => 1,
                    'ActionTypeId' => 2,
                    'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                    'CrUserId' => $user['UserId'],
                    'CrDateTime' => $crDateTime
                );
            }
            if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function deleteBy($factoryRequestId, $orderIds, $user){
        $statusName = $this->Mconstants->orderStatus[6];
        $crDateTime = getCurentDateTime();
        $this->load->model('Mfactoryorders');
        $this->db->trans_begin();
        $this->db->query('UPDATE ordershopifys SET OrderStatusId = 6 WHERE OrderStatusId = 2 AND OrderShopifyId IN ?', array($orderIds));
        $this->Mfactoryorders->deleteMultiple(array('FactoryRequestId' => $factoryRequestId));
        $this->save(array('RequestStatusId' => 0), $factoryRequestId);
        $actionLogs = array();
        foreach($orderIds as $orderId) {
            $actionLogs[] = array(
                'ItemId' => $orderId,
                'ItemTypeId' => 1,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' change order status to '.$statusName,
                'CrUserId' => $user['UserId'],
                'CrDateTime' => $crDateTime
            );
        }
        if(!empty($actionLogs)) $this->db->insert_batch('actionlogs', $actionLogs);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function deleteOrder($factoryRequestId, $orderShopifyId, $user){
        $this->load->model('Mordershopifys');
        $this->load->model('Mfactoryorders');
        $this->load->model('Mactionlogs');
        $this->db->trans_begin();
        $this->Mordershopifys->save(array('OrderStatusId' => 6), $orderShopifyId);
        $this->Mfactoryorders->deleteMultiple(array('FactoryRequestId' => $factoryRequestId, 'OrderShopifyId' => $orderShopifyId));
        $this->Mactionlogs->save(array(
            'ItemId' => $orderShopifyId,
            'ItemTypeId' => 1,
            'ActionTypeId' => 2,
            'Comment' => $user['FullName'] . ' change order status to '.$this->Mconstants->orderStatus[6],
            'CrUserId' => $user['UserId'],
            'CrDateTime' => getCurentDateTime()
        ));
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getSumQuantity($factoryRequestId){
        $retVal = 0;
        $frs = $this->getByQuery('SELECT SUM(Quantity) as SumQuantity FROM orderproducts INNER JOIN factoryorders ON orderproducts.OrderShopifyId = factoryorders.OrderShopifyId WHERE factoryorders.FactoryRequestId = ?', array($factoryRequestId));
        if(!empty($frs)) $retVal = $frs[0]['SumQuantity'];
        return $retVal;
    }
    
    public function getSumProductBySize($factoryRequestId){
        $query = "SELECT orderproducts.Sku2 AS Sku,products.ProductId,products.ProductImage,products.ProductImage2, orderproducts.Size,SUM(orderproducts.Quantity) AS Quantity
                FROM factoryorders INNER JOIN orderproducts ON orderproducts.OrderShopifyId = factoryorders.OrderShopifyId
                LEFT JOIN products ON orderproducts.Sku2 = products.Sku
                WHERE factoryorders.FactoryRequestId = ? GROUP BY orderproducts.Sku2, orderproducts.Size
                ORDER BY orderproducts.Sku2";
        return $this->getByQuery($query, array($factoryRequestId));
    }

    public function getSumProductByType($factoryRequestId){
        $query = "SELECT orderproducts.ProductType,SUM(orderproducts.Quantity) AS sumQuantity FROM factoryorders INNER JOIN orderproducts ON orderproducts.OrderShopifyId = factoryorders.OrderShopifyId WHERE factoryorders.FactoryRequestId = ? GROUP BY orderproducts.ProductType ORDER BY orderproducts.ProductType";
        return $this->getByQuery($query, array($factoryRequestId));
    }

    public function getSumProductByBarCode($factoryRequestId, $factoryId){
        $query = "SELECT CONCAT('F', $factoryId, '-', orderproducts.BarCode) AS BarCode,SUM(orderproducts.Quantity) AS sumQuantity, products.ProductImage FROM factoryorders INNER JOIN orderproducts ON orderproducts.OrderShopifyId = factoryorders.OrderShopifyId LEFT JOIN products ON orderproducts.Sku2 = products.Sku WHERE factoryorders.FactoryRequestId = ? GROUP BY orderproducts.BarCode ORDER BY orderproducts.Sku2";
        return $this->getByQuery($query, array($factoryRequestId));
    }
}
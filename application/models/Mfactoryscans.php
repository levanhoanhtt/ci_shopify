<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mfactoryscans extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "factoryscans";
        $this->_primary_key = "FactoryScanId";
    }

    public function insertTrackingCode($factoryId, $trackingCode, $factoryScanIds, $userId){
        $this->db->trans_begin();
        $internalTrackingId = $this->Minternaltrackings->save(array(
            'FactoryId' => $factoryId,
            'TrackingCode' => $trackingCode,
            'UseTime' => 1,
            'CrUserId' => $userId,
            'CrDateTime' => getCurentDateTime()
        ));
        if($internalTrackingId > 0){
            $updateData = array();
            foreach($factoryScanIds as $factoryScanId){
                $updateData[] = array(
                    'FactoryScanId' => $factoryScanId,
                    'InternalTrackingId' => $internalTrackingId
                );
            }
            if(!empty($updateData)) $this->db->update_batch('factoryscans', $updateData, 'FactoryScanId');
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function insert($postData, $factoryScanId = 0, $scanImage = array()){
        $this->db->trans_begin();
        $factoryScanId = $this->save($postData, $factoryScanId);
        if($factoryScanId > 0){
            $this->db->query('UPDATE orderproducts SET WarehouseQuantity = WarehouseQuantity + 1, WarehouseDate = ? WHERE OrderProductId = ?', array($postData['ScanDate'], $postData['OrderProductId']));
            $this->db->query('UPDATE ordershopifys SET WarehouseDate = ? WHERE OrderShopifyId = ?', array($postData['ScanDate'], $postData['OrderShopifyId']));
            if(!empty($scanImage)) {
                $scanImage['FactoryScanId'] = $factoryScanId;
                $this->Mscanimages->save($scanImage);
            }
        }
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getListImages($factoryScanId, $includeBaseUrl = false){
        $retVal = array();
        $sis = $this->getByQuery('SELECT ProductImage FROM scanimages WHERE FactoryScanId = ?', array($factoryScanId));
        foreach($sis as $si){
            if(!empty($si['ProductImage'])){
                if($includeBaseUrl) $retVal[] = base_url(PRODUCT_PATH . 'scans/'.$si['ProductImage']);
                else $retVal[] = $si['ProductImage'];
            }
        }
        return $retVal;
    }

    public function getLateOrders(){
        $query = "SELECT factoryscans.ScanDate, factoryrequests.RequestDate, factories.FactoryName, factoryscans.Quantity,
                  orderproducts.Price, factoryscans.ProductType, penaltyrates.PenaltyPercentage,
                  (DATEDIFF(factoryscans.ScanDate,factoryrequests.RequestDate)-penaltyrates.AllowDay) AS LateDay ,
                  (factoryscans.Quantity*orderproducts.Price*penaltyrates.PenaltyPercentage/100) AS Penalty
                  FROM factoryorders
                  JOIN factoryscans ON factoryscans.OrderShopifyId = factoryorders.OrderShopifyId
                  JOIN factoryrequests ON factoryorders.FactoryRequestId = factoryrequests.FactoryRequestId
				  JOIN penaltyrates ON factoryorders.FactoryId = penaltyrates.FactoryId
				  JOIN factories ON factoryorders.FactoryId = factories.FactoryId
				  JOIN orderproducts ON factoryorders.OrderShopifyId = orderproducts.OrderShopifyId
                  WHERE DATEDIFF(factoryscans.ScanDate,factoryrequests.RequestDate) > penaltyrates.AllowDay
                  AND factoryscans.ProductType = penaltyrates.ProductType AND penaltyrates.StatusId = ?";
        return $this->getByQuery($query, array(STATUS_ACTIVED));
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM factoryscans WHERE 1=1" . $this->buildQuery($postData).' ORDER BY ScanDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND FactoryId=".$postData['FactoryId'];
        if(isset($postData['OrderName']) && !empty($postData['OrderName'])) $query.=" AND OrderName LIKE '%{$postData['OrderName']}%'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND ScanDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND ScanDate <= '{$postData['EndDate']}'";
        if(isset($postData['NoTrackingCode']) && $postData['NoTrackingCode'] == 1) $query.=" AND InternalTrackingId=0";
        if(isset($postData['TrackingCode']) && !empty($postData['TrackingCode'])) $query.=" AND InternalTrackingId=(SELECT InternalTrackingId FROM internaltrackings WHERE TrackingCode='{$postData['TrackingCode']}')";
        return $query;
    }

    public function getLastScanDate(){
        $retVal = '';
        $fs = $this->getByQuery('SELECT ScanDate FROM factoryscans ORDER BY ScanDate DESC LIMIT 1');
        if(!empty($fs)) $retVal = $fs[0]['ScanDate'];
        return $retVal;
    }
}
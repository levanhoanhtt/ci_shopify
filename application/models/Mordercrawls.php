<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mordercrawls extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "ordercrawls";
        $this->_primary_key = "OrderCrawlId";
    }

    public function search($fromDate, $toDate, $shopIds){
        return $this->getByQuery('SELECT * FROM ordercrawls WHERE IsOrderReal=0 AND ShopId IN ? AND(CreatedDateTime BETWEEN ? AND ?)', array($shopIds, $fromDate, $toDate));
    }

    public function updateIsOrderReal($orderCrawlIds){
        $this->db->where_in('OrderCrawlId', $orderCrawlIds);
        $this->db->update('ordercrawls', array('IsOrderReal' => 1));
    }

    public function searchGetLocation(){
        $fromDate = ddMMyyyyToDate('01/09/2018');
        $dateNow = date('d/m/Y');
        $toDate = ddMMyyyyToDate($dateNow, 'd/m/Y', 'Y-m-d 23:59:59');
        return $this->getByQuery('SELECT * FROM ordercrawls WHERE IsOrderReal= 0 AND(CreatedDateTime BETWEEN ? AND ?)', array($fromDate, $toDate));
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderpackings extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderpackings";
        $this->_primary_key = "OrderPackingId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT op.*, os.OrderName FROM orderpackings op INNER JOIN ordershopifys os ON op.OrderShopifyId=os.OrderShopifyId WHERE 1=1" . $this->buildQuery($postData).' ORDER BY op.ScanDate DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['TrackingCode']) && !empty($postData['TrackingCode'])) $query.=" AND op.TrackingCode='{$postData['TrackingCode']}''";
        if(isset($postData['OrderName']) && !empty($postData['OrderName'])) $query.=" AND os.OrderName='{$postData['OrderName']}'";
        if(isset($postData['ScanDate']) && !empty($postData['ScanDate'])) $query.=" AND DATE(ScanDate)='{$postData['ScanDate']}'";
        return $query;
    }


    public function insert($postData, $userId){
        $this->db->trans_begin();
        $this->save($postData);
        $this->Mordershopifys->save(array('TransportStatusId' => 3, 'UpdateUserId' => $userId, 'UpdateDateTime' => $postData['ScanDate']), $postData['OrderShopifyId']);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        else {
            $this->db->trans_commit();
            return true;
        }
    }
}
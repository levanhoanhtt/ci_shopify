<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Minvoicemails extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "invoicemails";
        $this->_primary_key = "InvoiceMailId";
    }

    public function getCount($postData){
    	$query ="SELECT 1 FROM invoicemails im LEFT JOIN invoices i ON im.InvoiceId = i.InvoiceId WHERE i.InvoiceStatusId > 0 ".$this->buildQuery($postData);
        return count($this->getByQuery($query));
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = 'SELECT  i.InvoiceId, i.InvoiceCode, i.InvoiceDate, i.InvoiceStatusId, i.TeamId, i.FactoryId,  im.* , u.FullName
            FROM invoicemails im INNER JOIN invoices i ON im.InvoiceId = i.InvoiceId
            INNER JOIN users u ON im.UserId = u.UserId
            WHERE i.InvoiceStatusId > 0'.$this->buildQuery($postData).' ORDER BY im.CrDateTime DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['InvoiceStatusId']) && $postData['InvoiceStatusId'] > 0) $query.=" AND i.InvoiceStatusId=".$postData['InvoiceStatusId'];
        if(isset($postData['TeamId']) && $postData['TeamId'] > 0) $query.=" AND i.TeamId=".$postData['TeamId'];
        if(isset($postData['FactoryId']) && $postData['FactoryId'] > 0) $query.=" AND i.FactoryId=".$postData['FactoryId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND im.CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND im.CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }
}
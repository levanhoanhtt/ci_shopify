<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mscanimages extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "scanimages";
        $this->_primary_key = "ScanImageId";
    }
}

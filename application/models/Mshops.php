<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshops extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "shops";
        $this->_primary_key = "ShopId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM shops WHERE StatusId > 0" . $this->buildQuery($postData);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['ShopCode']) && !empty($postData['ShopCode'])) $query.=" AND ShopCode = '{$postData['ShopCode']}'";
        if(isset($postData['ShopName']) && !empty($postData['ShopName'])) $query.=" AND ShopName LIKE '%{$postData['ShopName']}%'";
        if(isset($postData['ShopUrl']) && !empty($postData['ShopUrl'])) $query.=" AND ShopUrl LIKE '%{$postData['ShopUrl']}%'";
        return $query;
    }

    public function getByTeamIds($teamIds){
        return $this->getByQuery('SELECT * FROM shops WHERE TeamId IN ?', array($teamIds));
    }

    public function getListMap(){
        $retVal = array();
        $listShops = $this->getBy(array('StatusId' => STATUS_ACTIVED), false,'', 'ShopId, ShopCode');
        foreach ($listShops as $s) {
            if(!empty($s['ShopCode'])) $retVal[$s['ShopId']] = $s['ShopCode'];
        }
        return $retVal;
    }

    public function getListNoLocation(){
        return $this->getByQuery("SELECT * FROM shops WHERE StatusId = ? AND (LocationIds IS NULL OR LocationIds = '[]')", array(STATUS_ACTIVED));
    }
}
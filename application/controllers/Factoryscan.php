<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Factoryscan extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Warehouse Scan Products',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/factory_scan.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'factoryscan')) {
            $this->load->model(array('Mfactoryscans','Mfactories', 'Minternaltrackings'));
            $factoryId = $user['FactoryId'];
            $listFactories = array();
            if($factoryId == 0) $listFactories = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
            else $listFactories = array($this->Mfactories->get($factoryId));
            $data['listFactories'] = $listFactories;
            if($factoryId == 0){
                if(count($listFactories) == 1) $factoryId = $listFactories[0]['FactoryId'];
                else $factoryId = $this->input->post('FactoryId');
            }
            $data['factoryId'] = $factoryId;
            $listFactoryScans = array();
            $rowCount = 0;
            $data['lastScanDate'] = '';
            if($factoryId > 0){
                $postData = $this->arrayFromPost(array('BeginDate', 'EndDate', 'TrackingCode', 'NoTrackingCode'));
                if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
                if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
                //if(!empty($postData['ScanDate'])) $postData['ScanDate'] = ddMMyyyyToDate($postData['ScanDate']);
                $postData['FactoryId'] = $factoryId;
                if($postData['NoTrackingCode'] == 1){
                    $listFactoryScans = $this->Mfactoryscans->search($postData, PHP_INT_MAX, 1);
                    $rowCount = count($listFactoryScans);
                }
                else {
                    $rowCount = $this->Mfactoryscans->getCount($postData);
                    if($rowCount > 0) {
                        $perPage = 1000;
                        $pageCount = ceil($rowCount / $perPage);
                        $page = $this->input->post('PageId');
                        if (!is_numeric($page) || $page < 1) $page = 1;
                        $listFactoryScans = $this->Mfactoryscans->search($postData, $perPage, $page);
                        $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
                    }
                }
                $data['lastScanDate'] = $this->Mfactoryscans->getLastScanDate();
            }
            $data['scanCount'] = $rowCount;
            $data['listFactoryScans'] = $listFactoryScans;
            $this->load->view('scan/factory_scan', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function insertTrackingCode(){
        $user = $this->checkUserLogin(true);
        $trackingCode = trim($this->input->post('TrackingCode'));
        $factoryScanIds = $this->input->post('FactoryScanIds');
        $factoryId = $this->input->post('FactoryId');
        if(!empty($trackingCode) && !empty($factoryScanIds) && $factoryId > 0){
            $this->loadModel(array('Mfactoryscans', 'Minternaltrackings'));
            $internalTrackingId = $this->Minternaltrackings->getFieldValue(array('TrackingCode' => $trackingCode), 'InternalTrackingId', 0);
            if($internalTrackingId == 0){
                $flag = $this->Mfactoryscans->insertTrackingCode($factoryId, $trackingCode, $factoryScanIds, $user['UserId']);
                if($flag) echo json_encode(array('code' => 1, 'message' => "Update internal tracking code success"));
                else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Tracking code is existed"));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }

    public function getImage(){
        $this->checkUserLogin(true);
        $factoryScanId = $this->input->post('FactoryScanId');
        if($factoryScanId > 0){
            $this->load->model('Mfactoryscans');
            $listImages = $this->Mfactoryscans->getListImages($factoryScanId);
            echo json_encode(array('code' => 1, 'data' => $listImages));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }
}
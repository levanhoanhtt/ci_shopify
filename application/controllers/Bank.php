<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Bank List',
			array('scriptFooter' => array('js' => 'js/bank.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($data['listActions'], 'bank')) {
			$this->load->model('Mbanks');
			$data['listBanks'] = $this->Mbanks->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/bank', $data);
		}
		else $this->load->view('user/permission', $data);
	}


	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('BankName', 'BankNumber', 'BankDesc'));
		if(!empty($postData['BankName']) && !empty($postData['BankNumber'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$bankId = $this->input->post('BankId');
			$this->load->model('Mbanks');
			$flag = $this->Mbanks->save($postData, $bankId);
			if ($flag > 0) {
				$postData['BankId'] = $flag;
				$postData['IsAdd'] = ($bankId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Update bank success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$bankId = $this->input->post('BankId');
		if($bankId > 0){
			$this->load->model('Mbanks');
			$flag = $this->Mbanks->changeStatus(0, $bankId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete bank success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

}
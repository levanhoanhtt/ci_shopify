<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'All abandoned checkouts',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_all.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'checkout')) {
            $this->load->model(array('Mteams', 'Mshops', 'Mcheckouts'));
            $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
            $data['listShops'] = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
            $postData = $this->arrayFromPost(array('CustomerName', 'CustomerEmail', 'BeginDate', 'EndDate'));
            if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
            $teamIds = $this->input->post('TeamIds');
            if(!is_array($teamIds)) $teamIds = array();
            if(!empty($teamIds)) $postData['TeamIds'] = $teamIds;
            $data['teamIds'] = $teamIds;
            $shopIds = $this->input->post('ShopIds');
            if(!is_array($shopIds)) $shopIds = array();
            if(!empty($shopIds)) $postData['ShopIds'] = $shopIds;
            $data['shopIds'] = $shopIds;
            $rowCount = $this->Mcheckouts->getCount($postData);
            $data['orderCount'] = $rowCount;
            $data['listCheckouts'] = array();
            if($rowCount > 0){
                $perPage = 1000;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listCheckouts'] = $this->Mcheckouts->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('checkout/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function exportExcel(){
        $user = $this->checkUserLogin();
        $this->load->model(array('Mteams', 'Mshops', 'Mcheckouts'));
        $flag = $this->Mactions->checkAccessFromDb('checkout', $user['UserId'], $user['RoleId']);
        $orderIds = $this->input->post('OrderIds');
        if ($flag && !empty($orderIds)) {
            $listTeams = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
            $listShops = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
            $orderIds = explode(',', $orderIds);
            $listOrders = $this->Mcheckouts->getByIds($orderIds);
            $fileUrl = FCPATH . 'assets/uploads/excels/exportCheckout.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach($listOrders as $o) {
                $sheet->setCellValue('A' . $i, ddMMyyyy($o['CreatedDateTime'], 'd/m/Y H:i'));
                $sheet->setCellValue('B' . $i, $this->Mconstants->getObjectValue($listTeams, 'TeamId', $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'TeamId'), 'TeamName'));
                $sheet->setCellValue('C' . $i, $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'ShopName'));
                $sheet->setCellValue('D' . $i, $o['CountryName']);
                $sheet->setCellValue('E' . $i, $o['CustomerName']);
                $sheet->setCellValue('F' . $i, $o['CustomerEmail']);
                $i++;
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            $filename = "Export_Order_All_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }
}
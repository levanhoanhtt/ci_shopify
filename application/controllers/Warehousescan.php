<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehousescan extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Warehouse Scan Products',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/warehouse_scan.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'warehousescan')) {
            $this->loadModel(array('Mfactories','Mwarehousescans', 'Minternaltrackings'));
            $data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
            $factoryId = $this->input->post('FactoryId');
            $data['listWarehouseScans'] = array();
            if($factoryId > 0){
                $postData = $this->arrayFromPost(array('ScanDate', 'TrackingCode', 'NoTrackingCode'));
                if(!empty($postData['ScanDate'])) $postData['ScanDate'] = ddMMyyyyToDate($postData['ScanDate']);
                $postData['FactoryId'] = $factoryId;
                if($postData['NoTrackingCode'] == 1) $data['listWarehouseScans'] = $this->Mwarehousescans->search($postData, PHP_INT_MAX, 1);
                else{
                    $rowCount = $this->Mwarehousescans->getCount($postData);
                    if($rowCount > 0) {
                        $perPage = 1000;
                        $pageCount = ceil($rowCount / $perPage);
                        $page = $this->input->post('PageId');
                        if (!is_numeric($page) || $page < 1) $page = 1;
                        $data['listWarehouseScans'] = $this->Mwarehousescans->search($postData, $perPage, $page);
                        $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
                    }
                }
            }
            $this->load->view('scan/warehouse_scan', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    /*public function image($warehouseScanId = 0){
        if($warehouseScanId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user, 'Warehouse Scan Image Products');
            if($this->Mactions->checkAccess($data['listActions'], 'warehousescan')) {
                $this->load->model('Mwarehousescans');
                $data['warehouseScanId'] = $warehouseScanId;
                $data['listScanImages'] = $this->Mwarehousescans->getListImages($warehouseScanId, $this->input->post('IsError'));
                $this->load->view('scan/warehouse_image', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('warehousescan');
    }*/

    public function compare(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Product Scan Factory - Warehouse',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/warehouse_compare.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'warehousescan/compare')) {
            $this->loadModel(array('Mfactories', 'Mwarehousescans'));
            $data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
            $factoryId = $this->input->post('FactoryId');
            $beginDate = trim($this->input->post('BeginDate'));
            $endDate = trim($this->input->post('EndDate'));
            $data['listScans'] = array();
            if($factoryId > 0 && !empty($beginDate) && !empty($endDate)){
                $beginDate = ddMMyyyyToDate($beginDate);
                $endDate = ddMMyyyyToDate($endDate, 'd/m/Y', 'Y-m-d 23:59:59');
                $data['listScans'] = $this->Mwarehousescans->getListCompare($factoryId, $beginDate, $endDate);
            }
            $this->load->view('scan/warehouse_compare', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function ownProduct(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user, 'Own Product');
        if($this->Mactions->checkAccess($data['listActions'], 'warehousescan/ownProduct')) {
            $this->loadModel(array('Mfactories', 'Mownproducts'));
            $data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
            $postData = $this->arrayFromPost(array('FactoryId'));
            if($user['FactoryId'] > 0) $postData['FactoryId'] = $user['FactoryId'];
            $data['listScans'] = array();
            $rowCount = $this->Mownproducts->getCount($postData);
            if($rowCount > 0) {
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if (!is_numeric($page) || $page < 1) $page = 1;
                $data['listScans'] = $this->Mownproducts->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('scan/factory_own', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function insertTrackingCode(){
        $user = $this->checkUserLogin(true);
        $factoryId = trim($this->input->post('FactoryId'));
        $trackingCode = trim($this->input->post('TrackingCode'));
        $warehouseScanIds = $this->input->post('WarehouseScanIds');
        if($factoryId > 0 && !empty($trackingCode) && !empty($warehouseScanIds)){
            $this->loadModel(array('Mwarehousescans', 'Minternaltrackings'));
            $internalTrackingId = $this->Minternaltrackings->getFieldValue(array('FactoryId' => $factoryId, 'TrackingCode' => $trackingCode, 'UseTime' => 1), 'InternalTrackingId', 0);
            if($internalTrackingId > 0){
                $flag = $this->Mwarehousescans->updateTrackingCode($internalTrackingId, $warehouseScanIds, $user['UserId']);
                if($flag) echo json_encode(array('code' => 1, 'message' => "Update internal tracking code success"));
                else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Tracking code is wrong"));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ordershopify extends MY_Controller{

    public function index($orderStatusId = 0){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Order List',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckfinder/ckfinder.js', 'js/ordershopify_list.js'))
            )
        );
        $listActions = $data['listActions'];
        if ($this->Mactions->checkAccess($listActions, 'ordershopify')) {

            $data['canDelete'] = $this->Mactions->checkAccess($listActions, 'ordershopify/delete');
            $data['canCancel'] = $this->Mactions->checkAccess($listActions, 'ordershopify/cancel');
            $data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'ordershopify/changeStatus');
            if ($orderStatusId < 0 || $orderStatusId > 12) $orderStatusId = 0;
            $data['orderStatusId'] = $orderStatusId;
            $teamIds = $user['TeamIds'];
            $this->load->model(array('Mteams', 'Mshops', 'Mordershopifys', 'Mfactories', 'Mfactoryrequests'));
            if (empty($teamIds)) $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
            else $data['listTeams'] = $this->Mteams->getListByIds($teamIds);
            //$data['teamIds'] = $teamIds;
            $listFactoryRequest = array();
            $factoryId = $user['FactoryId'];
            if ($factoryId == 0){
                $data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
                if($orderStatusId == 1) $listFactoryRequest = $this->Mfactoryrequests->getBy(array('RequestStatusId' => 1), false, 'RequestDate');
            }
            else{
                $data['listFactories'] = array($this->Mfactories->get($factoryId));
                if($orderStatusId == 1) $listFactoryRequest = $this->Mfactoryrequests->getBy(array('RequestStatusId' => 1, 'FactoryId' => $factoryId), false, 'RequestDate');
            }
            $data['listFactoryRequest'] = $listFactoryRequest;
            if (empty($teamIds)) $data['listShops'] = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
            else $data['listShops'] = $this->Mshops->getByTeamIds($teamIds);
            //$postData = $this->arrayFromPost(array('TeamId', 'ShopId', 'FactoryId', 'TransportStatusId', 'OrderStatusId', 'OrderName', 'CustomerName', 'CustomerEmail', 'TrackingCode', 'BeginDate', 'EndDate'));
            $postData = $this->arrayFromPost(array('TransportStatusId', 'OrderExtraStatusId', 'OrderName', 'CustomerName', 'CustomerEmail', 'TrackingCode', 'BeginDate', 'EndDate'));
            $postData['OrderStatusId'] = $orderStatusId;
            if (!empty($teamIds)) $postData['TeamIds'] = $teamIds;
            else{
                $teamIds = $this->input->post('TeamIds');
                if(!is_array($teamIds)) $teamIds = array();
                if(!empty($teamIds)) $postData['TeamIds'] = $teamIds;
            }
            $data['teamIds'] = $teamIds;
            $shopIds = $this->input->post('ShopIds');
            if(!is_array($shopIds)) $shopIds = array();
            if(!empty($shopIds)) $postData['ShopIds'] = $shopIds;
            $data['shopIds'] = $shopIds;
            if ($factoryId > 0){
                $postData['FactoryId'] = $factoryId;
                $data['factoryIds'] = array($factoryId);
            }
            else{
                $factoryIds = $this->input->post('FactoryIds');
                if(!is_array($factoryIds)) $factoryIds = array();
                if(!empty($factoryIds)) $postData['FactoryIds'] = $factoryIds;
                $data['factoryIds'] = $factoryIds;
            }
            if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
            //if ($orderStatusId == 1) $perPage = 1000;
            if(in_array($orderStatusId, array(1, 4, 5, 6))) $perPage = 1000;
            else $perPage = DEFAULT_LIMIT;
            $rowCount = $this->Mordershopifys->getCount($postData);
            $data['orderCount'] = $rowCount;
            $data['listOrderShopifys'] = array();
            if ($rowCount > 0) {
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if (!is_numeric($page) || $page < 1) $page = 1;
                $data['listOrderShopifys'] = $this->Mordershopifys->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('ordershopify/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function late(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user, 'Late Orders');
        if ($this->Mactions->checkAccess($data['listActions'], 'ordershopify/late')) {
            $this->load->model('Mfactoryscans');
            $data['lateOrders'] = $this->Mfactoryscans->getLateOrders();
            $this->load->view('ordershopify/late', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    

    public function edit($orderShopifyId = 0){
        if ($orderShopifyId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Order Detail',
                array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/ordershopify_update.js')))
            );
            $this->load->model(array('Mordershopifys', 'Morderproducts', 'Mshops', 'Mteams', 'Mfactoryrequests', 'Mactionlogs'));
            $orderShopify = $this->Mordershopifys->get($orderShopifyId);
            if ($orderShopify) {
                $listActions = $data['listActions'];
                if ($this->Mactions->checkAccess($listActions, 'ordershopify/view')) {
                    $canEdit = false;
                    $canUpdateCustomer = false;
                    if ($this->Mactions->checkAccess($listActions, 'ordershopify/edit')) {
                        if(in_array($orderShopify['OrderStatusId'], array(1, 4, 5, 6))) $canEdit = true;
                        elseif ($orderShopify['OrderStatusId'] == 2) {
                            $requestStatusId = $this->Mfactoryrequests->getStatusByOrder($orderShopifyId);
                            if ($requestStatusId != 2) $canEdit = true;
                        }
                        if(in_array($orderShopify['OrderStatusId'], array(1, 4, 5, 6, 2, 7))) $canUpdateCustomer = true;
                    }
                    $data['canEdit'] = $canEdit;
                    $data['canUpdateCustomer'] = $canUpdateCustomer;
                    if($canEdit) $data['canDeleteProduct'] = $this->Mactions->checkAccess($listActions, 'ordershopify/deleteProduct');
                    else $data['canDeleteProduct'] =false;
                    $data['orderShopifyId'] = $orderShopifyId;
                    $data['orderShopify'] = $orderShopify;
                    $data['shipping'] = json_decode($orderShopify['Shipping'], true);
                    $data['shop'] = $this->Mshops->get($orderShopify['ShopId'], true, '', array('ShopId', 'ShopName', 'TeamId'));
                    $data['team'] = $this->Mteams->get($data['shop']['TeamId'], true, '', array('TeamId', 'TeamName'));
                    if ($orderShopify['HasChild'] == 1) {
                        $data['listOrderProductsChild'] = $this->Mordershopifys->getBy(array('ParentOrderId' => $orderShopifyId));
                        $data['listOrderProducts'] = array();
                        $data['listActionLogs'] = array();
                    }
                    else {
                        $data['listOrderProductsChild'] = array();
                        $data['listOrderProducts'] = $this->Morderproducts->getBy(array('OrderShopifyId' => $orderShopifyId));
                        $data['listActionLogs'] = $this->Mactionlogs->getList($orderShopifyId, 1);
                    }
                    $this->load->view('ordershopify/edit', $data);
                }
                else $this->load->view('user/permission', $data);
            }
            else {
                $data['orderShopifyId'] = 0;
                $data['txtError'] = "Order not found";
                $this->load->view('ordershopify/edit', $data);
            }
        }
        else redirect('user/profile');
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $statusId = $this->input->post('StatusId');
        $orderShopifyIds = $this->input->post('OrderShopifyIds');
        if ($statusId >= 0 && !empty($orderShopifyIds)) {
            $this->load->model('Mordershopifys');
            $flag = $this->Mordershopifys->changeStatusBatch($orderShopifyIds, $statusId, $user);
            if ($flag) {
                if ($statusId == 0) $msg = 'Delete order success';
                else $msg = 'Update order status success';
                echo json_encode(array('code' => 1, 'message' => $msg));
            }
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    public function import(){
        $user = $this->checkUserLogin(true);
        $shopIds = $this->input->post('shopIds');
        $fromDate = $this->input->post('fromDate');
        $toDate = $this->input->post('toDate');
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        if($flag && $shopIds) {
            if (!empty($fromDate)) $fromDate = ddMMyyyyToDate($fromDate);
            if (!empty($toDate)) $toDate = ddMMyyyyToDate($toDate, 'd/m/Y', 'Y-m-d 23:59:59');
            $this->loadModel(array('Mordercrawls', 'Mordershopifys', 'Mshops', 'Mproducttypes'));
            $shopCodes = $factories = $productTypes = array();
            $listOrders = $this->Mordercrawls->search($fromDate, $toDate, $shopIds);
            if(!empty($listOrders)){
                $shopCodes = $this->Mshops->getListMap();
                $factoryProductTypes = $this->Mproducttypes->getListMap();
                $factories = $factoryProductTypes['Factory'];
                $productTypes = $factoryProductTypes['ProductType'];
            }
            $orderCrawlIds = array();
            foreach ($listOrders as $o) {
                $orderShopifyExist = $this->Mordershopifys->getFieldValue(array('OrderCode' => $o['OrderId']), 'OrderShopifyId', 0);
                if ($orderShopifyExist > 0) continue;
                $orderCrawlIds[] = $o['OrderCrawlId'];
                $orderDateTime = $o['CreatedDateTime'];
                $shopId = $o['ShopId'];
                $o = json_decode($o['Json'], true);
                $customer = '';
                $customerName = '';
                $customerEmail = '';
                $countryName = '';
                $shippingAddress = '[]';
                if (isset($o['customer'])) {
                    $customer = json_encode($o['customer']);
                    $customerName = $o['customer']['first_name'] . ' ' . $o['customer']['last_name'];
                    $customerEmail = $o['customer']['email'];
                }
                if(isset($o['billing_address'])){
                    if(isset($o['billing_address']['country'])) $countryName = $o['billing_address']['country'];
                }
                if(isset($o['shipping_address'])) $shippingAddress = json_encode($o['shipping_address']);
                $filterFactories = array();
                $itemIds = array();
                foreach ($o['line_items'] as $l) {
                    $itemIds[] = $l['id'];
                    $skuExtract = explode('-', $l['sku']);
                    if(count($skuExtract) < 3) continue;
                    //$shopCode = trim($skuExtract[0]);
                    $productTypeName = trim(strtoupper($skuExtract[2]));
                    if(!in_array($productTypeName, $productTypes)) continue;
                    $flag = false;
                    foreach($shopCodes as $sId => $sCode){
                        if($sId == $shopId){
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag) continue;
                    $factoryId = $this->getFactoryIdByType($factories, $productTypeName);
                    if($factoryId == 0) continue;
                    for ($i = 0; $i < $l['quantity']; $i++) {
                        $lClone = $l;
                        $lClone['quantity'] = 1;
                        $filterFactories[$factoryId][] = $lClone;
                    }
                }
                $orderName = str_replace('#', '', $o['name']);
                if(count($filterFactories) == 1){
                    foreach ($filterFactories as $factoryId => $listProducts) {
                        $qtyAll = count($listProducts);
                        $lineItemsChange = $listProducts;
                        $lineItemsChange = array_chunk($lineItemsChange, 4);
                        $lineItemsChangeAfter = array();
                        foreach ($lineItemsChange as $l) {
                            $item = array();
                            $sku = '';
                            foreach ($l as $h => $v) {
                                if ($sku == '') $item[0] = $v;
                                else {
                                    if ($sku == $v['sku']) $item[count($item) - 1]['quantity'] += 1;
                                    else $item[] = $v;
                                }
                                $sku = $v['sku'];
                            }
                            $lineItemsChangeAfter[] = $item;
                        }
                        $order = array(
                            'OrderCode' => $o['id'],
                            'OrderName' => $orderName,
                            'ShopId' => $shopId,
                            'Comment' => '',
                            'ParentOrderId' => 0,
                            'HasChild' => $qtyAll > 4 ? 1 : 0,
                            'TotalPrice' => $o['total_price'],
                            'Customer' => $customer,
                            'OrderDateTime' => $orderDateTime,
                            'CountryName' => $countryName,
                            'TrackingCode' => '',
                            'TransportStatusId' => 1,
                            'OrderStatusId' => 1,
                            'OrderExtraStatusId' => 0,
                            'Shipping' => $shippingAddress,
                            'CustomerName' => $customerName,
                            'CustomerEmail' => $customerEmail,
                            'FactoryId' => $factoryId,
                            'ItemIds' => json_encode($itemIds),
                            'IsSendTracking' => 1,
                            'CrDateTime' => getCurentDateTime()
                        );
                        $parentId = $this->Mordershopifys->save($order);
                        if ($qtyAll > 4) {
                            $count = 0;
                            foreach ($lineItemsChangeAfter as $v) {
                                $count++;
                                $order = array(
                                    'OrderCode' => $o['id'] . '_' . $count,
                                    'OrderName' => $orderName . '_' . $count,
                                    'ShopId' => $shopId,
                                    'Comment' => '',
                                    'ParentOrderId' => $parentId,
                                    'HasChild' => 0,
                                    'TotalPrice' => 0,
                                    'Customer' => $customer,
                                    'OrderDateTime' => $orderDateTime,
                                    'CountryName' => $countryName,
                                    'TrackingCode' => '',
                                    'TransportStatusId' => 1,
                                    'OrderStatusId' => 1,
                                    'OrderExtraStatusId' => 0,
                                    'Shipping' => $shippingAddress,
                                    'CustomerName' => $customerName,
                                    'CustomerEmail' => $customerEmail,
                                    'FactoryId' => $factoryId,
                                    'ItemIds' => json_encode($itemIds),
                                    'IsSendTracking' => 1,
                                    'CrDateTime' => getCurentDateTime()
                                );
                                $orderId = $this->Mordershopifys->save($order);
                                $orderProducts = array();
                                foreach ($v as $l) {
                                    $size = '';
                                    $productType = '';
                                    $sku1 = '';
                                    $sku2 = '';
                                    $color = '';
                                    $skuExtract = explode('-', $l['sku']);
                                    if(count($skuExtract) >= 4) {
                                        $size = $skuExtract[3];
                                        $productType = $skuExtract[2];
                                        $sku1 = $skuExtract[0] . '-' . $skuExtract[1];
                                        if(count($skuExtract) == 5) $color = '-' . $skuExtract[4];
                                        else $color = '';
                                        $sku2 = $skuExtract[0] . '-' . $skuExtract[1] . '-' . $skuExtract[2] . $color;
                                    }
                                    $orderProducts[] = array(
                                        'OrderShopifyId' => $orderId,
                                        'ProductName' => $l['name'],
                                        'Quantity' => $l['quantity'],
                                        'Price' => $l['price'],
                                        'Sku' => $l['sku'],
                                        'BarCode' => $order['OrderName'] . '-' . $l['sku'],
                                        'Size' => $size,
                                        'ProductType' => $productType,
                                        'Sku1' => $sku1,
                                        'Sku2' => $sku2,
                                        //'Color' => $color,
                                        'FactoryQuantity' => 0,
                                        'WarehouseQuantity' => 0
                                    );
                                }
                                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
                            }
                        }
                        else {
                            foreach ($lineItemsChangeAfter as $v) {
                                $orderProducts = array();
                                foreach ($v as $l) {
                                    $size = '';
                                    $productType = '';
                                    $sku1 = '';
                                    $sku2 = '';
                                    $color = '';
                                    $skuExtract = explode('-', $l['sku']);
                                    if(count($skuExtract) >= 4) {
                                        $size = $skuExtract[3];
                                        $productType = $skuExtract[2];
                                        $sku1 = $skuExtract[0] . '-' . $skuExtract[1];
                                        if(count($skuExtract) == 5) $color = '-' . $skuExtract[4];
                                        else $color = '';
                                        $sku2 = $skuExtract[0] . '-' . $skuExtract[1] . '-' . $skuExtract[2] . $color;
                                    }
                                    $orderProducts[] = array(
                                        'OrderShopifyId' => $parentId,
                                        'ProductName' => $l['name'],
                                        'Quantity' => $l['quantity'],
                                        'Price' => $l['price'],
                                        'Sku' => $l['sku'],
                                        'BarCode' => $order['OrderName'] . '-' . $l['sku'],
                                        'Size' => $size,
                                        'ProductType' => $productType,
                                        'Sku1' => $sku1,
                                        'Sku2' => $sku2,
                                        //'Color' => $color,
                                        'FactoryQuantity' => 0,
                                        'WarehouseQuantity' => 0
                                    );
                                }
                                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
                            }
                        }
                    }
                }
                elseif(count($filterFactories) > 1) {
                    $count = 0;
                    foreach ($filterFactories as $factoryId => $listProducts) {
                        $count++;
                        $qtyAll = count($listProducts);
                        $lineItemsChange = $listProducts;
                        $lineItemsChange = array_chunk($lineItemsChange, 4);
                        $lineItemsChangeAfter = array();
                        foreach ($lineItemsChange as $l) {
                            $item = array();
                            $sku = '';
                            foreach ($l as $h => $v) {
                                if ($sku == '') $item[0] = $v;
                                else {
                                    if ($sku == $v['sku']) $item[count($item) - 1]['quantity'] += 1;
                                    else $item[] = $v;
                                }
                                $sku = $v['sku'];
                            }
                            $lineItemsChangeAfter[] = $item;
                        }
                        $order = array(
                            'OrderCode' => $o['id'] . '_' . $count,
                            'OrderName' => $orderName . '_' . $count,
                            'ShopId' => $shopId,
                            'Comment' => '',
                            'ParentOrderId' => 0,
                            'HasChild' => $qtyAll > 4 ? 1 : 0,
                            'TotalPrice' => $o['total_price'],
                            'Customer' => $customer,
                            'OrderDateTime' => $orderDateTime,
                            'CountryName' => $countryName,
                            'TrackingCode' => '',
                            'TransportStatusId' => 1,
                            'OrderStatusId' => 1,
                            'OrderExtraStatusId' => 0,
                            'Shipping' => $shippingAddress,
                            'CustomerName' => $customerName,
                            'CustomerEmail' => $customerEmail,
                            'FactoryId' => $factoryId,
                            'ItemIds' => json_encode($itemIds),
                            'IsSendTracking' => 1,
                            'CrDateTime' => getCurentDateTime()
                        );
                        $parentId = $this->Mordershopifys->save($order);
                        if ($qtyAll > 4) {
                            foreach ($lineItemsChangeAfter as $v) {
                                $count++;
                                $order = array(
                                    'OrderCode' => $o['id'] . '_' . $count,
                                    'OrderName' => $orderName . '_' . $count,
                                    'ShopId' => $shopId,
                                    'Comment' => '',
                                    'ParentOrderId' => $parentId,
                                    'HasChild' => 0,
                                    'TotalPrice' => 0,
                                    'Customer' => $customer,
                                    'OrderDateTime' => $orderDateTime,
                                    'CountryName' => $countryName,
                                    'TrackingCode' => '',
                                    'TransportStatusId' => 1,
                                    'OrderStatusId' => 1,
                                    'OrderExtraStatusId' => 0,
                                    'Shipping' => $shippingAddress,
                                    'CustomerName' => $customerName,
                                    'CustomerEmail' => $customerEmail,
                                    'FactoryId' => $factoryId,
                                    'ItemIds' => json_encode($itemIds),
                                    'IsSendTracking' => 1,
                                    'CrDateTime' => getCurentDateTime()
                                );
                                $orderId = $this->Mordershopifys->save($order);
                                $orderProducts = array();
                                foreach ($v as $l) {
                                    $size = '';
                                    $productType = '';
                                    $sku1 = '';
                                    $sku2 = '';
                                    $color = '';
                                    $skuExtract = explode('-', $l['sku']);
                                    if(count($skuExtract) >= 4) {
                                        $size = $skuExtract[3];
                                        $productType = $skuExtract[2];
                                        $sku1 = $skuExtract[0] . '-' . $skuExtract[1];
                                        if(count($skuExtract) == 5) $color = '-' . $skuExtract[4];
                                        else $color = '';
                                        $sku2 = $skuExtract[0] . '-' . $skuExtract[1] . '-' . $skuExtract[2] . $color;
                                    }
                                    $orderProducts[] = array(
                                        'OrderShopifyId' => $orderId,
                                        'ProductName' => $l['name'],
                                        'Quantity' => $l['quantity'],
                                        'Price' => $l['price'],
                                        'Sku' => $l['sku'],
                                        'BarCode' => $order['OrderName'] . '-' . $l['sku'],
                                        'Size' => $size,
                                        'ProductType' => $productType,
                                        'Sku1' => $sku1,
                                        'Sku2' => $sku2,
                                        //'Color' => $color,
                                        'FactoryQuantity' => 0,
                                        'WarehouseQuantity' => 0
                                    );
                                }
                                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
                            }
                        }
                        else {
                            foreach ($lineItemsChangeAfter as $v) {
                                $orderProducts = array();
                                foreach ($v as $l) {
                                    $size = '';
                                    $productType = '';
                                    $sku1 = '';
                                    $sku2 = '';
                                    $color = '';
                                    $skuExtract = explode('-', $l['sku']);
                                    if(count($skuExtract) >= 4) {
                                        $size = $skuExtract[3];
                                        $productType = $skuExtract[2];
                                        $sku1 = $skuExtract[0] . '-' . $skuExtract[1];
                                        if(count($skuExtract) == 5) $color = '-' . $skuExtract[4];
                                        else $color = '';
                                        $sku2 = $skuExtract[0] . '-' . $skuExtract[1] . '-' . $skuExtract[2] . $color;
                                    }
                                    $orderProducts[] = array(
                                        'OrderShopifyId' => $parentId,
                                        'ProductName' => $l['name'],
                                        'Quantity' => $l['quantity'],
                                        'Price' => $l['price'],
                                        'Sku' => $l['sku'],
                                        'BarCode' => $order['OrderName'] . '-' . $l['sku'],
                                        'Size' => $size,
                                        'ProductType' => $productType,
                                        'Sku1' => $sku1,
                                        'Sku2' => $sku2,
                                        //'Color' => $color,
                                        'FactoryQuantity' => 0,
                                        'WarehouseQuantity' => 0
                                    );
                                }
                                if (!empty($orderProducts)) $this->db->insert_batch('orderproducts', $orderProducts);
                            }
                        }
                    }
                }
            }
            if(!empty($orderCrawlIds)) $this->Mordercrawls->updateIsOrderReal($orderCrawlIds);
            echo json_encode(array('code' => 1, 'message' => "Import Order success"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    private function getFactoryIdByType($factories, $productType){
        foreach($factories as $factoryId => $productTypes){
            if(in_array($productType, $productTypes)) return $factoryId;
        }
        return 0;
    }

    public function update(){
        $user = $this->checkUserLogin(true);
        $this->load->model(array('Mordershopifys', 'Mactionlogs'));
        $orderShopifyId = $this->input->post('OrderShopifyId');
        $orderShopify = $this->Mordershopifys->getBy(array('OrderStatusId >' => 0, 'OrderShopifyId' => $orderShopifyId), true);
        if ($orderShopify) {
            $shipping = json_decode($orderShopify['Shipping'], true);
            $shipping = array_merge($shipping, $this->input->post('Shipping'));
            $postData = $this->arrayFromPost(array('CustomerName', 'CustomerEmail'));
            $postData['Shipping'] = json_encode($shipping);
            $postData['Comment'] = $this->input->post('Comment', true);
            $actionLogs = array(
                'ItemId' => $orderShopifyId,
                'ItemTypeId' => 1,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' edit order',
                'CrUserId' => $user['UserId'],
                'CrDateTime' => getCurentDateTime()
            );
            $products = $this->input->post('Products');
            if (!is_array($products)) $products = array();
            for ($i = 0; $i < count($products); $i++) {
                $parts = explode('-', $products[$i]['Sku']);
                if(count($parts) >= 4){
                    $products[$i]['BarCode'] = $orderShopify['OrderName'] . '-' . $products[$i]['Sku'];
                    $products[$i]['Size'] = $parts[3];
                    $products[$i]['ProductType'] = $parts[2];
                    $products[$i]['Sku1'] = $parts[0] . '-' . $parts[1];
                    $color = '';
                    if(count($parts) == 5) $color = '-' . $parts[4];
                    $products[$i]['Sku2'] = $parts[0] . '-' . $parts[1] . '-' . $parts[2] . $color;
                }
            }
            $orderShopifyId = $this->Mordershopifys->update($postData, $orderShopifyId, $products, $actionLogs);
            if ($orderShopifyId > 0) echo json_encode(array('code' => 1, 'message' => "Update order success", 'data' => $orderShopifyId));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "Can't update this order"));
    }

    public function deleteProduct(){
        $user =  $this->checkUserLogin(true);
        $orderProductId = $this->input->post('OrderProductId');
        $orderShopifyId = $this->input->post('OrderShopifyId');
        $sku = trim($this->input->post('Sku'));
        if($orderProductId > 0 && !empty($sku) && $orderShopifyId > 0){
            $this->loadModel(array('Morderproducts', 'Mactionlogs', 'Mordercrawls', 'Mordershopifys'));
            $actionLogs = array(
                'ItemId' => $orderShopifyId,
                'ItemTypeId' => 1,
                'ActionTypeId' => 2,
                'Comment' => $user['FullName'] . ' delete sku '.$sku.' with quantity is '.$this->input->post('Quantity'),
                'CrUserId' => $user['UserId'],
                'CrDateTime' => getCurentDateTime()
            );
            $flag = $this->Morderproducts->deleteId($orderProductId, $actionLogs);
            if($flag){
                $orderName  = trim($this->input->post('OrderName'));
                if(!empty($orderName)){
                    $json = $this->Mordercrawls->getFieldValue(array('OrderName' => $orderName), 'Json');
                    if(!empty($json)){
                        $json = json_decode($json, true);
                        $itemIds = array();
                        foreach ($json['line_items'] as $l){
                            if($l['sku'] != $sku) $itemIds[] = $l['id'];
                        }
                        $this->Mordershopifys->save(array('ItemIds' => json_encode($itemIds)), $orderShopifyId);
                    }
                }
                echo json_encode(array('code' => 1, 'message' => "Delete product success"));
            }
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }

    public function importScanExcel(){
        $user = $this->checkUserLogin(true);
        $fileUrl = trim($this->input->post('FileUrl'));
        $importTypeId = $this->input->post('ImportTypeId');
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        if ($flag && !empty($fileUrl) && in_array($importTypeId, array(3, 4))){
            if (ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
            $fileUrl = FCPATH . $fileUrl;
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($fileUrl);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileUrl);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $countRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $scanData = array();
            $barCodes = array();
            if($importTypeId == 3){ //factory scan
                for($row = 3; $row <= $countRows; $row++) {
                    $factoryDate = trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());
                    $factoryDate = @ddMMyyyyToDate($factoryDate, 'Y.m.d');
                    if(!$factoryDate) $factoryDate = '';
                    $status = strtolower(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    if(!empty($factoryDate) && $status == 'delivered'){
                        $orderName = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $sKU = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                        $factorySend = intval(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                        $orderName = trim(str_replace('#', '', $orderName));
                        $sKU = trim(str_replace('#', '', $sKU));
                        $barCode = $orderName . '-' . $sKU;
                        if (!in_array($barCode, $barCodes)) {
                            $scanData[$barCode] = array(
                                'OrderName' => $orderName,
                                'SKU' => $sKU,
                                'FactorySend' => $factorySend,
                                'FactoryDate' => $factoryDate
                            );
                            $barCodes[] = $barCode;
                        }
                        else {
                            $scanData[$barCode]['FactorySend'] += $factorySend;
                            if (!empty($scanData[$barCode]['FactoryDate'])) {
                                if (strtotime($scanData[$barCode]['FactoryDate']) < strtotime($factoryDate)) $scanData[$barCode]['FactoryDate'] = $factoryDate;
                            }
                            else $scanData[$barCode]['FactoryDate'] = $factoryDate;
                        }
                    }
                }
            }
            else{ //warehouse scan
                for($row = 3; $row <= $countRows; $row++) {
                    $warehouseDate = trim($objWorksheet->getCellByColumnAndRow(10, $row)->getValue());
                    $warehouseDate = @ddMMyyyyToDate($warehouseDate, 'd/m/Y');
                    if(!$warehouseDate) $warehouseDate = '';
                    $status = strtolower(trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()));
                    if(!empty($warehouseDate) && $status == 'delivered'){
                        $orderName = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                        $sKU = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                        $warehouseReceive = intval(trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue()));
                        $orderName = trim(str_replace('#', '', $orderName));
                        $sKU = trim(str_replace('#', '', $sKU));
                        $barCode = $orderName . '-' . $sKU;
                        if (!in_array($barCode, $barCodes)) {
                            $scanData[$barCode] = array(
                                'OrderName' => $orderName,
                                'SKU' => $sKU,
                                'WarehouseReceive' => $warehouseReceive,
                                'WarehouseDate' => $warehouseDate
                            );
                            $barCodes[] = $barCode;
                        }
                        else {
                            $scanData[$barCode]['WarehouseReceive'] += $warehouseReceive;
                            if(!empty($scanData[$barCode]['WarehouseDate'])){
                                if(strtotime($scanData[$barCode]['WarehouseDate']) < strtotime($warehouseDate)) $scanData[$barCode]['WarehouseDate'] = $warehouseDate;
                            }
                            else $scanData[$barCode]['WarehouseDate'] = $warehouseDate;
                        }
                    }
                }
            }
            if(!empty($barCodes)) {
                $this->loadModel(array('Morderproducts', 'Mordershopifys'));
                $listOrderProducts = $this->Morderproducts->getScanByBarCodes($barCodes);
                if(!empty($listOrderProducts)){
                    $orderProductUpdates = array();
                    $orderDates = array();
                    if($importTypeId == 3) { //factory scan
                        foreach($listOrderProducts as $op) {
                            $barCode = $op['BarCode'];
                            if (isset($scanData[$barCode])) {
                                $factoryQuantity = $scanData[$barCode]['FactorySend'] + $op['FactoryQuantity'];
                                if($factoryQuantity > $op['Quantity']) $factoryQuantity = $op['Quantity'];
                                $tmp = array(
                                    'OrderProductId' => $op['OrderProductId'],
                                    'FactoryQuantity' => $factoryQuantity
                                );
                                if(!empty($scanData[$barCode]['FactoryDate'])) $tmp['FactoryDate'] = $scanData[$barCode]['FactoryDate'];
                                $orderProductUpdates[] = $tmp;
                                if(!isset($orderDates[$op['OrderShopifyId']])){
                                    $tmp = array();
                                    if(!empty($scanData[$barCode]['FactoryDate'])) $tmp['FactoryDate'] = $scanData[$barCode]['FactoryDate'];
                                    $orderDates[$op['OrderShopifyId']] = $tmp;
                                }
                                else{
                                    $tmp = $orderDates[$op['OrderShopifyId']];
                                    if(!empty($scanData[$barCode]['FactoryDate'])){
                                        if(isset($tmp['FactoryDate'])){
                                            if(strtotime($tmp['FactoryDate']) < strtotime($scanData[$barCode]['FactoryDate'])) $tmp['FactoryDate'] = $scanData[$barCode]['FactoryDate'];
                                        }
                                        else $tmp['FactoryDate'] = $scanData[$barCode]['FactoryDate'];
                                    }
                                }
                            }
                        }
                    }
                    else{ //warehouse scan
                        foreach($listOrderProducts as $op){
                            $barCode = $op['BarCode'];
                            if(isset($scanData[$barCode])){
                                $warehouseQuantity = $scanData[$barCode]['WarehouseReceive'] + $op['WarehouseQuantity'];
                                if($warehouseQuantity > $op['Quantity']) $warehouseQuantity = $op['Quantity'];
                                $tmp = array(
                                    'OrderProductId' => $op['OrderProductId'],
                                    'WarehouseQuantity' => $warehouseQuantity
                                );
                                if(!empty($scanData[$barCode]['WarehouseDate'])) $tmp['WarehouseDate'] = $scanData[$barCode]['WarehouseDate'];
                                $orderProductUpdates[] = $tmp;
                                if(!isset($orderDates[$op['OrderShopifyId']])){
                                    $tmp = array();
                                    if(!empty($scanData[$barCode]['WarehouseDate'])) $tmp['WarehouseDate'] = $scanData[$barCode]['WarehouseDate'];
                                    $orderDates[$op['OrderShopifyId']] = $tmp;
                                }
                                else{
                                    $tmp = $orderDates[$op['OrderShopifyId']];
                                    if(!empty($scanData[$barCode]['WarehouseDate'])){
                                        if(isset($tmp['WarehouseDate'])){
                                            if(strtotime($tmp['WarehouseDate']) < strtotime($scanData[$barCode]['WarehouseDate'])) $tmp['WarehouseDate'] = $scanData[$barCode]['WarehouseDate'];
                                        }
                                        else $tmp['WarehouseDate'] = $scanData[$barCode]['WarehouseDate'];
                                    }
                                    $orderDates[$op['OrderShopifyId']] = $tmp;
                                }
                            }
                        }
                    }
                    if(!empty($orderProductUpdates) && !empty($orderDates)){
                        $this->db->update_batch('orderproducts', $orderProductUpdates, 'OrderProductId');
                        $orderUpdates = array();
                        $actionLogs = array();
                        $updateDateTime = getCurentDateTime();
                        $orderExtraStatus = $this->Mconstants->orderExtraStatus;
                        foreach($orderDates as $orderId => $dates){
                            $listProducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $orderId), false, '', 'OrderProductId,Quantity,FactoryQuantity,WarehouseQuantity');
                            $n = count($listProducts);
                            $i = 0;
                            $j = 0;
                            $flag1 = false; //kho nhan thieu
                            $flag2 = false; //xuong gui thieu
                            foreach($listProducts as $p){
                                if($p['Quantity'] == $p['WarehouseQuantity']) $i++;
                                else{
                                    if($p['WarehouseQuantity'] > 0) $flag1 = true;
                                    elseif($p['Quantity'] == $p['FactoryQuantity']) $j++;
                                    elseif($p['FactoryQuantity'] > 0) $flag2 = true;
                                }
                            }
                            if($i == $n){ // kho nhan du
                                $tmp = array(
                                    'OrderShopifyId' => $orderId,
                                    'OrderStatusId' => 10,
                                    'OrderExtraStatusId' => 3,
                                    'TransportStatusId' => 2,
                                    'UpdateUserId' => $user['UserId'],
                                    'UpdateDateTime' => $updateDateTime
                                );
                                if(isset($dates['FactoryDate'])) $tmp['FactoryDate'] = $dates['FactoryDate'];
                                if(isset($dates['WarehouseDate'])) $tmp['WarehouseDate'] = $dates['WarehouseDate'];
                                $orderUpdates[] = $tmp;
                                $actionLogs[] = array(
                                    'ItemId' => $orderId,
                                    'ItemTypeId' => 1,
                                    'ActionTypeId' => 2,
                                    'Comment' => $user['FullName'] . ' change order status to '.$orderExtraStatus[3],
                                    'CrUserId' => $user['UserId'],
                                    'CrDateTime' => $updateDateTime
                                );
                            }
                            else{
                                if($flag1){  //kho nhan thieu
                                    $tmp = array(
                                        'OrderShopifyId' => $orderId,
                                        'OrderStatusId' => 10,
                                        'OrderExtraStatusId' => 4,
                                        'UpdateUserId' => $user['UserId'],
                                        'UpdateDateTime' => $updateDateTime
                                    );
                                    if(isset($dates['FactoryDate'])) $tmp['FactoryDate'] = $dates['FactoryDate'];
                                    if(isset($dates['WarehouseDate'])) $tmp['WarehouseDate'] = $dates['WarehouseDate'];
                                    $orderUpdates[] = $tmp;
                                    $actionLogs[] = array(
                                        'ItemId' => $orderId,
                                        'ItemTypeId' => 1,
                                        'ActionTypeId' => 2,
                                        'Comment' => $user['FullName'] . ' change order status to '.$orderExtraStatus[4],
                                        'CrUserId' => $user['UserId'],
                                        'CrDateTime' => $updateDateTime
                                    );
                                }
                                if($j == $n){ ////xuong gui du
                                    $tmp = array(
                                        'OrderShopifyId' => $orderId,
                                        'OrderStatusId' => 9,
                                        'OrderExtraStatusId' => 1,
                                        'UpdateUserId' => $user['UserId'],
                                        'UpdateDateTime' => $updateDateTime
                                    );
                                    if(isset($dates['FactoryDate'])) $tmp['FactoryDate'] = $dates['FactoryDate'];
                                    if(isset($dates['WarehouseDate'])) $tmp['WarehouseDate'] = $dates['WarehouseDate'];
                                    $orderUpdates[] = $tmp;
                                    $actionLogs[] = array(
                                        'ItemId' => $orderId,
                                        'ItemTypeId' => 1,
                                        'ActionTypeId' => 2,
                                        'Comment' => $user['FullName'] . ' change order status to '.$orderExtraStatus[1],
                                        'CrUserId' => $user['UserId'],
                                        'CrDateTime' => $updateDateTime
                                    );
                                }
                                elseif($flag2){ //xuong gui thieu
                                    $tmp = array(
                                        'OrderShopifyId' => $orderId,
                                        'OrderStatusId' => 9,
                                        'OrderExtraStatusId' => 2,
                                        'UpdateUserId' => $user['UserId'],
                                        'UpdateDateTime' => $updateDateTime
                                    );
                                    if(isset($dates['FactoryDate'])) $tmp['FactoryDate'] = $dates['FactoryDate'];
                                    if(isset($dates['WarehouseDate'])) $tmp['WarehouseDate'] = $dates['WarehouseDate'];
                                    $orderUpdates[] = $tmp;
                                    $actionLogs[] = array(
                                        'ItemId' => $orderId,
                                        'ItemTypeId' => 1,
                                        'ActionTypeId' => 2,
                                        'Comment' => $user['FullName'] . ' change order status to '.$orderExtraStatus[2],
                                        'CrUserId' => $user['UserId'],
                                        'CrDateTime' => $updateDateTime
                                    );
                                }
                            }
                        }
                        $flag = $this->Mordershopifys->updateOrderBatch($orderUpdates, $actionLogs);
                        if($flag) echo json_encode(array('code' => 1, 'message' => "Update order success"));
                        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
                    }
                    else echo json_encode(array('code' => -1, 'message' => "Data empty"));
                }
                else echo json_encode(array('code' => -1, 'message' => "Data empty"));
            }
            else echo json_encode(array('code' => -1, 'message' => "Data empty"));
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            unlink($fileUrl);
        }
        else echo json_encode(array('code' => -1, 'message' => "Please choose excel file"));
    }

    public function importExcel(){
        $user = $this->checkUserLogin(true);
        $fileUrl = trim($this->input->post('FileUrl'));
        $importTypeId = $this->input->post('ImportTypeId');
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        if ($flag && !empty($fileUrl) && in_array($importTypeId, array(1, 2, 5))){
            $this->loadModel(array('Morderproducts', 'Mordershopifys','Mshops','Mproducttypes'));
            if (ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
            $fileUrl = FCPATH . $fileUrl;
            $this->load->library('excel');
            $inputFileType = PHPExcel_IOFactory::identify($fileUrl);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($fileUrl);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $countRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

            $shopCodes = array();
            $factories = array();
            $productTypes = array();
            if($importTypeId == 5 && $countRows > 1){
                $shopCodes = $this->Mshops->getListMap();
                $factoryProductTypes = $this->Mproducttypes->getListMap();
                $factories = $factoryProductTypes['Factory'];
                $productTypes = $factoryProductTypes['ProductType'];
            }
            for ($row = 2; $row <= $countRows; $row++) {
                if($importTypeId == 1) {
                    $orderName = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $trackingCode = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
                    $orderName = trim(str_replace('#', '', $orderName));
                    if(!empty($trackingCode) && !empty($orderName)) $this->Mordershopifys->updateTracking($trackingCode, $orderName);
                }
                else if($importTypeId == 5){
                    $sku = trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue());
                    $skuExtract = explode('-', $sku);
                    if(count($skuExtract) < 3) continue;
                    $size = $skuExtract[3];
                    $productType = $skuExtract[2];
                    $sku1 = $skuExtract[0] . '-' . $skuExtract[1];
                    if(count($skuExtract) == 5) $color = '-' . $skuExtract[4];
                    else $color = '';
                    $sku2 = $skuExtract[0] . '-' . $skuExtract[1] . '-' . $skuExtract[2] . $color;
                    if(!in_array($productType, $productTypes)) continue;
                    $factoryId = $this->getFactoryIdByType($factories, $productType);
                    if($factoryId == 0) continue;
                    $orderName = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                    $orderName = trim(str_replace('#', '', $orderName));
                    $shopCode = strtoupper(substr($orderName, 0, 3));
                    $shopId = 0;
                    foreach($shopCodes as $sId => $sCode){
                        if($shopCode == strtoupper($sCode)){
                            $shopId = $sId;
                            break;
                        }
                    }
                    if($shopId == 0) continue;
                    $orderDateTime = getCurentDateTime();
                    $orderDateTimeStr = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());//11/3/2018 14:52
                    $orderDateTimeStr = explode(' ', $orderDateTimeStr);
                    if(count($orderDateTimeStr) == 2) $orderDateTime = ddMMyyyyToDate($orderDateTimeStr[0], 'm/d/Y').' '.$orderDateTimeStr[1];
                    $nameAll = trim($objWorksheet->getCellByColumnAndRow(34, $row)->getValue());
                    $strName = $this->splitName($nameAll);
                    $shipping = array(
                        'first_name' => $strName[1],
                        'address1' => trim($objWorksheet->getCellByColumnAndRow(36, $row)->getValue()),
                        'phone' => trim($objWorksheet->getCellByColumnAndRow(43, $row)->getValue()),
                        'city' => trim($objWorksheet->getCellByColumnAndRow(39, $row)->getValue()),
                        'zip' => trim($objWorksheet->getCellByColumnAndRow(40, $row)->getValue()),
                        'province' => trim($objWorksheet->getCellByColumnAndRow(41, $row)->getValue()),
                        'country' => trim($objWorksheet->getCellByColumnAndRow(42, $row)->getValue()),
                        'last_name' => $strName[0],
                        'address2' => trim($objWorksheet->getCellByColumnAndRow(37, $row)->getValue()),
                        'company' => trim($objWorksheet->getCellByColumnAndRow(38, $row)->getValue()),
                        'latitude' => '',
                        'longitude' => '',
                        'name' => $nameAll,
                        'country_code' => trim($objWorksheet->getCellByColumnAndRow(42, $row)->getValue()),
                        'province_code' => trim($objWorksheet->getCellByColumnAndRow(41, $row)->getValue()),
                    );
                    $orderShopify = array(
                        'OrderCode' =>'',
                        'OrderName' => $orderName,
                        'ShopId' => $shopId,
                        'Comment' => '',
                        'ParentOrderId' => 0,
                        'HasChild' => 0,
                        'TotalPrice' => trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue()),
                        'Customer' => '[]',
                        'OrderDateTime' => $orderDateTime,
                        'CountryName' => trim($objWorksheet->getCellByColumnAndRow(32, $row)->getValue()),
                        'TrackingCode' => '',
                        'TransportStatusId' => 1,
                        'OrderStatusId' => 1,
                        'OrderExtraStatusId' => 0,
                        'Shipping' => json_encode($shipping),
                        'CustomerName' => '',
                        'CustomerEmail' => trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()),
                        'FactoryId' => $factoryId,
                        'ItemIds' => '[]',
                        'IsSendTracking' => 1,
                        'CrDateTime' => getCurentDateTime(),
                    );
                    $orderShopifyId = $this->Mordershopifys->save($orderShopify);
                    if($orderShopifyId > 0){
                        $orderProducts = array(
                            'OrderShopifyId' => $orderShopifyId,
                            'ProductName' => trim($objWorksheet->getCellByColumnAndRow(17, $row)->getValue()),
                            'Price' => trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()),
                            'Sku' => $sku,
                            'Quantity' => trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()),
                            'Size' => $size,
                            'ProductType' => $productType,
                            'Sku1' => $sku1,
                            'Sku2' => $sku2,
                            'FactoryQuantity' => 0,
                            'WarehouseQuantity' => 0
                        );

                        if(!empty($orderProducts)) $this->Morderproducts->save($orderProducts);
                    }
                }
                else{
                    $orderName = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                    $orderName = trim(str_replace('#', '', $orderName));
                    if(!empty($orderName)) $this->Mordershopifys->updateStatus(4, $orderName);
                }
            }
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            unlink($fileUrl);
            if($importTypeId == 5) echo json_encode(array('code' => 1, 'message'=> "Import Order Success"));
            else echo json_encode(array('code' => 1, 'message' => $importTypeId == 1 ? "Update Tracking code Success" : "Update Waiting Order Success"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Please choose excel file"));
    }

    private function splitName($name) {
        $parts = explode(" ", trim($name));
        $num = count($parts);
        if($num > 1) $lastname = array_pop($parts);
        else $lastname = '';
        $firstname = implode(" ", $parts);
        return array($firstname, $lastname);
    }

    public function exportExcel(){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        $orderIds = $this->input->post('OrderIds');
        if($flag && !empty($orderIds)) {
            $this->loadModel(array('Mordershopifys', 'Morderproducts'));
            $orderIds = explode(',', $orderIds);
            $listOrders = $this->Mordershopifys->getByIds($orderIds);
            $fileUrl = FCPATH . 'assets/uploads/excels/exportOrder.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach($listOrders as $o) {
                $listOrderproducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $o['OrderShopifyId']));
                $shipping = json_decode($o['Shipping'], true);
                $customer = json_decode($o['Customer'], true);
                $listSku = '';
                $value = 0;
                $weight = 0;
                $qty = '';
                foreach ($listOrderproducts as $lo) {
                    $listSku .= '打印' . $lo['Sku'] . ' ;';
                    $value += 10 * $lo['Quantity'];
                    $weight += 0.5 * $lo['Quantity'];
                    $qty .= $lo['Quantity'] . ';';

                }
                $sheet->setCellValue('A' . $i, '#' . $o['OrderName']);
                $sheet->setCellValue('B' . $i, '#' . $o['OrderName'] . '-' . $shipping['name']);
                $sheet->setCellValue('C' . $i, $shipping['address1'].', '.$shipping['address2']);
                $sheet->setCellValue('D' . $i, $shipping['city']);
                $sheet->setCellValue('E' . $i, $shipping['province_code']);
                $sheet->setCellValueExplicit('F' . $i, $shipping['zip'], PHPExcel_Cell_DataType::TYPE_STRING);
                $sheet->setCellValue('G' . $i, $shipping['country_code']);
                $sheet->setCellValue('H' . $i, $shipping['phone']);
                //$sheet->setCellValue('I' . $i, $customer['email']);
                $sheet->setCellValue('I' . $i, '');
                if(isset($customer['id'])) $sheet->setCellValue('J' . $i, $customer['id']);
                $sheet->setCellValue('K' . $i, rtrim($listSku, ' ;'));
                $sheet->setCellValue('M' . $i, $value);
                $sheet->setCellValue('N' . $i, $weight);
                $sheet->setCellValue('O' . $i, rtrim($qty, ';'));
                $i++;
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            $filename = "Export_Courier_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }

    public function exportExcel2(){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        $orderIds = $this->input->post('OrderIds');
        if($flag && !empty($orderIds)) {
            $this->loadModel(array('Mordershopifys', 'Morderproducts'));
            $orderIds = explode(',', $orderIds);
            $listOrders = $this->Mordershopifys->getByIds($orderIds);
            $fileUrl = FCPATH . 'assets/uploads/excels/exportOrder2.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach($listOrders as $o) {
                $listOrderproducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $o['OrderShopifyId']));
                foreach ($listOrderproducts as $lo) {
                    $skuExtract = explode('-', $lo['Sku']);
                    $size = '';
                    if(count($skuExtract) >= 4) $size =  $skuExtract[3];
                    $sheet->setCellValue('A' . $i, '#' . $o['OrderName']);
                    $sheet->setCellValue('B' . $i, $lo['Sku']);
                    $sheet->setCellValue('C' . $i, $size);
                    $sheet->setCellValue('D' . $i, $lo['Quantity']);
                    $i++;
                }
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            $filename = "Export_Cross_Check_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }

    public function exportExcelOrderAll($shopId){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('orderall', $user['UserId'], $user['RoleId']);
        $shopId = intval($shopId);
        if($flag && !empty($shopId)) {
            $this->loadModel(array('Morderalls'));
            $listOrders = $this->Morderalls->getBy(array('ShopId' => $shopId));
            $fileUrl = FCPATH . 'assets/uploads/excels/exportOrder2.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach($listOrders as $o) {
                $Json = json_decode($o['Json'],true);
                foreach ($Json['line_items'] as $lo) {
                    $skuExtract = explode('-', $lo['sku']);
                    $size = '';
                    if(count($skuExtract) > 0) $size =  $skuExtract[count($skuExtract) - 1];
                    $sheet->setCellValue('A' . $i, $Json['name']);
                    $sheet->setCellValue('B' . $i, $lo['sku']);
                    $sheet->setCellValue('C' . $i, $size);
                    $sheet->setCellValue('D' . $i, $lo['quantity']);
                    $i++;
                }
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            $filename = "Export_Cross_Check_All_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }

    public function exportExcel3(){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        $orderIds = $this->input->post('OrderIds');
        if ($flag && !empty($orderIds)) {
            $this->load->model('Mordershopifys');
            $orderIds = explode(',', $orderIds);
            $order = $this->Mordershopifys->getByIds($orderIds);
            if ($order) {
                $fileUrl = FCPATH . 'assets/uploads/excels/exportOrder3.xls';
                $this->load->library('excel');
                $objReader = PHPExcel_IOFactory::createReader('Excel5');
                $objPHPExcel = $objReader->load($fileUrl);
                $objPHPExcel->setActiveSheetIndex(0);
                $sheet = $objPHPExcel->getActiveSheet();
                $i = 2;
                foreach ($order as $k => $o) {
                    $sheet->setCellValue('A' . $i, '#' . $o['OrderName']);
                    $sheet->setCellValue('C' . $i, $o['TrackingCode']);
                    $sheet->setCellValue('D' . $i, 'https://www.17track.net/en/track?nums=' . $o['TrackingCode']);
                    $sheet->getCell('D' . $i)->getHyperlink()->setUrl('https://www.17track.net/en/track?nums=' . $o['TrackingCode']);
                    $i++;
                }
                $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(true);
                // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
                $filename = "Export_Tracking_".date('Y-m-d').".xls";
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                $objPHPExcel->disconnectWorksheets();
                unset($objPHPExcel);
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    public function exportScanExcel($typeId = 4){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('ordershopify', $user['UserId'], $user['RoleId']);
        if ($flag){
            if(!in_array($typeId, array(4, 5))) $typeId = 4;
            $fileUrl = FCPATH . "assets/uploads/excels/exportOrder{$typeId}.xls";
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 3;
            $this->load->model('Morderproducts');
            if($typeId == 4) $listOrderProducts = $this->Morderproducts->getByOrderStatusId(7);
            else $listOrderProducts = $this->Morderproducts->getByOrderStatusId(array(8, 9, 11));
            foreach($listOrderProducts as $op){
                $sheet->setCellValue('A' . $i, $i - 2);
                $sheet->setCellValue('B' . $i, '#'.str_replace('-'.$op['Sku'], '', $op['BarCode']));
                $sheet->setCellValue('C' . $i, $op['Sku']);
                $sheet->setCellValue('D' . $i, '#'.$op['BarCode']);
                $sheet->setCellValue('E' . $i, $op['Size']);
                $sheet->setCellValue('F' . $i, $op['Quantity']);
                $sheet->setCellValue('J' . $i, 'delivered');
                $i++;
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            if($typeId == 4) $filename = "Export_Factory_Scan_".date('Y-m-d').".xls";
            else $filename = "Export_Warehouse_Scan_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }
}
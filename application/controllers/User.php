<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function index(){
		if(!$this->session->userdata('user')){
			$data = array('title' => 'Login');
			if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
			$this->load->helper('cookie');
			$data['userName'] = $this->input->cookie('userName', true);
			$data['userPass'] = $this->input->cookie('userPass', true);
			$this->load->view('user/login', $data);
		}
		else redirect('user/dashboard');
	}

	public function logout(){
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
		redirect('user');
	}

	public function forgotPass(){
		$this->load->view('user/forgotpass', array('title' => 'Forgot Password'));
	}

	public function sendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Hi {$user['FullName']}<br/>Pleae go to link ".base_url('user/changePass/'.$token).' to change password.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'In3d.manager@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Teeallover';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Forgot password', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					$this->load->view('user/forgotpass', array('title' => 'Forgot Password', 'txtSuccess' => 'Please check your email'));
				}
			}
			else $this->load->view('user/forgotpass', array('title' => 'Forgot Password', 'txtError' => 'User is not existed'));
		}
		else $this->load->view('user/forgotpass', array('title' => 'Forgot Password', 'txtError' => 'Email can not be left blank'));
	}

	public function changePass($token = ''){
		$data = array('title' => 'Change Password', 'token' => $token);
		$isWrongToken = true;
		if(!empty($token)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Token' => $token), true, "", "UserId");
			if($user){
				if($this->input->post('UserPass')) {
					$postData = $this->arrayFromPost(array('UserPass', 'RePass'));
					if (!empty($postData['UserPass']) && $postData['UserPass'] == $postData['RePass']) {
						$this->Musers->save(array('UserPass' => md5($postData['UserPass']), 'Token' => ''), $user['UserId'], array('Token'));
						$this->session->set_flashdata('txtSuccess', "Change Password success");
						redirect('user');
						exit();
					}
					else $data['txtError'] = "Password not match";
				}
			}
			else {
				$data['txtError'] = "Token is wrong";
				$isWrongToken = false;
			}
		}
		else {
			$data['txtError'] = "Token is wrong";
			$isWrongToken = false;
		}
		$data['isWrongToken'] = $isWrongToken;
		$this->load->view('user/changepass', $data);
	}

	public function dashboard(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Dashboard'
			//array('scriptFooter' => array('js' => array('vendor/plugins/jquery.matchHeight.js', 'js/dashboard.js')))
		);
		$this->load->view('user/dashboard', $data);
	}

	public function permission(){
		$this->load->view('user/permission');
	}

	public function profile(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'User Profile - '.$user['FullName'],
			array(
				//'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array(/*'vendor/plugins/datepicker/bootstrap-datepicker.js', */'ckfinder/ckfinder.js', 'js/user_profile.js'))
			)
		);
		/*$this->loadModel(array('Mprovinces', 'Mdistricts'));
		$data['listProvinces'] = $this->Mprovinces->getList();*/
		$this->load->view('user/profile', $data);
	}

	public function staff(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'User List',
			array('scriptFooter' => array('js' => 'js/user_list.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'user/staff')) {
			$data['deleteUser'] = true;// $this->Mactions->checkAccess($listActions, 'user/delete');
			$data['changeStatus'] = true;// $this->Mactions->checkAccess($listActions, 'user/edit');
			/*$this->load->model('Mprovinces');
			$data['listProvinces'] = $this->Mprovinces->getList();*/
			$postData = $this->arrayFromPost(array('UserName', 'FullName', 'Email', 'PhoneNumber', 'StatusId', 'GenderId', 'RoleId'));
			$rowCount = $this->Musers->getCount($postData);
			$data['listUsers'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listUsers'] = $this->Musers->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('user/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Add User',
			array(
				//'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array(/*'vendor/plugins/datepicker/bootstrap-datepicker.js', */'ckfinder/ckfinder.js', 'js/user_update.js'))
			)
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'user/staff')) {
			$this->loadModel(array('Mfactories', 'Mteams'));
			$data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('user/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($userId = 0){
		if($userId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Edit User',
				array(
					//'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array(/*'vendor/plugins/datepicker/bootstrap-datepicker.js', */'ckfinder/ckfinder.js', 'js/user_update.js'))
				)
			);
			$userEdit = $this->Musers->get($userId);
			if ($userEdit) {
				$listActions = $data['listActions'];
				if ($this->Mactions->checkAccess($listActions, 'user/staff')) {
					$data['canEdit'] = $this->Mactions->checkAccess($listActions, 'user/staff');
					$data['userId'] = $userId;
					$data['userEdit'] = $userEdit;
					$this->loadModel(array('Mfactories', 'Mteams', 'Muserteams'));
					$data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
					$data['teamIds'] = $this->Muserteams->getTeamIds($userId);
					$this->load->view('user/edit', $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else {
				$data['userId'] = 0;
				$data['txtError'] = "User not found";
				$this->load->view('user/edit', $data);
			}
		}
		else redirect('user/profile');
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Warehouse',
			array('scriptFooter' => array('js' => 'js/warehouse.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'warehouse')) {
			$this->load->model('Mwarehouses');
			$data['listWarehouses'] = $this->Mwarehouses->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/warehouse', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('WarehouseName'));
		if(!empty($postData['WarehouseName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$warehouseId = $this->input->post('WarehouseId');
			$this->load->model('Mwarehouses');
			$flag = $this->Mwarehouses->save($postData, $warehouseId);
			if ($flag > 0) {
				$postData['WarehouseId'] = $flag;
				$postData['IsAdd'] = ($warehouseId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Update Warehouse success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$warehouseId = $this->input->post('WarehouseId');
		if($warehouseId > 0){
			$this->load->model('Mwarehouses');
			$flag = $this->Mwarehouses->changeStatus(0, $warehouseId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete Warehouse success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

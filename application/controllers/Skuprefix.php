<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skuprefix extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Sku Prefix List',
			array('scriptFooter' => array('js' => 'js/skuprefix.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($data['listActions'], 'skuprefix')) {
			$this->load->model('Mskuprefixs');
			$data['listSkuprefixs'] = $this->Mskuprefixs->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/skuprefix', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('SkuPrefixName'));
		if(!empty($postData['SkuPrefixName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$skuPrefixId = $this->input->post('SkuPrefixId');
			$this->load->model('Mskuprefixs');
			$flag = $this->Mskuprefixs->save($postData, $skuPrefixId);
			if ($flag > 0) {
				$postData['SkuPrefixId'] = $flag;
				$postData['IsAdd'] = ($skuPrefixId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Update sku prefix success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$skuPrefixId = $this->input->post('SkuPrefixId');
		if($skuPrefixId > 0){
			$this->load->model('Mskuprefixs');
			$flag = $this->Mskuprefixs->changeStatus(0, $skuPrefixId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete sku prefix success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

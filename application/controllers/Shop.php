<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller { 

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Shop List',
			array('scriptFooter' => array('js' => 'js/shop_list.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'shop')) {
			$this->load->model('Mshops');
			$postData = $this->arrayFromPost(array('ShopCode', 'ShopName', 'ShopUrl'));
			$rowCount = $this->Mshops->getCount($postData);
			$data['listShops'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listShops'] = $this->Mshops->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('shop/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Add Shop',
			array('scriptFooter' => array('js' => array('js/shop_update.js')))
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'shop')) {
			$this->load->model('Mteams');
			$data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('shop/add', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function edit($shopId = 0){
		if($shopId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Edit Shop',
				array('scriptFooter' => array('js' => array('js/shop_update.js')))
			);
			$this->loadModel(array('Mshops'));
			$shop = $this->Mshops->get($shopId);
			if ($shop) {
				if($this->Mactions->checkAccess($data['listActions'], 'shop')) {
					$data['shopId'] = $shopId;
					$data['shop'] = $shop;
					$this->load->model('Mteams');
					$data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
					$this->load->view('shop/edit', $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else {
				$data['shopId'] = 0;
				$data['txtError'] = "Shop is not found";
				$this->load->view('shop/edit', $data);
			}
		}
		else redirect('shop');
	}

	public function update(){
	    $user = $this->checkUserLogin(true);
	    $postData = $this->arrayFromPost(array('ShopCode', 'ShopName', 'ShopUrl', 'StatusId', 'ApiKey', 'ApiSecret', 'TeamId'));
	    if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
	    if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
	    $shopId = $this->input->post('ShopId');
	    if($shopId > 0){
	        $postData['UpdateUserId'] = $user['UserId'];
	        $postData['UpdateDateTime'] = getCurentDateTime();
	    }
	    else{
	        $postData['CrUserId'] = $user['UserId'];
	        $postData['CrDateTime'] = getCurentDateTime();
	    }
	    $this->load->model('Mshops');
	    $shopId = $this->Mshops->save($postData, $shopId);
	    if($shopId > 0) echo json_encode(array('code' => 1, 'message' => "Update shop success", 'data' => $shopId));
	    else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$shopId = $this->input->post('ShopId');
		$statusId = $this->input->post('StatusId');
		if($shopId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$this->load->model('Mshops');
			$flag = $this->Mshops->changeStatus($statusId, $shopId, '', $user['UserId']);
			if($flag) {
				$txtSuccess = "";
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Delete shop success";
				else{
					$txtSuccess = "Change status success";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}
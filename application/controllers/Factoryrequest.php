<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Factoryrequest extends MY_Controller{

    private $limitPerExcel = 30;

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Factory Request Order',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/factoryrequest_list.js')),
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'factoryrequest')) {
            $data['limitPerExcel'] = $this->limitPerExcel;
            $this->loadModel(array('Mfactories', 'Mfactoryrequests'));
            $listFactories = array();
            $postData = $this->arrayFromPost(array('RequestStatusId', 'FactoryId', 'BeginDate', 'EndDate'));
            if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
            if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
            if ($user['RoleId'] == 1) $listFactories = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
            elseif ($user['RoleId'] == 3 && $user['FactoryId'] > 0) {
                $postData['FactoryId'] = $user['FactoryId'];
                $data['title'] = 'Request Order for ' . $this->Mfactories->getFieldValue(array('FactoryId' => $user['FactoryId']), 'FactoryName');
            }
            $data['listFactories'] = $listFactories;
            $rowCount = $this->Mfactoryrequests->getCount($postData);
            $data['listFactoryRequests'] = array();
            if ($rowCount > 0) {
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if (!is_numeric($page) || $page < 1) $page = 1;
                $data['listFactoryRequests'] = $this->Mfactoryrequests->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('factoryrequest/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function show($factoryRequestId = 0){
        if ($factoryRequestId > 0) {
            $user = $this->checkUserLogin();
            $data = $this->commonData($user,
                'Request Order Detail',
                array('scriptFooter' => array('js' => array('vendor/plugins/lazyload/jquery.lazyload.min.js', 'js/factoryrequest.js')))
            );
            if ($this->Mactions->checkAccess($data['listActions'], 'factoryrequest')) {
                $this->loadModel(array('Mfactoryrequests', 'Mordershopifys', 'Mteams'));
                $factoryRequest = $this->Mfactoryrequests->get($factoryRequestId);
                if ($factoryRequest) {
                    if($user['FactoryId'] > 0 && $user['FactoryId'] != $factoryRequest['FactoryId']) $this->load->view('user/permission', $data);
                    else {
                        $data['factoryRequestId'] = $factoryRequestId;
                        $data['factoryRequest'] = $factoryRequest;
                        $data['canEdit'] = $factoryRequest['RequestStatusId'] == 1;
                        $data['limitPerExcel'] = $this->limitPerExcel;
                        $listFactoryOrders = array();
                        $listFactoryOrderRaws = $this->Mfactoryrequests->getSumProductBySize($factoryRequestId);
                        foreach ($listFactoryOrderRaws as $fo) {
                            $flag = false;
                            if (isset($listFactoryOrders[$fo['Sku']])) {
                                if (isset($listFactoryOrders[$fo['Sku']][$fo['Size']])) $listFactoryOrders[$fo['Sku']][$fo['Size']]['Quantity']++;
                                else $flag = true;
                            }
                            else $flag = true;
                            if ($flag) {
                                $listFactoryOrders[$fo['Sku']][$fo['Size']] = array(
                                    'ProductId' => $fo['ProductId'],
                                    'ProductImage' => $fo['ProductImage'],
                                    'ProductImage2' => $fo['ProductImage2'],
                                    'Quantity' => $fo['Quantity']
                                );
                            }
                        }
                        $data['listFactoryOrders'] = $listFactoryOrders;
                        $data['listSumQuantityWithPrpoductType'] = $this->Mfactoryrequests->getSumProductByType($factoryRequestId);
                        $data['listOrderShopifys'] = $this->Mordershopifys->getListByRequest($factoryRequestId);
                        $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
                    }
                }
                else {
                    $data['factoryRequestId'] = 0;
                    $data['canEdit'] = false;
                    $data['txtError'] = "Request not found";
                }
                $this->load->view('factoryrequest/show', $data);
            }
            else $this->load->view('user/permission', $data);
        }
        else redirect('factoryrequest');
    }

    public function exportPdf($factoryRequestId = 0){
        //$this->genBarcode('99-DCC1011-DCC-A0019-LMS-XXL');
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0) {
            $this->load->model('Mfactoryrequests');
            $factoryRequest = $this->Mfactoryrequests->get($factoryRequestId);
            if ($factoryRequest && $factoryRequest['RequestStatusId'] == 2) {
                $listQuantities = $this->Mfactoryrequests->getSumProductByBarCode($factoryRequestId, $factoryRequest['FactoryId']);
                $listBarcode = array();
                foreach ($listQuantities as $q) {
                    $this->genBarcode($q['BarCode'], 'code128', 70);
                    for ($i = 0; $i < $q['sumQuantity']; $i++) {
                        $listBarcode[] = base_url(PRODUCT_PATH . 'barcodes/' . $q['BarCode'] . '.png');
                    }
                }
                $listBarcode = array_chunk($listBarcode, 3);
                $this->load->view('factoryrequest/exportPdf', array('listBarcode' => $listBarcode));
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    public function exportQRCode($factoryRequestId = 0){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0) {
            $this->load->model('Mfactoryrequests');
            $factoryRequest = $this->Mfactoryrequests->get($factoryRequestId);
            if ($factoryRequest && $factoryRequest['RequestStatusId'] == 2) {
                $this->load->library('ciqrcode');
                $listQuantities = $this->Mfactoryrequests->getSumProductByBarCode($factoryRequestId, $factoryRequest['FactoryId']);
                $listBarcode = array();
                $j = 0;
                foreach ($listQuantities as $q) {
                    $params['data'] = $q['BarCode'];
                    $params['level'] = 'H';
                    $params['size'] = 6;
                    $params['savename'] = "./" . PRODUCT_PATH . "qr/{$q['BarCode']}.png";
                    $this->ciqrcode->generate($params);
                    for ($i = 0; $i < $q['sumQuantity']; $i++) {
                        $j++;
                        $listBarcode[$j]['src'] = $params['savename'];
                        $listBarcode[$j]['code'] = $q['BarCode'];
                    }
                }
                $listBarcode = array_chunk($listBarcode, 3);
                $this->load->view('factoryrequest/export_qr', array('listBarcode' => $listBarcode));
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }

    public function exportIMGQRCode($factoryRequestId = 0){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0) {
            $this->load->model('Mfactoryrequests');
            $factoryRequest = $this->Mfactoryrequests->get($factoryRequestId);
            if ($factoryRequest && $factoryRequest['RequestStatusId'] == 2) {
                $this->load->library('ciqrcode');
                $listQuantities = $this->Mfactoryrequests->getSumProductByBarCode($factoryRequestId, $factoryRequest['FactoryId']);
                $listBarcode = array();
                $j = 0;
                foreach ($listQuantities as $q) {
                    $params['data'] = $q['BarCode'];
                    $params['level'] = 'H';
                    $params['size'] = 6;
                    $params['savename'] = "./" . PRODUCT_PATH . "qr/{$q['BarCode']}.png";
                    $this->ciqrcode->generate($params);
                    for ($i = 0; $i < $q['sumQuantity']; $i++) {
                        $j++;
                        $listBarcode[$j]['src'] = $params['savename'];
                        $listBarcode[$j]['code'] = $q['BarCode'];
                        $listBarcode[$j]['productImage'] = $q['ProductImage'];
                    }
                }
                $listBarcode = array_chunk($listBarcode, 3);
                $this->load->view('factoryrequest/export_img_qr', array('listBarcode' => $listBarcode));
            }
            else echo "<script>window.close();</script>";
        }
        else echo "<script>window.close();</script>";
    }


    public function insert(){
        $user = $this->checkUserLogin(true);
        $factoryId = $this->input->post('FactoryId');
        $orderShopifyIds = $this->input->post('OrderShopifyIds');
        if($factoryId > 0 && !empty($orderShopifyIds)) {
            $postData = array(
                'RequestCode' => 'RQ-' . date('YmdHi'),
                'RequestDate' => getCurentDateTime(),
                'FactoryId' => $factoryId,
                'Comment' => trim($this->input->post('Comment')),
                'RequestStatusId' => 1,
                'CrUserId' => $user['UserId']
            );
            $factoryRequestId = $this->input->post('FactoryRequestId');
            $this->load->model('Mfactoryrequests');
            $flag = $this->Mfactoryrequests->insert($postData, $orderShopifyIds, $factoryRequestId, $user);
            if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Request to factory success"));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    public function changeStatus(){
        $user = $this->checkUserLogin(true);
        $factoryRequestId = $this->input->post('FactoryRequestId');
        if ($factoryRequestId > 0) {
            $this->loadModel(array('Mfactoryorders', 'Mfactoryrequests'));
            $orderIds = $this->Mfactoryorders->getListOrderIds($factoryRequestId);
            $flag = $this->Mfactoryrequests->manufacturing($factoryRequestId, $orderIds, $user);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Manufacturing success"));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    public function deleteBy(){
        $user = $this->checkUserLogin(true);
        $factoryRequestId = intval($this->input->post('FactoryRequestId'));
        if($factoryRequestId > 0){
            $this->loadModel(array('Mfactoryorders', 'Mfactoryrequests'));
            $requestStatusId = $this->Mfactoryrequests->getFieldValue(array('FactoryRequestId' => $factoryRequestId), 'RequestStatusId', 0);
            if($requestStatusId == 1) {
                $orderIds = $this->Mfactoryorders->getListOrderIds($factoryRequestId);
                $flag = $this->Mfactoryrequests->deleteBy($factoryRequestId, $orderIds, $user);
                if ($flag) echo json_encode(array('code' => 1, 'message' => "Remove Order Success"));
                else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
            }
            else echo json_encode(array('code' => -1, 'message' => "This request can not delete"));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }

    public function deleteOrder(){
        $user = $this->checkUserLogin(true);
        $orderShopifyId = intval($this->input->post('OrderShopifyId'));
        $factoryRequestId = intval($this->input->post('FactoryRequestId'));
        if($orderShopifyId && $orderShopifyId > 0){
            $this->load->model('Mfactoryrequests');
            $flag = $this->Mfactoryrequests->deleteOrder($factoryRequestId, $orderShopifyId, $user);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Remove Order Success"));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    public function exportOrderExcel($exportTypeId = 0, $factoryRequestId = 0){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0 && in_array($exportTypeId, array(1, 2))) {
            $this->load->model(array('Mordershopifys', 'Morderproducts'));
            $listOrders = $this->Mordershopifys->getListOrderByRequest($factoryRequestId);
            $fileUrl = FCPATH . 'assets/uploads/excels/'.($exportTypeId == 1 ? 'exportOrder' : 'exportOrder2').'.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach($listOrders as $o) {
                $listOrderproducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $o['OrderShopifyId']));
                if($exportTypeId == 1){
                    $shipping = json_decode($o['Shipping'], true);
                    $customer = json_decode($o['Customer'], true);
                    $listSku = '';
                    $value = 0;
                    $weight = 0;
                    $qty = '';
                    foreach ($listOrderproducts as $lo) {
                        $listSku .= '打印' . $lo['Sku'] . ' ;';
                        $value += 10 * $lo['Quantity'];
                        $weight += 0.5 * $lo['Quantity'];
                        $qty .= $lo['Quantity'] . ';';

                    }
                    $sheet->setCellValue('A' . $i, '#' . $o['OrderName']);
                    $sheet->setCellValue('B' . $i, '#' . $o['OrderName'] . '-' . $shipping['name']);
                    $sheet->setCellValue('C' . $i, $shipping['address1']);
                    $sheet->setCellValue('D' . $i, $shipping['city']);
                    $sheet->setCellValue('E' . $i, $shipping['province_code']);
                    $sheet->setCellValueExplicit('F' . $i, $shipping['zip'], PHPExcel_Cell_DataType::TYPE_STRING);
                    $sheet->setCellValue('G' . $i, $shipping['country_code']);
                    $sheet->setCellValue('H' . $i, $shipping['phone']);
                    //$sheet->setCellValue('I' . $i, $customer['email']);
                    $sheet->setCellValue('I' . $i, '');
                    if(isset($customer['id'])) $sheet->setCellValue('J' . $i, $customer['id']);
                    $sheet->setCellValue('K' . $i, rtrim($listSku, ' ;'));
                    $sheet->setCellValue('M' . $i, $value);
                    $sheet->setCellValue('N' . $i, $weight);
                    $sheet->setCellValue('O' . $i, rtrim($qty, ';'));
                    $i++;
                }
                else{
                    foreach ($listOrderproducts as $lo) {
                        $sheet->setCellValue('A' . $i, '#' . $o['OrderName']);
                        $sheet->setCellValue('B' . $i, $lo['Sku']);
                        $sheet->setCellValue('D' . $i, $lo['Quantity']);
                        $i++;
                    }
                }
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            if($exportTypeId == 1) $filename = "Export_Courier_".date('Y-m-d').".xls";
            else $filename = "Export_Cross_Check_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }

    public function exportScanExcel($typeId = 4, $factoryRequestId = 0){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0) {
            if(!in_array($typeId, array(4, 5))) $typeId = 4;
            $fileUrl = FCPATH . "assets/uploads/excels/exportOrder{$typeId}.xls";
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 3;
            $this->load->model('Morderproducts');
            $listOrderProducts = $this->Morderproducts->getByFactoryRequestId($factoryRequestId);
            foreach($listOrderProducts as $op){
                $sheet->setCellValue('A' . $i, $i - 2);
                $sheet->setCellValue('B' . $i, '#'.str_replace('-'.$op['Sku'], '', $op['BarCode']));
                $sheet->setCellValue('C' . $i, $op['Sku']);
                $sheet->setCellValue('D' . $i, '#'.$op['BarCode']);
                $sheet->setCellValue('E' . $i, $op['Size']);
                $sheet->setCellValue('F' . $i, $op['Quantity']);
                $sheet->setCellValue('J' . $i, 'delivered');
                $i++;
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            // foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            if($typeId == 4) $filename = "Export_Factory_Scan_".date('Y-m-d').".xls";
            else $filename = "Export_Warehouse_Scan_".date('Y-m-d').".xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }

    public function countSku(){
        $this->checkUserLogin(true);
        $factoryRequestId = intval($this->input->post('FactoryRequestId'));
        if($factoryRequestId > 0){
            $this->load->model('Mfactoryrequests');
            $listFactoryOrderRaws = $this->Mfactoryrequests->getSumProductBySize($factoryRequestId);
            $listFactoryOrders = array();
            foreach ($listFactoryOrderRaws as $fo) {
                $flag = false;
                if (isset($listFactoryOrders[$fo['Sku']])) {
                    if (isset($listFactoryOrders[$fo['Sku']][$fo['Size']])) $listFactoryOrders[$fo['Sku']][$fo['Size']]['Quantity']++;
                    else $flag = true;
                }
                else $flag = true;
                if ($flag) {
                    $listFactoryOrders[$fo['Sku']][$fo['Size']] = array(
                        'ProductId' => $fo['ProductId'],
                        'ProductImage' => $fo['ProductImage'],
                        'ProductImage2' => $fo['ProductImage2'],
                        'Quantity' => $fo['Quantity']
                    );
                }
            }
            echo json_encode(array('code' => 1, 'data' => count($listFactoryOrders)));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }

    public function sumQuantity(){
        $this->checkUserLogin(true);
        $factoryRequestId = intval($this->input->post('FactoryRequestId'));
        if($factoryRequestId > 0){
            $this->load->model('Mfactoryrequests');
            $quantity = $this->Mfactoryrequests->getSumQuantity($factoryRequestId);
            echo json_encode(array('code' => 1, 'data' => $quantity));
        }
        else echo json_encode(array('code' => -1, 'data' => 0));
    }

    public function exportExcel($factoryRequestId = 0, $file = 0){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
        if($flag && $factoryRequestId > 0) {
            ini_set('memory_limit', '-1');
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $fileUrl = FCPATH . 'assets/uploads/excels/exportFactory.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $listFactoryOrders = array();
            $listSumQuantityWithPrpoductType = array();
            $count = 0;
            $step = $this->limitPerExcel;
            $lastFile = false;
            $this->load->model('Mfactoryrequests');
            $factoryRequest = $this->Mfactoryrequests->get($factoryRequestId);
            if ($factoryRequest) {
                $listFactoryOrderRaws = $this->Mfactoryrequests->getSumProductBySize($factoryRequestId);
                foreach ($listFactoryOrderRaws as $fo) {
                    $flag = false;
                    if (isset($listFactoryOrders[$fo['Sku']])) {
                        if (isset($listFactoryOrders[$fo['Sku']][$fo['Size']])) $listFactoryOrders[$fo['Sku']][$fo['Size']]['Quantity']++;
                        else $flag = true;
                    }
                    else $flag = true;
                    if ($flag) {
                        $listFactoryOrders[$fo['Sku']][$fo['Size']] = array(
                            'ProductId' => $fo['ProductId'],
                            'ProductImage' => $fo['ProductImage'],
                            'ProductImage2' => $fo['ProductImage2'],
                            'Quantity' => $fo['Quantity']
                        );
                    }
                }
                $listSumQuantityWithPrpoductType = $this->Mfactoryrequests->getSumProductByType($factoryRequestId);;
                $totalSku = count($listFactoryOrders);
                if($file == ceil($totalSku/$step)) $lastFile = true;
            }
            $border = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );
            $i = 2;
            $sumColoumn = array(
                'S' => 0,
                'M' => 0,
                'L' => 0,
                'XL' => 0,
                '2XL' => 0,
                '3XL' => 0,
                '4XL' => 0,
                '5XL' => 0,
                '6XL' => 0,
                'KIDS' => 0,
                'B' => 0,
                'Q' => 0,
                'SM' => 0,
                'TB' => 0,
                'TW' => 0,
                'UK' => 0,
                'UQ' => 0,
                'FL' => 0,
                'US' => 0,
                'all' => 0
            );
            foreach($listFactoryOrders as $sku => $fo) {
                $count++;
                $sumQuantity = 0;
                $productId = 0;
                $productImage = '';
                $productImage2 = '';
                $sizeQuantity = array();
                foreach ($fo as $size => $info) {
                    $sumQuantity += $info['Quantity'];
                    $productId = $info['ProductId'];
                    $productImage = $info['ProductImage'];
                    $productImage2 = $info['ProductImage2'];
                    $sizeQuantity[strtoupper($size)] = $info['Quantity'];
                }
                if (isset($sizeQuantity['S'])) $sumColoumn['S'] += $sizeQuantity['S'];
                if (isset($sizeQuantity['M'])) $sumColoumn['M'] += $sizeQuantity['M'];
                if (isset($sizeQuantity['L'])) $sumColoumn['L'] += $sizeQuantity['L'];
                if (isset($sizeQuantity['XL'])) $sumColoumn['XL'] += $sizeQuantity['XL'];
                $xxl = 0;
                if (isset($sizeQuantity['2XL'])) $xxl += $sizeQuantity['2XL'];
                if (isset($sizeQuantity['XXL'])) $xxl += $sizeQuantity['XXL'];
                $sumColoumn['2XL'] += $xxl;
                $xxxl = 0;
                if (isset($sizeQuantity['3XL'])) $xxxl += $sizeQuantity['3XL'];
                if(isset($sizeQuantity['XXXL'])) $xxxl += $sizeQuantity['XXXL'];
                $sumColoumn['3XL'] += $xxxl;
                if (isset($sizeQuantity['4XL'])) $sumColoumn['4XL'] += $sizeQuantity['4XL'];
                if (isset($sizeQuantity['5XL'])) $sumColoumn['5XL'] += $sizeQuantity['5XL'];
                if (isset($sizeQuantity['6XL'])) $sumColoumn['6XL'] += $sizeQuantity['6XL'];

                if (isset($sizeQuantity['KIDS'])) $sumColoumn['KIDS'] += $sizeQuantity['KIDS'];
                if (isset($sizeQuantity['B'])) $sumColoumn['B'] += $sizeQuantity['B'];
                if (isset($sizeQuantity['Q'])) $sumColoumn['Q'] += $sizeQuantity['Q'];
                if (isset($sizeQuantity['SM'])) $sumColoumn['SM'] += $sizeQuantity['SM'];
                if (isset($sizeQuantity['TB'])) $sumColoumn['TB'] += $sizeQuantity['TB'];
                if (isset($sizeQuantity['TW'])) $sumColoumn['TW'] += $sizeQuantity['TW'];
                if (isset($sizeQuantity['UK'])) $sumColoumn['UK'] += $sizeQuantity['UK'];
                if (isset($sizeQuantity['UQ'])) $sumColoumn['UQ'] += $sizeQuantity['UQ'];
                if (isset($sizeQuantity['FL'])) $sumColoumn['FL'] += $sizeQuantity['FL'];
                if (isset($sizeQuantity['US'])) $sumColoumn['6XL'] += $sizeQuantity['US'];

                $sumColoumn['all'] += $sumQuantity;
                if($file > 0 && ($count <= ($file-1)*$step || $count > $step*$file)) continue;
                $sheet->setCellValue('A' . $i, $sku);
                if ($productId > 0) {
                    // $sheet->setCellValue('B' . $i, base_url('product/viewImage/' . $productId));
                    // $sheet->getCell('B' . $i)->getHyperlink()->setUrl(base_url('product/viewImage/' . $productId));
                    if(!empty($productImage)){
                        $logo = FCPATH.IMAGE_PATH.$productImage;
                        if(file_exists($logo)) {
                            $objDrawing = new PHPExcel_Worksheet_Drawing();
                            $objDrawing->setName('Logo');
                            $objDrawing->setDescription('Logo');
                            $objDrawing->setPath($logo);
                            $objDrawing->setResizeProportional(true);
                            $objDrawing->setOffsetY($objDrawing->getOffsetY() + 20);
                            $objDrawing->setOffsetX(30);
                            $objDrawing->setCoordinates('B' . $i);
                            $objDrawing->setHeight(130);
                            $objDrawing->setWorksheet($sheet);
                        }
                    }
                    if(!empty($productImage2)){
                        $logo = FCPATH.IMAGE_PATH.$productImage2;
                        if(file_exists($logo)) {
                            $objDrawing1 = new PHPExcel_Worksheet_Drawing();
                            $objDrawing1->setName('Logo');
                            $objDrawing1->setDescription('Logo');
                            $objDrawing1->setPath($logo);
                            $objDrawing1->setResizeProportional(true);
                            $objDrawing1->setOffsetY($objDrawing1->getOffsetY() + 20);
                            $objDrawing1->setOffsetX(200);
                            $objDrawing1->setCoordinates('B' . $i);
                            $objDrawing1->setHeight(130);
                            $objDrawing1->setWorksheet($sheet);
                        }
                    }
                }
                $sheet->setCellValue('C' . $i, isset($sizeQuantity['S']) ? $sizeQuantity['S'] : '-');
                $sheet->setCellValue('D' . $i, isset($sizeQuantity['M']) ? $sizeQuantity['M'] : '-');
                $sheet->setCellValue('E' . $i, isset($sizeQuantity['L']) ? $sizeQuantity['L'] : '-');
                $sheet->setCellValue('F' . $i, isset($sizeQuantity['XL']) ? $sizeQuantity['XL'] : '-');
                $sheet->setCellValue('G' . $i, $xxl > 0 ? $xxl : '-');
                $sheet->setCellValue('H' . $i, $xxxl > 0 ? $xxxl : '-');
                $sheet->setCellValue('I' . $i, isset($sizeQuantity['4XL']) ? $sizeQuantity['4XL'] : '-');
                $sheet->setCellValue('J' . $i, isset($sizeQuantity['5XL']) ? $sizeQuantity['5XL'] : '-');
                $sheet->setCellValue('K' . $i, isset($sizeQuantity['6XL']) ? $sizeQuantity['6XL'] : '-');

                $sheet->setCellValue('L' . $i, isset($sizeQuantity['KIDS']) ? $sizeQuantity['KIDS'] : '-');
                $sheet->setCellValue('M' . $i, isset($sizeQuantity['B']) ? $sizeQuantity['B'] : '-');
                $sheet->setCellValue('N' . $i, isset($sizeQuantity['Q']) ? $sizeQuantity['Q'] : '-');
                $sheet->setCellValue('O' . $i, isset($sizeQuantity['SM']) ? $sizeQuantity['SM'] : '-');
                $sheet->setCellValue('P' . $i, isset($sizeQuantity['TB']) ? $sizeQuantity['TB'] : '-');
                $sheet->setCellValue('Q' . $i, isset($sizeQuantity['TW']) ? $sizeQuantity['TW'] : '-');
                $sheet->setCellValue('R' . $i, isset($sizeQuantity['UK']) ? $sizeQuantity['UK'] : '-');
                $sheet->setCellValue('S' . $i, isset($sizeQuantity['UQ']) ? $sizeQuantity['UQ'] : '-');
                $sheet->setCellValue('T' . $i, isset($sizeQuantity['FL']) ? $sizeQuantity['FL'] : '-');
                $sheet->setCellValue('U' . $i, isset($sizeQuantity['US']) ? $sizeQuantity['US'] : '-');

                $sheet->setCellValue('V' . $i, $sumQuantity);
                $i++;
            }
            if($lastFile || $file == 0){
                $sheet->setCellValue('C' . $i, $sumColoumn['S']);
                $sheet->setCellValue('D' . $i, $sumColoumn['M']);
                $sheet->setCellValue('E' . $i, $sumColoumn['L']);
                $sheet->setCellValue('F' . $i, $sumColoumn['XL']);
                $sheet->setCellValue('G' . $i, $sumColoumn['2XL']);
                $sheet->setCellValue('H' . $i, $sumColoumn['3XL']);
                $sheet->setCellValue('I' . $i, $sumColoumn['4XL']);
                $sheet->setCellValue('J' . $i, $sumColoumn['5XL']);
                $sheet->setCellValue('K' . $i, $sumColoumn['6XL']);

                $sheet->setCellValue('L' . $i, $sumColoumn['KIDS']);
                $sheet->setCellValue('M' . $i, $sumColoumn['B']);
                $sheet->setCellValue('N' . $i, $sumColoumn['Q']);
                $sheet->setCellValue('O' . $i, $sumColoumn['SM']);
                $sheet->setCellValue('P' . $i, $sumColoumn['TB']);
                $sheet->setCellValue('Q' . $i, $sumColoumn['TW']);
                $sheet->setCellValue('R' . $i, $sumColoumn['UK']);
                $sheet->setCellValue('S' . $i, $sumColoumn['UQ']);
                $sheet->setCellValue('T' . $i, $sumColoumn['FL']);
                $sheet->setCellValue('U' . $i, $sumColoumn['US']);

                $sheet->setCellValue('V' . $i, $sumColoumn['all']);
                $sheet->getStyle("A1:V" . ($i))->applyFromArray($border);
                $sheet->getStyle("A1:V" . ($i - 1))->applyFromArray($border);
                $i = $i + 2;
                $sheet->setCellValue('U' . $i, "Sum product");
                $sheet->mergeCells("U{$i}:V{$i}");
                $sheet->getStyle("U{$i}")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle("U{$i}:V{$i}")->applyFromArray($border);
                $i++;
                foreach ($listSumQuantityWithPrpoductType as $sro) {
                    $sheet->setCellValue('U' . $i, $sro['ProductType']);
                    $sheet->setCellValue('V' . $i, $sro['sumQuantity']);
                    $sheet->getStyle("U{$i}:V{$i}")->applyFromArray($border);
                    $i++;
                }                
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            
            $filename = "exportFactory_{$factoryRequestId}_".date('Y-m-d').".xls";
            if($file > 0) $filename = "exportFactory_{$factoryRequestId}_".date('Y-m-d')."_File".$file.".xls";
            /*header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
            header('Cache-Control: max-age=0');*/
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            //$objWriter->save('php://output');
            $objWriter->save(FCPATH.'assets/uploads/downloads/'.$filename);
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
            redirect(base_url('assets/uploads/downloads/'.$filename));
        }
        else echo "<script>window.close();</script>";
    }

    private function genBarcode($code, $bcs = 'code128', $height = 60, $text = 1){
        $drawText = ($text != 1) ? FALSE : TRUE;
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $barcodeOptions = array('text' => $code, 'barHeight' => $height, 'drawText' => $drawText, 'factor' => 1, 'fontSize' => 50, 'font' => 4);
        $rendererOptions = array('imageType' => 'png', 'horizontalPosition' => 'center', 'verticalPosition' => 'middle');
        // $imageResource = Zend_Barcode::render($bcs, 'image', $barcodeOptions, $rendererOptions);
        $imageResource = Zend_Barcode::draw($bcs, 'image', $barcodeOptions, $rendererOptions);
        imagepng($imageResource, "./" . PRODUCT_PATH . "barcodes/{$code}.png");
        // return $imageResource;
    }
}
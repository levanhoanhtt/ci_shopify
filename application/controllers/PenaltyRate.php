<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penaltyrate extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Penalty rate list',
            array('scriptFooter' => array('js' => 'js/penalty_rate.js'))
        );
        if($this->Mactions->checkAccess($data['listActions'], 'penaltyrate')) {
            $this->loadModel(['Mpenaltyrate', 'Mfactories', 'Mproducttypes']);
            $data['listPenalties'] = $this->Mpenaltyrate->getAll();
            $data['factories'] = $this->Mfactories->get(0, false, '', ['FactoryId', 'FactoryName']);
            $data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
            $this->load->view('setting/penalty_rate', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function update(){
        $this->checkUserLogin(true);
        $this->loadModel(['Mpenaltyrate', 'Mfactories']);
        $postData = $this->arrayFromPost(array('FactoryId', 'ProductType', 'AllowDay', 'PenaltyPercentage'));
        $postData['StatusId'] = STATUS_ACTIVED;
        if(!empty($postData['FactoryId']) && !empty($postData['ProductType']) && !empty($postData['AllowDay']) && !empty($postData['PenaltyPercentage'])){
            $checkExist = $this->Mpenaltyrate->checkExist($postData['FactoryId'], $postData['ProductType']);
            if($checkExist){
                $flag = $this->Mpenaltyrate->save($postData, $checkExist['PenaltyRateId']);
                if($checkExist['StatusId'] == 0){
                    $postData['IsAdd'] = 1;
                }
                if($flag > 0){
                    $postData['PenaltyRateId'] = $flag;
                    $postData['FactoryName'] = $this->Mfactories->get($postData['FactoryId'], true, '', ['FactoryName'])['FactoryName'];
                    echo json_encode(array('code' => 1, 'message' => "Updated Successfully", 'data' => $postData));
                }
                else echo json_encode(array('code' => -1, 'message' => "Something wrong happened"));
            }
            else{
                $flag = $this->Mpenaltyrate->save($postData);
                if($flag > 0){
                    $postData['PenaltyRateId'] = $flag;
                    $postData['FactoryName'] = $this->Mfactories->get($postData['FactoryId'], true, '', ['FactoryName'])['FactoryName'];
                    $postData['IsAdd'] = 1;
                    echo json_encode(array('code' => 1, 'message' => "Created Successfully", 'data' => $postData));
                }
                else echo json_encode(array('code' => -1, 'message' => "Something wrong happened"));
            }
        }else{
            echo json_encode(array('code' => -1, 'message' => "Something wrong happened"));
        }
    }

    public function delete(){
        $this->checkUserLogin(true);
        $id = $this->input->post('PenaltyRateId');
        if($id > 0){
            $this->load->model('Mpenaltyrate');
            $flag = $this->Mpenaltyrate->changeStatus(0, $id);
            if($flag) echo json_encode(array('code' => 1, 'message' => "Deleted Success"));
            else echo json_encode(array('code' => -1, 'message' => "Deleted Fail"));
        }
        else echo json_encode(array('code' => -1, 'message' => "Deleted Fail"));
    }
}
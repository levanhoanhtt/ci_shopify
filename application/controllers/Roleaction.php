<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roleaction extends MY_Controller {

	public function index($roleId = 0, $partId = 0){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Grant permission',
			array('scriptFooter' => array('js' => 'js/role_action.js'))
		);
		if ($this->Mactions->checkAccess($data['listActions'], 'roleaction')) {
			$data['roleId'] = $roleId > 0 ? $roleId : 0;
			$data['listActiveActions'] = $this->Mactions->getHierachy();
			$this->load->view('setting/role_action', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function getAction(){
		$this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		if($roleId > 0){
			$this->load->model('Mroleactions');
			echo json_encode(array('code' => 1, 'data' => $this->Mroleactions->getBy(array('RoleId' => $roleId), false, '', 'ActionId')));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$roleId = $this->input->post('RoleId');
		if ($roleId > 0) {
			$valueData = array();
			$actionIds = $this->input->post('ActionIds');
			if(!empty($actionIds)){
				$crDateTime = getCurentDateTime();
				$actionIds = json_decode($actionIds, true);
				foreach($actionIds as $actionId) $valueData[] = array('RoleId' => $roleId, 'ActionId' => $actionId, 'CrUserId' => $user['UserId'], 'CrDateTime' => $crDateTime);
			}
			$this->load->model('Mroleactions');
			$flag = $this->Mroleactions->updateBatch($roleId, $valueData);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Grant permission success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

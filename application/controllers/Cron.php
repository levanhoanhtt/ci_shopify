<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

	public function crawl(){
		$this->loadModel(array('Mshops', 'Mordercrawls'));
		$listShops = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
		foreach($listShops as $s){
			$shopUrl = str_replace(array('https://', 'http://'), '', $s['ShopUrl']);
			$shopUrl = 'https://' . $s['ApiKey'] . ':' . $s['ApiSecret'] . '@' . $shopUrl . 'admin/orders.json?status=any&financial_status=paid&fulfillment_status=unshipped&limit=250&page=';
			$crDateTime = getCurentDateTime();
			$i = 0;
			while($i < 10){
				$isBreak = false;
				$i++;
				$url = $shopUrl . $i;
				echo $crDateTime.'-'.$s['ShopName'].': '.$url.PHP_EOL;
				$json = $this->initCurl($url);
				if($json && !empty($json)){
					$json = @json_decode($json, true);
					if($json && isset($json['orders']) && !empty($json['orders'])){
						foreach($json['orders'] as $order){
							$orderCrawlId = $this->Mordercrawls->getFieldValue(array('OrderId' => $order['id'], 'ShopId' => $s['ShopId']), 'OrderCrawlId', 0);
							if($orderCrawlId == 0) {
								$orderCrawlId = $this->Mordercrawls->save(array(
									'OrderId' => $order['id'],
									'OrderName' => str_replace('#', '', $order['name']),
									'IsOrderReal' => 0,
									'Json' => json_encode($order),
									'ShopId' => $s['ShopId'],
									'CreatedDateTime' => str_replace('T', ' ', substr($order['created_at'], 0, 19)),
									'CrDateTime' => $crDateTime
								));
								echo $orderCrawlId.PHP_EOL;
							}
							else{
								echo 'Y '. $orderCrawlId.PHP_EOL;
								if($i > 0) {
									$isBreak = true;
									break;
								}
							}
						}
						if(count($json['orders']) < 250) $isBreak = true;
					}
					else $isBreak = true;
				}
				else $isBreak = true;
				if($isBreak) break;
			}
            $this->db->reconnect();
		}
	}

	public function crawlAll(){
		$this->loadModel(array('Mshops', 'Morderalls'));
		$listShops = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
        //$listShops = $this->Mshops->getBy(array('ShopId' => 107));
		foreach($listShops as $s){
			$shopUrl = str_replace(array('https://', 'http://'), '', $s['ShopUrl']);
			$shopUrl = 'https://' . $s['ApiKey'] . ':' . $s['ApiSecret'] . '@' . $shopUrl . 'admin/orders.json?status=any&limit=250&page=';
			$crDateTime = getCurentDateTime();
			$i = 0;
			while($i < 10){
				$isBreak = false;
				$i++;
				$url = $shopUrl . $i;
				echo $crDateTime.'-'.$s['ShopName'].': '.$url.PHP_EOL;
				$json = $this->initCurl($url);
				if($json && !empty($json)){
					$json = @json_decode($json, true);
					if($json && isset($json['orders']) && !empty($json['orders'])){
						foreach($json['orders'] as $order){
							$countryName = '';
							if(isset($order['billing_address'])){
								if(isset($order['billing_address']['country'])) $countryName = $order['billing_address']['country'];
							}
							$orderAlllId = $this->Morderalls->getFieldValue(array('OrderId' => $order['id'], 'ShopId' => $s['ShopId']), 'OrderAllId', 0);
							if($orderAlllId == 0) {
								$customerName = '';
								$customerEmail = '';
								if (isset($order['customer'])) {
									$customerName = $order['customer']['first_name'] . ' ' . $order['customer']['last_name'];
									$customerEmail = $order['customer']['email'];
								}
								$orderAlllId = $this->Morderalls->save(array(
									'ShopId' => $s['ShopId'],
									'OrderId' => $order['id'],
									'OrderName' => str_replace('#', '', $order['name']),
									'CustomerName' => $customerName,
									'CustomerEmail' => $customerEmail,
									'CountryName' => $countryName,
									'Json' => json_encode($order),
									'CreatedDateTime' => str_replace('T', ' ', substr($order['created_at'], 0, 19)),
									'CrDateTime' => $crDateTime
								));
								echo $orderAlllId.PHP_EOL;
							}
							else{
								echo 'Y '. $orderAlllId.PHP_EOL;
								if($i > 0) {
									$isBreak = true;
									break;
								}
							}
						}
						if(count($json['orders']) < 250) $isBreak = true;
					}
					else $isBreak = true;
				}
				else $isBreak = true;
				if($isBreak) break;
			}
            $this->db->reconnect();
		}
	}

	public function checkout(){
		$this->loadModel(array('Mshops', 'Mcheckouts'));
		$statusNames = array('open', 'closed');
		$listShops = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
		foreach($listShops as $s){
			$crDateTime = getCurentDateTime();
			$shopUrl1 = str_replace(array('https://', 'http://'), '', $s['ShopUrl']);
			foreach($statusNames as $statusName) {
				$shopUrl = 'https://' . $s['ApiKey'] . ':' . $s['ApiSecret'] . '@' . $shopUrl1 . 'admin/checkouts.json?status='.$statusName.'&limit=250&page=';
				$i = 0;
				while ($i < 10) {
					$isBreak = false;
					$i++;
					$url = $shopUrl . $i;
					echo $crDateTime . '-' . $s['ShopName'] . ': ' . $url . PHP_EOL;
					$json = $this->initCurl($url);
					if ($json && !empty($json)) {
						$json = @json_decode($json, true);
						if ($json && isset($json['checkouts']) && !empty($json['checkouts'])) {
							foreach ($json['checkouts'] as $order) {
								$checkoutId = $this->Mcheckouts->getFieldValue(array('CheckoutCode' => $order['id'], 'ShopId' => $s['ShopId']), 'CheckoutId', 0);
								if ($checkoutId == 0) {
									$customerName = '';
									$customerEmail = '';
									if (isset($order['customer'])) {
										$customerName = $order['customer']['first_name'] . ' ' . $order['customer']['last_name'];
										$customerEmail = $order['customer']['email'];
									}
									$countryName = '';
									if (isset($order['shipping_address'])) $countryName = $order['shipping_address']['country'];
									$checkoutId = $this->Mcheckouts->save(array(
										'ShopId' => $s['ShopId'],
										'CheckoutCode' => $order['id'],
										'CustomerName' => $customerName,
										'CustomerEmail' => $customerEmail,
										'CountryName' => $countryName,
										'Json' => json_encode($order),
										'CreatedDateTime' => str_replace('T', ' ', substr($order['created_at'], 0, 19)),
										'CrDateTime' => $crDateTime
									));
									echo $checkoutId . PHP_EOL;
								}
								else {
									echo 'Y ' . $checkoutId . PHP_EOL;
									if ($i > 0) {
										$isBreak = true;
										break;
									}
								}
							}
							if (count($json['checkouts']) < 250) $isBreak = true;
						}
						else $isBreak = true;
					}
					else $isBreak = true;
					if ($isBreak) break;
				}
			}
            $this->db->reconnect();
		}
	}

	private function initCurl($url, $postData = ''){
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		set_time_limit (60);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
		if (!empty($postData)) {
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt ($ch, CURLOPT_POST, TRUE);
		}
		curl_setopt ($ch, CURLOPT_REFERER, $url);
		if(curl_errno($ch)){
			curl_close($ch);
			return '';
		}
		$result = curl_exec ($ch);
		curl_close($ch);
		return $result;
	}

	public function compare(){ //doi soat xuong kho - bo
		$this->loadModel(array('Minternaltrackings', 'Mfactoryscans', 'Mwarehousescans', 'Mownproducts'));
		$internalTrackingId = $this->Minternaltrackings->getFirstTrackingId();
		if($internalTrackingId > 0){
			$listFactoryScans = $this->Mfactoryscans->getBy(array('InternalTrackingId' => $internalTrackingId));
			$listWarehouseScans = $this->Mwarehousescans->getBy(array('InternalTrackingId' => $internalTrackingId));
			$listOwnProducts = $this->Mownproducts->getListCheck();
			$updateOldOwnProducts = array();
			foreach($listOwnProducts as $op) {
				$updateOldOwnProducts[] = array(
					'OwnProductId' => $op['OwnProductId'],
					'IsCheck' => 1
				);
			}
			//step 1: tinh no cu
			$newOwnProducts = array(); //tinh no lan nay
			foreach($listWarehouseScans as $wc){
				foreach($listOwnProducts as $op) {
					$op['InternalTrackingId'] = $internalTrackingId;
					$op['OldOwnQuantity'] = $op['OwnQuantity'];
					if($wc['BarCode'] == $op['BarCode']){
						if($wc['Quantity'] >= $op['OwnQuantity']){
							$op['CurrentQuantity'] = $op['OwnQuantity'];
							$op['OwnQuantity'] = 0;
						}
						else{
							$op['CurrentQuantity'] = $wc['Quantity'];
							$op['OwnQuantity'] -= $wc['Quantity'];
						}
					}
					unset($op['OwnProductId']);
					$newOwnProducts[] = $op;
				}
			}
			//step 2: doi soat xuong - kho o lan nay + gop vao cai step 1
			foreach($listFactoryScans as $fc){
				$flag = false;
				foreach($listWarehouseScans as $wc){
					if($wc['BarCode'] == $fc['BarCode']){
						$flag = true;
						$ownQuantity = $fc['Quantity'] - $wc['Quantity'];
						if($ownQuantity > 0){
							$index = -1;
							foreach($newOwnProducts as $i => $op){
								if($wc['BarCode'] == $op['BarCode']){ // them vao no cu da xet o tren
									$index = $i;
									$op['OwnQuantity'] += $ownQuantity;
									$newOwnProducts[$i] = $op;
									break;
								}
							}
							if($index == -1){ //no moi trong lan nay
								$newOwnProducts[] = array(
									'LastDate' => $wc['ScanDate'],
									'BarCode' => $wc['BarCode'],
									'FactoryId' => $wc['FactoryId'],
									'Sku' => $wc['Sku'],
									'OrderName' => $wc['OrderName'],
									'OrderShopifyId' => $wc['OrderShopifyId'],
									'OrderProductId' => $wc['OrderProductId'],
									'OldOwnQuantity' => 0,
									'OwnQuantity' => $ownQuantity,
									'CurrentQuantity' => 0,
									'ProductType' => $wc['ProductType'],
									'InternalTrackingId' => $internalTrackingId,
									'IsCheck' => 0
								);
							}
						}
						break;
					}
				}
				if(!$flag){
					$newOwnProducts[] = array(
						'LastDate' => $fc['ScanDate'],
						'BarCode' => $fc['BarCode'],
						'FactoryId' => $fc['FactoryId'],
						'Sku' => $fc['Sku'],
						'OrderName' => $fc['OrderName'],
						'OrderShopifyId' => $fc['OrderShopifyId'],
						'OrderProductId' => $fc['OrderProductId'],
						'OldOwnQuantity' => 0,
						'OwnQuantity' => $fc['Quantity'],
						'CurrentQuantity' => 0,
						'ProductType' => $fc['ProductType'],
						'InternalTrackingId' => $internalTrackingId,
						'IsCheck' => 0
					);
				}
			}
			//insert $newOwnProducts - no tai hien tai
			//update $updateOldOwnProducts
			//update da check $internalTrackingId
			$flag = $this->Mownproducts->insert($updateOldOwnProducts, $newOwnProducts, $internalTrackingId);
			if($flag) echo 'OK';
			else echo 'Not OK';
			echo getCurentDateTime().'- Calc success'.PHP_EOL;
		}
		else echo getCurentDateTime().'- Nothing'.PHP_EOL;
	}

	public function waitPacking(){
		$this->loadModel(array('Mordershopifys', 'Morderproducts'));
		$listOrders = $this->Mordershopifys->getBy(array('TransportStatusId' => 1, 'OrderStatusId >' => 0, 'OrderStatusId !=' => 3), true);
		$orderIds = array();
		foreach($listOrders as $o){
			$listProducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $o['OrderShopifyId']), false, '', 'OrderProductId,Quantity,WarehouseQuantity');
			$n = count($listProducts);
			$i = 0;
			foreach($listProducts as $p){
				if($p['Quantity'] == $p['WarehouseQuantity']) $i++;
			}
			if($i == $n) $orderIds[] = $o['OrderShopifyId'];
		}
		if(!empty($orderIds)) $this->Mordershopifys->changeTransportStatusBatch($orderIds, 2);
		echo getCurentDateTime().'- Wait Pack '.count($orderIds).' orders'.PHP_EOL;
	}

	//trancking from china
	private function getCookie($cookieFileName){
		$ch = curl_init();
		//curl_setopt($ch, CURLOPT_URL, 'http://post.8dt.com/onlineOrder/checkLogin.do');
		curl_setopt($ch, CURLOPT_URL, 'http://post.8dt.com/onlineOrder/checkLoginJsonP.do?userCode=Platform&pwd=aop2017');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		/*curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "userCode=Platform&pwd=aop2017");*/
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFileName);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFileName);
		curl_exec($ch);;
		curl_close($ch);
		return true;
	}

	private function getHtml($cookieFileName, $productUrl, $postData){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFileName);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFileName);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("ys-loginUserCode_system=s%3APlatform; JSESSIONID=D3063331650735AF0D0B3E6A7207C54F"));
		curl_setopt($ch, CURLOPT_URL,$productUrl);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
		if (!empty($postData)) {
			curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt ($ch, CURLOPT_POST, TRUE);
		}
		$retVal = curl_exec ($ch);
		curl_close ($ch);
		return $retVal;
	}

	public function tracking(){
		$now = getCurentDateTime();
		$cookieFileName = FCPATH . 'assets/uploads/excels/cookie.txt';
		$this->getCookie($cookieFileName);
		$json = $this->getHtml($cookieFileName, 'http://post.8dt.com/onlineOrder/secondaryParcel/list.do', 'limit=1000&start=0&status=2&online=&ebayId=');
		//echo 'H '.$json;die();
		$json = json_decode($json, true);
		if($json && isset($json['success']) && $json['success']){
			$this->load->model(array('Mordershopifys'));
			foreach($json['rows'] as $row){
				$orderName = trim(str_replace('#', '', $row['refNo']));
				$trackingCode = trim($row['aptrackingnumber']);
				if(!empty($trackingCode) && !empty($orderName)){
					$id = $this->Mordershopifys->getFieldValue(array('OrderName' => $orderName, 'TrackingCode' => $trackingCode), 'OrderShopifyId', 0);
					if($id > 0){
                        echo $now.'- '.$orderName.': '.$trackingCode.PHP_EOL;
					    break;
                    }
					else{
					    $this->Mordershopifys->updateTracking($trackingCode, $orderName);
                        echo $now.'- '.$orderName.': '.$trackingCode.PHP_EOL;
                    }
				}
				else echo 'Empty'.PHP_EOL;
			}
		}
		else echo $now.'- '.'Cookie Expired'.PHP_EOL;
	}

	public function shipped(){
		$now = getCurentDateTime();
		$cookieFileName = FCPATH . 'assets/uploads/excels/cookie.txt';
		$this->getCookie($cookieFileName);
		$json = $this->getHtml($cookieFileName, 'http://post.8dt.com/onlineOrder/secondaryParcel/list.do', 'limit=1000&start=0&status=4&online=&ebayId=');
		$json = json_decode($json, true);
		if($json && isset($json['success']) && $json['success']){
			$this->load->model(array('Mordershopifys'));
			foreach($json['rows'] as $row){
				$orderName = trim(str_replace('#', '', $row['refNo']));
				if(!empty($orderName)){
					$id = $this->Mordershopifys->getFieldValue(array('OrderName' => $orderName, 'OrderStatusId' => 12), 'OrderShopifyId', 0);
					if($id > 0){
                        echo $now.'- '.$orderName.': Shipped'.PHP_EOL;
					    break;
                    }
					else{
					    $this->Mordershopifys->updateStatus(12, $orderName, trim($row['aptrackingnumber']));
                        echo $now.'- '.$orderName.': Shipped'.PHP_EOL;
                    }
				}
                else echo 'Empty'.PHP_EOL;
			}
		}
		else echo $now.'- '.'Cookie Expired'.PHP_EOL;
	}

	//tracking to shopify
	private function fulfillmentsApi($apiLink,$postData){
		$ch = curl_init($apiLink);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($postData))
		);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($ch);
		return $result;

	}

	public function sendTrackingCode(){
		$this->loadModel(array('Mordershopifys','Mshops', 'Mordercrawls'));
		$listOrderShopifys = $this->Mordershopifys->getListSendTracking();
		if(empty($listOrderShopifys)) return;
		$listShops = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED, 'ShopCode !=' => ''));
		$listLinkApis = array();
		$shopLocations = array();
		foreach ($listShops as $s) {
			$shopUrl = str_replace(array('https://', 'http://'), '', $s['ShopUrl']);
			$shopUrl = 'https://' . $s['ApiKey'] . ':' . $s['ApiSecret'] . '@' . $shopUrl . 'admin/orders/{orderId}/fulfillments.json';
			$listLinkApis[$s['ShopId']] = $shopUrl;
			$shopLocations[$s['ShopId']] = empty($s['LocationIds']) ? array() : json_decode($s['LocationIds'], true);
		}
		foreach ($listOrderShopifys as $o) {
			$itemIds = json_decode($o['ItemIds'],true);
			if(!empty($itemIds) && isset($listLinkApis[$o['ShopId']])){
				$locationIds = $shopLocations[$o['ShopId']];
				if (empty($locationIds)){
					$postData = '{"fulfillment":{"tracking_number":"' . $o['TrackingCode'] . '","line_items":[';
					foreach ($itemIds as $i) $postData .= '{"id":' . $i . '},';
					$postData = rtrim($postData, ',') . ']}}';
					$apiLink = str_replace('{orderId}', $o['OrderCode'], $listLinkApis[$o['ShopId']]);
					//echo $o['OrderName'] . PHP_EOL;echo $apiLink . PHP_EOL;echo $postData . PHP_EOL;
					$result = $this->fulfillmentsApi($apiLink, $postData);
					//echo 'Json: '.$result.PHP_EOL;die();
					if (!empty($result)) {
						if(strpos($result, 'already fulfilled') !== false){
							$this->Mordershopifys->save(array('IsSendTracking' => 2), $o['OrderShopifyId']);
							echo 'Import Tracking ' . $o['OrderShopifyId'] . ' success on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
						}
						else{
							$result = @json_decode($result, true);
							if (!isset($result['errors'])) {
								$this->Mordershopifys->save(array('IsSendTracking' => 2), $o['OrderShopifyId']);
								echo 'Import Tracking ' . $o['OrderShopifyId'] . ' success on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
							}
							else echo 'Import Tracking ' . $o['OrderShopifyId'] . ' failed on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
						}
					}
					else echo 'Import failed' . $o['OrderShopifyId'] . ' -' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
					echo '-------------------------------------------' . PHP_EOL;
				}
				else{
					foreach($locationIds as $locationId){
						$postData = '{"fulfillment":{"tracking_number":"' . $o['TrackingCode'] . '","location_id":"' . $locationId . '","line_items":[';
						foreach ($itemIds as $i) $postData .= '{"id":' . $i . '},';
						$postData = rtrim($postData, ',') . ']}}';
						$apiLink = str_replace('{orderId}', $o['OrderCode'], $listLinkApis[$o['ShopId']]);
						//echo $o['OrderName'] . PHP_EOL;echo $apiLink . PHP_EOL;echo $postData . PHP_EOL;
						$result = $this->fulfillmentsApi($apiLink, $postData);
						//echo 'Json: '.$result.PHP_EOL;die();
						if (!empty($result)) {
							if(strpos($result, 'already fulfilled') !== false){
								$this->Mordershopifys->save(array('IsSendTracking' => 2), $o['OrderShopifyId']);
								echo 'Import ('.$locationId.') Tracking ' . $o['OrderShopifyId'] . ' success on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
								break;
							}
							else{
								$result = @json_decode($result, true);
								if (!isset($result['errors'])) {
									$this->Mordershopifys->save(array('IsSendTracking' => 2), $o['OrderShopifyId']);
									echo 'Import ('.$locationId.') Tracking ' . $o['OrderShopifyId'] . ' success on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
									break;
								}
								else echo 'Import ('.$locationId.') Tracking ' . $o['OrderShopifyId'] . ' failed on ' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
							}
						}
						else echo 'Import ('.$locationId.') failed' . $o['OrderShopifyId'] . ' -' . $o['TrackingCode'] . ': ' . $o['OrderName'] . PHP_EOL;
						echo '-------------------------------------------' . PHP_EOL;
					}
				}
            }
		}
	}

	public function updateStatusFactoryLate(){
		$this->load->model('Mordershopifys');
		$listOrders = $this->Mordershopifys->getListPrepareFactoryLate();
		if(!empty($listOrders)){
			$orderUpdates = array();
			$actionLogs = array();
			$updateDateTime = getCurentDateTime();
			$statusName = $this->Mconstants->orderExtraStatus[5];
			foreach($listOrders as $o){
				if($o['OrderStatusId'] == 9){
					$orderUpdates[] = array(
						'OrderShopifyId' => $o['OrderShopifyId'],
						'OrderExtraStatusId' => 5,
						'UpdateDateTime' => $updateDateTime
					);
				}
				else {
					$orderUpdates[] = array(
						'OrderShopifyId' => $o['OrderShopifyId'],
						'OrderStatusId' => 8,
						'UpdateDateTime' => $updateDateTime
					);
				}
				$actionLogs[] = array(
					'ItemId' => $o['OrderShopifyId'],
					'ItemTypeId' => 1,
					'ActionTypeId' => 2,
					'Comment' => 'System change order status to '.$statusName,
					'CrUserId' => 0,
					'CrDateTime' => $updateDateTime
				);
			}
			$flag = $this->Mordershopifys->updateOrderBatch($orderUpdates, $actionLogs);
			echo $flag ? 'Update status factory late success'.PHP_EOL : 'Update status factory late failed'.PHP_EOL;
		}
		else echo 'Empty order'.PHP_EOL;
	}

	public function updateStatusWarehouseLate(){
		$this->load->model('Mordershopifys');
		$listOrders = $this->Mordershopifys->getListPrepareWarehouseLate();
		if(!empty($listOrders)){
			$orderUpdates = array();
			$actionLogs = array();
			$updateDateTime = getCurentDateTime();
			$statusName = $this->Mconstants->orderExtraStatus[6];
			foreach($listOrders as $o){
				if($o['OrderStatusId'] == 10){
					$orderUpdates[] = array(
						'OrderShopifyId' => $o['OrderShopifyId'],
						'OrderExtraStatusId' => 6,
						'UpdateDateTime' => $updateDateTime
					);
				}
				else {
					$orderUpdates[] = array(
						'OrderShopifyId' => $o['OrderShopifyId'],
						'OrderStatusId' => 11,
						'UpdateDateTime' => $updateDateTime
					);
				}
				$actionLogs[] = array(
					'ItemId' => $o['OrderShopifyId'],
					'ItemTypeId' => 1,
					'ActionTypeId' => 2,
					'Comment' => 'System change order status to '.$statusName,
					'CrUserId' => 0,
					'CrDateTime' => $updateDateTime
				);
			}
			$flag = $this->Mordershopifys->updateOrderBatch($orderUpdates, $actionLogs);
			echo $flag ? 'Update status warehouse late success'.PHP_EOL : 'Update status warehouse late failed'.PHP_EOL;
		}
		else echo 'Empty order'.PHP_EOL;
	}

	public function updateStatusWarehouseEnough(){
		$this->loadModel(array('Mordershopifys', 'Morderproducts'));
		$orderIds = $this->Mordershopifys->getListEnoughWarehouse();
		if(!empty($orderIds)) {
			$updateDateTime = getCurentDateTime();
			$statusName = $this->Mconstants->orderExtraStatus[3];
			$orderUpdates = array();
			$actionLogs = array();
			foreach ($orderIds as $orderId) {
				$listProducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $orderId), false, '', 'OrderProductId,Quantity,WarehouseQuantity');
				$n = count($listProducts);
				$i = 0;
				foreach ($listProducts as $p) {
					if ($p['Quantity'] == $p['WarehouseQuantity']) $i++;
				}
				if ($i == $n) {
					$orderUpdates[] = array(
						'OrderShopifyId' => $orderId,
						'OrderStatusId' => 10,
						'OrderExtraStatusId' => 3,
						'TransportStatusId' => 2,
						'UpdateDateTime' => $updateDateTime
					);
					$actionLogs[] = array(
						'ItemId' => $orderId,
						'ItemTypeId' => 1,
						'ActionTypeId' => 2,
						'Comment' => 'System change order status to ' . $statusName,
						'CrUserId' => 0,
						'CrDateTime' => $updateDateTime
					);
				}
			}
			$flag = $this->Mordershopifys->updateOrderBatch($orderUpdates, $actionLogs);
			echo $flag ? 'Update status warehouse enough success'.PHP_EOL : 'Update status warehouse enough failed'.PHP_EOL;
		}
		else echo 'Empty order'.PHP_EOL;
	}

	public function deleteExportExcel(){
		array_map('unlink', glob(FCPATH.'assets/uploads/downloads/*'));
	}

	public function inportOrderProduct(){
		$fileUrl = '/assets/uploads/files/SSS-18.19.06.xlsx';
		if (!empty($fileUrl)){
			if (ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
			$fileUrl = FCPATH . $fileUrl;
			$this->load->library('excel');
			$inputFileType = PHPExcel_IOFactory::identify($fileUrl);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileUrl);
			$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
			$countRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
			for ($row = 2; $row <= $countRows; $row++) {
				$sku = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
				$skus = explode('-', $sku);
				if(count($skus) == 5) {
					$orderName = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
					$barCode = str_replace('#', '', $orderName).'-'.trim($skus[0]).'-'.trim($skus[1]).'-'.trim($skus[2]).'-'.trim($skus[3]);
					$color = trim($skus[4]);
					$this->db->query("UPDATE orderproducts SET Sku = CONCAT(Sku, '-', ?), BarCode = CONCAT(BarCode, '-', ?), Sku2 = CONCAT(Sku2, '-', ?) WHERE BarCode = ?", array($color, $color, $color, $barCode));
					echo $orderName . '-' . $sku . '<br>';
				}
			}
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
		}
		else echo 'clgt';
	}

	public function updateItemIds(){
        $this->loadModel(array('Mordershopifys', 'Mordercrawls', 'Morderproducts'));
        $listOrderShopifys = $this->Mordershopifys->getListSendTracking();
        foreach ($listOrderShopifys as $o){
            $json = $this->Mordercrawls->getFieldValue(array('OrderName' => $o['OrderName']), 'Json');
            if(!empty($json)){
                $json = json_decode($json, true);
                $orderProducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $o['OrderShopifyId']), false, '', 'Sku');
                $skus = array();
                foreach ($orderProducts as $op) $skus[] = $op['Sku'];
                $itemIds = array();
                foreach ($json['line_items'] as $l){
                    if(in_array($l['sku'], $skus)) $itemIds[] = $l['id'];
                }
                $this->Mordershopifys->save(array('ItemIds' => json_encode($itemIds)), $o['OrderShopifyId']);
                echo $o['OrderName'].': '.json_encode($itemIds).PHP_EOL;
            }
        }
	}
	
	public function getLocation(){
        $this->load->model('Mshops');
		$listShops = $this->Mshops->getListNoLocation();
		//$listShops = $this->Mshops->getBy(array('ShopId' => 18));
		foreach($listShops as $s){
			$locationIds =  array();
			$shopUrl = str_replace(array('https://', 'http://'), '', $s['ShopUrl']);
			$shopUrl = 'https://' . $s['ApiKey'] . ':' . $s['ApiSecret'] . '@' . $shopUrl . 'admin/locations.json';
			//echo $shopUrl;die();
			$json = $this->initCurl($shopUrl);
			$json = @json_decode($json, true);
			if($json && isset($json['locations']) && !empty($json['locations'])){
				foreach($json['locations'] as $l){
					if($l['active']  == true) $locationIds[] = $l['id'];
				}
			}
			$this->Mshops->changeStatus(json_encode($locationIds), $s['ShopId'], 'LocationIds');
			echo $s['ShopId'].': '.json_encode($locationIds).PHP_EOL;
		}		
	}

	public function fix(){
        $this->load->model('Mshops');
	    $list = $this->Mshops->getByQuery("select OrderProductId, Sku, OrderName from orderproducts INNER JOIN ordershopifys ON orderproducts.OrderShopifyId = ordershopifys.OrderShopifyId WHERE BarCode = '' AND orderproducts.OrderShopifyId > 0");
	    $data = array();
	    foreach ($list as $p){
	        $data[] = array(
	            'OrderProductId' => $p['OrderProductId'],
                'BarCode' => $p['OrderName'].'-'.$p['Sku']
            );
        }
        if(!empty($data)){
            $this->db->update_batch('orderproducts', $data, 'OrderProductId');
            echo 'OK';
        }
        else echo 'Empty';

    }
}
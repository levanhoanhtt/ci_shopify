<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Invoice List',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/invoice_list.js'))
			)
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'invoice')){
			$data['canDelete'] = $this->Mactions->checkAccess($listActions, 'invoice/delete');
			$data['canSendMail'] = $this->Mactions->checkAccess($listActions, 'invoice/edit');
			$teamIds = $user['TeamIds'];
			$this->load->model(array('Mteams', 'Mfactories', 'Minvoices'));
			if (empty($teamIds)) $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
			else $data['listTeams'] = $this->Mteams->getListByIds($teamIds);
			$factoryId = $user['FactoryId'];
			if($factoryId == 0) $listFactories = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
			else $listFactories = array($this->Mfactories->get($factoryId));
			$data['listFactories'] = $listFactories;
			$postData = $this->arrayFromPost(array('InvoiceStatusId', 'TeamId', 'FactoryId', 'BeginDate', 'EndDate'));
			if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$perPage = DEFAULT_LIMIT;
			$rowCount = $this->Minvoices->getCount($postData);
			$data['invoiceCount'] = $rowCount;
			$data['listInvoices'] = array();
			if ($rowCount > 0) {
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listInvoices'] = $this->Minvoices->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('invoice/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function logMail(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Invoice List',
			array(
				'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
				'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/invoice_mail.js'))
			)
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($listActions, 'invoice/logMail')){
			$teamIds = $user['TeamIds'];
			$this->load->model(array('Mteams', 'Mfactories', 'Minvoices','Minvoicemails'));
			if (empty($teamIds)) $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
			else $data['listTeams'] = $this->Mteams->getListByIds($teamIds);
			$factoryId = $user['FactoryId'];
			if($factoryId == 0) $listFactories = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
			else $listFactories = array($this->Mfactories->get($factoryId));
			$data['listFactories'] = $listFactories;
			$postData = $this->arrayFromPost(array('InvoiceStatusId', 'TeamId', 'FactoryId', 'BeginDate', 'EndDate'));
			if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
			$perPage = DEFAULT_LIMIT;
			$rowCount = $this->Minvoicemails->getCount($postData);
			$data['invoiceCount'] = $rowCount;
			$data['listInvoices'] = array();
			if ($rowCount > 0) {
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if (!is_numeric($page) || $page < 1) $page = 1;
				$data['listInvoices'] = $this->Minvoicemails->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('invoice/logMail', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function show($invoiceId = 0){
		if ($invoiceId > 0) {
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Invoice Detail',
				array('scriptFooter' => array('js' => 'js/invoice.js'))
			);
			$listActions = $data['listActions'];
			if ($this->Mactions->checkAccess($listActions, 'invoice/view')){
				$this->loadModel(array('Minvoices', 'Mteams', 'Mfactories', 'Mproducttypes'));
				$invoice = $this->Minvoices->get($invoiceId);
				if ($invoice) {
					$data['invoiceId'] = $invoiceId;
					$data['invoice'] = $invoice;
					$canEdit = $this->Mactions->checkAccess($listActions, 'invoice/edit');
					$data['canEdit'] = $canEdit;
					$data['canUpdatePrice'] = $canEdit && $invoice['InvoiceStatusId'] != 2;
					$data['canReceived'] = $canEdit && $invoice['InvoiceStatusId'] == 1;
					$data['teamName'] = $this->Mteams->getFieldValue(array('TeamId' => $invoice['TeamId']), 'TeamName');
					$data['factoryName'] = $this->Mfactories->getFieldValue(array('FactoryId' => $invoice['FactoryId']), 'FactoryName');
					$data['listSumProductByTypes'] = $this->Minvoices->getSumProductByType($invoiceId);
					$productTypePrices = array();
					if(!empty($invoice['PriceList'])) $productTypePrices = json_decode($invoice['PriceList'], true);
					if(empty($productTypePrices)) $productTypePrices = $this->Mproducttypes->getListPriceMap();
					$data['productTypePrices'] = $productTypePrices;
				}
				else {
					$data['invoiceId'] = 0;
					$data['canEdit'] = false;
					$data['canReceived'] = false;
					$data['txtError'] = "Invoice not found";
				}
				$this->load->view('invoice/show', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('invoice');
	}

	public function exportPdf($invoiceId = 0){
		$user = $this->checkUserLogin();
		$flag = $this->Mactions->checkAccessFromDb('invoice/view', $user['UserId'], $user['RoleId']);
		if($flag && $invoiceId > 0){
			$this->loadModel(array('Minvoices', 'Mteams', 'Mfactories', 'Mproducttypes', 'Mbanks', 'Mconfigs'));
			$invoice = $this->Minvoices->get($invoiceId);
			if($invoice) {
				$data = array(
					'invoice' => $invoice,
					'teamName' => $this->Mteams->getFieldValue(array('TeamId' => $invoice['TeamId']), 'TeamName'),
					'factoryName' => $this->Mfactories->getFieldValue(array('FactoryId' => $invoice['FactoryId']), 'FactoryName'),
					'listSumProductByTypes' => $this->Minvoices->getSumProductByType($invoiceId),
					'listBanks' =>  $this->Mbanks->getBy(array('StatusId' => STATUS_ACTIVED)),
					'payoneer' => $this->Mconfigs->getConfigValue('PAYONEER', '')
				);
				$productTypePrices = array();
				if(!empty($invoice['PriceList'])) $productTypePrices = json_decode($invoice['PriceList'], true);
				if(empty($productTypePrices)) $productTypePrices = $this->Mproducttypes->getListPriceMap();
				$data['productTypePrices'] = $productTypePrices;
				$this->load->view('invoice/exportPdf', $data);
			}
			else echo "<script>window.close();</script>";
		}
		else echo "<script>window.close();</script>";
	}

	public function exportExcel($invoiceId = 0){
		$user = $this->checkUserLogin();
		$flag = $this->Mactions->checkAccessFromDb('factoryrequest', $user['UserId'], $user['RoleId']);
		if($flag && $invoiceId > 0){
			$this->loadModel(array('Minvoices', 'Mteams', 'Mfactories', 'Mproducttypes', 'Mbanks', 'Mconfigs'));
			$invoice = $this->Minvoices->get($invoiceId);
			if($invoice) {
				$fileUrl = FCPATH . 'assets/uploads/excels/'.'exportInvoice.xls';
	            $this->load->library('excel');
	            $objReader = PHPExcel_IOFactory::createReader('Excel5');
	            $objPHPExcel = $objReader->load($fileUrl);
	            $objPHPExcel->setActiveSheetIndex(0);
	            $sheet = $objPHPExcel->getActiveSheet();
	            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
				        )
				    )
				);
				$styleFontArray = array(
			    'font'  => array(
			        'bold'  => true,
			        'size'  => 15,
			    ));

	            $sheet->setCellValue('E5', 'Date: ' . ddMMyyyy($invoice['InvoiceDate'], 'Y/m/d'));
	            $teamName = $this->Mteams->getFieldValue(array('TeamId' => $invoice['TeamId']), 'TeamName');
	            $sheet->setCellValue('C7', 'To: ' . $teamName);
	            $listSumProductByTypes = $this->Minvoices->getSumProductByType($invoiceId);
	            $productTypePrices = array();
				if(!empty($invoice['PriceList'])) $productTypePrices = json_decode($invoice['PriceList'], true);
				if(empty($productTypePrices)) $productTypePrices = $this->Mproducttypes->getListPriceMap();
	            $i = 10;
	            foreach ($listSumProductByTypes as $p) {
	            	$type = strtoupper($p['ProductType']);
	            	if(isset($productTypePrices[$type])) $price = $productTypePrices[$type];
                    else $price = 0;
	            	$sheet->setCellValue('C' . $i, $type);
	            	$sheet->getStyle('C' . $i)->applyFromArray($styleArray);
	            	$sheet->setCellValue('D' . $i, 'USD');
	            	$sheet->getStyle('D' . $i)->applyFromArray($styleArray);
	            	$sheet->setCellValue('E' . $i, priceFormat($price, true));
	            	$sheet->getStyle('E' . $i)->applyFromArray($styleArray);
	            	$sheet->setCellValue('F' . $i, priceFormat($p['sumQuantity']));
	            	$sheet->getStyle('F' . $i)->applyFromArray($styleArray);
	            	$sheet->setCellValue('G' . $i, priceFormat($p['sumQuantity'] * $price, true));
	            	$sheet->getStyle('G' . $i)->applyFromArray($styleArray);
	            	$i++;
	            	$sheet->insertNewRowBefore($i + 1, 1);
	            }
	            $C = $i + 6;
	            $sheet->mergeCells("C".$C.":E".$C);
	            $sheet->setCellValue('C'.$C, $invoice['Comment']);
	            $G_USD = $i+2;
	            $G_PO = $i+3;
	            $G_VND = $i+4;
	            $sheet->setCellValue('G' . $G_USD, priceFormat($invoice['OrderPrice'], true).'$');
	            $sheet->setCellValue('G' . $G_PO, priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['POPercent'] / 100, true).'$');
	            $sheet->setCellValue('G' . $G_VND, priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['ExchangeRate'], true).'VND');
	            $listBanks = $this->Mbanks->getBy(array('StatusId' => STATUS_ACTIVED));
	            $countBank = $i + 9;
	            foreach ($listBanks as $b){
	            	$sheet->setCellValueExplicit('D'.$countBank, $b['BankName'], PHPExcel_Cell_DataType::TYPE_STRING);
	            	$sheet->getStyle('D'.$countBank)->applyFromArray($styleFontArray);
	            	$sheet->setCellValueExplicit('E'.$countBank, $b['BankNumber'], PHPExcel_Cell_DataType::TYPE_STRING);
	            	$sheet->getStyle('E'.$countBank)->applyFromArray($styleFontArray);
	            	$sheet->setCellValueExplicit('F'.$countBank, $b['BankDesc'], PHPExcel_Cell_DataType::TYPE_STRING);
	            	$sheet->getStyle('F'.$countBank)->applyFromArray($styleFontArray);
	            	$countBank ++;
	            	$sheet->insertNewRowBefore($countBank + 1, 1);
	            	
	            }
	            $countPayoneer = $countBank + 2;

	            $payoneer = $this->Mconfigs->getConfigValue('PAYONEER', '');
	            $sheet->setCellValue('D'.$countPayoneer, $payoneer);
	            $sheet->getStyle('D'.$countPayoneer)->applyFromArray($styleFontArray);
	            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
	            $cellIterator->setIterateOnlyExistingCells(true);
	            $filename = "Export_Invoice_".date('Y-m-d').".xls";
	            header('Content-Type: application/vnd.ms-excel');
	            header('Content-Disposition: attachment;filename="' . $filename . '"');
	            header('Cache-Control: max-age=0');
	            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	            $objWriter->save('php://output');
	            $objPHPExcel->disconnectWorksheets();
	            unset($objPHPExcel);
			}
			else echo "<script>window.close();</script>";
		}
		else echo "<script>window.close();</script>";
	}

	public function insert(){
		$user = $this->checkUserLogin(true);
		$factoryId = $this->input->post('FactoryId');
		$teamOrders = $this->input->post('TeamOrders');
		if($factoryId > 0 && !empty($teamOrders)){
			$comment = trim($this->input->post('Comment'));
			$crDateTime = getCurentDateTime();
			$this->loadModel(array('Minvoices', 'Mproducttypes', 'Mteams', 'Mconfigs'));
			$config = $this->Mconfigs->getListMap(2);
			$productTypePrices = $this->Mproducttypes->getListPriceMap();
			$priceList = json_encode($productTypePrices);
			$teamCodes = array();
			$i = 0;
			$j = 0;
			foreach($teamOrders as $teamId => $orderShopifyIds){
				if($teamId > 0 && is_array($orderShopifyIds) && !empty($orderShopifyIds)){
					$i++;
					if(!isset($teamCodes[$teamId])) $teamCodes[$teamId] = $this->Mteams->getFieldValue(array('TeamId' => $teamId), 'TeamCode');
					$postData = array(
						'InvoiceCode' => 'IV-F'.$factoryId.'-'.$teamCodes[$teamId].'-'.date('Ymd'),
						'InvoiceDate' => $crDateTime,
						'TeamId' => $teamId,
						'FactoryId' => $factoryId,
						'OrderPrice' => $this->Minvoices->getTotalPriceByType($orderShopifyIds, $productTypePrices),
						'OffsetPrice' => 0,
						'ExchangeRate' => isset($config['EXCHANGE_RATE']) ? $config['EXCHANGE_RATE'] : 0,
						'POPercent' => isset($config['PO_PERCENT']) ? $config['PO_PERCENT'] : 0,
						'InvoiceStatusId' => 3,
						'Comment' => $comment,
						'PriceList' => $priceList,
						'CrUserId' => $user['UserId']
					);
					$invoiceId = $this->Minvoices->insert($postData, $orderShopifyIds, $user);
					if($invoiceId > 0) $j++;
				}
			}
			if($i == $j) echo json_encode(array('code' => 1, 'message' => "Send invoice success"));
			elseif($j == 0) echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			elseif($i > $j) echo json_encode(array('code' => 1, 'message' => "Send success {$j} invoices, failed ".($i - $j).' invoices'));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function received(){
		$user = $this->checkUserLogin(true);
		$invoiceId = $this->input->post('InvoiceId');
		if($invoiceId > 0){
			$this->loadModel(array('Minvoiceorders', 'Minvoices'));
			$orderIds = $this->Minvoiceorders->getListOrderIds(array($invoiceId));
			$flag = $this->Minvoices->received($invoiceId, $orderIds, $user);
			if ($flag) echo json_encode(array('code' => 1, 'message' => "Received success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function deleteBatch(){
        $user = $this->checkUserLogin(true);
        $invoiceIds = $this->input->post('InvoiceIds');
        if (!empty($invoiceIds)){
			$this->loadModel(array('Minvoiceorders', 'Minvoices'));
			$orderIds = $this->Minvoiceorders->getListOrderIds($invoiceIds);
            $flag = $this->Minvoices->deleteBatch($invoiceIds, $orderIds, $user);
            if ($flag) echo json_encode(array('code' => 1, 'message' => 'Delete invoice success'));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
    }

    public function update(){
    	$user = $this->checkUserLogin(true);
    	$invoiceId = $this->input->post('InvoiceId');
		$postData = $this->arrayFromPost(array('InvoiceStatusId', 'PriceList', 'OrderPrice', 'OffsetPrice', 'Comment'));
    	if($invoiceId > 0 && $postData['InvoiceStatusId'] > 0 && $postData['OrderPrice'] > 0 && !empty($postData['PriceList'])){
			$crDateTime = getCurentDateTime();
			$postData['UpdateUserId'] = $user['UserId'];
			$postData['UpdateDateTime'] = $crDateTime;
			$actionLogs = array(
				'ItemId' => $invoiceId,
				'ItemTypeId' => 2,
				'ActionTypeId' => 2,
				'Comment' => $user['FullName'] . 'change invoice status to '. $this->Mconstants->invoiceStatus[$postData['InvoiceStatusId']],
				'CrUserId' => $user['UserId'],
				'CrDateTime' => $crDateTime
			);
            $this->loadModel(array('Minvoices'));
            $flag = $this->Minvoices->update($postData, $invoiceId, $actionLogs);
            if ($flag) echo json_encode(array('code' => 1, 'message' => "Update Invoice Success", 'data' => $invoiceId));
            else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
        }
        else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }

    public function sendInvoiceMail(){
    	$user = $this->checkUserLogin(true);
    	$invoiceIds = $this->input->post('InvoiceIds');
		$i = count($invoiceIds);
		if($i > 0){
			$this->loadModel(array('Minvoices', 'Mteams', 'Mfactories', 'Mproducttypes','Mbanks','Mconfigs'));
			$configs = $this->session->userdata('configs');
			if (!$configs) $configs = array();
			$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'In3d.manager@gmail.com';
			$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Teeallover';
            $payoneer = $this->Mconfigs->getConfigValue('PAYONEER', '');
			$productTypePriceDefaults = $this->Mproducttypes->getListPriceMap();
            $listBanks = $this->Mbanks->getBy(array('StatusId' => STATUS_ACTIVED));
			$teams = array();
			$factoryNames = array();
			$j = 0;
			$invoiceMails = array();
			$invoiceUpdates = array();
			$crDateTime = getCurentDateTime();
			foreach($invoiceIds as $invoiceId){
				$team = false;
				$invoice = $this->Minvoices->get($invoiceId);
				if($invoice){
					if(!isset($teams[$invoice['TeamId']])) {
						$team = $this->Mteams->get($invoice['TeamId']);
						$teams[$invoice['TeamId']] = $team;
					}
					else $team = $teams[$invoice['TeamId']];
				}
				if($team){
					$emailTo = array();
					$emails = explode(',', $team['Emails']);
					foreach($emails as $email){
						if(filter_var($email, FILTER_VALIDATE_EMAIL)) $emailTo[] = $email;
					}
					if(!empty($emailTo)){
						if(!isset($factoryNames[$invoice['FactoryId']])) $factoryNames[$invoice['FactoryId']] = $this->Mfactories->getFieldValue(array('FactoryId' => $invoice['FactoryId']), 'FactoryName');
						$data = array(
							'invoice' => $invoice,
							'teamName' => $team['TeamName'],
							'factoryName' => $factoryNames[$invoice['FactoryId']],
							'listSumProductByTypes' => $this->Minvoices->getSumProductByType($invoiceId),
							'listBanks' =>  $listBanks,
							'payoneer' => $payoneer
						);
						$productTypePrices = array();
						if(!empty($invoice['PriceList'])) $productTypePrices = json_decode($invoice['PriceList'], true);
						if(empty($productTypePrices)) $productTypePrices = $productTypePriceDefaults;
						$data['productTypePrices'] = $productTypePrices;
						$message = $this->load->view('invoice/mail', $data, true);
						$flag = $this->sendMail($emailFrom, $companyName, $emailTo, 'Pay Invoice '.$invoice['InvoiceCode'], $message);
						if($flag){
							$j++;
							$invoiceMails[] = array('InvoiceId' => $invoiceId, 'UserId' => $user['UserId'], 'CrDateTime' => $crDateTime);
							$invoiceUpdates[] = array('InvoiceId' => $invoiceId, 'InvoiceStatusId' => 1, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
						}
					}
				}
			}
			if($j > 0) $this->Minvoices->updateNotReceived($invoiceMails, $invoiceUpdates);
			$data = array('StatusName' => '<span class="'.$this->Mconstants->labelCss[1].'">'.$this->Mconstants->invoiceStatus[1].'</span>');
			if($i == $j) echo json_encode(array('code' => 1, 'message' => "Send invoice email to team success", 'data' => $data));
			elseif($j == 0) echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			elseif($i > $j) echo json_encode(array('code' => 1, 'message' => "Send success {$j} invoices, failed ".($i - $j).' invoices', 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
    }
}

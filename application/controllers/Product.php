<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Product List',
			array('scriptFooter' => array('js' => 'js/product.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'product')) {
			$this->load->model('Mproducts');
            $postData = $this->arrayFromPost(array('Sku'));
            $rowCount = $this->Mproducts->getCount($postData);
            $data['listProducts'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listProducts'] = $this->Mproducts->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
			$this->load->view('product/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function add(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Add product',
			array(
				'scriptHeader' => array('css' => 'rickyfiles/ricky.files.css'),
				'scriptFooter' => array('js' => array('ckfinder/ckfinder.js'/*,'rickyfiles/ricky.files.js'*/, 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js'))
			)
		);
		if($this->Mactions->checkAccess($data['listActions'], 'product')) $this->load->view('product/add', $data);
		else $this->load->view('user/permission', $data);
	}

	public function edit($productId = 0){
		if($productId > 0){
			$user = $this->checkUserLogin();
			$data = $this->commonData($user,
				'Edit Product',
				array(
					'scriptHeader' => array('css' => 'rickyfiles/ricky.files.css'),
					'scriptFooter' => array('js' => array('ckfinder/ckfinder.js'/*, 'rickyfiles/ricky.files.js'*/, 'vendor/plugins/sortable/Sortable.min.js', 'js/product_update.js'))
				)
			);
			if($this->Mactions->checkAccess($data['listActions'], 'product')) {
				$this->load->model('Mproducts');
				$product = $this->Mproducts->get($productId);
				if($product && $product['StatusId'] == STATUS_ACTIVED){
					$data['productId'] = $productId;
					if(!empty($product['Images'])) $product['Images'] = json_decode($product['Images'], true);
					else $product['Images'] = array();
					if(!empty($product['ProductImage2'])) array_unshift($product['Images'], $product['ProductImage2']);
					array_unshift($product['Images'], $product['ProductImage']);
					$data['product'] = $product;
				}
				else{
					$data['productId'] = 0;
					$data['txtError'] = "Product not found";
				}
				$this->load->view('product/edit', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('product');
	}

	public function viewImage($productId = 0, $sku = ''){
		$this->checkUserLogin();
		$product = false;
		$this->load->model('Mproducts');
		if($productId > 0) $product = $this->Mproducts->get($productId);
		elseif(!empty($sku)) $product = $this->Mproducts->getBy(array('Sku' => $sku));
		if($product){
			$images = array(IMAGE_PATH.$product['ProductImage']);
			if(!empty($product['ProductImage2'])) $images[] = IMAGE_PATH.$product['ProductImage2'];
			if(!empty($product['Images'])){
				$imgs = json_decode($product['Images'], true);
				foreach($imgs as $img) $images[] = IMAGE_PATH.$img;
			}
			//$data['images'] = $images;
			$this->load->view('product/view_image', array('user' => false, 'title' => $product['Sku'].' images', 'images' => $images));
		}
		else echo "<script>window.close();</script>";
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('Sku', 'ProductName'));
		$images = $this->input->post('Images');
		if(!empty($postData['Sku']) && !empty($images)) {
			$productId = $this->input->post('ProductId');
			$this->load->model('Mproducts');
			$flag = $this->Mproducts->checkExist($productId, $postData['Sku']);
			if($flag) echo json_encode(array('code' => 0, 'message' => "Sku is existed"));
			else {
				$postData['StatusId'] = STATUS_ACTIVED;
				$postData['ProductImage'] = '';
				$postData['ProductImage2'] = '';
				$postData['Images'] = array();
				$i = 0;
				foreach($images as $img){
					$img = replaceFileUrl($img);
					$i++;
					if($i == 1) $postData['ProductImage'] = $img;
					elseif($i == 2) $postData['ProductImage2'] = $img;
					else $postData['Images'][] = $img;
				}
				$postData['Images'] = json_encode($postData['Images']);
				if(!empty($postData['ProductImage'])){
					$img = PRODUCT_PATH.'skus/'.$postData['ProductImage'];
					$folder = '';
					$parts = explode('/', $postData['ProductImage']);
					if(count($parts) > 1){
						for($i = 0; $i < count($parts) - 1; $i++){
							$folder .= $parts[$i].'/';
						}
						$folder = PRODUCT_PATH.'skus/'.$folder;
					}
					if(!empty($folder)) @mkdir($folder, 0777);
					if(!file_exists($img)) $this->compressImage(IMAGE_PATH.$postData['ProductImage'], $img, 50);
				}
				$productId = $this->Mproducts->save($postData, $productId);
				if ($productId > 0) echo json_encode(array('code' => 1, 'message' => "Update product success", 'data' => $productId));
				else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	private function compressImage($source, $destination, $quality) {
		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') $image = @imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') $image = @imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') $image = @imagecreatefrompng($source);
		@imagejpeg($image, $destination, $quality);
		return $destination;
	}

	public function delete(){
		$this->checkUserLogin(true);
		$productId = $this->input->post('ProductId');
		if($productId > 0){
			$this->load->model('Mproducts');
			$flag = $this->Mproducts->changeStatus(0, $productId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete product success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	//tai anh ve tu driver
	/*public function download(){
		$this->load->model('Mgoogleimages');
		$listFiles = $this->Mgoogleimages->getBy(array('IsDownload' => 0));
		//$listFiles = $this->Mgoogleimages->get();
		foreach($listFiles as $file){
			$parts = explode('-', $file['FileName']);
			if(count($parts) >= 3){
				$folder = IMAGE_PATH.trim($parts[0]).'/'.trim($parts[0]).'-'.trim($parts[1]).'/';
				@mkdir($folder, 0777, true);
				$fileName = $folder.$file['FileName'];
				$flag = false;
				if(!file_exists($fileName)){
					file_put_contents($fileName, file_get_contents('https://drive.google.com/uc?export=download&id='.$file['FileId']));
					$flag = true;
				}
				//if($file['IsDownload'] == 0)
					$this->Mgoogleimages->save(array('IsDownload' => 1), $file['GoogleImageId']);
				if($flag) echo 'Download '.$fileName.PHP_EOL;
				//else echo 'Existed '.$fileName.PHP_EOL;
			}
		}
	}*/

	public $skuImages = array();

	public function importSku(){ //da tai anh ve
		$this->recurseDir(IMAGE_PATH);
		$this->load->model('Mproducts');
		foreach($this->skuImages as $sku => $skuImages){
			$flag = $this->Mproducts->checkExist(0, $sku);
			if(!$flag){
				$images = array();
				$image1 = '';
				$image2 = '';
				$imageTmp1 = '';
				$imageTmp2 = '';
				foreach($skuImages as $img){
					$img1 = strtolower($img);
					if(strpos($img1, 'front') !== false){
						$imageTmp1 = replaceFileUrl($img, IMAGE_PATH);
						if(strpos($img1, 'mockup') !== false) $image1 = replaceFileUrl($img, IMAGE_PATH);
					}
					elseif(strpos($img1, 'back') !== false){
						$imageTmp2 = replaceFileUrl($img, IMAGE_PATH);
						if(strpos($img1, 'mockup') !== false) $image2 = replaceFileUrl($img, IMAGE_PATH);
					}
					else{
                        if(empty($imageTmp1)) $imageTmp1 = replaceFileUrl($img, IMAGE_PATH);
                        elseif(empty($imageTmp2)) $imageTmp2 = replaceFileUrl($img, IMAGE_PATH);
                        $images[] = replaceFileUrl($img, IMAGE_PATH);
                    }
				}
				if(empty($image1)) $image1 = $imageTmp1;
				if(empty($image2)) $image2 = $imageTmp2;
				$this->Mproducts->save(array(
					'Sku' => $sku,
					'ProductName' => $sku,
					'ProductImage' => $image1,
					'ProductImage2' => $image2,
					'Images' => json_encode($images),
					'StatusId' => STATUS_ACTIVED
				));
				echo 'Insert '.$sku.PHP_EOL;
			}
			//else echo 'Existed '.$sku.PHP_EOL;
		}
	}

	private function recurseDir($dir) {
		if(is_dir($dir)) {
			if($dh = opendir($dir)){
				while($file = readdir($dh)){
					if($file != '.' && $file != '..'){
						if(is_dir($dir . $file)) $this->recurseDir($dir . $file . '/');
						else{
							$parts = explode('/', $dir);
							$n = count($parts);
							if($n > 2){
								$sku = trim(str_replace(' ', '', $parts[$n-2]));
								$parts = explode('-', $file);
								if(count($parts) > 3){
									$sku1 = trim($parts[2]);
									$parts2 = explode(' ', $sku1);
									if(count($parts2) > 1) $sku1 = trim($parts2[0]);
									$sku = trim($parts[0]).'-'.trim($parts[1]).'-'.$sku1;//.'-'.$parts[3];
									if(!in_array(strtolower($parts[3]), array('front', 'back', 'mockup'))) $sku .= '-'.$parts[3];
								}
                                $sku = str_replace(array('-front.jpg', '-front.png', '-back.jpg', '-back.png', '-front_mockup.jpg', '-front_mockup.png', '-back_mockup.jpg', '-back_mockup.png'), '', $sku);
								$flag = strpos($sku, '.') === false && strpos($sku, '-') > 0 && strpos($sku, '5 ') === false && strpos($sku, '5hoodie') === false;
								if($flag) $this->skuImages[$sku][] = $dir . $file;
							}
						}
					}
				}
			}
			closedir($dh);
		}
	}

	public function compress(){
		$this->load->model('Mproducts');
		$listProducts = $this->Mproducts->getBy(array('StatusId' => STATUS_ACTIVED));
		foreach($listProducts as $p){
			if(!empty($p['ProductImage'])){
				$img = PRODUCT_PATH.'skus/'.str_replace(' ', '_', $p['ProductImage']);
				if(!file_exists($img)) {
					$folder = '';
					$parts = explode('/', $p['ProductImage']);
					if (count($parts) > 1) {
						for ($i = 0; $i < count($parts) - 1; $i++) {
							$folder .= $parts[$i] . '/';
						}
						$folder = PRODUCT_PATH . 'skus/' . $folder;
					}
					if (!empty($folder)) @mkdir($folder, 0777, true);
					@$this->compressImage(IMAGE_PATH . $p['ProductImage'], $img, 10);
					echo $p['ProductImage'] . PHP_EOL;
				}
			}
		}
	}

	public function getImageSize(){
		$this->load->model('Mproducts');
		$listProducts = $this->Mproducts->getBy(array('StatusId' => STATUS_ACTIVED));
		foreach($listProducts as $p){
			//$file_size = filesize(IMAGE_PATH.'HUG/HUG-0623/HUG-0623-LMS-back-mockup-DESKTOP-O1SCATN.jp'); // Get file size in bytes
			$size1 = $size2 = 90;
			if(!empty($p['ProductImage'])) $size1 = filesize(IMAGE_PATH.$p['ProductImage']) / 1024; //kb
			if(!empty($p['ProductImage2'])) $size2 = filesize(IMAGE_PATH.$p['ProductImage2']) / 1024;
			if($size1 < 80){
				$parts = explode('/', $p['ProductImage']);
				$this->db->query('UPDATE googleimages SET IsDownload = 0 WHERE FileName = ?', array($parts[count($parts) - 1]));
				unlink(IMAGE_PATH.$p['ProductImage']);
				echo $size1.': '.$p['ProductImage'].PHP_EOL;
			}
			if($size2 < 80){
				$parts = explode('/', $p['ProductImage2']);
				$this->db->query('UPDATE googleimages SET IsDownload = 0 WHERE FileName = ?', array($parts[count($parts) - 1]));
				unlink(IMAGE_PATH.$p['ProductImage2']);
				echo $size2.': '.$p['ProductImage2'].PHP_EOL;
			}
		}
	}
}
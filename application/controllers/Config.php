<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Common Setting',
			array('scriptFooter' => array('js' => 'js/config.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'config')) {
			$this->load->model('Mconfigs');
			$data['listConfigs'] = $this->Mconfigs->get();
			$this->load->view('setting/config', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$user = $this->checkUserLogin(true);
		$configId = $this->input->post('ConfigId');
		$configValue = trim($this->input->post('ConfigValue'));
		if($configId > 0 && !empty($configValue)){
			$this->load->model('Mconfigs');
			$configId = $this->Mconfigs->save(array('ConfigValue' => $configValue, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), $configId);
			if($configId > 0){
				$configs = $this->Mconfigs->getListMap();
				$this->session->set_userdata('configs', $configs);
				echo json_encode(array('code' => 1, 'message' => "Update config success"));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

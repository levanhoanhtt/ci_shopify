<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function updateProfile(){
		$user = $this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'NewPass', 'FullName', 'Email', 'Avatar'));
		$flag = false;
		if (!empty($postData['NewPass'])) {
			if ($user['UserPass'] == md5($postData['UserPass'])) {
				$flag = true;
				$postData['UserPass'] = md5($postData['NewPass']);
				unset($postData['NewPass']);
			}
			else echo json_encode(array('code' => -1, 'message' => "Old Password is wrong"));
		}
		else {
			$flag = true;
			unset($postData['UserPass']);
			unset($postData['NewPass']);
		}
		if ($flag) {
			if ($this->Musers->checkExist($user['UserId'], $postData['UserName'], $postData['Email'])) {
				echo json_encode(array('code' => -1, 'message' => "Username or Email is exist in system"));
			}
			else {
				$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
				$flag = $this->Musers->save($postData, $user['UserId']);
				if ($flag) {
					$user = array_merge($user, $postData);
					$this->session->set_userdata('user', $user);
					echo json_encode(array('code' => 1, 'message' => "Update profile success"));
				}
				else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			}
		}
	}

	public function changeStatus(){
		$user = $this->checkUserLogin(true);
		$userId = $this->input->post('UserId');
		$statusId = $this->input->post('StatusId');
		if($userId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$flag = $this->Musers->changeStatus($statusId, $userId, '', $user['UserId']);
			if($flag) {
				$txtSuccess = "";
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Delete user success";
				else{
					$txtSuccess = "Change status user success";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function saveUser(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'FullName', 'Email', 'StatusId', 'Avatar', 'RoleId', 'FactoryId', 'TeamId'));
		$userId = $this->input->post('UserId');
		if(empty($postData['UserName'])) $postData['UserName'] = $postData['PhoneNumber'];
		if(!empty($postData['UserName']) && !empty($postData['FullName'])) {
			if ($this->Musers->checkExist($userId, $postData['UserName'], $postData['Email'])) {
				echo json_encode(array('code' => -1, 'message' => "Username or Email is exist in system"));
			}
			else {
				$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
				$userPass = $postData['UserPass'];
				if ($userId == 0){
					$postData['UserPass'] = md5($userPass);
					$postData['CrUserId'] = ($user) ? $user['UserId'] : 0;
					$postData['CrDateTime'] = getCurentDateTime();
				}
				else {
					unset($postData['UserPass']);
					$newPass = trim($this->input->post('NewPass'));
					if (!empty($newPass)) $postData['UserPass'] = md5($newPass);
					$postData['UpdateUserId'] = ($user) ? $user['UserId'] : 0;
					$postData['UpdateDateTime'] = getCurentDateTime();
				}
				$teamIds = $this->input->post('TeamIds');
				if(!is_array($teamIds)) $teamIds = array();
				$userId = $this->Musers->update($postData, $userId, $teamIds);
				if ($userId > 0) {
					if($user && $user['UserId'] == $userId){
						$user = array_merge($user, $postData);
						$this->session->set_userdata('user', $user);
					}
					if ($this->input->post('IsSendPass') == 'on') {
						$message = "Hi {$postData['FullName']}<br/>Your login is:<br/>URL: " . base_url() . "<br/>UserName: {$postData['UserName']}<br/>Password: {$userPass}";
						$configs = $this->session->userdata('configs');
						if (!$configs) $configs = array();
						$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'In3d.manager@gmail.com';
						$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Teeallover';
						$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Login info', $message);
					}
					echo json_encode(array('code' => 1, 'message' => "Update user success", 'data' => $userId));
				}
				else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function checkLogin(){
		header('Content-Type: application/json');
		//log_message('error', json_encode($_POST));
		//log_message('error', json_encode(file_get_contents('php://input')));
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'IsRemember', 'IsGetConfigs'));
		$userName = $postData['UserName'];
		$userPass = $postData['UserPass'];
		if(!empty($userName) && !empty($userPass)) {
			$configs = array();
			$user = $this->Musers->login($userName, $userPass);
			if ($user) {
				if (empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
				$user['SessionId'] = uniqid();
				$this->loadModel(array('Muserteams', 'Mconfigs'));
				$user['TeamIds'] = $this->Muserteams->getTeamIds($user['UserId']);
				$this->session->set_userdata('user', $user);
				if ($postData['IsGetConfigs'] == 1) {
					$configs = $this->Mconfigs->getListMap();
					$this->session->set_userdata('configs', $configs);
				}
				if ($postData['IsRemember'] == 'on') {
					$this->load->helper('cookie');
					$this->input->set_cookie(array('name' => 'userName', 'value' => $userName, 'expire' => '86400'));
					$this->input->set_cookie(array('name' => 'userPass', 'value' => $userPass, 'expire' => '86400'));
				}
				//echo json_encode(array('code' => 1, 'message' => "Đăng nhập thành công", 'data' => array('User' => $user, 'Configs' => $configs, 'message' => "Đăng nhập thành công")));
				echo json_encode(array('code' => 1, 'message' => "Login success", 'data' => array('RoleId' => $user['RoleId'], 'SessionId' => $user['SessionId'], 'message' => "Login success")));
			}
			else echo json_encode(array('code' => 0, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "UserName or Password can not be left blank"));
	}

	public function forgotPass(){
		header('Content-Type: application/json');
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$userPass = bin2hex(mcrypt_create_iv(5, MCRYPT_DEV_RANDOM));
				$message = "Xin chào {$user['FullName']}.<br/> Mật khẩu mới của bạn là {$userPass}";
				$this->load->model('Mconfigs');
				$configs = $this->Mconfigs->getListMap();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'In3d.manager@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Teeallover';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Mật khẩu mới của bạn', $message);
				if($flag){
					$this->Musers->save(array('UserPass' => md5($userPass)), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Send password to {$email} success", 'data' => array('message' => "Send password to {$email} success")));
				}
				else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			}
			else echo json_encode(array('code' => 0, 'message' => "User is not existed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Email can not be left blank"));
	}

	public function checkStatus(){
		header('Content-Type: application/json');
		$userName = trim($this->input->post('UserName'));
		if(!empty($userName)){
			$userId = $this->Musers->getFieldValue(array('UserName' => $userName, 'StatusId' => STATUS_ACTIVED), 'UserId', 0);
			if($userId > 0) echo json_encode(array('code' => 1, 'message' => "User is actived"));
			else echo json_encode(array('code' => -1, 'message' => "User is not existed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "UserName can not be left blank"));
	}

	public function logout(){
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
	}

	public function requestSendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$token = substr($token, 0, 14);
				$message = "Hi {$user['FullName']}<br/>Please go to ".base_url('user/changePass/'.$token).' to change password.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'In3d.manager@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Teeallover';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Reset Password', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Please check your email"));
				}
				else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
			}
			else echo json_encode(array('code' => 0, 'message' => "User is not existed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Email can not be left blank"));
	}
}
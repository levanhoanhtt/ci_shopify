<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function getItemInfo(){
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$barCode = $this->input->post('Sku');
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($barCode)){
			//F3-DCC1011-DCC-A0019-LMS-M => 1 la ma xuong (mã cũ)
			//F3-TAOP1052-AOP-1001-LMS-M-RED ==> mã mới
			$parts = explode('-', $barCode);
			if(count($parts) >= 6){
				$sku = $parts[2].'-'.$parts[3].'-'.$parts[4];
				if(count($parts) == 7) $sku .= '-'.$parts[6];
				$this->load->model('Mproducts');
				$p = $this->Mproducts->getBy(array('Sku' => $sku, 'StatusId' => STATUS_ACTIVED), true);
				if(!$p) $p = $this->Mproducts->getBy(array('Sku' => $parts[2].'-'.$parts[3], 'StatusId' => STATUS_ACTIVED), true);
				if($p){
					if(empty($p['ProductImage'])) $image = IMAGE_PATH.NO_IMAGE;
					else{
						if(file_exists(PRODUCT_PATH.'skus/'.$p['ProductImage'])) $image = base_url(PRODUCT_PATH.'skus/'. $p['ProductImage']);
						else $image = base_url(IMAGE_PATH.$p['ProductImage']);
					}
					$data = array('Sku' => $barCode, 'ProductName' => empty($p['ProductName']) ? $p['Sku'] : $p['ProductName'], 'ProductImage' => $image);
					echo json_encode(array('code' => 1, 'message' => "Get Product info success", 'data' => $data));
				}
				else echo json_encode(array('code' => 0, 'message' => "Product Not Found"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Product Not Found"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function importItem(){ // nhap kho - bo
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$barCode = trim($this->input->post('Sku'));
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($barCode)){
		//if(!empty($barCode)){
			//1-DCC1011-DCC-A0019-LMS-M => 1 la ma xuong
			$parts = explode('-', $barCode);
			if(count($parts) == 6) {
				$factoryId = intval(str_replace('F', '', $parts[0]));
				if($factoryId > 0){
					$this->loadModel(array('Morderproducts', 'Mwarehousescans', 'Mscanimages', 'Mordershopifys'));
					$sku = $parts[2].'-'.$parts[3].'-'.$parts[4].'-'.$parts[5];
					$op = $this->Morderproducts->getBy(array('BarCode' => $parts[1].'-'.$sku), true);
					if($op){
						$productImage = '';
						if(isset($_FILES['Image'])){
							$fileAvatar = $_FILES['Image'];
							if (in_array($fileAvatar['type'], array('image/jpeg', 'image/png'))) {
								$dirName = date('Y_m_d');
								$folderName = PRODUCT_PATH . 'scans/'. $dirName;
								if (!file_exists($folderName.'/')) mkdir($folderName, 0777);
								$fileName = date('YmdHis') . '_' . makeSlug($fileAvatar['name']) . '.jpg';
								if (move_uploaded_file($fileAvatar["tmp_name"], $folderName.'/'.$fileName)) $productImage = $dirName.'/'.$fileName;
							}
						}
						$isError = $this->input->post('IsError') == 1 ? 1 : 2;
						$wc = $this->Mwarehousescans->getBy(array('BarCode' => $barCode, 'InternalTrackingId' => 0), true);
						$warehouseScanId = 0;
						if($wc){
							$warehouseScanId = $wc['WarehouseScanId'];
							$postData = array('ScanDate' => getCurentDateTime(), 'Quantity' => $wc['Quantity'] + 1);
						}
						else{
							$postData = array(
								'ScanDate' => getCurentDateTime(),
								'BarCode' => $barCode,
								'FactoryId' => $factoryId,
								'Sku' => $parts[2].'-'.$parts[3].'-'.$parts[4].'-'.$parts[5],
								'OrderName' => $parts[1],
								'OrderShopifyId' => $op['OrderShopifyId'],
								'OrderProductId' => $op['OrderProductId'],
								'Quantity' => $isError == 1 ? 0 : 1,
								'ProductType' => $parts[4]
							);
						}
						$scanImage = array(
							'ProductImage' => $productImage,
							'IsError' => $isError,
							'Comment' => trim($this->input->post('Comment'))
						);
						$flag = $this->Mwarehousescans->insert($postData, $warehouseScanId, $scanImage, $op['OrderProductId']);
						if($flag > 0){
							//check du so luong sp chua
							$listProducts = $this->Morderproducts->getBy(array('OrderShopifyId' => $op['OrderShopifyId']), false, '', 'Quantity,WarehouseQuantity');
							$n = count($listProducts);
							$i = 0;
							foreach($listProducts as $p){
								if($p['Quantity'] == $p['WarehouseQuantity']) $i++;
							}
							if($i == $n) $this->Mordershopifys->save(array('TransportStatusId' => 2), $op['OrderShopifyId']);
							echo json_encode(array('code' => 1, 'message' => "Import Product success"));
						}
						else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
					}
					else echo json_encode(array('code' => -1, 'message' => "Product Not Found"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Factory Not Found"));
			}
			else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function exportItem(){ //xuat xuong - cai moi la nhap kho
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$barCode = $this->input->post('Sku'); //sku ghep
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($barCode)){
			//F3-DCC1011-DCC-A0019-LMS-M => 1 la ma xuong (mã cũ)
			//F3-TAOP1052-AOP-1001-LMS-M-RED ==> mã mới
			$parts = explode('-', $barCode);
			if(count($parts) >= 6) {
				$factoryId = intval(str_replace('F', '', $parts[0]));
				if($factoryId > 0) {
					$this->loadModel(array('Morderproducts', 'Mfactoryscans', 'Mscanimages'));
					$sku = $parts[2] . '-' . $parts[3] . '-' . $parts[4] . '-' . $parts[5];// . '-' . $parts[6];
					if(count($parts) == 7) $sku .= '-' . $parts[6];
					$op = $this->Morderproducts->getBy(array('BarCode' => $parts[1] . '-' . $sku), true);
					if($op){
						$productImage = '';
						if (isset($_FILES['Image'])) {
							$fileAvatar = $_FILES['Image'];
							if (in_array($fileAvatar['type'], array('image/jpeg', 'image/png'))) {
								$dirName = date('Y_m_d');
								$folderName = PRODUCT_PATH . 'scans/' . $dirName;
								if (!file_exists($folderName . '/')) mkdir($folderName, 0777);
								$fileName = date('YmdHis') . '_' . makeSlug($fileAvatar['name']) . '.jpg';
								if (move_uploaded_file($fileAvatar["tmp_name"], $folderName . '/' . $fileName)) $productImage = $dirName . '/' . $fileName;
							}
						}
						$scanImage = array();
						if(!empty($productImage)) {
							$scanImage = array(
								'WarehouseScanId' => 0,
								'ProductImage' => $productImage,
								'IsError' => 2,
								'Comment' => ''
							);
						}
						$factoryScanId = 0;
						$fc = $this->Mfactoryscans->getBy(array('BarCode' => $barCode, 'InternalTrackingId' => 0), true);
						if($fc){
							$postData = array('ScanDate' => getCurentDateTime(), 'OrderShopifyId' => $op['OrderShopifyId'], 'OrderProductId' => $op['OrderProductId'], 'Quantity' => $fc['Quantity'] + 1);
							$factoryScanId = $fc['FactoryScanId'];
						}
						else {
							$postData = array(
								'ScanDate' => getCurentDateTime(),
								'BarCode' => $barCode,
								'FactoryId' => $factoryId,
								'Sku' => $sku,
								'OrderName' => $parts[1],
								'OrderShopifyId' => $op['OrderShopifyId'],
								'OrderProductId' => $op['OrderProductId'],
								'Quantity' => 1,
								'ProductType' => $parts[4],
								'InternalTrackingId' => 0
							);
						}
						$flag = $this->Mfactoryscans->insert($postData, $factoryScanId, $scanImage);
						if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Export Product success"));
						else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
					}
					else echo json_encode(array('code' => -1, 'message' => "Product Not Found"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Product Not Found"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Factory Not Found"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function getOrderItems(){
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$trackingCode = $this->input->post('OrderCode');
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($trackingCode)){
			$this->loadModel(array('Mordershopifys', 'Morderproducts', 'Mfactoryorders'));
			$order = $this->Mordershopifys->getBy(array('TrackingCode' => $trackingCode, 'OrderStatusId >' => 0, 'OrderStatusId !=' => 3), true);
			if($order){
				$factoryId = $this->Mfactoryorders->getFieldValue(array('OrderShopifyId' => $order['OrderShopifyId']), 'FactoryId', 0);
				if($factoryId > 0) {
					$products = $this->Morderproducts->getBy(array('OrderShopifyId' => $order['OrderShopifyId']), false, '', 'ProductName,Sku,Quantity,Size,ProductType');
					for($i = 0; $i < count($products); $i++){
						$products[$i]['Sku'] = $factoryId.'-'.$order['OrderName'].'-'.$products[$i]['Sku'];
						$products[$i]['ProductName'] = $products[$i]['Sku'];
					}
					$data = array(
						'message' => 'Get Order info success',
						'Products' => $products,
						'Info' => array(
							'OrderName' => $order['OrderName'],
							'OrderCode' => $trackingCode,
							'OrderStatusId' => array('id' => $order['OrderStatusId'], 'name' => $this->Mconstants->orderStatus[$order['OrderStatusId']]),
							'TransportStatusId' => array('id' => $order['TransportStatusId'], 'name' => $this->Mconstants->transportStatus[$order['TransportStatusId']])
						)
					);
					echo json_encode(array('code' => 1, 'data' => $data));
				}
				else echo json_encode(array('code' => -1, 'message' => "Order not set to Factory"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Order Not Found"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function packing(){ //dong hang
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$trackingCode = $this->input->post('OrderCode');
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($trackingCode)){
			$this->loadModel(array('Mordershopifys', 'Morderpackings'));
			$orderShopifyId = $this->Mordershopifys->getFieldValue(array('TrackingCode' => $trackingCode, 'OrderStatusId >' => 0, 'OrderStatusId !=' => 3, 'TransportStatusId' => 2), 'OrderShopifyId', 0);
			if($orderShopifyId > 0){
				$productImage = '';
				if(isset($_FILES['Image'])){
					$fileAvatar = $_FILES['Image'];
					if (in_array($fileAvatar['type'], array('image/jpeg', 'image/png'))) {
						$dirName = date('Y_m_d');
						$folderName = PRODUCT_PATH . 'packings/'. $dirName;
						if (!file_exists($folderName.'/')) mkdir($folderName, 0777);
						$fileName = date('YmdHis') . '_' . makeSlug($fileAvatar['name']) . '.jpg';
						if (move_uploaded_file($fileAvatar["tmp_name"], $folderName.'/'.$fileName)) $productImage = $dirName.'/'.$fileName;
					}
				}
				$postData = array(
					'ScanDate' => getCurentDateTime(),
					'TrackingCode' => $trackingCode,
					'OrderShopifyId' => $orderShopifyId,
					'Image' => $productImage,
					'Comment' => $this->input->post('Comment')
				);
				$flag = $this->Morderpackings->insert($postData, $user['UserId']);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Packed Order success"));
				else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Order Not Found"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function exportPackage(){ //chuyen trang thai DH tu dong hang sang dang giao
		header('Content-Type: application/json');
		$user = $this->checkUserLogin(true);
		$trackingCode = $this->input->post('OrderCode');
		if($user['SessionId'] == $this->input->post('SessionId') && !empty($trackingCode)){
			$this->load->model('Mordershopifys');
			$orderShopifyId = $this->Mordershopifys->getFieldValue(array('TrackingCode' => $trackingCode, 'TransportStatusId' => 3), 'OrderShopifyId', 0);
			if($orderShopifyId > 0) {
				$flag = $this->Mordershopifys->save(array('TransportStatusId' => 4, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), $orderShopifyId);
				echo json_encode(array('code' => 1, 'message' => "Change status success"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Order is not Packed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "You need to login!"));
	}

	public function getFactoryScan(){
		header('Content-Type: application/json');
		$userName = trim($this->input->post('UserName'));
		$userPass = trim($this->input->post('UserPass'));
		$postData = $this->arrayFromPost(array('FactoryId', 'OrderName', 'BeginDate', 'EndDate'));
		if(!empty($userName) && !empty($userPass) && ($postData['FactoryId'] > 0 || !empty($postData['OrderName']) || !empty($postData['BeginDate']) || !empty($postData['EndDate']))){
			$user = $this->Musers->login($userName, $userPass);
			if($user) {
				if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$this->load->model('Mfactoryscans');
				$listFactoryScans = $this->Mfactoryscans->search($postData);
				$data = array();
				foreach($listFactoryScans as $fc){
					$data[] = array(
						'ScanDate' => ddMMyyyy($fc['ScanDate'], 'd/m/Y H:i'),
						'OrderName' => $fc['OrderName'],
						'Sku' => $fc['Sku'],
						'Quantity' => $fc['Quantity'],
						'Images' => $this->Mfactoryscans->getListImages($fc['FactoryScanId'], true)
					);
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}
			else echo json_encode(array('code' => -1, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function getListWithStatus(){
		header('Content-Type: application/json');
		$userName = trim($this->input->post('UserName'));
		$userPass = trim($this->input->post('UserPass'));
		$postData = $this->arrayFromPost(array('ShopCode', 'BeginDate', 'EndDate'));
		if(!empty($userName) && !empty($userPass) && !empty($postData['ShopCode'])){
			$user = $this->Musers->login($userName, $userPass);
			if($user) {
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$this->load->model('Mordershopifys');
				$listOrders = $this->Mordershopifys->search($postData, 0, 1, 'OrderShopifyId, OrderName, OrderStatusId, OrderExtraStatusId, CustomerName, CustomerEmail, OrderDateTime');
				$orderStatus = $this->Mconstants->orderStatus;
				$orderExtraStatus = $this->Mconstants->orderExtraStatus;
				for($i = 0; $i < count($listOrders); $i++){
					$listOrders[$i]['OrderStatusName'] = isset($orderStatus[$listOrders[$i]['OrderStatusId']]) ? $orderStatus[$listOrders[$i]['OrderStatusId']] : '';
					$listOrders[$i]['OrderExtraStatusName'] = isset($orderExtraStatus[$listOrders[$i]['OrderExtraStatusId']]) ? $orderExtraStatus[$listOrders[$i]['OrderExtraStatusId']] : '';
				}
				echo json_encode(array('code' => 1, 'data' => $listOrders));
			}
			else echo json_encode(array('code' => -1, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	/*public function statistics(){
		header('Content-Type: application/json');
		$userName = trim($this->input->post('UserName'));
		$userPass = trim($this->input->post('UserPass'));
		$postData = $this->arrayFromPost(array('ShopCode', 'BeginDate', 'EndDate'));
		if(!empty($userName) && !empty($userPass) && !empty($postData['ShopCode'])){
			$user = $this->Musers->login($userName, $userPass);
			if($user) {
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate'], 'd/m/Y', 'Y-m-d 23:59:59');
				$orderStatusStatistics = array(
					5 => 'purchase_order',
					6 => 'paid_order',
					2 => 'sent_factory',
					12 => 'shipped',
					4 => 'waiting',
					3 => 'canceled'
				);
				$this->load->model('Mordershopifys');
				$statistics = $this->Mordershopifys->statisticCounts($postData, $orderStatusStatistics);
				$data = array();
				foreach($statistics as $id => $count){
					if(isset($orderStatusStatistics[$id])) $data[$orderStatusStatistics[$id]] = intval($count);
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}
			else echo json_encode(array('code' => -1, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}

	public function getOrderStatus(){
		header('Content-Type: application/json');
		$userName = trim($this->input->post('UserName'));
		$userPass = trim($this->input->post('UserPass'));
		$orderName = trim($this->input->post('OrderName'));
		if(!empty($userName) && !empty($userPass) && !empty($orderName)){
			$user = $this->Musers->login($userName, $userPass);
			if($user){
				$this->load->model('Mordershopifys');
				$orderStatusId = $this->Mordershopifys->getFieldValue(array('OrderName' => $orderName), 'OrderStatusId', 0);
				if($orderStatusId > 0) echo json_encode(array('code' => 1, 'data' => array('OrderStatusId' => intval($orderStatusId), 'OrderStatusName' => $this->Mconstants->orderStatus[$orderStatusId])));
				else echo json_encode(array('code' => -1, 'message' => "Not exist order"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Login failed"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}*/
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Team List',
			array(
                'scriptFooter' => array('js' => array('js/team.js'))
            )
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($data['listActions'], 'team')) {
			$this->load->model('Mteams');
			$data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/team', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('TeamCode', 'TeamName', 'Emails'));
		if(!empty($postData['TeamName']) && !empty($postData['Emails'])){
			$postData['StatusId'] = STATUS_ACTIVED;
			$teamId = $this->input->post('TeamId');
			$this->load->model('Mteams');
			$flag = $this->Mteams->save($postData, $teamId);
			if ($flag > 0) {
				$postData['TeamId'] = $flag;
				$postData['IsAdd'] = ($teamId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Update team success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$teamId = $this->input->post('TeamId');
		if($teamId > 0){
			$this->load->model('Mteams');
			$flag = $this->Mteams->changeStatus(0, $teamId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete team success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

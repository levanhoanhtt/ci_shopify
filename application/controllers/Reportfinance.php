<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportfinance extends MY_Controller{

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Report Finance',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/report_finance.js'))
            )
        );
        if ($this->Mactions->checkAccess($data['listActions'], 'reportfinance')) {
            //$teamId = $user['TeamId'];
            $teamIds = $user['TeamIds'];
            $this->load->model(array('Mteams', 'Mshops', 'Mordershopifys'));
            if(empty($teamIds)) $data['listTeams'] = $this->Mteams->getBy(array('StatusId' => STATUS_ACTIVED));
            else $data['listTeams'] = $this->Mteams->getListByIds($teamIds);
            if(empty($teamIds)) $data['listShops'] = $this->Mshops->getBy(array('StatusId' => STATUS_ACTIVED));
            else $data['listShops'] = $this->Mshops->getByTeamIds($teamIds);
            $postData = $this->arrayFromPost(array('TeamId', 'ShopId', 'ToDate', 'FromDate'));
            if (empty($postData['TeamId']) && empty($postData['ShopId']) && empty($postData['ToDate']) && empty($postData['FromDate'])) $data['listReport'] = array();
            else $data['listReport'] = $this->Mordershopifys->searchReport($postData);
            $this->load->view('report_finance/report', $data);
        }
        else $this->load->view('user/permission', $data);
    }

    public function export(){
        $user = $this->checkUserLogin();
        $flag = $this->Mactions->checkAccessFromDb('reportfinance', $user['UserId'], $user['RoleId']);
        $listExportStr = $this->input->post('ListExport');
        if($flag && !empty($listExportStr)){
            $listExport = json_decode($listExportStr, true);
            $fileUrl = FCPATH . 'assets/uploads/excels/exportReport.xls';
            $this->load->library('excel');
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $objPHPExcel = $objReader->load($fileUrl);
            $objPHPExcel->setActiveSheetIndex(0);
            $sheet = $objPHPExcel->getActiveSheet();
            $i = 2;
            foreach ($listExport as $export) {
                $sheet->setCellValue('A' . $i, $export['CrDateTime']);
                $sheet->setCellValue('B' . $i, $export['TeamName']);
                $sheet->setCellValue('C' . $i, $export['ShopName']);
                $sheet->setCellValue('D' . $i, $export['ProductType']);
                $sheet->setCellValue('E' . $i, $export['Quantity']);
                $sheet->setCellValue('F' . $i, $export['Price']);
                $sheet->setCellValue('G' . $i, $export['Sum']);
                $i++;
            }
            $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
            $filename = "exportReport.xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
            unset($objPHPExcel);
        }
        else echo "<script>window.close();</script>";
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Factory extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Factory',
			array('scriptFooter' => array('js' => 'js/factory.js'))
		);
		$listActions = $data['listActions'];
		if($this->Mactions->checkAccess($data['listActions'], 'factory')) {
			$this->load->model('Mfactories');
			$data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/factory', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('FactoryName', 'FactoryDay', 'WarehouseDay'));
		if(!empty($postData['FactoryName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$factoryId = $this->input->post('FactoryId');
			$this->load->model('Mfactories');
			$flag = $this->Mfactories->save($postData, $factoryId);
			if ($flag > 0) {
				$postData['FactoryId'] = $flag;
				$postData['IsAdd'] = ($factoryId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Update factory success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$factoryId = $this->input->post('FactoryId');
		if($factoryId > 0){
			$this->load->model('Mfactories');
			$flag = $this->Mfactories->changeStatus(0, $factoryId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete factory success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller {

	private static $resources = array(
		'Images' => array(
			'directory' => 'assets/uploads/images',
			'maxSize' => 0,
			'allowedExtensions' => 'bmp,gif,jpeg,jpg,png,svg'
		)
	);

	private function createPathAndFolderRoot($pathSource){
		$pathSource = explode('/',$pathSource);
		$path = '';
		foreach ($pathSource as $value){
			$path.=$value.'/';
			if(!file_exists($path)){
				mkdir($path,0777);
			}
		}
		return $pathSource[count($pathSource)-1];
	}

	public function initData(){
		$resourceName = $this->input->post('resourceName');
		if(isset(self::$resources[$resourceName])) {
			$pathSource = self::$resources[$resourceName]['directory'];
			//$pathSource = $this->input->post('pathSource');
			$data = array('pathSource' => $pathSource);
			$data['folderRoot'] = $this->createPathAndFolderRoot($pathSource);
			//get folders
			$folders = $this->getFolder($pathSource);
			//get firt files
			$paths = scandir($pathSource);
			unset($paths[0]);
			unset($paths[1]);
			$filesFirst = array_filter($paths, function ($filter) use ($pathSource) {
				return is_file($pathSource . '/' . $filter);
			});
			$filesFirst = array_map(function ($item) use ($pathSource) {
				return $pathSource . '/' . $item;
			}, array_values($filesFirst));

			$data['folders'] = json_encode($folders);
			$data['filesFirst'] = json_encode($filesFirst);
			echo json_encode(array('code' => 1, 'data' => $data));
			//echo json_encode($data);
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	private function scanDir($path){
		$paths = scandir($path);
		unset($paths[0]);
		unset($paths[1]);
		return $paths;
	}

	public function loadImage(){
		$path = $this->input->post('path');
		$images = [];
		if(file_exists($path) && is_dir($path)) {
			$images = $this->scanDir($path);
		}

		$images = array_filter($images,function($name) use ($path){
			return is_file($path . '/' . $name);
		});

		$images = array_map(function ($map) use ($path) {
			return $path . '/' . $map;
		}, array_values($images));

		echo json_encode($images);
	}

	private function getFolder($pathSource){
		$folders = [];
		$folderLv1 = scandir($pathSource);
		unset($folderLv1[0]);
		unset($folderLv1[1]);
		foreach ($folderLv1 as $value) {
			if(file_exists($pathSource. '/' . $value) &&  is_dir($pathSource. '/' . $value)) {
				$folderLv2 = $this->scanDir($pathSource .'/' . $value);
				$folders[$value] = $folderLv2;
				$folders[$value] = array_values(array_filter($folders[$value],function($name) use($value,$pathSource){
					return is_dir($pathSource . '/'. $value . '/' .$name);
				}));
			}
		}
		return $folders;
	}

	public function loadFolder(){
		$path = $this->input->post('path');
		$folders = $this->getFolder($path);
		echo json_encode($folders);
	}

	public function createFolder(){
		$path = $this->input->post('path');
		$folderName = $this->input->post('folderName');
		if (!file_exists($path.'/' . $folderName)) {
			$folderName = makeSlug($folderName);
			mkdir($path.'/' . $folderName, 0777);
			echo json_encode(['status' => 1]);
		}
		else echo json_encode(['status' => -1]);
	}

	public function deleteFolder(){
		/*$path = $this->input->post('path');
		$this->deleteItem($path,$path,0);*/
		echo json_encode(['status'=>1]);
	}

	private function deleteItem($path,$pathOrigin,$type){
		if(!file_exists($pathOrigin)){
			return false;
		}
		if($type == 1 &&  $path == $pathOrigin) {
			$folder = $this->scanDir($path);
			if(count($folder) == 0) {
				rmdir($path);
			}
			return false;
		}
		if(!file_exists($path)) return false;

		if(is_file($path)) {
			unlink($path);
			$newPathArray = explode("/",$path);
			$newPath = implode('/',array_slice($newPathArray,0,count($newPathArray)-1));
			$this->deleteItem($newPath,$pathOrigin,1);
		}
		if (is_dir($path)) {
			$folder = $this->scanDir($path);
			if(count($folder) == 0) {
				rmdir($path);
				$newPathArray = explode("/",$path);
				$newPath = implode('/',array_slice($newPathArray,0,count($newPathArray)-1));
				$this->deleteItem($newPath,$pathOrigin,1);
			}
			else{
				foreach ($folder as $value){
					$this->deleteItem($path.'/'.$value,$pathOrigin,0);
				}
			}
		}
		return true;
	}

	public function uploadFile(){
		$files = $_FILES;
		$data = [];
		//$pathNewCreate = [];
		foreach ($files as $key => $file){
			$path = explode('+',$key)[0];
			$pathArray = explode('/',$path);
			$newPath = $pathArray[0];
			for ($i = 1 ;$i < count($pathArray) ; $i++){
				$newPath.='/'.$pathArray[$i];
				if(!file_exists($newPath)) {
					mkdir($newPath, 0777);
				}
			}
			$fileName = makeSlug($file['name']);
			move_uploaded_file($file['tmp_name'],$newPath.'/'.$fileName);
			$data[] = $newPath.'/'.$fileName;
		}
		echo json_encode($data);
	}

	public function deleteFile(){
		$paths = $this->input->post('paths');
		foreach ($paths as $path){
			if(file_exists($path) && is_file($path)) unlink($path);
		}
	}
}
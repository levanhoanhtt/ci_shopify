<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orderpacking extends MY_Controller {

    public function index(){
        $user = $this->checkUserLogin();
        $data = $this->commonData($user,
            'Order Packing List',
            array(
                'scriptHeader' => array('css' => array('vendor/plugins/datepicker/datepicker3.css')),
                'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/orderpacking.js'))
            )
        );
        if($this->Mactions->checkAccess($data['listActions'], 'orderpacking')) {
            $this->load->model('Morderpackings');
            $postData = $this->arrayFromPost(array('TrackingCode', 'OrderName', 'ScanDate'));
            if(!empty($postData['ScanDate'])) $postData['ScanDate'] = ddMMyyyyToDate($postData['ScanDate']);
            $rowCount = $this->Morderpackings->getCount($postData);
            $data['listOrderPackings'] = array();
            if($rowCount > 0){
                $perPage = DEFAULT_LIMIT;
                $pageCount = ceil($rowCount / $perPage);
                $page = $this->input->post('PageId');
                if(!is_numeric($page) || $page < 1) $page = 1;
                $data['listOrderPackings'] = $this->Morderpackings->search($postData, $perPage, $page);
                $data['paggingHtml'] = getPaggingHtml($page, $pageCount);
            }
            $this->load->view('orderpacking/list', $data);
        }
        else $this->load->view('user/permission', $data);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Producttype extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user,
			'Product Type',
			array('scriptFooter' => array('js' => 'js/product_type.js'))
		);
		if($this->Mactions->checkAccess($data['listActions'], 'producttype')) {
			$this->loadModel(array('Mproducttypes', 'Mfactories'));
			$data['listFactories'] = $this->Mfactories->getBy(array('StatusId' => STATUS_ACTIVED));
			$data['listProductTypes'] = $this->Mproducttypes->getBy(array('StatusId' => STATUS_ACTIVED));
			$this->load->view('setting/product_type', $data);
		}
		else $this->load->view('user/permission', $data);
	}

	public function update(){
		$this->checkUserLogin(true);
		$postData = $this->arrayFromPost(array('ProductTypeName', 'FactoryId', 'Price'));
		$postData['Price'] = replacePrice($postData['Price']);
		if(!empty($postData['ProductTypeName']) && $postData['FactoryId'] > 0 && $postData['Price'] > 0) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$productTypeId = $this->input->post('ProductTypeId');
			$this->loadModel(array('Mproducttypes', 'Mfactories'));
			$flag = $this->Mproducttypes->save($postData, $productTypeId);
			if ($flag > 0) {
				$postData['ProductTypeId'] = $flag;
				$postData['IsAdd'] = ($productTypeId > 0) ? 0 : 1;
				$postData['FactoryName'] = $this->Mfactories->getFieldValue(array('FactoryId' => $postData['FactoryId']), 'FactoryName');
				echo json_encode(array('code' => 1, 'message' => "Update product type success", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
	
	public function delete(){
		$this->checkUserLogin(true);
		$productTypeId = $this->input->post('ProductTypeId');
		if($productTypeId > 0){
			$this->load->model('Mproducttypes');
			$flag = $this->Mproducttypes->changeStatus(0, $productTypeId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Delete product type success"));
			else echo json_encode(array('code' => 0, 'message' => "An error occurred during the execution"));
		}
		else echo json_encode(array('code' => -1, 'message' => "An error occurred during the execution"));
	}
}

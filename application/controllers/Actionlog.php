<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actionlog extends MY_Controller {

	public function index(){
		$user = $this->checkUserLogin();
		$data = $this->commonData($user, 'Order Log');
		if($this->Mactions->checkAccess($data['listActions'], 'actionlog')) {
			$this->loadModel(array('Mordershopifys', 'Mactionlogs'));
			$rowCount = $this->Mactionlogs->getCount(1);
			$data['listActionlogs'] = array();
			if($rowCount > 0){
				$perPage = DEFAULT_LIMIT;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listActionLogs'] = $this->Mactionlogs->search(1, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('actionlog/list', $data);
		}
		else $this->load->view('user/permission', $data);
	}


}
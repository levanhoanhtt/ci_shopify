<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('orderall'); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="control-label">Team</label>
                                <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamIds[]', $teamIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label">Shop</label>
                                <?php $this->Mconstants->selectObject($listShops, 'ShopId', 'ShopName', 'ShopIds[]', $shopIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="OrderName" value="<?php echo set_value('OrderName'); ?>" placeholder="Order Name">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="CustomerName" value="<?php echo set_value('CustomerName'); ?>" placeholder="Customer Name">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="CustomerEmail" value="<?php echo set_value('CustomerEmail'); ?>" placeholder="Customer Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <a href="javascript:void(0)" id="aExportAll" target="_blank" class="btn btn-info">Export Excel</a>
                                <input type="text" hidden="hidden" id="exportUrl" value="<?php echo base_url('ordershopify/exportExcelOrderAll'); ?>/">
                            </div>
                            <div class="col-sm-3">
                                <ul class="list-inline">
                                    <li><input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search"></li>
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                </ul>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <form action="<?php echo base_url('orderall/exportExcel') ?>" method="POST" target="_blank" id="exportForm" style="display: none;">
                            <input type="hidden" name="OrderIds" value="" id="orderIds"/>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php $title = '<span class="label label-success">'.$orderCount.'</span> Orders';
                    sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-md-3">
                                <select class="form-control input-sm select-action" id="selectAction">
                                    <option value="">Choose action</option>
                                    <option value="export_excel">Export Excel</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><label class="lbCheckbox"><input type="checkbox" class="iCheckTable checkAll" id="checkAll" value="0"><span></span></label></th>
                                <th>Book Created</th>
                                <th>Order Name</th>
                                <th>Team</th>
                                <th>Shop</th>
                                <th>Country</th>
                                <th>Customer Name</th>
                                <th>Customer Email</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php foreach ($listOrderAlls as $o):?>
                                <tr>
                                    <td><label class="lbCheckbox"><input type="checkbox" class="iCheckTable iCheckItem" value="<?php echo $o['OrderAllId']; ?>"><span></span></label></td>
                                    <td><?php echo ddMMyyyy($o['CreatedDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $o['OrderName']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'TeamId'), 'TeamName'); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'ShopName'); ?></td>
                                    <td><?php echo $o['CountryName']; ?></td>
                                    <td><?php echo $o['CustomerName']; ?></td>
                                    <td><?php echo $o['CustomerEmail']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
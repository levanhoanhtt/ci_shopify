<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Scan Date</th>
                                <th>Request Date</th>
                                <th>Factory</th>
                                <th>Quantity</th>
                                <th>ProductType</th>
                                <th>Late Day</th>
                                <th>Price</th>
                                <th>Penalty Percentage</th>
                                <th>Penalty</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($lateOrders as $order){?>
                                    <tr>
                                        <td><?php echo $order['ScanDate']?></td>
                                        <td><?php echo $order['RequestDate']?></td>
                                        <td><?php echo $order['FactoryName']?></td>
                                        <td><?php echo $order['Quantity']?></td>
                                        <td><?php echo $order['ProductType']?></td>
                                        <td><?php echo $order['LateDay']?></td>
                                        <td><?php echo round($order['Price'], 2)?></td>
                                        <td><?php echo $order['PenaltyPercentage']?></td>
                                        <td><?php echo round($order['Penalty'],2)?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
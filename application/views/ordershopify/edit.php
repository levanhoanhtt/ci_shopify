<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canUpdateCustomer){ ?><li><button class="btn btn-primary submit">Save</button></li><?php } ?>
                    <li><a href="<?php echo base_url('ordershopify'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($orderShopifyId > 0){ ?>
                    <?php echo form_open('ordershopify/update', array('id' => 'orderShopifyForm')); ?>
                    <div class="row">
                        <div class="col-sm-8 no-padding">
                            <div class="box box-default padding15">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Order Name: <span id="spanOrderName"><?php echo $orderShopify['OrderName']; ?></span></h3>
                                    <?php if($canEdit){ ?>
                                        <div class="box-tools pull-right">
                                            <button type="button" class="btn btn-info btn-sm" id="btnAddSku">Thêm sku</button>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="box-body">
                                    <div class="no-padding divTable">
                                        <?php if(empty($listOrderProductsChild)){ ?>
                                        <table class="table table-hover table-bordered">
                                            <thead class="theadNormal">
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Sku</th>
                                                    <th style="width: 100px;">Quantity</th>
                                                    <th style="width: 100px;">Price</th>
                                                    <th style="width: 100px;">Sum</th>
                                                    <?php if($canDeleteProduct){ ?><th style="width: 5px;"></th><?php } ?>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyOrderProducts">
                                                <?php $disabled = '';
                                                if(!$canEdit) $disabled = ' disabled';
                                                $totalMoney = 0;
                                                foreach($listOrderProducts as $o){
                                                    $money = $o['Quantity'] * $o['Price'];
                                                    $totalMoney += $money; ?>
                                                    <tr data-id="<?php echo $o['OrderProductId'] ?>" class="trProduct">
                                                        <td><input type="text" class="form-control productName" value="<?php echo $o['ProductName']; ?>"<?php echo $disabled; ?>></td>
                                                        <td><input type="text" class="form-control sku" value="<?php echo $o['Sku']; ?>"<?php echo $disabled; ?>></td>
                                                        <td><input class="form-control quantity cost" value="<?php echo priceFormat($o['Quantity']); ?>"<?php echo $disabled; ?>></td>
                                                        <td><input class="form-control price cost" value="<?php echo priceFormat($o['Price'], true); ?>"<?php echo $disabled; ?>></td>
                                                        <td><input class="form-control sumPrice text-right" disabled value="<?php echo priceFormat($money, true); ?>"></td>
                                                        <?php if($canDeleteProduct){ ?><td><a href="javascript:void(0)" class="link_delete"><i class="fa fa-times" title="Delete"></i></a></td><?php } ?>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td colspan="6" class="text-right fz-15"><b>Sum Cost: <span id="spanTotalMoney"><?php echo priceFormat($totalMoney, true); ?></span></b></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" class="text-right fz-15"><b>Total Cost: <span id="spanTotalMoney"><?php if( $orderShopify['TotalPrice'] > 0) echo priceFormat($orderShopify['TotalPrice'], true); ?></span></b></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <?php } else { ?>
                                            <table class="table table-hover table-bordered">
                                                <thead>
                                                    <tr><th>Child Orders</th></tr>
                                                </thead>

                                                <tbody>
                                                    <?php foreach($listOrderProductsChild as $op){ ?>
                                                        <tr>
                                                            <td><a target="_blank" href="<?php echo base_url('ordershopify/edit') . '/' .$op['OrderShopifyId'] ?>"><?php echo $op['OrderName'] ?></a></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td class="text-right fz-15"><b>Total Cost: <span id="spanTotalMoney"><?php if( $orderShopify['TotalPrice'] > 0) echo priceFormat($orderShopify['TotalPrice'], true); ?></span></b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                    </div>
                                    <div class="row" style="margin-top: 10px;">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Comment</label>
                                                <textarea class="form-control" rows="3" id="comment"><?php echo $orderShopify['Comment'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $this->load->view('includes/action_logs'); ?>
                        </div>
                        <div class="col-sm-4">
                            <div class="box box-default padding15">
                                <div class="form-group">
                                    <label class="control-label">Customer Name</label>
                                    <input type="text" class="form-control hmdrequired" id="customerName1" value="<?php echo $orderShopify['CustomerName']; ?>" data-field="Customer Name">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Customer Email</label>
                                    <input type="text" class="form-control hmdrequired" id="customerEmail" value="<?php echo $orderShopify['CustomerEmail']; ?>" data-field="Customer Email">
                                </div>
                            </div>
                            <div class="box box-default padding15">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Shipping Information</h3>
                                </div>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label class="control-label">Name <span class="required">*</span></label>
                                        <input type="text" class="form-control hmdrequired" id="customerName" value="<?php echo $shipping['name']; ?>" data-field="Name">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Phone</label>
                                        <input type="text" class="form-control" id="customerPhone" value="<?php echo $shipping['phone']; ?>" data-field="Phone">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Address 1</label>
                                        <input type="text" class="form-control" id="customerAddress1" value="<?php echo $shipping['address1']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Address 2</label>
                                        <input type="text" class="form-control" id="customerAddress2" value="<?php echo $shipping['address2']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input type="text" class="form-control" id="customerCity" value="<?php echo $shipping['city']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Zip</label>
                                        <input type="text" class="form-control" id="customerZip" value="<?php echo $shipping['zip']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Province</label>
                                        <input type="text" class="form-control" id="customerProvince" value="<?php echo $shipping['province']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <input type="text" class="form-control" id="customerCountry" value="<?php echo $shipping['country']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Province Code</label>
                                        <input type="text" class="form-control" id="customerProvinceCode" value="<?php echo $shipping['province_code']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Country Code</label>
                                        <input type="text" class="form-control" id="customerCountryCode" value="<?php echo $shipping['country_code']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="box box-default padding15">
                                <div class="form-group">
                                    <label class="control-label">Team</label>
                                    <input type="text" class="form-control" value="<?php echo $team['TeamName']; ?>" disabled>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Shop</label>
                                    <input type="text" class="form-control" value="<?php echo $shop['ShopName']; ?>" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="list-inline pull-right margin-right-10">
                        <?php if($canUpdateCustomer){ ?><li><button class="btn btn-primary submit">Save</button></li><?php } ?>
                        <li><a href="<?php echo base_url('ordershopify'); ?>" class="btn btn-default">Cancel</a></li>
                        <input type="text" name="OrderShopifyId" id="orderShopifyId" hidden="hidden" value="<?php echo $orderShopifyId; ?>">
                        <input type="text" hidden="hidden" id="deleteProductUrl" value="<?php echo base_url('ordershopify/deleteProduct'); ?>">
                    </ul>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
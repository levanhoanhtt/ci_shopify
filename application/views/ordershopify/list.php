<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" id="ulFilter">
                        <li<?php if($orderStatusId == 0) echo '  class="active"'; ?>><a href="<?php echo base_url('ordershopify'); ?>">All Order</a></li>
                        <?php foreach($this->Mconstants->orderStatus as $i => $v){ ?>
                            <li<?php if($orderStatusId == $i) echo '  class="active"'; ?>><a href="<?php echo base_url('ordershopify/'.$i); ?>"><?php echo $v; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('ordershopify/'.$orderStatusId); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="control-label">Team</label>
                                <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamIds[]', $teamIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Shop</label>
                                <?php $this->Mconstants->selectObject($listShops, 'ShopId', 'ShopName', 'ShopIds[]', $shopIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label">Factory</label>
                                <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryIds[]', $factoryIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="OrderName" value="<?php echo set_value('OrderName'); ?>" placeholder="Order Name">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('transportStatus', 'TransportStatusId', set_value('TransportStatusId'), true, '--Choose Transport Status--');; ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($orderStatusId == 9 || $orderStatusId == 10) $this->Mconstants->selectConstants('orderExtraStatus', 'OrderExtraStatusId', set_value('OrderExtraStatusId'), true, '--Order Extra Status--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="TrackingCode" value="<?php echo set_value('TrackingCode'); ?>" placeholder="Tracking Code">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="CustomerName" value="<?php echo set_value('CustomerName'); ?>" placeholder="Customer Name">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="CustomerEmail" value="<?php echo set_value('CustomerEmail'); ?>" placeholder="Customer Email">
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <style>
                            .col-sm-12 ul.list-inline li{margin-bottom: 10px;}
                        </style>
                        <div class="row">
                            <div class="col-sm-12">
                                <ul class="list-inline">
                                    <li><input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search"></li>
                                    <li><button type="button" class="btn btn-primary" id="btnGetOrder">Get Orders</button></li>
                                    <li><button type="button" class="btn btn-info" id="btnImportTracking">Import Tracking</button></li>
                                    <li><button type="button" class="btn btn-info" id="btnImportWaitingOrder">Import Waiting Order</button></li>
                                    <!--<li><button type="button" class="btn btn-info" id="btnImportExcelCheckScan">Import Excel Check Scan</button></li>-->
                                    <li><button type="button" class="btn btn-info" id="btnImportExcelFactory">Import Excel Factory Scan</button></li>
                                    <li><button type="button" class="btn btn-info" id="btnImportWarehouseFactory">Import Excel Warehouse Scan</button></li>
                                    <li><a href="<?php echo base_url('ordershopify/exportScanExcel/4'); ?>" class="btn btn-primary" target="_blank">Export Excel Factory Scan</a></li>
                                    <li><a href="<?php echo base_url('ordershopify/exportScanExcel/5'); ?>" class="btn btn-primary" target="_blank">Export Excel Warehouse Scan</a></li>
                                    <li><a href="javascript:void(0)" id="btnImportCsvOrder" class="btn btn-primary" >Import Other Order</a></li>
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    <input type="text" hidden="hidden" id="orderStatusIdHidden" value="<?php echo $orderStatusId; ?>">
                                    <input type="text" hidden="hidden" id="factoryIdHidden" value="<?php echo implode(',', $factoryIds); ?>">
                                </ul>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <form action="<?php echo base_url('ordershopify/exportExcel') ?>" method="POST" target="_blank" id="exportForm" style="display: none;">
                          <input type="hidden" name="OrderIds" value="" id="orderIds"/>
                          <input type="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php $title = '<span class="label label-success">'.$orderCount.'</span> Orders';
                    sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : '');
                    $orderStatus = $this->Mconstants->orderStatus; ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-3">
                                    <select class="form-control input-sm select-action" id="selectAction">
                                        <option value="">Choose action</option>
                                        <option value="export_excel">Export Courier</option>
                                        <option value="export_excel2">Export Cross-Check</option>
                                        <option value="export_excel3">Export Tracking</option>
                                        <?php if($orderStatusId == 1 || $orderStatusId == 4) echo '<option value="send_invoice">Send Invoice</option>'; ?>
                                        <option value="">--------------</option>
                                        <?php if($changeStatus){
                                            foreach($orderStatus as $i => $v){
                                                if(!in_array($i, array(3, $orderStatusId))) echo '<option value="change_status-'.$i.'">'.$v.'</option>';
                                            }
                                        }
                                        else {
                                            if($orderStatusId == 1) echo '<option value="change_status-4">Waiting</option>';
                                            elseif($orderStatusId == 4) echo '<option value="change_status-1">Not Send</option>';
                                            elseif($orderStatusId == 5) echo '<option value="change_status-1">Not Send</option>';
                                            elseif($orderStatusId == 6) echo '<option value="change_status-1">Not Send</option>';
                                        }
                                        if(in_array($orderStatusId, array(1, 4, 5, 6))){ ?>
                                            <option value="">--------------</option>
                                            <option value="send_factory">Send Factory</option>
                                            <?php if($canDelete){ ?><option value="change_status-0">Delete Order</option><?php } ?>
                                            <?php if($canCancel){ ?><option value="change_status-3">Cancel Order</option><?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php $flag = in_array($orderStatusId, array(8, 9, 10, 11)); ?>
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><label class="lbCheckbox"><input type="checkbox" class="checkAll" id="checkAll" value="0"><span></span></label></th>
                                <th>Book Created</th>
                                <?php if($flag){ ?>
                                    <th>Factory Date</th>
                                    <th>Warehouse Date</th>
                                <?php } ?>
                                <th>Order Name</th>
                                <th>Team</th>
                                <th>Shop</th>
                                <th>Country</th>
                                <th>Tracking Code</th>
                                <th>Order Status</th>
                                <th>Transport Status</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $transportStatus = $this->Mconstants->transportStatus;
                            foreach ($listOrderShopifys as $o){ ?>
                                <tr id="trItem_<?php echo $o['OrderShopifyId']; ?>">
                                    <td><label class="lbCheckbox"><input type="checkbox" class="iCheckItem" value="<?php echo $o['OrderShopifyId']; ?>"><span></span></label></td>
                                    <td><?php echo ddMMyyyy($o['OrderDateTime'], 'd/m/Y H:i'); ?></td>
                                    <?php if($flag){ ?>
                                        <td><?php echo ddMMyyyy($o['FactoryDate']); ?></td>
                                        <td><?php echo ddMMyyyy($o['WarehouseDate']); ?></td>
                                    <?php } ?>
                                    <td><a href="<?php echo base_url('ordershopify/edit/' . $o['OrderShopifyId']); ?>"><?php echo $o['OrderName']; ?></a></td>
                                    <td>
                                        <?php $teamId = $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'TeamId');
                                        echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $teamId, 'TeamName'); ?>
                                        <input type="text" hidden="hidden" id="teamId_<?php echo $o['OrderShopifyId']; ?>" value="<?php echo $teamId; ?>">
                                    </td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listShops, 'ShopId', $o['ShopId'], 'ShopName'); ?></td>
                                    <td><?php echo $o['CountryName']; ?></td>
                                    <td><a href="https://www.17track.net/en/track?nums=<?php echo $o['TrackingCode']; ?>" target="_blank"><?php echo $o['TrackingCode']; ?></a></td>
                                    <td><span class="<?php echo $labelCss[$o['OrderStatusId']]; ?>"><?php echo $orderStatus[$o['OrderStatusId']]; ?></span></td>
                                    <td><span class="<?php echo $labelCss[$o['TransportStatusId']]; ?>"><?php echo $transportStatus[$o['TransportStatusId']]; ?></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeItemStatusUrl" value="<?php echo base_url('ordershopify/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="sendFactoryUrl" value="<?php echo base_url('factoryrequest/insert'); ?>">
                    <input type="text" hidden="hidden" id="sendInvoiceUrl" value="<?php echo base_url('invoice/insert'); ?>">
                    <input type="text" hidden="hidden" id="importOrderUrl" value="<?php echo base_url('ordershopify/importExcel'); ?>">
                    <input type="text" hidden="hidden" id="importOrderScanUrl" value="<?php echo base_url('ordershopify/importScanExcel'); ?>">
                    <input type="text" hidden="hidden" id="getOrderUrl" value="<?php echo base_url('ordershopify/import'); ?>">
                </div>
            </section>
        </div>
        <div id="modalGetDataOrder" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Get orders</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">From Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" id="fromDate" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">To Date</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" id="toDate" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="listShop mgt-10 row-margin">
                            <?php $i = 0;
                            foreach ($listTeams as $t){
                                $i++;
                                if($i % 3 == 1) echo '<div class="row">'; ?>
                                <div class="col-sm-4"  id="divTeam_<?php echo $t['TeamId']; ?>">
                                    <div>
                                        <input type="checkbox" class="iCheckTable cbTeam" value="<?php echo $t['TeamId']; ?>" checked>
                                        <label><?php echo $t['TeamName']; ?></label>
                                    </div>
                                    <div class="team-shops">
                                        <?php foreach($listShops as $s){
                                            if($s['TeamId'] == $t['TeamId']){ ?>
                                                <div>
                                                    <input type="checkbox" class="iCheckTable cbShop" value="<?php echo $s['ShopId']; ?>" checked>
                                                    <label class="control-label"><?php echo $s['ShopName']; ?></label>
                                                </div>
                                            <?php }
                                        } ?>
                                    </div>
                                </div>
                                <?php if($i % 3 == 0) echo '</div>'; ?>
                            <?php }
                            if($i % 3 > 0) echo '</div>'; ?>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <i class="fa fa-spinner fa-spin fa-2x fa-fw loading-getorder"></i>
                        <button type="button" class="btn btn-primary submit">Get Orders</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <div id="modalSendFactory" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send to factory</h4>
                    </div>
                    <div class="modal-body">
                        <p style="font-size: 20px">Sum order: <span style="color: red;font-weight: bold" class="countOrder">0</span></p>
                        <div class="form-group">
                            <label class="control-label">Factory</label>
                            <select id="factoryId1" class="form-control">
                                <?php foreach ($listFactories as $f){ ?>
                                    <option value="<?php echo $f['FactoryId']; ?>"><?php echo $f['FactoryName']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Factory Request</label>
                            <select id="factoryRequestId" class="form-control">
                                <option value="0">--None--</option>
                                <?php foreach ($listFactoryRequest as $fr){ ?>
                                    <option value="<?php echo $fr['FactoryRequestId']; ?>" data-id="<?php echo $fr['FactoryId']; ?>" style="display: none;"><?php echo $fr['RequestCode']; ?> (<?php echo ddMMyyyy($fr['RequestDate']); ?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Comment </label>
                            <textarea class="form-control" id="commentFactory"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button type="button" id="btnSendFactory" class="btn btn-success" style="margin-right: 5px">Send Factory</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modalSendInvoice" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send invoice</h4>
                    </div>
                    <div class="modal-body">
                        <p style="font-size: 20px">Sum order: <span style="color: red;font-weight: bold" class="countOrder">0</span></p>
                        <p style="font-size: 20px">Factory: <span style="color: red;font-weight: bold" id="spanFactoryName"></span></p>

                        <div class="form-group">
                            <label class="control-label">Comment </label>
                            <textarea class="form-control" id="commentInvoice"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="text-right">
                                <button type="button" id="btnSendInvoice" class="btn btn-success" style="margin-right: 5px">Send Invoice</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                                <input type="text" hidden="hidden" id="factoryIdInvoice" value="0">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalImportExcel" tabindex="-1" role="dialog" aria-labelledby="modalImportExcel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Import TrackingCode from Excel</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="fileExcelUrl" placeholder="File Excel" disabled>
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-info btn-flat" id="btnUploadExcel">Upload</button>
                                </span>
                            </div>
                        </div>
                        <img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter" style="display: none;">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="btnImportExcel">Import</button>
                        <input type="text" hidden="hidden" id="importTypeId" value="1">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('invoice'); ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamId', set_value('TeamId'), true, '--Choose Team--', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php if($user['FactoryId'] > 0) $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', $user['FactoryId']);
                                else $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectConstants('invoiceStatus', 'InvoiceStatusId', set_value('InvoiceStatusId'), true, '--Choose Invoice Status--'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                <input type="text" hidden="hidden" id="invoiceStatusIdSearch" value="<?php echo set_value('InvoiceStatusId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php $title = '<span class="label label-success">'.$invoiceCount.'</span> Invoices';
                    sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-3">
                                    <select class="form-control input-sm select-action" id="selectAction">
                                        <option value="">Choose action</option>
                                            <?php if($canSendMail){ ?><option value="mail">Send Mail</option><?php } ?>
                                            <?php if($canDelete){ ?><option value="delete">Delete Invoice</option><?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><label class="lbCheckbox"><input type="checkbox" class="checkAll" id="checkAll" value="0"><span></span></label></th>
                                <th>Invoice Date</th>
                                <th>Invoice Code</th>
                                <th>Team</th>
                                <th>Factory</th>
                                <th class="text-right">Order Price</th>
                                <th class="text-right">Offset Price</th>
                                <th class="text-right">Paid Price (VND)</th>
                                <th class="text-right">PO Price</th>
                                <th class="text-center">Status</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyInvoice">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $invoiceStatus = $this->Mconstants->invoiceStatus;
                            foreach ($listInvoices as $i){ ?>
                                <tr id="trItem_<?php echo $i['InvoiceId']; ?>">
                                    <td><label class="lbCheckbox"><input type="checkbox" class="iCheckItem" value="<?php echo $i['InvoiceId']; ?>"><span></span></label></td>
                                    <td><?php echo ddMMyyyy($i['InvoiceDate'], 'd/m/Y'); ?></td>
                                    <td><a href="<?php echo base_url('invoice/show/' . $i['InvoiceId']); ?>"><?php echo $i['InvoiceCode']; ?></a></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $i['TeamId'], 'TeamName'); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $i['FactoryId'], 'FactoryName'); ?></td>
                                    <td class="text-right"><?php echo priceFormat($i['OrderPrice'], true); ?></td>
                                    <td class="text-right"><?php echo priceFormat($i['OffsetPrice'], true); ?></td>
                                    <td class="text-right"><?php echo priceFormat(($i['OrderPrice'] + $i['OffsetPrice']) * $i['ExchangeRate'], true); ?></td>
                                    <td class="text-right"><?php echo priceFormat(($i['OrderPrice'] + $i['OffsetPrice']) * $i['POPercent'] / 100, true); ?></td>
                                    <td class="text-center"><span class="<?php echo $labelCss[$i['InvoiceStatusId']]; ?>"><?php echo $invoiceStatus[$i['InvoiceStatusId']]; ?></span></td>
                                    <td><?php echo $i['Comment']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                        <input type="text" hidden="hidden" id="deleteUrl" value="<?php echo base_url('invoice/deleteBatch'); ?>">
                        <input type="text" hidden="hidden" id="sendMailInvoiceUrl" value="<?php echo base_url('invoice/sendInvoiceMail'); ?>">
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
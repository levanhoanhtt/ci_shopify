<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content-header">
            <h1><?php echo $title; ?></h1>
        </section>
        <section class="content">
            <div class="box box-default">
                <?php sectionTitleHtml('Search'); ?>
                <div class="box-body row-margin">
                    <?php echo form_open('invoice/logMail'); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamId', set_value('TeamId'), true, '--Choose Team--', ' select2'); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php if($user['FactoryId'] > 0) $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', $user['FactoryId']);
                            else $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php $this->Mconstants->selectConstants('invoiceStatus', 'InvoiceStatusId', set_value('InvoiceStatusId'), true, '--Choose Invoice Status--'); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="box box-success">
                <?php $title = '<span class="label label-success">' . $invoiceCount . '</span> Invoices Email';
                sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Invoice Date</th>
                            <th>Invoice Code</th>
                            <th>Team</th>
                            <th>Factory</th>
                            <th class="text-center">Status</th>
                            <th>Sender</th>
                            <th>Send Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $labelCss = $this->Mconstants->labelCss;
                        $invoiceStatus = $this->Mconstants->invoiceStatus;
                        foreach ($listInvoices as $i) { ?>
                            <tr>
                                <td><?php echo ddMMyyyy($i['InvoiceDate']); ?></td>
                                <td><a href="<?php echo base_url('invoice/show/' . $i['InvoiceId']); ?>"><?php echo $i['InvoiceCode']; ?></a></td>
                                <td><?php echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $i['TeamId'], 'TeamName'); ?></td>
                                <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $i['FactoryId'], 'FactoryName'); ?></td>
                                <td class="text-center"><span class="<?php echo $labelCss[$i['InvoiceStatusId']]; ?>"><?php echo $invoiceStatus[$i['InvoiceStatusId']]; ?></span></td>
                                <td><?php echo $i['FullName'] ?></td>
                                <td><?php echo ddMMyyyy($i['CrDateTime'], 'd/m/Y H:i'); ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
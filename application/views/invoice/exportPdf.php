<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url(); ?>">
	<title>Invoice - Shopify</title>
</head>
<style type="text/css">
	#container{max-width: 980px;left: 0;right: 0;margin: auto;border: 1px solid #000;padding-left: 15px;padding-right:15px;position: absolute;}
	h1, h4{text-align: center;font-weight: bold;}
	.div_table table, .div_table table th, .div_table table td {border: 1px solid black;border-collapse: collapse;}
	.div_table{padding-top: 20px;padding-bottom: 20px;}
	.div_total{max-width: 100%!important;}
	.text-right{text-align: right;}
	.col-3{width: 30%;float: right;}
	.div_bank{width: 70%;margin-top: 86px;}
	.td_bold{font-weight: bold;}
	.payoneer, .text-center{text-align: center;}
	.td_red{color: red;}
	.td_bold_bank{font-size: 15px!important;}
</style>
<body onload="window.print()">
	<div id="container">
		<h1>INVOICE</h1>
		<h4>Date: <?php echo ddMMyyyy($invoice['InvoiceDate'], 'Y/m/d'); ?></h4>
		<label>To: <?php echo $teamName; ?></label>
		<div class="div_table">
			<table width="100%">
				<thead>
					<tr>
						<th>Charge Id</th>
						<th>Currency</th>
						<th>Unit Price</th>
						<th>Quantity</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($listSumProductByTypes as $p){
                        $type = strtoupper($p['ProductType']);
                        if(isset($productTypePrices[$type])) $price = $productTypePrices[$type];
                        else $price = 0; ?>
                        <tr>
                            <td class="text-center"><?php echo $type; ?></td>
                            <td class="text-center">USD</td>
                            <td class="text-center"><?php echo priceFormat($price, true); ?></td>
                            <td class="text-center"><?php echo priceFormat($p['sumQuantity']); ?></td>
                            <td class="text-center"><?php echo priceFormat($p['sumQuantity'] * $price, true); ?></td>
                        </tr>
                    <?php } ?>
				</tbody>
			</table>
		</div>
		<div class="div_total">
			<table class="col-3">
				<tbody>
					<tr>
						<td>Total in USD</td>
						<td class="text-right"><?php echo priceFormat($invoice['OrderPrice'], true); ?> $</td>
					</tr>
					<tr>
						<td>Total in PO</td>
						<td class="text-right"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['POPercent'] / 100, true); ?> $</td>
					</tr>
					<tr>
						<td>Total in VND</td>
						<td class="text-right"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['ExchangeRate'], true); ?> VND</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="div_bank">
			<table width="100%">
			<tbody>
				<tr>
					<td colspan="1" class="td_bold td_red">Order Note: <?php echo $invoice['Comment']; ?></td>
				</tr>
				<tr>
					<td colspan="1" style="padding-top: 20px"></td>
				</tr>
				<tr>
					<td colspan="1" class="td_bold">BANK ACCOUNT</td>
				</tr>
				
				<?php foreach ($listBanks as $key => $b):?>
					<tr>
						<td class="td_bold td_bold_bank"><?php echo $b['BankName'] ?></td>
						<td class="td_bold td_bold_bank"><?php echo $b['BankNumber'] ?></td>
						<td class="td_bold td_bold_bank"><?php echo $b['BankDesc'] ?></td>
					</tr>
				<?php endforeach; ?>
				<tr>
					<td class="td_bold payoneer">Payoneer</td>
					<td rowspan="2" class="td_bold td_bold_bank"><?php echo $payoneer ?></td>
				</tr>
				
			</tbody>
		</table>
		</div>
		<p class="td_bold td_red">*Nội dung: Tên khách hàng, chuyển khoản đơn hàng ngày/tháng/năm.</p>
	</div>
</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <title>Invoice - Shopify</title>
</head>
<body>
    <div style="max-width: 980px;left: 0;right: 0;margin: auto;border: 1px solid #000;padding-left: 15px;padding-right:15px;position: absolute;">
        <h1 style="text-align: center;font-weight: bold;">INVOICE</h1>
        <h4 style="text-align: center;font-weight: bold;">Date: <?php echo ddMMyyyy($invoice['InvoiceDate'], 'Y/m/d'); ?></h4>
        <label>To: <?php echo $teamName; ?></label>
        <div  style="padding-top: 20px;padding-bottom: 20px;">
            <table width="100%" style="border: 1px solid black;border-collapse: collapse;">
                <thead>
                    <tr style="border: 1px solid black;border-collapse: collapse;">
                        <th style="border: 1px solid black;border-collapse: collapse;">Charge Id</th>
                        <th style="border: 1px solid black;border-collapse: collapse;">Currency</th>
                        <th style="border: 1px solid black;border-collapse: collapse;">Unit Price</th>
                        <th style="border: 1px solid black;border-collapse: collapse;">Quantity</th>
                        <th style="border: 1px solid black;border-collapse: collapse;">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($listSumProductByTypes as $p){
                        $type = strtoupper($p['ProductType']);
                        if(isset($productTypePrices[$type])) $price = $productTypePrices[$type];
                        else $price = 0; ?>
                        <tr style="border: 1px solid black;border-collapse: collapse;">
                            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;"><?php echo $type; ?></td>
                            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;">USD</td>
                            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;"><?php echo priceFormat($price, true); ?></td>
                            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;"><?php echo priceFormat($p['sumQuantity']); ?></td>
                            <td style="border: 1px solid black;border-collapse: collapse;text-align: center;"><?php echo priceFormat($p['sumQuantity'] * $price, true); ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div style="max-width: 100%!important;">
            <table style="width: 30%;float: right;">
                <tbody>
                    <tr>
                        <td>Total in USD</td>
                        <td style="text-align: right;"><?php echo priceFormat($invoice['OrderPrice'], true); ?> $</td>
                    </tr>
                    <tr>
                        <td>Total in PO</td>
                        <td style="text-align: right;"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['POPercent'] / 100, true); ?> $</td>
                    </tr>
                    <tr>
                        <td>Total in VND</td>
                        <td style="text-align: right;"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['ExchangeRate'], true); ?> VND</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div  style="width: 70%;margin-top: 86px;">
            <table width="100%">
            <tbody>
                <tr>
                    <td colspan="1" style="font-weight: bold;color: red;">Order Note: <?php echo $invoice['Comment']; ?></td>
                </tr>
                <tr>
                    <td colspan="1" style="padding-top: 20px"></td>
                </tr>
                <tr>
                    <td colspan="1" style="font-weight: bold!important;">BANK ACCOUNT</td>
                </tr>
                
                <?php foreach ($listBanks as $key => $b):?>
                    <tr>
                        <td style="font-weight: bold;font-size:15px;"><?php echo $b['BankName'] ?></td>
                        <td style="font-weight: bold;font-size:15px;"><?php echo $b['BankNumber'] ?></td>
                        <td style="font-weight: bold;font-size:15px;"><?php echo $b['BankDesc'] ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td style="font-weight: bold;">Payoneer</td>
                    <td rowspan="2"><a href="javascript:void(0);" style="font-weight: bold;font-size:15px;"><?php echo $payoneer ?></a></td>
                </tr>
                
            </tbody>
        </table>
        </div>
        <p style="font-weight: bold!important;color: red;">*Nội dung: Tên khách hàng, chuyển khoản đơn hàng ngày/tháng/năm.</p>
        <p style="font-weight: bold!important;color: red;">*Khách hàng sau khi thanh toán vui lòng chụp lại màn hình thanh toán và trả lời phía dưới email invoice.</p>
    </div>
</body>
</html>
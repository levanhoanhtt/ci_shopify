<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canReceived) { ?><li><button class="btn btn-primary btnReceived">Received</button></li><?php } ?>
                    <li><a href="<?php echo base_url('invoice'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if ($invoiceId > 0) {
                    $invoiceStatusId = $invoice['InvoiceStatusId']; ?>
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>Invoice Code : <?php echo $invoice['InvoiceCode']; ?> <span id="spanInvoiceStatus"><span class="<?php echo $this->Mconstants->labelCss[$invoiceStatusId]; ?>"><?php echo $this->Mconstants->invoiceStatus[$invoiceStatusId]; ?></span></span></p>
                                    <p>Request Date: <?php echo ddMMyyyy($invoice['InvoiceDate'], 'd/m/Y H:i'); ?></p>
                                    <p>Received Date: <?php echo ddMMyyyy($invoice['UpdateDateTime'], 'd/m/Y H:i'); ?></p>
                                    <p>Team : <?php echo $teamName; ?></p>
                                    <p>Factory : <?php echo $factoryName; ?></p>
                                </div>
                                <div class="col-sm-6">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Exchange Rate</td>
                                            <td class="text-right" id="tdExchangeRate"><?php echo priceFormat($invoice['ExchangeRate'], true); ?></td>
                                        </tr>
                                        <tr>
                                            <td>PO Percent (%)</td>
                                            <td class="text-right" id="tdPOPercent"><?php echo $invoice['POPercent']; ?></td>
                                        </tr>
                                        <tr class="no-padding">
                                            <td<?php if($canUpdatePrice) echo ' style="line-height: 34px;"'; ?>>Offset in USD</td>
                                            <td class="text-right">
                                                <?php if(!$canUpdatePrice) echo priceFormat($invoice['OffsetPrice'], true);
                                                else echo '<input type="text" class="form-control cost" id="offsetPrice" value="'.priceFormat($invoice['OffsetPrice'], true).'">'; ?>
                                                <input type="text" hidden="hidden" id="offsetPriceOriginal" value="<?php echo $invoice['OffsetPrice']; ?>">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding divTable">
                            <div class="col-sm-12">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Charge Id</th>
                                        <th class="text-center">Currency</th>
                                        <th class="text-center">Unit Price</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyInvoices">
                                    <?php foreach($listSumProductByTypes as $p){
                                        $type = strtoupper($p['ProductType']);
                                        if(isset($productTypePrices[$type])) $price = $productTypePrices[$type];
                                        else $price = 0; ?>
                                        <tr>
                                            <td class="text-center type"><?php echo $type; ?></td>
                                            <td class="text-center">USD</td>
                                            <td class="text-center" style="width: 25%">
                                                <?php if($canUpdatePrice){ ?>
                                                    <input type="text" class="form-control cost price" value="<?php echo priceFormat($price, true); ?>">
                                                <?php } else echo priceFormat($price, true); ?>
                                            </td>
                                            <td class="text-center quantity" ><?php echo priceFormat($p['sumQuantity']); ?></td>
                                            <td class="text-center totalPrice"><?php echo priceFormat($p['sumQuantity'] * $price, true); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12 form-group">
                                <ul class="list-inline text-right">
                                    <?php if($canUpdatePrice) echo '<li><button class="btn btn-primary" type="button" id="btnUpdatePrice">Update Price & Note</button></li>';
                                    if($invoice['InvoiceStatusId'] == 3) echo '<li><button class="btn btn-info" type="button" id="btnSendMail">Send Mail</button></li>'; ?>
                                    <li><a href="<?php echo base_url('invoice/exportPdf/' . $invoiceId); ?>" target="_blank" class="btn btn-default">Export PDF</a></li>
                                    <li><a href="<?php echo base_url('invoice/exportExcel/' . $invoiceId); ?>" target="_blank" class="btn btn-default">Export Excel</a></li>
                                </ul>
                            </div>
                            <div class="row" style="padding: 10px 15px">
                                <div class="col-sm-6 form-group">
                                    <p style="font-weight: bold;font-size: 16px">Note:</p>
                                    <?php if($canUpdatePrice){ ?>
                                        <textarea class="form-control" rows="3" id="comment"><?php echo $invoice['Comment']; ?></textarea>
                                    <?php } else{ ?>
                                        <p><?php echo $invoice['Comment']; ?></p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <table class="table table-hover table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Total in USD</td>
                                            <td class="text-right" id="tdOrderPrice"><?php echo priceFormat($invoice['OrderPrice'], true); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total in PO</td>
                                            <td class="text-right" id="tdTotalPO"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['POPercent'] / 100, true); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total in VND</td>
                                            <td class="text-right" id="tdTotalVND"><?php echo priceFormat(($invoice['OrderPrice'] + $invoice['OffsetPrice']) * $invoice['ExchangeRate'], true); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <?php if($canReceived) { ?>
                                        <p class="text-right">
                                            <button class="btn btn-primary btnReceived">Received</button>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                            <input type="text" hidden="hidden" id="invoiceId" value="<?php echo $invoiceId; ?>">
                            <input type="text" hidden="hidden" id="receivedUrl" value="<?php echo base_url('invoice/received'); ?>">
                            <input type="text" hidden="hidden" id="updateInvoiceUrl" value="<?php echo base_url('invoice/update'); ?>">
                            <input type="text" hidden="hidden" id="sendMailInvoiceUrl" value="<?php echo base_url('invoice/sendInvoiceMail'); ?>">
                        </div>
                    </div>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
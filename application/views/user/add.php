<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Save</button></li>
                    <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Full Name <span class="required">*</span></label>
                            <input type="text" name="FullName" class="form-control hmdrequired" value="" data-field="Full Name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">User Name <span class="required">*</span></label>
                            <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="" data-field="User Name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Email <span class="required">*</span></label>
                            <input type="text" name="Email" class="form-control hmdrequired" value="" data-field="Email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                        </div>
                        <div class="form-group">
                            <?php $avatar = (set_value('Avatar')) ? set_value('Avatar') : NO_IMAGE; ?>
                            <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;">
                            <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Role</label>
                                    <?php $this->Mconstants->selectConstants('roles', 'RoleId'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6" id="divFactory" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label">Factory</label>
                                    <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', 0, true, '--Choose Factory--', ' select2'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Password <span class="required">*</span></label>
                                    <input type="text" id="newPass" name="UserPass" class="form-control hmdrequired" value="" data-field="Mật khẩu">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Re Password <span class="required">*</span></label>
                                    <input type="text" id="rePass" class="form-control hmdrequired" value="" data-field="Mật khẩu">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Team</label>
                            <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamIds[]', 0, false, '', ' select2', ' multiple'); ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="javascript:void(0)" class="btn btn-default" id="generatorPass">Generate Password</a>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label"><input type="checkbox" name="IsSendPass" class="iCheck" checked="checked"> Send password to email</label>
                            </div>
                        </div>
                        <ul class="list-inline pull-right margin-right-10">
                            <li><input class="btn btn-primary submit" type="submit" name="submit" value="Save"></li>
                            <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Cancel</a></li>
                            <input type="text" name="UserId" id="userId" hidden="hidden" value="0">
                            <input type="text" id="userEditUrl" hidden="hidden" value="<?php echo base_url('user/edit'); ?>">
                        </ul>
                    </div>
                    <!--<div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Team</label>

                        </div>
                    </div>-->
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
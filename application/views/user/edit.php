<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if($canEdit){ ?><li><button class="btn btn-primary submit">Save</button></li><?php } ?>
                    <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($userId > 0){ ?>
                    <?php echo form_open('api/user/saveUser', array('id' => 'userForm')); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Full Name <span class="required">*</span></label>
                                <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $userEdit['FullName']; ?>" data-field="Full Name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">User Name <span class="required">*</span></label>
                                <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="<?php echo $userEdit['UserName']; ?>" data-field="User Name">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Email <span class="required">*</span></label>
                                <input type="text" name="Email" class="form-control hmdrequired" value="<?php echo $userEdit['Email']; ?>" data-field="Email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Status</label>
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', $userEdit['StatusId']); ?>
                            </div>
                            <div class="form-group">
                                <?php $avatar = (empty($userEdit['Avatar']) ? NO_IMAGE : $userEdit['Avatar']); ?>
                                <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;max-width: 100%;">
                                <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Role</label>
                                        <?php $this->Mconstants->selectConstants('roles', 'RoleId', $userEdit['RoleId']); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6" id="divFactory" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label">Factory</label>
                                        <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', $userEdit['FactoryId'], true, '--Choose Factory--', ' select2'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Team</label>
                                <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamIds[]', $teamIds, false, '', ' select2', ' multiple'); ?>
                            </div>
                            <?php if($canEdit){ ?>
                                <ul class="list-inline pull-right margin-right-10">
                                    <li><input class="btn btn-primary submit" type="submit" name="submit" value="Save"></li>
                                    <li><a href="<?php echo base_url('user/staff'); ?>" class="btn btn-default">Cancel</a></li>
                                    <input type="text" name="UserId" id="userId" hidden="hidden" value="<?php echo $userId; ?>">
                                    <input type="text" name="UserPass" hidden="hidden" value="<?php echo $userEdit['UserPass']; ?>">
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
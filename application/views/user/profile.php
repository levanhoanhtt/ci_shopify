<?php $this->load->view('includes/header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <?php $this->load->view('includes/breadcrumb'); ?>
        <section class="content">
            <?php echo form_open('api/user/updateProfile', array('id' => 'profileForm')); ?>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Full Name <span class="required">*</span></label>
                        <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $user['FullName']; ?>" data-field="Full Name">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Email <span class="required">*</span></label>
                        <input type="text" name="Email" class="form-control hmdrequired" value="<?php echo $user['Email']; ?>" data-field="Email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php $avatar = (empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']); ?>
                        <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;">
                        <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">User Name <span class="required">*</span></label>
                                <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="<?php echo $user['UserName']; ?>" data-field="User Name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Old Password</label>
                                <input type="password" name="UserPass" id="userPass" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" id="newPass" name="NewPass" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Re Password</label>
                                <input type="password" id="rePass" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right">
                        <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Update">
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </section>
    </div>
</div>
<?php $this->load->view('includes/footer'); ?>
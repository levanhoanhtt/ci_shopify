<!DOCTYPE html>
<html>
<head>
	<title>BarCode</title>
</head>
<body onload="window.print()">
<style type="text/css">
	*{margin: 0; padding: 0}
	.wrapA4{height: 1149px; width: 812px; text-align: center; margin: 0 auto; }
	.wrapA4 img{box-sizing: border-box; width: 264px; height: 80px}
	.wrapA4 > div{margin-bottom: 30px}
</style>
	<div class="wrapA4">
		<?php foreach ($listBarcode as $l): ?>
			<div>
				<?php foreach ($l as $img): ?>
					<img src="<?php echo $img ?>">
				<?php endforeach ?>
			</div>
		<?php endforeach ?>
	</div>
</body>
</html>
<div class="modal fade" id="exportExcelModal" tabindex="-1" role="dialog" aria-labelledby="exportExcelModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Click to Download</h4>
            </div>
            <div class="modal-body">
                <ul class="list-inline" id="ulLinkExport"></ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input type="text" hidden="hidden" id="limitPerExcel" value="<?php echo $limitPerExcel; ?>">
            </div>
        </div>
    </div>
</div>
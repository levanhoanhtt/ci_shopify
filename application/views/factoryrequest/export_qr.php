<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url(); ?>">
	<title>QRCode - Shopify</title>
</head>
<body onload="window.print()">
<style type="text/css">
	*{margin: 0; padding: 0}
	.wrapA4{height: 1149px; width: 812px; text-align: center; margin: 0 auto; }
	.wrapA4 img{box-sizing: border-box; width: 180px; height: 180px; margin: 0 10px 0;}
	.item {width:33%;display: inline-block;margin-bottom: 75px;}
	.code{font-size: 18px;position:relative;top:-10px; display: block; padding:0 30px;line-height: 16px;}
	.mgt-8{margin-top: -15px;}
</style>
	<div class="wrapA4">
	<pre>
	 </pre>
		<?php foreach ($listBarcode as $k => $l): ?>
			<div <?php if($k > 0) echo 'class="mgt-8"' ?>>
				<?php foreach ($l as $img): ?>
					<div class="item">
						<img src="<?php echo $img['src'] ?>">
						 <?php $code = explode('-',$img['code']);
						 $topCode =  $code[0] . '-' . $code[1];
						 $botCode = '';
						 for($i = 2; $i < count($code); $i++) $botCode .= $code[$i].'-';
						 $botCode = substr($botCode, 0, strlen($botCode) - 1); ?>
						 <span class="code"><?php  echo $topCode . '<br>' . $botCode; ?></span>
					</div>
				<?php endforeach ?>
			</div>
		<?php endforeach ?>
	</div>
</body>
</html>
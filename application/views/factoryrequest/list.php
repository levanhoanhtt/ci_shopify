<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <?php if(!empty($listFactories)){ ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('factoryrequest'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('requestStatus', 'RequestStatusId', set_value('RequestStatusId'), true, '--Choose Request Status--');; ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-9"></div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary pull-left" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <?php } ?>
            <section class="content">
                <style>#tbodyOrder .aExport1{color: red;}</style>
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;"></th>
                                <th>Receive Date</th>
                                <th>Produce Date</th>
                                <th>Request Code</th>
                                <th>Factory Name</th>
                                <th class="text-center">Sum Quantity</th>
                                <th>Status</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $labelCss = $this->Mconstants->labelCss;
                            $requestStatus = $this->Mconstants->requestStatus;
                            foreach($listFactoryRequests as $fr){
                                //if($fr['sumQuantity'] > 0){ ?>
                                <tr id="trItem_<?php echo $fr['FactoryRequestId']; ?>">
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo base_url('factoryrequest/exportOrderExcel/1/' . $fr['FactoryRequestId']); ?>" target="_blank">Export Courier</a></li>
                                                <li><a href="<?php echo base_url('factoryrequest/exportOrderExcel/2/' . $fr['FactoryRequestId']); ?>" target="_blank">Export Cross-Check</a></li>
                                                <li><a href="<?php echo base_url('factoryrequest/exportExcel/' . $fr['FactoryRequestId']); ?>" class="link_export" data-id="<?php echo $fr['FactoryRequestId']; ?>" target="_blank">Export Excel</a></li>
                                                <li><a href="<?php echo base_url('factoryrequest/exportScanExcel/4/' . $fr['FactoryRequestId']); ?>" target="_blank">Export Factory Scan</a></li>
                                                <li><a href="<?php echo base_url('factoryrequest/exportScanExcel/5/' . $fr['FactoryRequestId']); ?>" target="_blank">Export Warehouse Scan</a></li>
                                                <?php if($fr['RequestStatusId'] == 1){ ?>
                                                    <li><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $fr['FactoryRequestId']; ?>">Delete</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo ddMMyyyy($fr['RequestDate']); ?></td>
                                    <td><?php echo ddMMyyyy($fr['UpdateDateTime']); ?></td>
                                    <td><a href="<?php echo base_url('factoryrequest/show/' . $fr['FactoryRequestId']); ?>"><?php echo $fr['RequestCode']; ?></a></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $fr['FactoryId'], 'FactoryName'); ?></td>
                                    <td class="text-center tdQuantity" data-id="<?php echo $fr['FactoryRequestId']; ?>"><i class="fa fa-spinner"></i><?php //echo $fr['sumQuantity']; ?></td>
                                    <td><span class="<?php echo $labelCss[$fr['RequestStatusId']]; ?>"><?php echo $requestStatus[$fr['RequestStatusId']]; ?></span></td>
                                    <td><?php echo $fr['Comment']; ?></td>
                                </tr>
                                <?php //}
                            } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteUrl" value="<?php echo base_url('factoryrequest/deleteBy'); ?>">
                    <input type="text" hidden="hidden" id="countSkuUrl" value="<?php echo base_url('factoryrequest/countSku'); ?>">
                    <input type="text" hidden="hidden" id="sumQuantityUrl" value="<?php echo base_url('factoryrequest/sumQuantity'); ?>">
                    <div id="divCountSku" style="display: none;"></div>
                </div>
                <?php $this->load->view('factoryrequest/modal'); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
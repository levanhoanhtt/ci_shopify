<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <?php if ($canEdit) { ?><li><button class="btn btn-primary submit">Manufacturing</button></li><?php } ?>
                    <li><a href="<?php echo base_url('factoryrequest'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if ($factoryRequestId > 0) { ?>
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>Request Code : <?php echo $factoryRequest['RequestCode']; ?></p>
                                    <p>Request Date: <?php echo ddMMyyyy($factoryRequest['RequestDate'], 'd/m/Y H:i'); ?></p>
                                    <p>Manufacturing Date: <?php echo ddMMyyyy($factoryRequest['UpdateDateTime'], 'd/m/Y H:i'); ?></p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <ul class="list-inline">
                                        <li><a href="<?php echo base_url('factoryrequest/exportExcel/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default" id="aExportExcel">Export Excel</a></li>
                                        <li><a href="<?php echo base_url('factoryrequest/exportScanExcel/4/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default">Factory Scan</a></li>
                                        <li><a href="<?php echo base_url('factoryrequest/exportScanExcel/5/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default">Warehouse Scan</a></li>
                                        <li><a href="<?php echo base_url('factoryrequest/exportIMGQRCode/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default" id="aExportPdf"<?php if ($canEdit) echo ' style="display: none;"'; ?>>Barcode Export</a></li>
                                        <li><a href="javascript:void(0)" class="btn btn-default" id="aListOrder">List orders</a></li>
                                    </ul>
                                    <ul class="list-inline">
                                        <li><a href="<?php echo base_url('factoryrequest/exportOrderExcel/1/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default">Export Courier</a></li>
                                        <li><a href="<?php echo base_url('factoryrequest/exportOrderExcel/2/' . $factoryRequestId); ?>" target="_blank" class="btn btn-default">Export Cross-Check</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="box-body table-responsive no-padding divTable">
                            <style>
                                #tbodyOrder .image-product {max-height: 80px;}
                                .removeOrderRequest{cursor: pointer;}
                            </style>
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>Sku</th>
                                    <th class="text-center">Image Link</th>
                                    <th class="text-center">S</th>
                                    <th class="text-center">M</th>
                                    <th class="text-center">L</th>
                                    <th class="text-center">XL</th>
                                    <th class="text-center">2XL</th>
                                    <th class="text-center">3XL</th>
                                    <th class="text-center">4XL</th>
                                    <th class="text-center">5XL</th>
                                    <th class="text-center">6XL</th>
                                    <th class="text-center">KIDS</th>
                                    <th class="text-center">B</th>
                                    <th class="text-center">Q</th>
                                    <th class="text-center">SM</th>
                                    <th class="text-center">TB</th>
                                    <th class="text-center">TW</th>
                                    <th class="text-center">UK</th>
                                    <th class="text-center">UQ</th>
                                    <th class="text-center">FL</th>
                                    <th class="text-center">US</th>
                                    <th class="text-center">Sum</th>
                                </tr>
                                </thead>
                                <tbody id="tbodyOrder">
                                <?php $day = date('mdH');
                                $labelCss = $this->Mconstants->labelCss;
                                $requestStatus = $this->Mconstants->requestStatus;
                                $sumColoumn = array(
                                    'S' => 0,
                                    'M' => 0,
                                    'L' => 0,
                                    'XL' => 0,
                                    '2XL' => 0,
                                    '3XL' => 0,
                                    '4XL' => 0,
                                    '5XL' => 0,
                                    '6XL' => 0,
                                    'KIDS' => 0,
                                    'B' => 0,
                                    'Q' => 0,
                                    'SM' => 0,
                                    'TB' => 0,
                                    'TW' => 0,
                                    'UK' => 0,
                                    'UQ' => 0,
                                    'FL' => 0,
                                    'US' => 0,
                                    'all' => 0
                                );
                                foreach ($listFactoryOrders as $sku => $fo) {
                                    $sumQuantity = 0;
                                    $productId = 0;
                                    $productImage = '';
                                    $productImage2 = '';
                                    $sizeQuantity = array();
                                    foreach ($fo as $size => $info) {
                                        $sumQuantity += $info['Quantity'];
                                        $productId = $info['ProductId'];
                                        $productImage = $info['ProductImage'];
                                        $productImage2 = $info['ProductImage2'];
                                        $sizeQuantity[strtoupper($size)] = $info['Quantity'];
                                    } ?>
                                    <tr>
                                        <td><?php echo $sku; ?></td>
                                        <td class="text-center">
                                            <?php if ($productId > 0 && !empty($productImage)) { ?>
                                                <ul class="list-inline">
                                                    <li><a href="<?php echo base_url('product/viewImage/' . $productId); ?>" target="_blank"><img src="<?php echo IMAGE_PATH . $productImage.'?'.$day; ?>" class="image-product"></a></li>
                                                    <?php if (!empty($productImage2)) { ?>
                                                        <li><a href="<?php echo base_url('product/viewImage/' . $productId); ?>" target="_blank"><img src="<?php echo IMAGE_PATH . $productImage2.'?'.$day; ?>" class="image-product"></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['S'])) {
                                                echo $sizeQuantity['S'];
                                                $sumColoumn['S'] += $sizeQuantity['S'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['M'])) {
                                                echo $sizeQuantity['M'];
                                                $sumColoumn['M'] += $sizeQuantity['M'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['L'])) {
                                                echo $sizeQuantity['L'];
                                                $sumColoumn['L'] += $sizeQuantity['L'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['XL'])) {
                                                echo $sizeQuantity['XL'];
                                                $sumColoumn['XL'] += $sizeQuantity['XL'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php $sumTmp = 0;
                                            if (isset($sizeQuantity['2XL'])) $sumTmp += $sizeQuantity['2XL'];
                                            if (isset($sizeQuantity['XXL'])) $sumTmp += $sizeQuantity['XXL'];
                                            $sumColoumn['2XL'] += $sumTmp;
                                            echo $sumTmp > 0 ? $sumTmp : '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php $sumTmp = 0;
                                            if (isset($sizeQuantity['3XL'])) $sumTmp += $sizeQuantity['3XL'];
                                            elseif (isset($sizeQuantity['XXXL'])) $sumTmp += $sizeQuantity['XXXL'];
                                            $sumColoumn['3XL'] += $sumTmp;
                                            echo $sumTmp > 0 ? $sumTmp : '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['4XL'])) {
                                                echo $sizeQuantity['4XL'];
                                                $sumColoumn['4XL'] += $sizeQuantity['4XL'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['5XL'])) {
                                                echo $sizeQuantity['5XL'];
                                                $sumColoumn['5XL'] += $sizeQuantity['5XL'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['6XL'])) {
                                                echo $sizeQuantity['6XL'];
                                                $sumColoumn['6XL'] += $sizeQuantity['6XL'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['KIDS'])) {
                                                echo $sizeQuantity['KIDS'];
                                                $sumColoumn['KIDS'] += $sizeQuantity['KIDS'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['B'])) {
                                                echo $sizeQuantity['B'];
                                                $sumColoumn['B'] += $sizeQuantity['B'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['Q'])) {
                                                echo $sizeQuantity['Q'];
                                                $sumColoumn['Q'] += $sizeQuantity['Q'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['SM'])) {
                                                echo $sizeQuantity['SM'];
                                                $sumColoumn['SM'] += $sizeQuantity['SM'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['TB'])) {
                                                echo $sizeQuantity['TB'];
                                                $sumColoumn['TB'] += $sizeQuantity['TB'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['TW'])) {
                                                echo $sizeQuantity['TW'];
                                                $sumColoumn['TW'] += $sizeQuantity['TW'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['UK'])) {
                                                echo $sizeQuantity['UK'];
                                                $sumColoumn['UK'] += $sizeQuantity['UK'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['UQ'])) {
                                                echo $sizeQuantity['UQ'];
                                                $sumColoumn['UQ'] += $sizeQuantity['UQ'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['FL'])) {
                                                echo $sizeQuantity['FL'];
                                                $sumColoumn['FL'] += $sizeQuantity['FL'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center">
                                            <?php if (isset($sizeQuantity['US'])) {
                                                echo $sizeQuantity['US'];
                                                $sumColoumn['US'] += $sizeQuantity['US'];
                                            }
                                            else echo '-'; ?>
                                        </td>
                                        <td class="text-center" style="background: #3c8dbc; color :#fff">
                                            <?php echo $sumQuantity;
                                            $sumColoumn['all'] += $sumQuantity; ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr class="text-center" style="background: #3c8dbc; color :#fff">
                                    <td></td>
                                    <td></td>
                                    <td><?php echo $sumColoumn['S'] ?></td>
                                    <td><?php echo $sumColoumn['M'] ?></td>
                                    <td><?php echo $sumColoumn['L'] ?></td>
                                    <td><?php echo $sumColoumn['XL'] ?></td>
                                    <td><?php echo $sumColoumn['2XL'] ?></td>
                                    <td><?php echo $sumColoumn['3XL'] ?></td>
                                    <td><?php echo $sumColoumn['4XL'] ?></td>
                                    <td><?php echo $sumColoumn['5XL'] ?></td>
                                    <td><?php echo $sumColoumn['6XL'] ?></td>
                                    <td><?php echo $sumColoumn['KIDS'] ?></td>
                                    <td><?php echo $sumColoumn['B'] ?></td>
                                    <td><?php echo $sumColoumn['Q'] ?></td>
                                    <td><?php echo $sumColoumn['SM'] ?></td>
                                    <td><?php echo $sumColoumn['TB'] ?></td>
                                    <td><?php echo $sumColoumn['TW'] ?></td>
                                    <td><?php echo $sumColoumn['UK'] ?></td>
                                    <td><?php echo $sumColoumn['UQ'] ?></td>
                                    <td><?php echo $sumColoumn['FL'] ?></td>
                                    <td><?php echo $sumColoumn['US'] ?></td>
                                    <td style="background: #3c8dbc; color :#fff"><?php echo $sumColoumn['all'] ?></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="row" style="padding: 10px 15px">
                                <div class="col-md-6">
                                    <p style="font-weight: bold;font-size: 16px">Note:</p>
                                    <p><?php echo $factoryRequest['Comment']; ?></p>
                                </div>
                                <div class="col-md-6">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th class="text-center" colspan="2">Sum as Product type</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($listSumQuantityWithPrpoductType as $sro): ?>
                                            <tr id="trItem_0">
                                                <td><?php echo $sro['ProductType'] ?></td>
                                                <td><?php echo $sro['sumQuantity'] ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <?php if ($canEdit) { ?>
                                        <p class="text-right">
                                            <button class="btn btn-primary submit">Manufacturing</button>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                            <input type="text" hidden="hidden" id="factoryRequestId" value="<?php echo $factoryRequestId; ?>">
                            <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('factoryrequest/changeStatus'); ?>">
                        </div>
                    </div>
                    <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">List Order Request</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="box-body table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th style="width: 50px;">No</th>
                                                <th>Book Created</th>
                                                <th class="text-center">Order Name</th>
                                                <th class="text-center">Team</th>
                                                <th class="text-center">Shop</th>
                                                <th class="text-center">Country</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyOrderRequest">
                                            <?php $i = 0;
                                            foreach ($listOrderShopifys as $o){
                                                $i++; ?>
                                                <tr id="order_<?php echo $o['OrderShopifyId'] ?>">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo ddMMyyyy($o['OrderDateTime'], 'd/m/Y H:i'); ?></td>
                                                    <td><a href="<?php echo base_url('ordershopify/edit/' . $o['OrderShopifyId']); ?>" target="_blank"><?php echo $o['OrderName']; ?></a></td>
                                                    <td><?php echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $o['TeamId'], 'TeamName'); ?></td>
                                                    <td><?php echo $o['ShopName']; ?></td>
                                                    <td><?php echo $o['CountryName']; ?></td>
                                                    <td>
                                                        <?php if ($factoryRequest['RequestStatusId'] == 1){ ?>
                                                            <span class="label label-danger removeOrderRequest" data-id="<?php echo $o['OrderShopifyId']; ?>"><i class="fa fa-times" aria-hidden="true"></i> Remove</span>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <input type="text" hidden="hidden" id="deleteOrderUrl" value="<?php echo base_url('factoryrequest/deleteOrder'); ?>">
                                    <input type="text" hidden="hidden" id="isReloadPage" value="0">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('factoryrequest/modal'); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
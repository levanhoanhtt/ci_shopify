<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div style="display: none;">
                    <?php echo form_open('actionlog'); ?>
                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                    <?php echo form_close(); ?>
                </div>
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Order Code</th>
                                <th>Staff Name</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyLog">
                            <?php if(!empty($listActionLogs)): ?>
                            <?php $i = 0;
                            $fullNames = array();
                            foreach($listActionLogs as $s){
                                $i++;
                                if(!isset($fullNames[$s['CrUserId']])) $fullNames[$s['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $s['CrUserId']), 'FullName'); ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('ordershopify/edit/'.$s['ItemId']); ?>"><?php echo $this->Mordershopifys->getFieldValue(array('OrderShopifyId' => $s['ItemId']), 'OrderName'); ?></a></td>
                                    <td><?php echo $fullNames[$s['CrUserId']]; ?></td>
                                    <td><?php echo ddMMyyyy($s['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                </tr>
                            <?php } ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
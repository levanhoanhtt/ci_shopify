<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('product/add'); ?>" class="btn btn-primary">Add Sku</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('product'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="Sku" placeholder="Sku" value="<?php echo set_value('Sku'); ?>">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary pull-left" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <style>#tbodyProduct img{max-height: 200px;</style>
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Sku</th>
                                <th>Product Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProduct">
                            <?php $day = date('mdH');
                            foreach ($listProducts as $p):?>
                                <tr id="product_<?php echo $p['ProductId']; ?>">
                                    <td class="text-center">
                                        <ul class="list-inline">
                                            <li><img src="<?php echo IMAGE_PATH . $p['ProductImage'].'?'.$day; ?>" class="image-product"></li>
                                            <?php if (!empty($p['ProductImage2'])) { ?>
                                                <li><img src="<?php echo IMAGE_PATH . $p['ProductImage2'].'?'.$day; ?>" class="image-product"></li>
                                            <?php } ?>
                                        </ul>
                                    </td>
                                    <td><a href="<?php echo base_url('product/edit/'.$p['ProductId']); ?>"><?php echo $p['Sku']; ?></a></td>
                                    <td><?php echo $p['ProductName']; ?></td>
                                    <td class="actions"><a href="javascript:void(0)" class="link_delete" data-id="<?php echo $p['ProductId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteProductUrl" value="<?php echo base_url('product/delete'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Save</button></li>
                    <li><a href="<?php echo base_url('product'); ?>" id="aProductList" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <style>
                    #ulImages li{position: relative;margin-right: 5px;margin-bottom: 5px;}
                    #ulImages img{height: 200px;}
                    #ulImages i{color: red;position: absolute;top: 3px;right: 7px;cursor: pointer;}
                </style>
                <?php echo form_open('product/update', array('id' => 'productForm')); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Sku <span class="required">*</span></label>
                            <input type="text" id="sku" name="Sku" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Product Name</label>
                            <input type="text" id="productName" name="ProductName" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Product Image <button type="button" class="btn btn-default" id="btnImage"><i class="fa fa-upload"></i></button></label>
                    <ul class="list-inline" id="ulImages"></ul>
                </div>
                <input type="text" name="ProductId" id="productId" hidden="hidden" value="0">
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
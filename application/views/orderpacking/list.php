<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('orderpacking'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="OrderName" class="form-control" value="<?php echo set_value('OrderName'); ?>" placeholder="Order Name">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="TrackingCode" class="form-control" value="<?php echo set_value('TrackingCode'); ?>" placeholder="Tracking Code">
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    <input type="text" class="form-control datepicker" placeholder="ScanDate" name="ScanDate" value="<?php echo set_value('ScanDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary pull-left" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <style>#tbodyScan .ingProduct{max-height: 50px;}</style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Scan Date</th>
                                <th>Tracking Code</th>
                                <th>Order Name</th>
                                <th>Image</th>
                                <th>Comment</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyScan">
                            <?php $productPath = PRODUCT_PATH.'packings/';
                            foreach($listOrderPackings as $fr){ ?>
                                <tr>
                                    <td><?php echo ddMMyyyy($fr['ScanDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $fr['TrackingCode']; ?></td>
                                    <td><a href="<?php echo base_url('ordershopify/edit/',$fr['OrderShopifyId']); ?>"><?php echo $fr['OrderName']; ?></a></td>
                                    <td class="text-center"><?php if(!empty($fr['Image'])){ ?><a href="<?php echo base_url($productPath.$fr['Image']); ?>" target="_blank"><img src="<?php echo $productPath.$fr['Image']; ?>" class="ingProduct"></a><?php } ?></td>
                                    <td><?php echo $fr['Comment'];?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>" id="baseUrl"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('includes/favicon'); ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/select2/select2.min.css"/>
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/all.css">
    <?php if (isset($scriptHeader)) outputScript($scriptHeader); ?>
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pace/pace.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <header class="main-header">
        <a href="<?php echo base_url(); ?>" class="logo">
            <span class="logo-mini"><b>R</b>KY</span>
            <span class="logo-lg"><b>Teeallover</b></span>
        </a>
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <?php $avatar = USER_PATH.((!empty($user['Avatar'])) ? $user['Avatar'] : NO_IMAGE); ?>
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="user-image">
                            <span class="hidden-xs"><?php echo $user['FullName']; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="img-circle">
                                <p>
                                    <?php echo $user['FullName']; ?>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php echo site_url('user/profile'); ?>" class="btn btn-default btn-flat">Trang cá nhân</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php echo site_url('user/logout'); ?>" class="btn btn-default btn-flat">Đăng xuất</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <?php $listActions1 = $listActions2 = $listActions3 = array();
                foreach($listActions as $act){
                    if($act['DisplayOrder'] > 0){
                        if($act['ActionLevel'] == 1) $listActions1[] = $act;
                        elseif($act['ActionLevel'] == 2) $listActions2[] = $act;
                        elseif($act['ActionLevel'] == 3) $listActions3[] = $act;
                    }
                }
                foreach($listActions1 as $act1) {
                    $listActionLv2 = array();
                    foreach($listActions2 as $act2){
                        if($act2['ParentActionId'] == $act1['ActionId']) $listActionLv2[] = $act2;
                    }
                    if(!empty($listActionLv2)){ ?>
                        <li class="treeview">
                            <a href="javascript:void(0)">
                                <i class="fa <?php echo empty($act1['FontAwesome']) ? 'fa-circle-o' : $act1['FontAwesome']; ?>"></i> <span><?php echo $act1['ActionName']; ?></span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <?php foreach($listActionLv2 as $act2){
                                    if($act2['DisplayOrder'] > 0){
                                        $listActionLv3 = array();
                                        foreach($listActions3 as $act3){
                                            if($act3['ParentActionId'] == $act2['ActionId']) $listActionLv3[] = $act3;
                                        }
                                        if(!empty($listActionLv3)){ ?>
                                            <li>
                                                <a href="javascript:void(0)">
                                                    <i class="fa <?php echo empty($act2['FontAwesome']) ? 'fa-circle-o' : $act2['FontAwesome']; ?>"></i> <?php echo $act2['ActionName']; ?>
                                                    <span class="pull-right-container">
                                                    <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                                </a>
                                                <ul class="treeview-menu">
                                                    <?php foreach($listActionLv3 as $act3){ ?>
                                                        <li><a href="<?php echo base_url($act3['ActionUrl']); ?>"><i class="fa <?php echo empty($act3['FontAwesome']) ? 'fa-circle-o' : $act3['FontAwesome']; ?>"></i> <?php echo $act3['ActionName']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } else{ ?>
                                            <li><a href="<?php echo base_url($act2['ActionUrl']); ?>"><i class="fa <?php echo empty($act2['FontAwesome']) ? 'fa-circle-o' : $act2['FontAwesome']; ?>"></i> <?php echo $act2['ActionName']; ?></a></li>
                                        <?php } ?>
                                    <?php }
                                } ?>
                            </ul>
                        </li>
                    <?php } else{ ?>
                        <li><a href="<?php echo base_url($act1['ActionUrl']); ?>"><i class="fa <?php echo empty($act1['FontAwesome']) ? 'fa-circle-o' : $act1['FontAwesome']; ?>"></i> <span><?php echo $act1['ActionName']; ?></span></a></li>
                    <?php }
                } ?>
            </ul>
        </section>
    </aside>
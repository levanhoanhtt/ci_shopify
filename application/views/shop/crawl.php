<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('shop'); ?>" class="btn btn-primary">Back</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <input type="hidden" name="" id="crawlUrl" value="<?php echo base_url('shop/crawlOrders') ?>">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->Mconstants->selectObject($listShops, 'ShopId', 'ShopName', 'ShopId', 0, true, 'Select Shop', ' select2'); ?>

                                <div class="progress progress-sm active" style="display: none;margin-top: 3px;">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" id="myBar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Get Order">
                            </div>
                        </div>

                        
                    </div>
                </div>
            </section>
            <section class="content">
            </section>
        </div>
    </div>

<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><button class="btn btn-primary submit">Save</button></li>
                    <li><a href="<?php echo base_url('shop'); ?>" class="btn btn-default">Cancel</a></li>
                </ul>
            </section>
            <section class="content">
                <?php echo form_open('shop/update', array('id' => 'shopForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Shop Code <span class="required">*</span></label>
                            <input type="text" name="ShopCode" id="shopCode" class="form-control hmdrequired" value="" data-field="Shop Code">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Shop Name <span class="required">*</span></label>
                            <input type="text" name="ShopName" id="shopName" class="form-control hmdrequired" value="" data-field="Shop Name">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label">Shop Url <span class="required">*</span></label>
                            <input type="text" name="ShopUrl" id="shopUrl" class="form-control hmdrequired" value="" data-field="ShopUrl">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Team</label>
                            <?php $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamId', 0, false, '', ' select2'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">ApiKey <span class="required">*</span></label>
                            <input type="text" name="ApiKey" id="apiKey" class="form-control hmdrequired" value="" data-field="ApiKey">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">ApiSecret <span class="required">*</span></label>
                            <input type="text" name="ApiSecret" id="apiSecret" class="form-control hmdrequired" value="" data-field="ApiSecret">
                        </div>
                    </div>
                </div>
                <ul class="list-inline pull-right margin-right-10">
                    <li><button class="btn btn-primary submit">Save</button></li>
                    <li><a href="<?php echo base_url('shop'); ?>" class="btn btn-default">Cancel</a></li>
                    <input type="text" name="ShopId" id="shopId" hidden="hidden" value="0">
                    <input type="text" id="shopEditUrl" hidden="hidden" value="<?php echo base_url('shop/edit'); ?>">
                </ul>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
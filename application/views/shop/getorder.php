<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <!-- <li><a href="<?php echo base_url('shop/add'); ?>" class="btn btn-primary">Thên Shop</a></li> -->
                </ul>
            </section>
            <!-- <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('shop'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ShopName" class="form-control" value="<?php echo set_value('ShopName'); ?>" placeholder="ShopName">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ShopUrl" class="form-control" value="<?php echo set_value('ShopUrl'); ?>" placeholder="ShopURL">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section> -->
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                
                            </tr>
                            </thead>
                            <tbody id="tbodyShop">
                                <?php foreach ($listOrders['orders'] as $k => $v): ?>
                                    <tr>
                                        <td><?php echo $k + 1 ?></td>
                                        <td><?php echo $v['id'] ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                   
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('shop/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ul class="list-inline">
                    <li><a href="<?php echo base_url('shop/add'); ?>" class="btn btn-primary">Add Shop</a></li>
                </ul>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('shop'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ShopCode" class="form-control" value="<?php echo set_value('ShopCode'); ?>" placeholder="Shop Code">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ShopName" class="form-control" value="<?php echo set_value('ShopName'); ?>" placeholder="Shop Name">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ShopUrl" class="form-control" value="<?php echo set_value('ShopUrl'); ?>" placeholder="Shop URL">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Shop Code</th>
                                <th>Shop Name</th>
                                <th>Shop Url</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyShop">
                            <?php $i = 0;
                            $status = $this->Mconstants->status;
                            $labelCss = $this->Mconstants->labelCss;
                            // pre($listShops);
                            foreach($listShops as $s){
                                $i++; ?>
                                <tr id="shop_<?php echo $s['ShopId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $s['ShopCode']; ?></td>
                                    <td><a href="<?php echo base_url('shop/edit/'.$s['ShopId']); ?>"><?php echo $s['ShopName']; ?></a></td>
                                    <td><a href="<?php echo $s['ShopUrl']; ?>" target="_blank"><?php echo $s['ShopUrl']; ?></a></td>
                                    <td id="statusName_<?php echo $s['ShopId']; ?>"><span class="<?php echo $labelCss[$s['StatusId']]; ?>"><?php echo $status[$s['StatusId']]; ?></span></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $s['ShopId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $s['ShopId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $s['ShopId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $s['ShopId']; ?>" value="<?php echo $s['StatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                   
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('shop/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
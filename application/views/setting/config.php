<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Config name</th>
                                <th>Config value</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyConfig">
                            <?php $i = 0;
                            foreach($listConfigs as $c){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $c['ConfigName']; ?></td>
                                    <td><input type="text" class="form-control" id="configValue_<?php echo $c['ConfigId']; ?>" value="<?php echo $c['ConfigValue']; ?>"></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_update" data-id="<?php echo $c['ConfigId']; ?>" title="Update"><i class="fa fa-save"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateConfigUrl" value="<?php echo base_url('config/update'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
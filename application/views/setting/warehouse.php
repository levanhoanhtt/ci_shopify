<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('warehouse/update', array('id' => 'warehouseForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Warehouse</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyWarehouse">
                            <?php
                            foreach($listWarehouses as $w){ ?>
                                <tr id="warehouse_<?php echo $w['WarehouseId']; ?>">
                                    <td id="warehouseName_<?php echo $w['WarehouseId']; ?>"><?php echo $w['WarehouseName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $w['WarehouseId']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $w['WarehouseId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="warehouseName" name="WarehouseName" value="" data-field="Warehouse Name"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="WarehouseId" id="warehouseId" value="0" hidden="hidden">
                                    <input type="text" id="deleteWarehouseUrl" value="<?php echo base_url('warehouse/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
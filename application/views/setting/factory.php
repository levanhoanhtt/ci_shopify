<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('factory/update', array('id' => 'factoryForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Factory Id</th>
                                <th>Factory Name</th>
                                <th>Factory Day</th>
                                <th>Warehouse Day</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyFactory">
                            <?php
                            foreach($listFactories as $f){ ?>
                                <tr id="factory_<?php echo $f['FactoryId']; ?>">
                                    <td><?php echo $f['FactoryId']; ?></td>
                                    <td id="factoryName_<?php echo $f['FactoryId']; ?>"><?php echo $f['FactoryName']; ?></td>
                                    <td id="factoryDay_<?php echo $f['FactoryId']; ?>"><?php echo $f['FactoryDay']; ?></td>
                                    <td id="warehouseDay_<?php echo $f['FactoryId']; ?>"><?php echo $f['WarehouseDay']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $f['FactoryId']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $f['FactoryId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td id="tdFactoryId"></td>
                                <td><input type="text" class="form-control hmdrequired" id="factoryName" name="FactoryName" value="" data-field="Factory Name"></td>
                                <td><input type="text" name="FactoryDay" id="factoryDay" class="form-control hmdrequired" value="" data-field="Factory Day"></td>
                                <td><input type="text" name="WarehouseDay" id="warehouseDay" class="form-control hmdrequired" value="" data-field="Warehouse Day"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="FactoryId" id="factoryId" value="0" hidden="hidden">
                                    <input type="text" id="deleteFactoryUrl" value="<?php echo base_url('factory/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('penaltyrate/update', array('id' => 'penaltyForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Factory</th>
                                <th>Product</th>
                                <th>Allow Days</th>
                                <th>Penalty Percentage</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyPenalty">
                            <?php
                            foreach($listPenalties as $penalty){ ?>
                                <tr id="penalty_<?php echo $penalty['PenaltyRateId']; ?>">
                                    <td id="penalty_<?php echo $penalty['PenaltyRateId']; ?>"><?php echo $penalty['FactoryName']; ?></td>
                                    <td id="productype_<?php echo $penalty['PenaltyRateId']; ?>"><?php echo $penalty['ProductType'] ?></td>
                                    <td id="penaltyallow_<?php echo $penalty['PenaltyRateId']; ?>"><?php echo $penalty['AllowDay'] ?></td>
                                    <td id="penaltypercent_<?php echo $penalty['PenaltyRateId']; ?>"><?php echo $penalty['PenaltyPercentage'] ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $penalty['PenaltyRateId'] ?>" data-factory-id="<?php echo $penalty['FactoryId']?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $penalty['PenaltyRateId'] ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <select class="form-control hmdrequired" id="factories-list" name="FactoryId">
                                        <?php foreach ($factories as $factory){ ?>
                                            <option value="<?php echo $factory['FactoryId']?>"><?php echo $factory['FactoryName']?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select class="form-control hmdrequired" name="ProductType" id="products-list">
                                        <?php foreach($listProductTypes as $pt){ ?>
                                            <option value="<?php echo $pt['ProductTypeName']; ?>"><?php echo $pt['ProductTypeName']; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td><input type="number" name="AllowDay" id="penalty-day" class="form-control hmdrequired"></td>
                                <td><input type="number" name="PenaltyPercentage" id="penalty-percent" class="form-control hmdrequired"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="PenaltyRateId" id="penaltyId" value="0" hidden="hidden">
                                    <input type="text" id="deletePenaltyUrl" value="<?php echo base_url('penaltyrate/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
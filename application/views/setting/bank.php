<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('bank/update', array('id' => 'bankForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Bank Name</th>
                                <th>Bank Number</th>
                                <th>Bank Desc</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyBank">
                            <?php foreach($listBanks as $b){ ?>
                                <tr id="bank_<?php echo $b['BankId']; ?>">
                                    <td id="bankName_<?php echo $b['BankId']; ?>"><?php echo $b['BankName']; ?></td>
                                    <td id="bankNumber_<?php echo $b['BankId']; ?>"><?php echo $b['BankNumber']; ?></td>
                                    <td id="bankDesc_<?php echo $b['BankId']; ?>"><?php echo $b['BankDesc']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $b['BankId']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $b['BankId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="bankName" name="BankName" value="" data-field="Bank"></td>
                                <td><input type="text" class="form-control hmdrequired" id="bankNumber" name="BankNumber" value="" data-field="Bank"></td>
                                <td><input type="text" class="form-control hmdrequired" id="bankDesc" name="BankDesc" value="" data-field="Bank"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="BankId" id="bankId" value="0" hidden="hidden">
                                    <input type="text" id="deleteBankUrl" value="<?php echo base_url('bank/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
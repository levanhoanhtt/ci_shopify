<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('team/update', array('id' => 'teamForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Team Code</th>
                                <th>Team Name</th>
                                <th>Email (separated by commas)</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTeam">
                            <?php
                            foreach($listTeams as $w){ ?>
                                <tr id="team_<?php echo $w['TeamId']; ?>">
                                    <td id="teamCode_<?php echo $w['TeamId']; ?>"><?php echo $w['TeamCode']; ?></td>
                                    <td id="teamName_<?php echo $w['TeamId']; ?>"><?php echo $w['TeamName']; ?></td>
                                    <td id="email_<?php echo $w['TeamId']; ?>"><?php echo $w['Emails']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $w['TeamId']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $w['TeamId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="teamCode" name="TeamCode" value="" data-field="Team Code"></td>
                                <td><input type="text" class="form-control hmdrequired" id="teamName" name="TeamName" value="" data-field="Team Name"></td>
                                <td> <input type="text" class="form-control hmdrequired" id="email" name="Emails" data-field="Emails">
                                </td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TeamId" id="teamId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTeamUrl" value="<?php echo base_url('team/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
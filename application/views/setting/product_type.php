<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <?php echo form_open('producttype/update', array('id' => 'productTypeForm')); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Product Type</th>
                                <th>Factory</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyProductType">
                            <?php
                            foreach($listProductTypes as $pt){ ?>
                                <tr id="productType_<?php echo $pt['ProductTypeId']; ?>">
                                    <td id="productTypeName_<?php echo $pt['ProductTypeId']; ?>"><?php echo $pt['ProductTypeName']; ?></td>
                                    <td id="factoryName_<?php echo $pt['ProductTypeId']; ?>"><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $pt['FactoryId'], 'FactoryName'); ?></td>
                                    <td id="price_<?php echo $pt['ProductTypeId']; ?>"><?php echo priceFormat($pt['Price'], true); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Edit"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $pt['ProductTypeId']; ?>" title="Delete"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="factoryId_<?php echo $pt['ProductTypeId']; ?>" value="<?php echo $pt['FactoryId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><input type="text" class="form-control hmdrequired" id="productTypeName" name="ProductTypeName" value="" data-field="Product Type Name"></td>
                                <td><?php echo $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId'); ?></td>
                                <td><input type="text" class="form-control hmdrequired" id="price" name="Price" value="0" data-field="Price"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Update"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Cancel"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ProductTypeId" id="productTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteProductTypeUrl" value="<?php echo base_url('producttype/delete'); ?>" hidden="hidden">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('warehousescan/ownProduct'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php if($user['FactoryId'] > 0) $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', $user['FactoryId']);
                                else $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Scan Date</th>
                                <th>SKU</th>
                                <th>Order Name</th>
                                <th>Factory Name</th>
                                <th>Own Quantity</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listScans as $wc){ ?>
                                <tr>
                                    <td><?php echo ddMMyyyy($wc['LastDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $wc['Sku']; ?></td>
                                    <td><?php echo $wc['OrderName']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $wc['FactoryId'], 'FactoryName'); ?></td>
                                    <td><?php echo $wc['OwnQuantity']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
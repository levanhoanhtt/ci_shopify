<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('warehousescan/compare'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off" placeholder="Begin Date">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off" placeholder="End Date">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Scan Date</th>
                                <th>SKU</th>
                                <th>Order Name</th>
                                <th>Factory Name</th>
                                <th>Factory Send</th>
                                <th>Warehouse Receive</th>
                                <th>New Own</th>
                                <th>Pay old debt</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($listScans as $wc){ ?>
                                <tr<?php if($wc['IsCheck'] == 0) echo ' class="info"'; ?>>
                                    <td><?php echo ddMMyyyy($wc['LastDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $wc['Sku']; ?></td>
                                    <td><?php echo $wc['OrderName']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $wc['FactoryId'], 'FactoryName'); ?></td>
                                    <td><?php echo $wc['FactoryQuantity']; ?></td>
                                    <td><?php echo $wc['WarehouseQuantity']; ?></td>
                                    <td><?php if($wc['OwnQuantity'] > 0) echo $wc['OwnQuantity']; ?></td>
                                    <td><?php if($wc['CurrentQuantity'] > 0) echo $wc['CurrentQuantity']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
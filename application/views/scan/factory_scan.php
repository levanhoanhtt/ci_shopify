<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php $noTrackingCode = set_value('NoTrackingCode'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('factoryscan'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', $factoryId, true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="TrackingCode" placeholder="Internal Tracking" value="<?php echo set_value('TrackingCode'); ?>">
                            </div>
                            <div class="col-sm-3">
                                <input type="checkbox" class="iCheck" name="NoTrackingCode" value="1"<?php if($noTrackingCode == 1) echo ' checked'; ?>> No Tracking Code
                            </div>
                            <div class="col-sm-3">
                                <?php if($noTrackingCode == 1){ ?><button type="button" class="btn btn-primary" id="btnTrackingCode">Gán mã nội địa</button><?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                <input type="text" hidden="hidden" id="factoryIdSelect" value="<?php echo set_value('FactoryId'); ?>">
                            </div>
                            <div class="col-sm-3"></div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('Last Scan <span class="label label-success">'.ddMMyyyy($lastScanDate, 'd/m/Y H:i').'</span> - Total <span class="label label-success">'.$scanCount.'</span>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($noTrackingCode == 1){ ?><th><input type="checkbox" class="iCheckTable checkAll" id="checkAll" value="0"></th><?php } ?>
                                <th>Scan Date</th>
                                <th>SKU</th>
                                <th>Order Name</th>
                                <th>Quantity</th>
                                <th>Factory Name</th>
                                <th>Internal Tracking</th>
                                <th>Image</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyScan">
                            <?php $trackingCodes = array(0 => '');
                            foreach ($listFactoryScans as $fc){
                                if($fc['InternalTrackingId'] > 0 && !isset($trackingCodes[$fc['InternalTrackingId']]) && $noTrackingCode != 1) $trackingCodes[$fc['InternalTrackingId']] = $this->Minternaltrackings->getFieldValue(array('InternalTrackingId' => $fc['InternalTrackingId']), 'TrackingCode'); ?>
                                <tr>
                                    <?php if($noTrackingCode == 1){ ?><td><input type="checkbox" class="iCheckTable iCheckItem" value="<?php echo $fc['FactoryScanId']; ?>"></td><?php } ?>
                                    <td><?php echo ddMMyyyy($fc['ScanDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $fc['Sku']; ?></td>
                                    <td><?php echo $fc['OrderName']; ?></td>
                                    <td><?php echo $fc['Quantity']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $fc['FactoryId'], 'FactoryName');; ?></td>
                                    <td><?php if($noTrackingCode != 1) echo $trackingCodes[$fc['InternalTrackingId']]; ?></td>
                                    <td class="actions"><a href="javascript:void(0)" class="link_image" data-id="<?php echo $fc['FactoryScanId']; ?>" title="Images"><i class="fa fa-eye"></i></a></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="insertTrackingCodeUrl" value="<?php echo base_url('factoryscan/insertTrackingCode'); ?>">
                    <input type="text" hidden="hidden" id="getImageUrl" value="<?php echo base_url('factoryscan/getImage'); ?>">
                    <input type="text" hidden="hidden" id="scanImagePath" value="<?php echo PRODUCT_PATH . 'scans/'; ?>">
                </div>
            </section>
        </div>
        <?php $this->load->view('scan/modal_add_tracking'); ?>
        <div id="modalScanImage" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Scan Image</h4>
                    </div>
                    <div class="modal-body">
                        <style>
                            #listImages img{width: 100%;}
                        </style>
                        <div class="row" id="listImages"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
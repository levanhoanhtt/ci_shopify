<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <?php $noTrackingCode = set_value('NoTrackingCode'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('warehousescan'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listFactories, 'FactoryId', 'FactoryName', 'FactoryId', set_value('FactoryId'), true, '--Choose Factory--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    <input type="text" class="form-control datepicker" name="ScanDate" value="<?php echo set_value('ScanDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="TrackingCode" placeholder="Internal Tracking" value="<?php echo set_value('TrackingCode'); ?>">
                            </div>
                            <div class="col-sm-3">
                                <input type="checkbox" class="iCheck" name="NoTrackingCode" value="1"<?php if($noTrackingCode == 1) echo ' checked'; ?>> No Tracking Code
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php if($noTrackingCode == 1){ ?><button type="button" class="btn btn-primary" id="btnTrackingCode">Gán mã nội địa</button><?php } ?>
                            </div>
                            <div class="col-sm-6"></div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <?php if($noTrackingCode == 1){ ?><th><input type="checkbox" class="iCheckTable checkAll" id="checkAll" value="0"></th><?php } ?>
                                <th>Scan Date</th>
                                <th>SKU</th>
                                <th>Order Name</th>
                                <th>Quantity</th>
                                <th>Factory Name</th>
                                <th>Internal Tracking</th>
                                <!--<th></th>-->
                            </tr>
                            </thead>
                            <tbody id="tbodyScan">
                            <?php $trackingCodes = array(0 => '');
                            foreach($listWarehouseScans as $wc){
                                if($wc['InternalTrackingId'] > 0 && !isset($trackingCodes[$wc['InternalTrackingId']]) && $noTrackingCode != 1) $trackingCodes[$wc['InternalTrackingId']] = $this->Minternaltrackings->getFieldValue(array('InternalTrackingId' => $wc['InternalTrackingId']), 'TrackingCode'); ?>
                                <tr>
                                    <?php if($noTrackingCode == 1){ ?><td><input type="checkbox" class="iCheckTable iCheckItem" value="<?php echo $wc['WarehouseScanId']; ?>"></td><?php } ?>
                                    <td><?php echo ddMMyyyy($wc['ScanDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $wc['Sku']; ?></td>
                                    <td><?php echo $wc['OrderName']; ?></td>
                                    <td><?php echo $wc['Quantity']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listFactories, 'FactoryId', $wc['FactoryId'], 'FactoryName');; ?></td>
                                    <td><?php if($noTrackingCode != 1) echo $trackingCodes[$wc['InternalTrackingId']]; ?></td>
                                    <!--<td class="actions"><a href="<?php echo base_url('warehousescan/image/' . $wc['WarehouseScanId']); ?>" target="_blank" title="Detail"><i class="fa fa-info"></i></a></td>-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="insertTrackingCodeUrl" value="<?php echo base_url('warehousescan/insertTrackingCode'); ?>">
                </div>
            </section>
        </div>
        <?php $this->load->view('scan/modal_add_tracking'); ?>
    </div>
<?php $this->load->view('includes/footer'); ?>
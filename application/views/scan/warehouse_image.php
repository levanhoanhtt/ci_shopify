<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('warehousescan/image/'.$warehouseScanId); ?>
                        <div class="row">
                            <div class="col-sm-3">
                               <?php $this->Mconstants->selectConstants('scanErrors', 'IsError', set_value('IsError'), true, '--Is Error--'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary pull-left" value="Search">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title); ?>
                    <style>#tbodyScan .ingProduct{max-height: 50px;}</style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Image</th>
                                <th>Comment</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyScan">
                            <?php $scanErrors= $this->Mconstants->scanErrors;
                            $labelCss = $this->Mconstants->labelCss;
                            $productPath = PRODUCT_PATH.'scans/';
                            foreach($listScanImages as $w){ ?>
                                <tr>
                                    <td class="text-center"><?php if(!empty($w['ProductImage'])){ ?><a href="<?php echo base_url($productPath.$w['ProductImage']); ?>" target="_blank"><img src="<?php echo $productPath.$w['ProductImage']; ?>" class="ingProduct"></a><?php } ?></td>
                                    <td><?php echo $w['Comment']; ?></td>
                                    <td><span class="<?php echo $labelCss[$w['IsError']]; ?>"><?php echo $scanErrors[$w['IsError']]; ?></span></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
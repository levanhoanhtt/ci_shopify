<div id="modalTrackingCode" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Insert internal tracking code</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <span style="font-size: 24px">Sum order choosen: </span>
                    <span style="font-size: 24px;color: red;font-weight: bold" id="countProduct">0</span>
                </div>
                <div class="form-group">
                    <label class="control-label">Internal Tracking Code</label>
                    <input type="text" class="form-control" id="trackingCodeInsert" value="">
                </div>
                <div class="form-group">
                    <div class="text-right">
                        <button type="button" id="btnInsertTrackingCode" class="btn btn-success" style="margin-right: 5px">Submit</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
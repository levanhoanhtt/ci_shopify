<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
            </section>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Search'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('reportfinance'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php if($user['TeamId'] > 0) $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamId', $user['TeamId']);
                                else $this->Mconstants->selectObject($listTeams, 'TeamId', 'TeamName', 'TeamId', set_value('TeamId'), true, '--Choose Team--', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listShops, 'ShopId', 'ShopName', 'ShopId', set_value('ShopId'), true, '--Choose Shop--', ' select2'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" value="<?php echo set_value('FromDate'); ?>" name="FromDate" autocomplete="off" placeholder="From Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" value="<?php echo set_value('ToDate'); ?>" name="ToDate" autocomplete="off" placeholder="To Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                	<div class="input-group">
                                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Search">
                                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            		</div>
                            	</div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <form action="<?php echo base_url('reportfinance/export') ?>" method="POST" target="_blank" id="exportForm" style="display: none;">
                            <input type="hidden" name="ListExport" value="" id="listExport"/>
                            <input type="submit" value="Submit">
                        </form>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php $btn = '<button type="button" class="btn btn-primary pull-right" id="btnExport">Export Excel</button>';
                    sectionTitleHtml($title, $btn); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Team</th>
                                <th>Shop</th>
                                <th>Product Type</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Sum</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php
                            $i = 1;
                            foreach ($listReport as $report):?>
                                <tr>
                                    <td><?php echo ddMMyyyy($report['OrderDateTime']); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listTeams, 'TeamId', $this->Mconstants->getObjectValue($listShops, 'ShopId', $report['ShopId'], 'TeamId'), 'TeamName'); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listShops, 'ShopId', $report['ShopId'], 'ShopName'); ?></td>
                                    <td><?php echo $report['ProductType']; ?></td>
                                    <td class="quantity<?php echo $i;?>"><?php echo $report['Quantity']; ?></td>
                                    <td class="no-padding"><input type="number" class="form-control price" dataId="<?php echo $i;?>" value=""></td>
                                    <td class="text-right sum_price<?php echo $i; $i++?>">0</td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
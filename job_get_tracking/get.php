<?php
function getCookie($cookieFileName){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://hx333-express.com/ver.asp');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "hybh=hn-8456&cxpasskey=123456&image.x=21&image.y=5");
    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFileName);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFileName);
    curl_exec($ch);;
    curl_close($ch);
    return true;
}

function getHtml($cookieFileName, $productUrl){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieFileName);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieFileName);
    curl_setopt($ch, CURLOPT_URL,$productUrl);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
    $retVal = curl_exec ($ch);
    curl_close ($ch);
    return $retVal;
}

function getDateSql($date){
    $dates = explode('-', $date);
    if(count($dates) == 3){
        if($dates[1] < 10) $m = '0' . $dates[1];
        else $m = $dates[1];
        if($dates[2] < 10) $d = '0' . $dates[2];
        else $d = $dates[2];
        return $dates[0] . '-' . $m . '-' . $d;
    }
    return '';
}

function checkExist($tracking, $dateTQStr, $conn){
    $stmt = $conn->prepare("SELECT TrackingCrawlId, DateVNStr FROM trackingcrawls WHERE Tracking = ? AND DateTQStr = ? LIMIT 1");
    $stmt->execute(array($tracking, $dateTQStr));
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) return $row;
    return false;
}
require_once 'lib.php';
require_once 'simple_html_dom.php';
$cookieFileName = dirname(__FILE__)."/cookies.txt";
getCookie($cookieFileName);
$page = 0;
$labels = array('Tracking', 'PackageCount', 'ProductTypeName', 'TrackingCost', 'DateTQStr', 'DateVNStr', 'StatusName');
while(true){
    $page++;
    $htmlText = getHtml($cookieFileName, 'http://hx333-express.com/kddfindcx.asp?Page=' . $page);
    $html = str_get_html($htmlText);
    if($html){
        $trackings = array();
        foreach($html->find('table tr') as $tr){
            if($tr->align == 'left'){
                $trackings[] = array(
                    'Tracking' => $tr->find('td', 0)->plaintext,
                    'PackageCount' => $tr->find('td', 2)->plaintext,
                    'ProductTypeName' => $tr->find('td', 3)->plaintext,
                    'TrackingCost' => $tr->find('td', 4)->plaintext,
                    'DateTQStr' => $tr->find('td', 7)->plaintext,
                    'DateVNStr' => $tr->find('td', 8)->plaintext,
                    'StatusName' => $tr->find('td', 9)->plaintext
                );
            }
        }
        if(empty($trackings)) break;
        $isBreak = false;
        foreach ($trackings as $t) {
            foreach ($labels as $lb) $t[$lb] = trim(str_replace('&nbsp;', '', $t[$lb]));
            if($t['DateTQStr'] == '2019-10-23'){
                $isBreak = true;
                break;
            }
            $t['DateTQ'] = getDateSql($t['DateTQStr']);
            $t['DateVN'] = getDateSql($t['DateVNStr']);
            $tl = checkExist($t['Tracking'], $t['DateTQStr'], $conn);
            if(!$tl) {
                $stmt = $conn->prepare('INSERT INTO trackingcrawls(Tracking,PackageCount,ProductTypeName,TrackingCost,DateTQStr,DateVNStr,DateTQ,DateVN,StatusName,IsSync,CrDateTime) VALUES(?,?,?,?,?,?,?,?,?,?,NOW())');
                $stmt->execute(array($t['Tracking'], $t['PackageCount'], $t['ProductTypeName'], $t['TrackingCost'], $t['DateTQStr'], $t['DateVNStr'], $t['DateTQ'], $t['DateVN'], $t['StatusName'], 0));
                echo '<pre>' . print_r($t, true) . '</pre>';
            }
            else{
                if(!empty($t['DateVNStr'] && empty($tl['DateVNStr']))){
                    $stmt = $conn->prepare('UPDATE trackingcrawls SET DateVNStr = ?, DateVN = ?, IsSync = ? AND CrDateTime = NOW() WHERE TrackingCrawlId = ?');
                    $stmt->execute(array($t['DateVNStr'], $t['DateVN'], 0, $tl['TrackingCrawlId']));
                    echo '<pre>' . print_r($t, true) . '</pre>';
                }
                else{
                    $isBreak = true;
                    break;
                }
            }
        }
        if($isBreak) break;
    }
    else break;
    //break;
    usleep(1000);
}
$conn = null;
<?php
function getListSync($conn){
    $retVal = array();
    $stmt = $conn->prepare("SELECT * FROM trackingcrawls WHERE IsSync = ? LIMIT 1000");
    $stmt->execute(array(0));
    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) $retVal[] = $row;
    return $retVal;
}
$action = isset($_GET['action']) ? trim($_GET['action']) : '';
if($action == 'get') {
    require_once 'lib.php';
    $listTrackings = getListSync($conn);
    echo json_encode($listTrackings);
    if(!empty($listTrackings)){
        $ids = array();
        foreach($listTrackings as $t) $ids[] = $t['TrackingCrawlId'];
        //echo '<pre>'.print_r($ids, true).'</pre>';
        $stmt = $conn->prepare("UPDATE trackingcrawls SET IsSync = 1 WHERE TrackingCrawlId IN(".implode(',', $ids).")");
        $stmt->execute();
    }
    $conn = null;
}
/*elseif($action == 'update'){
    $ids = isset($_POST['ids']) ? $_POST['ids'] : '';
    if(!empty($ids)){
        require_once 'lib.php';
        $stmt = $conn->prepare("UPDATE trackingcrawls SET IsSync = ? WHERE TrackingCrawlId IN({$ids})");
        $stmt->execute(array(1));
        $conn = null;
    }
}*/
